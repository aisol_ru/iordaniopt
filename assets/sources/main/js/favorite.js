$(function() {

    $(document).on('click', '.js-user-favorite-button', function() {
        var button = $(this);

        $.post('/ajax/favorite/set', {good_id: button.data('id')}, function(data) {
            button.toggleClass('active');
            $('.js-menu-favorite-count').text(data.count);
        }, 'json');

    });

    $(document).on('click', '.js-user-favorite-flush-button', function() {
        if (confirm('Очистить список отложенных товаров?') === false) {
            return;
        }
        $.post('/ajax/favorite/flush', {}, function() {
            $('.js-menu-favorite-count').text(0);
            window.location.reload();
        }, 'json');

    });

});