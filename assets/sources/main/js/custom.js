$(function () {

    $('.js-magnifier').each(function () {
        var block = $(this);
        var image = block.data('image');
        var innerImage = block.find('img');
        var innerImageWidth = innerImage.get(0).naturalWidth;
        var innerImageHeight = innerImage.get(0).naturalHeight;

        if (innerImageWidth > 400 && innerImageHeight > 400) {
            block.zoom({
                url: image,
            });
        }
    });

    function loadList(target) {
        if (target.prop('disabled') === true) {
            return;
        }
        target.prop('disabled', true);
        target.text('Загрузка...');
        var page = Number(target.data('page'));
        page++;
        var totalPage = Number(target.data('total-page'));
        var filterForm = $('#catalog-list-filter');
        var sort = $('#catalog-list-sort');

        if (target.data('is-filter') === true) {
            var formData = new FormData(filterForm[0]);
            formData.append('get-list', '1');
            formData.append(target.data('sort-param'), sort.val());
            formData.append(target.data('page-param'), '' + page);
            var url = new URL(location.origin + filterForm.attr('action'));
            for (var [name, value] of formData) {
                url.searchParams.set(name, value);
            }
        } else {
            var formData = new FormData();
            formData.append('get-list', '1');
            formData.append(target.data('sort-param'), sort.val());
            formData.append(target.data('page-param'), '' + page);
            var url = new URL(location.origin + filterForm.attr('action'));
            for (var [name, value] of formData) {
                url.searchParams.set(name, value);
            }
            url.searchParams.set('category_outer_code', target.data('category-outer-code'));
        }

        var urlData = new FormData(filterForm[0]);
        urlData.append(target.data('sort-param'), sort.val());
        urlData.append(target.data('page-param'), '' + page);
        var urlNew = new URL(location.origin + filterForm.attr('action'));
        for (var [name, value] of urlData) {
            urlNew.searchParams.set(name, value);
        }

        var catalogListWrapper = $('.js-catalog-list');

        var xhr = new XMLHttpRequest();
        var tokenMeta = $('[name="csrf-token"]');
        xhr.open('GET', url);
        xhr.setRequestHeader('X-CSRF-Token', tokenMeta.attr('content'));

        xhr.onload = function () {
            if (xhr.status !== 200) {
                target.prop('disabled', false);
                return;
            }

            catalogListWrapper.append(xhr.response);

            window.history.pushState({ catalog: catalogListWrapper.html(), page: page }, '', urlNew.toString());

            if (page >= totalPage) {
                target.remove();
                observer.unobserve(entry.target);
                return;
            }

            target.data('page', page);
            target.prop('disabled', false);
            target.text('Показать еще');
        };

        xhr.onerror = function () {
            target.prop('disabled', false);
            target.text('Показать еще');
        };

        xhr.send();
    }

    var observer = new IntersectionObserver(function (entries, observer) {
        entries.forEach(function (entry) {
            if (entry.isIntersecting === true) {
                var button = $(entry.target);

                loadList(button);

            }
        });
    }, {root: null, rootMargin: '0px', threshold: 0.2});

    var paginationButton = $('.js-catalog-pagination');
    if (paginationButton.length > 0) {

        window.onpopstate = function (event) {
            if (event.state && event.state.catalog) {
                $('.js-catalog-list').html(event.state.catalog);
                var paginationButton = $('.js-catalog-pagination');
                paginationButton.data('page', event.state.page);
                observer.unobserve(paginationButton[0]);
            } else {
                window.location.reload();
            }
        };

        if (window.history.state && window.history.state.catalog) {
            $('.js-catalog-list').html(window.history.state.catalog);
            paginationButton.data('page', window.history.state.page);
        }

        observer.observe(paginationButton[0]);
        $(document).on('click', '.js-catalog-pagination', function () {
            loadList($(this));
        });
    }

    $('.js-history-back').on('click', function (event) {
        event.preventDefault();
        window.history.back();
    });

    $(document).on('click', '.checkbox-row label', function () {
        var elems = $(this)
            .closest('.checkbox-row-block')
            .find('.js-filter-category');

        // if (elems.length > 0) {
        //     $(".js-filter-category").each(function () {
        //         if (elems.get(0) !== $(this).get(0)) {
        //             $(this).slideUp("fast");
        //         }
        //     })
        // }

        elems.slideDown();
    });

    $(document).on('change', '.js-input-filter-catalog', function () {

        var childBlock = $(this)
            .closest('.checkbox-row-block')
            .find('.js-filter-category');

        var childInputs = childBlock
            .find('input[type="checkbox"]');

        if ($(this).prop('checked')) {
            childInputs.prop('checked', true);
            childInputs.prop('disabled', true);
        } else {
            childInputs.prop('checked', false);
            childInputs.prop('disabled', false);
        }
    });

    $(document).on('click', '.js-filter-category label', function () {

        var target = $(this);

        var childInp = target
            .closest('input');

        var parentInp = target
            .closest('.js-filter-category')
            .closest('.checkbox-row-block')
            .find('.js-input-filter-catalog');

        var otherInps = parentInp
            .closest('.checkbox-row-block')
            .find('.js-filter-category input');

        if (parentInp.prop('checked')) {

            parentInp.prop({
                disabled: false,
            });
            parentInp.prop({
                checked: false,
            });

            otherInps.prop({
                disabled: false,
            });
            otherInps.prop({
                checked: false,
            });

        }

        childInp.prop({
            checked: true,
        });
        childInp.prop({
            disabled: false,
        });

    });

    $(document).on('change', '#catalog-list-filter', function (e) {
        $(this).trigger('submit');
    });

    $(document).on('submit', '#catalog-list-filter', function (e) {
        return false;
    });

    $('#search-catalog-on-main').on('change', function () {
        $(this).closest('form').trigger('submit');
    });

    $('#catalogfilter-word').on('change', function () {
        $('#catalog-list-filter').trigger('submit');
    });

    autoCompleteTextBoxInit('#search-catalog-on-main');
    autoCompleteTextBoxInit('#catalogfilter-word');

    function autoCompleteTextBoxInit(selector) {

        if (document.querySelector(selector) === null) {
            return;
        }

        new autoComplete({
            data: {
                src: async () => {
                    let input = $(selector);
                    let url = "/ajax/default/find-select-catalog?value=" + input.val();
                    const source = await fetch(url, {
                        method: "GET",
                    });
                    return await source.json();
                },
            },
            sort: function (a, b) {
                if (a.match < b.match) {
                    return -1;
                }
                if (a.match > b.match) {
                    return 1;
                }
                return 0;
            },
            placeHolder: "",
            selector: selector,
            threshold: 0,
            debounce: 0,
            searchEngine: "strict",
            highlight: true,
            maxResults: 5,
            resultsList: {
                render: true,
                container: function (source) {
                    source.setAttribute("id", "autoComplete_results_list" + $(selector).data("type"));
                },
                destination: selector,
                position: "afterend",
                element: "ul",
            },
            resultItem: {
                content: function (data, source) {
                    source.innerHTML = data.match;
                },
                element: "li",
            },
            // noResults: function () {
            //     const result = document.createElement("li");
            //     result.setAttribute("class", "autoComplete_result no-result");
            //     result.setAttribute("tabindex", "1");
            //     result.innerHTML = "Нет результатов";
            //     document.querySelector(selector).appendChild(result);
            // },
            onSelection: function (feedback) {
                const value = feedback.selection.value;
                document.querySelector(selector).value = value;
            },
        });
    }

    initGoodFilter();

    function initGoodFilter() {

        filterInsert = $(".js-select-insert button.active").data("insert");
        filterCoating = $("#catalog-select-coating").val();

        if (filterInsert === undefined) {
            filterInsert = null;
        }
        if (filterCoating === undefined) {
            filterCoating = null;
        }


        sizesShowHide(function (sizeElem) {
            var ife = true;
            if (filterCoating !== null) {
                ife = ife && (sizeElem.data('coating') === filterCoating);
            }
            if (filterInsert !== null) {
                ife = ife && sizeElem.data('insert') === filterInsert;
            }
            return ife;
        });

        initEventsGoodFilter();
    }

    function initEventsGoodFilter() {
        $(".js-select-insert button").on("click", function () {
            $(this).addClass("active");
            $(this).siblings().removeClass("active");

            filterInsert = $(this).data("insert");
            sizesShowHide(function (sizeElem) {
                var ife = sizeElem.data('insert') === filterInsert;
                if (filterCoating !== null) {
                    ife = ife && (sizeElem.data('coating') === filterCoating || filterCoating === "-1");
                }
                return ife;
            });
        });

        $("#catalog-select-coating").on("change", function () {
            filterCoating = $(this).val();
            sizesShowHide(function (sizeElem) {
                var ife = sizeElem.data('coating') === filterCoating || filterCoating === "-1";
                if (filterInsert !== null) {
                    ife = ife && sizeElem.data('insert') === filterInsert;
                }
                return ife;
            });
        });
    }

    function sizesShowHide(condition) {
        var sizesList = $(".js-catalog-item-size");
        sizesList.each(function () {
            if (condition($(this))) {
                $(this).show();
            } else {
                $(this).hide();
            }
        });
    }

});
