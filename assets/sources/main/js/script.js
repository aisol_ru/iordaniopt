$(document).ready(function () {

    function hidePreloader() {
        $('.js-preloader').addClass('hidden').fadeOut();
    }

    setTimeout(hidePreloader, 3000);

    // $(document).on('click', 'a[href^="#"]', function (event) {
    //   event.preventDefault();
    //
    // var mobileHeaderOffset = 0
    //
    // if ($(window).width() < 991 ) {
    //   var mobileHeaderOffset = 60
    // }
    //
    //
    // $('html, body').animate({
    //     scrollTop: $($.attr(this, 'href')).offset().top - mobileHeaderOffset
    //   }, 1000);
    // });
    //
    // $(".toTopLogo").click(function() {
    //   $("html, body").animate({ scrollTop: 0 }, "slow");
    //   return false;
    // });

    $('.js-main-page-slider').slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        dots: true,
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    arrows: false
                }
            }
        ]
    });

    $('.js-burger-btn').on('click', function () {
        $(this).closest('body').toggleClass('active');
        $(this).closest('header').find('.fade-wrapper').fadeIn();
        $(this).closest('.header').find('.desktop-header').toggleClass('active');
    });


    $('.js-close-desktop-header').on('click', function () {
        $(this).closest('body').toggleClass('active');
        $(this).closest('body').find('.fade-wrapper').fadeOut();
        $(this).closest('.header').find('.desktop-header').toggleClass('active');
    });


    $('.js-header-manager-block').on('click', function () {
        $(this).toggleClass('active');
        $(this).closest('.header-manager-block-wrap').toggleClass('active').find('.h-manager-info-wrap').toggleClass('active');
    });


    $('.js-clu-fav').on('click', function () {
        $(this).toggleClass('active');
    });

    $('.js-mobile-filter-btn').on('click', function () {
        $(this).closest('.catalog-section').find('.catalog-filter-col').addClass('active');
        $(this).closest('body').find('.fade-wrapper').fadeIn();
    });

    $('.js-close-mobile-filter').on('click', function () {
        $(this).closest('.catalog-section').find('.catalog-filter-col').removeClass('active');
        $(this).closest('body').find('.fade-wrapper').fadeOut();
    });


    function CheckInputVal(elem, isFocus) {
        if (elem.val() === '') {
            elem.closest('.input-row').find('label').removeClass('active');
        }
        if (elem.val() != '' || isFocus) {
            elem.closest('.input-row').find('label').addClass('active');
        }
    }

    $('.account-form-col .input-row input').on('focus', function () {
        CheckInputVal($(this), true);
    });

    $('.account-form-col .input-row input').on('focusout', function () {
        CheckInputVal($(this), false);
    });

    $('.account-form-col .input-row input').each(function () {
        CheckInputVal($(this), false);
    });

    function noUIInit(stepsSlider, input0, input1, min, max) {
        var inputs = [input0, input1];
        var inpVal0 = Number(input0.value);
        var inpVal1 = Number(input1.value);

        if (inpVal1 === 0) {
            inpVal1 = max;
        }
        if (inpVal0 === 0) {
            inpVal0 = min;
        }

        noUiSlider.create(stepsSlider, {
            start: [inpVal0, inpVal1],
            connect: true,
            format: wNumb({
                decimals: 0
            }),
            range: {
                'min': min,
                'max': max
            }
        });

        stepsSlider.noUiSlider.on('update', function (values, handle) {
            inputs[handle].value = values[handle];
        });

        stepsSlider.noUiSlider.on('end', function (values, handle) {
            $(inputs[handle]).trigger('change')
        });

        inputs.forEach(function (input, handle) {

            input.addEventListener('change', function () {
                pipsRange.noUiSlider.setHandle(handle, this.value);
            });

            input.addEventListener('keydown', function (e) {

                var values = pipsRange.noUiSlider.get();
                var value = Number(values[handle]);

                // [[handle0_down, handle0_up], [handle1_down, handle1_up]]
                var steps = pipsRange.noUiSlider.steps();

            });

        });
    }

    if ($('#price-slider-block').length > 0){
        var stepsSlider = document.getElementById('price-slider-block');
        var input0 = document.getElementById('price-slider-input1');
        var input1 = document.getElementById('price-slider-input2');
        noUIInit(
            stepsSlider,
            input0,
            input1,
            $(stepsSlider).data('min'),
            $(stepsSlider).data('max')
        );
    }

    $(document).on('pjax:complete', function () {
        var stepsSlider = document.getElementById('price-slider-block');
        var input0 = document.getElementById('price-slider-input1');
        var input1 = document.getElementById('price-slider-input2');
        noUIInit(
            stepsSlider,
            input0,
            input1,
            $(stepsSlider).data('min'),
            $(stepsSlider).data('max')
        );
    })


    $('.js-catalog-item-pic-big-carousel').slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        dots: false,
        asNavFor: '.js-catalog-item-pic-small-carousel'
    });


    $('.js-catalog-item-pic-small-carousel').slick({
        infinite: false,
        vertical: true,
        slidesToShow: 4,
        slidesToScroll: 1,
        arrows: true,
        focusOnSelect: true,
        asNavFor: '.js-catalog-item-pic-big-carousel',
        responsive: [
            {
                breakpoint: 550,
                settings: {
                    vertical: false
                }
            }
        ]
    });

    $('.js-catalog-item-variation-carousel').slick({
        infinite: false,
        slidesToShow: 4,
        slidesToScroll: 1,
        arrows: true,
        focusOnSelect: true,
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 3,
                }
            }
        ]

    });

    $('.js-same-product-list').slick({
        infinite: false,
        slidesToShow: 6,
        slidesToScroll: 1,
        arrows: true,
        responsive: [
            {
                breakpoint: 1080,
                settings: {
                    slidesToShow: 4,
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 3,
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 2,
                }
            }
        ]
    });


    $('.account-links-list a').on('click', function () {
        $(this).addClass('active');
        $(this).siblings().removeClass('active');

    });

    $('.account-links-list').on('click', function () {
        $(this).toggleClass('rolled-list');
    });


    $('.basket-brand-selector ').on('click', function () {
        $(this).toggleClass('active');
    });


    $('.js-close-floating-size-panel').on('click', function () {
        $(this).closest('.floatin-size-panel').fadeOut();
    });

    //
    //
    // $('.js-welcome-carousel').slick({
    //   infinite: true,
    //   slidesToShow:4,
    //   slidesToScroll: 1,
    //   arrows: true,
    //   dots: false,
    //   responsive: [
    //     {
    //       breakpoint: 991,
    //       settings: {
    //         slidesToShow: 3
    //       }
    //     },
    //     {
    //       breakpoint: 768,
    //       settings: {
    //         slidesToShow: 2,
    //         slidesToScroll: 1
    //       }
    //     },
    //     {
    //       breakpoint: 480,
    //       settings: {
    //         slidesToShow: 1,
    //         slidesToScroll: 1
    //       }
    //     }
    //     // You can unslick at a given breakpoint now by adding:
    //     // settings: "unslick"
    //     // instead of a settings object
    //   ]
    // });


    // $('.search-call-btn').on('click', function(){
    //     $(this).toggleClass('active')
    //     $(this).closest('.header-search-block').find('.header-search-block-wrap').toggleClass('active');
    // });

    // $('.js-burger-btn').on('click', function(){
    //   $(this).closest('body').toggleClass('active')
    //   $(this).closest('.site-wrapper').find('.nav-wrapper').toggleClass('active');
    // });


    // $('.js-call-modal').on('click', function(){
    //   $(this).closest('body').find('.modal-wrapper').fadeIn();
    // });
    //
    // $('.js-close-modal').on('click', function(){
    //   $(this).closest('.modal-wrapper').fadeOut();
    // });


    // $('.js-clu-fav').on('click', function(){
    //     $(this).toggleClass('active');
    // });

});



