$(function() {

    $('.js-order-image-archive').each(function() {
        var $button = $(this);
        var status = Number($button.data('status'));
        var interval = null;

        var loadFile = function() {
            var file = $button.data('file');
            var elem = window.document.createElement('a');
            elem.href = file;
            document.body.appendChild(elem);
            elem.click();
            document.body.removeChild(elem)
        };

        var firstGetFile = function() {
            getFile();
            interval = setInterval(getFile, 10000);
            $button.prop('disabled', true);
            $button.find('.js-btn-preloader').show();
        };

        var getFile = function() {

            var number = Number($button.data('number'));

            $.get('/ajax/account/order-get-archive', {number: number}, function(data) {
                if (data.success === true) {
                    if (data.file !== false) {
                        $button.data('file', data.file);
                        clearInterval(interval);
                        $button.off('click', firstGetFile);
                        $button.on('click', loadFile);
                        $button.prop('disabled', false);
                        $button.find('.js-btn-preloader').hide();
                    }
                }
            }, 'json');
        };

        if (status === 0) {
            $button.on('click', firstGetFile);
            return;
        }

        if (status === 1) {
            getFile();
            interval = setInterval(getFile, 10000);
        }

        if (status === 2) {
            $button.on('click', loadFile);
        }
    });

});
