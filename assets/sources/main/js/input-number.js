
$(document).on('focusout', '.js-input-number input[type="text"]', function() {
    var input = $(this);
    var iterate = input.data('iterate') ? Number(input.data('iterate')) : 1;
    var value = Number(input.val());
    var checkMax = !input.data('check-max') === false;

    if (value === 0) {
        input.val('');
        return;
    }

    if (checkMax === true && isNaN(value) === false) {
        var stock = Number(input.data('stock'));
        if (value > stock) {
            value = stock;
        }
    }

    if (isNaN(value) === true || value < iterate) {
        input.val(iterate);
    } else {
        if (iterate > 1) {
            input.val(value - (value % iterate) + iterate);
        } else {
            input.val(value);
        }
    }

    input.trigger('change');
});

$(document).on('click', '.js-input-number .js-input-number-button', function() {
    var button = $(this);
    var input = button.closest('.js-input-number').find('input[type="text"]');
    if (input.is(':disabled') === true) {
        return;
    }
    var iterate = input.data('iterate') ? Number(input.data('iterate')) : 1;
    var newValue = Number(input.val());
    var value = newValue;
    var action = button.data('action');
    var checkMax = !input.data('check-max') === false;

    if (action === '+') {
        // if(button.data('count') === 0)
        // {
        //     $.growl.error({title: 'Данного размера нет в наличии', message: ''});
        //     return;
        // }
        newValue = value + iterate;
    }

    if (action === '-') {
        newValue = (value - iterate) >= 0 ? (value - iterate) : 0;
    }

    if (checkMax === true) {
        var stock = Number(input.data('stock'));
        if (newValue > stock) {
            newValue = stock;
        }
    }

    if (newValue > 0) {
        input.val(newValue);
    } else {
        input.val('');
    }

    if (newValue !== value) {
        input.trigger('change');
    }
});
