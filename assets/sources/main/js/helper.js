var Helper = function () {
    return {

        timeDuration: function(value) {
        var hours = Math.floor(value / 3600);
        var minutes = Math.floor((value - (hours * 3600)) / 60);
        var seconds = Math.floor(value - ((hours * 3600) + (minutes * 60)));
        var result = '';
        if (hours > 0) {
            result += `${hours}:`;
        }

        result += `${hours > 0 ? (`0${minutes}`) : minutes}:`;
        result += `${seconds < 10 ? (`0${seconds}`) : seconds}`;

        return result;
    }



    };
}();
