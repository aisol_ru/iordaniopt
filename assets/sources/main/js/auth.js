$(function() {

    var CHECK_PHONE = '';

    function openAuthPopupPhone() {
        $.fancybox.open({
            src: '#login-modal-phone',
            type : 'inline',
            afterClose: function() {
                $('#modal-phone-input').val('');
            },
            beforeShow: function() {
                CHECK_PHONE = '';
            },
        });
    }

    function openAuthPopup() {
        $.fancybox.open({
            src: '#login-modal',
            type : 'inline',
        });
    }

    function openRestorePopup() {
        $.fancybox.close({src: '#login-modal'});
        $.fancybox.open({
            src: '#restore-modal',
            type : 'inline',
        });
    }

    function openRegPopup() {
        $.fancybox.close({src: '#login-modal'});
        $.fancybox.open({
            src: '#reg-modal',
            type : 'inline',
        });
    }

    var GET_CODE_TIMER = null;
    var RE_CODE_TIME = 120;

    function updateReCodeInterval() {
        RE_CODE_TIME = 120;
        GET_CODE_TIMER = setInterval(function() {
            RE_CODE_TIME--;
            $('.js-auth-get-re-code-timer').text(Helper.timeDuration(RE_CODE_TIME));

            if (RE_CODE_TIME <= 0) {
                clearInterval(GET_CODE_TIMER);
            }
        }, 1000);
    }

    function openAuthPopupCode() {
        updateReCodeInterval();

        $.fancybox.open({
            src: '#login-modal-code',
            type : 'inline',
            afterClose: function() {
                $('#modal-code-input').val('');
                if (GET_CODE_TIMER) {
                    clearInterval(GET_CODE_TIMER);
                }
            },
        });
    }

    $('.js-auth-button').on('click', function(event) {
        event.preventDefault();
        openAuthPopup();
    });

    $('.js-reg-button').on('click', function() {
        openRegPopup();
    });

    $('.js-restore-button').on('click', function() {
        openRestorePopup();
    });

    $(document).on('click', '.js-auth-get-code', function() {
        var button = $(this);
        var input = $('#modal-phone-input');
        var errorInput = $('#modal-phone-input-error');
        var phone = input.val();
        errorInput.text('');
        if (/^\+7 \([0-9]{3}\) [0-9]{3}-[0-9]{2}-[0-9]{2}$/.test(phone) === false) {
            errorInput.text('Введите номер телефона');
            return;
        }

        button.prop('disabled', true);
        $.post('/ajax/auth/get-code', {phone: phone}, function(data) {
            if (data.success === true) {
                CHECK_PHONE = phone;
                $.fancybox.close();
                openAuthPopupCode();
            } else {
                errorInput.text(data.error);
            }
            button.prop('disabled', false);
        }, 'json').fail(function() {
            button.prop('disabled', false);
        });

    });

    $(document).on('click', '.js-auth-get-re-code', function() {
        var link = $(this);
        if (RE_CODE_TIME > 0 || link.attr('disabled')) {
            return;
        }

        link.prop('disabled', true);
        $.post('/ajax/auth/get-code', {phone: CHECK_PHONE}, function(data) {
            if (data.success === true) {
                updateReCodeInterval();
                $.growl.notice({title: 'Вам был выслан новый код', duration: 10000, message: ''});
            }
        }, 'json').fail(function() {
            link.prop('disabled', false);
        });

    });

    $(document).on('click', '.js-auth-check-code', function() {
        var button = $(this);
        var input = $('#modal-code-input');
        var code = input.val().trim();

        if (code.length === 0) {
            return;
        }

        button.prop('disabled', true);

        $.post('/ajax/auth/check-code', {code: code}, function(data) {

            if (data.success === true) {
                window.location = data.redirect;
            } else {
                if (data.end) {
                    $.fancybox.close();
                    openAuthPopupPhone();
                    $.growl.error({title: 'Ошибка!', message: 'Неверный код. Попробуйте еще раз'});
                } else {
                    $.growl.error({title: 'Ошибка!', message: 'Неверный код'});
                    input.val('');
                }
            }

            button.prop('disabled', false);
        }, 'json').fail(function() {
            button.prop('disabled', false);
        });
    });

    $(document).on('click', '.js-auth-check-back', function() {
        $.fancybox.close();
        openAuthPopupPhone();
    });

});
