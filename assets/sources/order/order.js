var Order = function () {
    return {

        /**
         * Форматирует число по заданному формату
         * @param {numeric} number число для форматирования
         * @param {integer} decimals количество цифер после запятой
         * @param {string} decPoint разделитель дроброй части
         * @param {string} thousandsSep разделитель целой части
         * @returns {string} форматированное число
         */
        number_format: function (number, decimals, decPoint, thousandsSep) {
            decimals = decimals || 0;
            number = parseFloat(number);

            if (!decPoint || !thousandsSep) {
                decPoint = '.';
                thousandsSep = ' ';
            }

            var roundedNumber = Math.round(Math.abs(number) * ('1e' + decimals)) + '';
            var numbersString = decimals ? roundedNumber.slice(0, decimals * -1) : roundedNumber;
            var decimalsString = decimals ? roundedNumber.slice(decimals * -1) : '';
            var formattedNumber = "";

            while (numbersString.length > 3) {
                formattedNumber += thousandsSep + numbersString.slice(-3);
                numbersString = numbersString.slice(0, -3);
            }

            return (number < 0 ? '-' : '') + numbersString + formattedNumber + (decimalsString ? (decPoint + decimalsString) : '');
        },

        /**
         * Возвращает необходимую форму слова из массива forms в зависимости от count
         * @param {integer} count количество
         * @param {Array} forms массив форм вида ['товар', 'товара', 'товаров']
         * @returns {string} форма слова из forms
         */
        plural_form: function (count, forms) {
            return count % 10 == 1 && count % 100 != 11 ? forms[0] : (count % 10 >= 2 && count % 10 <= 4 && (count % 100 < 10 || count % 100 >= 20) ? forms[1] : forms[2]);
        },

        /**
         * Обновление кнопки корзины
         * @param {Number} count количество
         */
        refreshMenuBasket: function (count) {

            $('.js-menu-basket-count').text(count);
        },

        /**
         * Добавление товара в заказ
         * @param {Number} goodId идентификатор товара
         * @param {Number} insertId
         * @param {Number} coatingId
         * @param {Number} count количество
         * @param {function} callback функция, которая выполнится после запроса
         */
        add: function (goodId, insertId, coatingId, count, comment, callback) {
            if (!callback) {
                callback = function () {
                };
            }

            $.post('/ajax/order/add', {good_id: goodId, insert_id: insertId, coating_id:coatingId, count: count, comment: comment}, callback, 'json');
        },

        /**
         * Обновление количества товаров в корзине
         * @param {Number} goodId идентификатор товара
         * @param {Number} insertId
         * @param {Number} coatingId
         * @param {Number} count количество
         * @param {function} callback функция, которая выполнится после запроса
         */
        update: function (goodId, insertId, coatingId, count, callback) {
            if (!callback) {
                callback = function () {
                };
            }

            $.post('/ajax/order/update', {good_id: goodId, insert_id: insertId, coating_id: coatingId, count: count}, callback, 'json');
        },

        /**
         * Обновление количества товаров в корзине
         * @param {Number} goodId идентификатор товара
         * @param {Number} price_id тип цены
         * @param {function} callback функция, которая выполнится после запроса
         */
        remove: function (goodId, insertId, coatingId, callback) {
            if (!callback) {
                callback = function () {
                };
            }

            $.post('/ajax/order/remove', {good_id: goodId, insert_id: insertId, coating_id: coatingId}, callback, 'json');
        },

        /**
         * Отчистка корзины
         * @param {function} callback функция, которая выполнится после запроса
         */
        flush: function (callback) {
            if (!callback) {
                callback = function () {
                };
            }

            $.post('/ajax/order/flush', {}, callback, 'json');
        },

        /**
         * Повтор заказа
         * @param {Number} number номер заказа
         * @param {function} callback функция, которая выполнится после запроса
         */
        repeat: function (number, callback) {
            if (!callback) {
                callback = function () {
                };
            }

            $.post('/ajax/order/repeat', {number: number}, callback, 'json');
        }


    };
}();
