function goodSetSamplePrice() {
    var element = $('.js-good-sample');
    var option = element.find(`option[value="${element.val()}"]`);
    var price = $('.js-good-price-unit').data('price');
    $('.js-good-price-unit').text(Order.number_format(price, 0, '.', ' '));
    goodCountSum();
}

function goodCountSum() {
    var sum = 0;
    var sample = $('.js-good-sample');
    var option = sample.find(`option[value="${sample.val()}"]`);
    var priceUnit = $('.js-good-sum').data('price');

    $('.js-good-count').each(function () {
        var element = $(this);
        var count = Number(element.val());
        var price = Number(element.data("price"));

        if (isNaN(count) === true) {
            return;
        }
        // sum += count * priceUnit;
        sum += count * price;
    });

    $('.js-good-sum').text(Order.number_format(sum, 0, '.', ' '));
}

goodSetSamplePrice();
goodCountSum();

$(document).on('change', '.js-good-sample', goodSetSamplePrice);

$(document).on('change', '.js-good-count', goodCountSum);


$('.js-good-add').on('click', function () {
    var $button = $(this);
    var goodId = $button.data('id');
    var hasSizes = Number($button.data('has-sizes'));
    var count;
    var comment = $('#catalog-item-comment').val();

    var insertBlock = $('.js-select-insert button.active');
    var coatingBlock = $('#catalog-select-coating');
    var insertId = insertBlock.data('insert');
    var coatingId = coatingBlock.val();

    if (hasSizes) {
        count = [];
        var totalCount = 0;
        $('.js-good-count:visible').each(function () {
            var element = $(this);
            var value = Number(element.val());
            if (isNaN(value) === true) {
                return;
            }
            totalCount += value;
            count.push({
                size: element.data('size'),
                count: value,
                price: element.data('price'),
                weight: Number(element.data('size-weight'))
            });
        });
        if (totalCount === 0) {
            $.growl.error({title: 'Введите количество хотя бы 1-го размера', message: ''});
            return;
        }
    } else {
        count = Number($('.js-good-count').val());
        if (isNaN(count) === true || count === 0) {
            $.growl.error({title: 'Выберите количество товара', message: ''});
            return;
        }
    }
    $button.prop('disabled', true);

    Order.add(goodId, insertId, coatingId, count, comment, function (data) {
        $.growl.notice({title: 'Товар добавлен в корзину', message: ''});
        Order.refreshMenuBasket(data.good_count);
        $button.text('Добавить еще');
        $button.prop('disabled', false);

        $('.js-good-count:visible').each(function () {
            $(this).val(null);
        });
        $('.js-good-sum').val(0);
    });
});

$(document).on('click', '.js-basket-flush', function () {
    Order.flush(function () {
        window.location.reload();
    });
});

$(document).on('click', '.js-order-repeat', function () {
    var $button = $(this);
    $button.prop('disabled', true);
    var number = Number($button.data('number'));

    Order.repeat(number, function (data) {
        if (data.success === true) {
            Order.refreshMenuBasket(data.good_count);
            window.location.href = data.redirect;
        }
        $button.prop('disabled', false);
    });
});

$(document).on('click', '.js-order-repeat-good', function () {
    var $button = $(this);
    $button.prop('disabled', true);
    var goodId = $button.data('id');
    var priceId = $button.data('price-id');
    var count = $button.data('count');

    Order.add(goodId, priceId, count, function (data) {
        $.growl.notice({title: 'Товар добавлен в корзину', message: ''});
        Order.refreshMenuBasket(data.good_count);

        $button.prop('disabled', false);
    });
});

$(document).on('click', '.js-basket-remove', function () {
    var $button = $(this);
    var goodId = $button.data('good-id');

    var insertId = $button.data('insert-id');
    var coatingId = $button.data('coating-id');

    $button.prop('disabled', true);
    Order.remove(goodId, insertId, coatingId, function (data) {
        Order.refreshMenuBasket(data.good_count);

        $('.js-basket-count').text(Order.number_format(data.good_count, 0, '.', ' '));
        $('.js-basket-weight').text(Order.number_format(data.good_weight, 2, '.', ' '));
        $('.js-basket-sum').text(Order.number_format(data.sum, 0, '.', ' '));

        $button.closest('.js-basket-row').remove();

        if (data.good_count === 0) {
            window.location.reload();
        }
    });
});

$(document).on('click', '.js-basket-edit-panel-close', function () {
    var $button = $(this);
    var $panel = $button.closest('.js-basket-edit-panel');

    $panel.fadeOut();

    $panel.find('.js-basket-input-quant').each(function () {
        var $input = $(this);
        $input.val($input.data('default'));
    });
});

$(document).on('click', '.js-basket-show-edit-panel', function () {
    $('.js-basket-edit-panel:visible').each(function () {
        var $elem = $(this);
        $elem.find('.js-basket-edit-panel-close').trigger('click');
    });


    var $button = $(this);
    var $goodRow = $button.closest('.js-basket-row');
    var $panel = $goodRow.find('.js-basket-edit-panel');

    $panel.fadeIn();
});

$(document).on('click', '.js-basket-edit', function () {
    var $button = $(this);
    var $goodRow = $button.closest('.js-basket-row');
    var $panel = $goodRow.find('.js-basket-edit-panel');
    var goodId = $button.data('id');
    var hasSizes = Number($button.data('has-sizes'));
    var count;

    var insertId = $button.data('insert-id');
    var coatingId = $button.data('coating-id');

    if (hasSizes) {
        count = [];
        var totalCount = 0;
        $panel.find('.js-basket-edit-good-count').each(function () {
            var element = $(this);
            var value = Number(element.val());
            if (isNaN(value) === true) {
                return;
            }
            totalCount += value;
            count.push({
                size: element.data('size'),
                price: element.data('price'),
                count: value,
                weight: Number(element.data("size-weight")),
            });
        });
        if (totalCount === 0) {
            $.growl.error({title: 'Введите количество хотя бы 1-го размера', message: ''});
            return;
        }
    } else {
        count = Number($panel.find('.js-basket-edit-good-count').val());
        if (isNaN(count) === true || count === 0) {
            $.growl.error({title: 'Выберите количество товара', message: ''});
            return;
        }
    }
    $button.prop('disabled', true);

    Order.update(goodId, insertId, coatingId, count, function (data) {
        Order.refreshMenuBasket(data.good_count);

        $('.js-basket-count').text(Order.number_format(data.good_count, 0, '.', ' '));
        $('.js-basket-weight').text(Order.number_format(data.good_weight, 2, '.', ' '));
        $('.js-basket-sum').text(Order.number_format(data.sum, 0, '.', ' '));

        $.pjax.reload('#basket-pjax', {timeout: 5000});

        $button.prop('disabled', false);
    });
});
