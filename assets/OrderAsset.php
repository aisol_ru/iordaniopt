<?php

namespace app\assets;

use app\components\AssetBundle;
use yii\web\JqueryAsset;

class OrderAsset extends AssetBundle
{

    public $sourcePath = '@app/assets/sources/order';

    public $js = [
        'order.js',
        'default.js',
    ];
    public $depends = [
        JqueryAsset::class,
    ];

}
