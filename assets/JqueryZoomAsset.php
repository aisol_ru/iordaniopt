<?php

namespace app\assets;

use yii\web\AssetBundle;
use yii\web\JqueryAsset;

class JqueryZoomAsset extends AssetBundle
{

    public $sourcePath = '@bower/jquery-zoom';
    public $js = [];
    public $css = [];
    public $depends = [
        JqueryAsset::class,
    ];

    public function init()
    {
        parent::init();

        if (YII_DEBUG === true) {
            $this->js = ['jquery.zoom.js'];
        } else {

            $this->js = ['jquery.zoom.min.js'];
        }
    }

}
