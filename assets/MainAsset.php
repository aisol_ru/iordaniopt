<?php

namespace app\assets;

use app\components\AssetBundle;
use yii\web\JqueryAsset;

class MainAsset extends AssetBundle
{

    public $sourcePath = '@app/assets/sources/main';

    public $css = [
        'https://fonts.googleapis.com/css2?family=Open+Sans&display=swap',
        'css/normalize.css',
        'plugins/noUiSlider/nouislider.min.css',
        'plugins/slick/slick.css',
        'css/main.css',
        'fonts/fonts.css',
        'css/custom.css',
    ];
    public $js = [
        'plugins/slick/slick.js',
        'plugins/noUiSlider/wNumb.min.js',
        'plugins/noUiSlider/nouislider.min.js',
        'js/helper.js',
        'js/auth.js',
        'js/input-number.js',
        'js/favorite.js',
        'js/account.js',
        'js/script.js',
        'js/custom.js',
    ];
    public $depends = [
        JqueryAsset::class,
        JqueryZoomAsset::class,
        FancyBox3Asset::class,
        OrderAsset::class,
        GrowlAsset::class,
        AutoCompleteAsset::class,
    ];
    

}
