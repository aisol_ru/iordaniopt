<?php

namespace app\assets;

use yii\web\AssetBundle;

class AutoCompleteAsset extends AssetBundle
{

    public $sourcePath = '@app/assets/sources/autoComplete';

    public $css = [
        'css/autoComplete.css',
    ];
    public $js = [
        'js/autoComplete.js',
    ];


    public static function path($relativePath = '')
    {
        $obj = new self();
        return \Yii::$app->assetManager->getPublishedUrl($obj->sourcePath) . '/' . $relativePath;
    }

    public function init()
    {
        parent::init();

        if (YII_DEBUG && !\Yii::$app->request->isPjax) {
            $this->publishOptions['forceCopy'] = true;
        }

    }

}
