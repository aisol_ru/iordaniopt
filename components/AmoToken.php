<?php

namespace app\components;

use app\models\Key;
use http\Exception\RuntimeException;

class AmoToken extends \League\OAuth2\Client\Token\AccessToken
{

    protected $accessToken;

    public function __construct()
    {
        $key = Key::findOne(15);

        $this->accessToken = json_decode($key->value, true);
    }

    public function getToken()
    {
        return $this->accessToken["access_token"];
    }

    /**
     * Returns the refresh token, if defined.
     *
     * @return string|null
     */
    public function getRefreshToken()
    {
        if (!empty($this->accessToken["refresh_token"])) {
            return $this->accessToken["refresh_token"];
        }
        return null;
    }

    /**
     * Returns the expiration timestamp in seconds, if defined.
     *
     * @return integer|null
     */
    public function getExpires()
    {
        if (!empty($this->accessToken["expires"])) {
            return $this->accessToken["expires"];
        }
        return null;
    }

    /**
     * Checks if this token has expired.
     *
     * @return boolean true if the token has expired, false otherwise.
     * @throws RuntimeException if 'expires' is not set on the token.
     */
    public function hasExpired()
    {
        if (empty($this->getExpires())) {
            throw new RuntimeException();
        }
        return $this->getExpires() < time();
    }

    /**
     * Returns additional vendor values stored in the token.
     *
     * @return array
     */
    public function getValues()
    {
        return [];
    }

    /**
     * Returns a string representation of the access token
     *
     * @return string
     */
    public function __toString()
    {
        return "";
    }

    /**
     * Returns an array of parameters to serialize when this is serialized with
     * json_encode().
     *
     * @return array
     */
    public function jsonSerialize()
    {
        return json_decode($this->accessToken, true);
    }
}