<?php

namespace app\components;

use Yii;

class AssetBundle extends \yii\web\AssetBundle
{


    public static function path($relativePath = '')
    {
        $obj = new static();
        return Yii::$app->getAssetManager()->getPublishedUrl($obj->sourcePath) . '/' . $relativePath;
    }


    public function init()
    {
        parent::init();

        if (YII_DEBUG === true && Yii::$app->getRequest()->isPjax === false) {
            $this->publishOptions['forceCopy'] = true;
        }

    }

}
