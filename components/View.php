<?php

namespace app\components;

use app\models\Domain;
use app\models\Key;
use app\models\structure\Tree;
use Yii;
use yii\base\Event;
use yii\web\Request;

class View extends \rmrevin\yii\minify\View
{
    /**
     * @var string Кеш метрик
     */
    const METRIC_CASHE = 'metrics';

    /**
     * @var Tree текущая страница
     */
    public $tree = null;

    /**
     * @var string Описание
     */
    public $description = '';

    /**
     * @var string Ключевые слова
     */
    public $keywords = '';

    /**
     * @var string Заголок H1 страницы
     */
    public $h1 = '';

    /**
     * Подключение скриптов метрик
     */
    private function metricsInit()
    {
        if (Yii::$app->request->isAjax === true || Yii::$app->request->isPjax === true) {
            return;
        }
        $metrics = Yii::$app->cache->get(self::METRIC_CASHE);
        if ($metrics === false) {
            $metrics = Key::getGroup(12);
            Yii::$app->cache->add(self::METRIC_CASHE, $metrics);
        }
        foreach ($metrics as $name => $metric) {
            $this->registerJs(strip_tags($metric), self::POS_END, $name);
        }
    }

    public function init()
    {
        $user = Yii::$app->getUser();
        if ($user->isGuest === false && $user->can('developer') === true) {
            $this->enableMinify = false;
        }

        $this->on(self::EVENT_BEGIN_PAGE, function($event) {
            /* @var $event Event */
            /* @var $view View */
            $view = $event->sender;

            if (empty($view->description) === false) {
                $view->registerMetaTag(['content' => $view->description, 'name' => 'description']);
            }
            if (empty($view->keywords) === false) {
                $view->registerMetaTag(['content' => $view->keywords, 'name' => 'keywords']);
            }

            $domain = $view->getDomain();
            if (empty($domain->favicon) === false) {
                $view->registerLinkTag(['sizes' => '57x57', 'href' => $domain->getResizeCache('favicon', 57, 57, 5), 'rel' => 'apple-touch-icon']);
                $view->registerLinkTag(['sizes' => '60x60', 'href' => $domain->getResizeCache('favicon', 60, 60, 5), 'rel' => 'apple-touch-icon']);
                $view->registerLinkTag(['sizes' => '72x72', 'href' => $domain->getResizeCache('favicon', 72, 72, 5), 'rel' => 'apple-touch-icon']);
                $view->registerLinkTag(['sizes' => '76x76', 'href' => $domain->getResizeCache('favicon', 76, 76, 5), 'rel' => 'apple-touch-icon']);
                $view->registerLinkTag(['sizes' => '114x114', 'href' => $domain->getResizeCache('favicon', 114, 114, 5), 'rel' => 'apple-touch-icon']);
                $view->registerLinkTag(['sizes' => '120x120', 'href' => $domain->getResizeCache('favicon', 120, 120, 5), 'rel' => 'apple-touch-icon']);
                $view->registerLinkTag(['sizes' => '144x144', 'href' => $domain->getResizeCache('favicon', 144, 144, 5), 'rel' => 'apple-touch-icon']);
                $view->registerLinkTag(['sizes' => '152x152', 'href' => $domain->getResizeCache('favicon', 152, 152, 5), 'rel' => 'apple-touch-icon']);
                $view->registerLinkTag(['sizes' => '180x180', 'href' => $domain->getResizeCache('favicon', 180, 180, 5), 'rel' => 'apple-touch-icon']);


                $view->registerLinkTag(['sizes' => '192x192', 'href' => $domain->getResizeCache('favicon', 192, 192, 5), 'rel' => 'icon', 'type' => 'image/png']);
                $view->registerLinkTag(['sizes' => '96x96', 'href' => $domain->getResizeCache('favicon', 96, 96, 5), 'rel' => 'icon', 'type' => 'image/png']);
                $view->registerLinkTag(['sizes' => '32x32', 'href' => $domain->getResizeCache('favicon', 32, 32, 5), 'rel' => 'icon', 'type' => 'image/png']);
                $view->registerLinkTag(['sizes' => '16x16', 'href' => $domain->getResizeCache('favicon', 16, 16, 5), 'rel' => 'icon', 'type' => 'image/png']);
            }

            $request = Yii::$app->getRequest();
            if ($request instanceof Request && $request->enableCsrfValidation === true) {
                $view->registerCsrfMetaTags();
            }

        });

        parent::init();
        $this->metricsInit();
    }

    /**
     * Получение текущего домена
     * @return Domain|null
     */
    public function getDomain()
    {
        /* @var $urlManager UrlManager */
        $urlManager = Yii::$app->getUrlManager();

        return $urlManager->domain;
    }

}
