<?php

namespace app\components;

use app\components\interfaces\UrlManagerMultiDomain;
use app\models\user\User;
use Yii;
use yii\helpers\Html;
use app\models\Key;
use yii\helpers\Url;

class Mailer
{

    /**
     * идентификатор параметра в базе, который содержит адреса
     */
    const PARAMETER_EMAILS_GROUP = 1;

    /**
     * Генерирует email адрес из текущего домена
     * @return string сгенерированный email
     */
    static function generateEmailByDomain()
    {
        $host = Yii::$app->urlManager->getHostInfo();
        $domainParse = preg_replace('/(^.*\/\/www\.)|(^.*\/\/)/', '', $host);
        return 'noanswer@' . $domainParse;
    }

    /**
     * Получает адреса из параметра указанного в self::PARAMETER_EMAILS_ID и разбивает их по запятой
     * @return array|string массив адресов или один адрес
     */
    static function getEmails()
    {
        $parameter = \app\models\parameter\Parameter::getValue(self::PARAMETER_EMAILS_GROUP);
        if (empty($parameter) === true) {
            return null;
        }
        if (is_array($parameter) === true) {
            $emails = [];
            foreach ($parameter as $value) {
                $emailsParse = str_replace(' ', '', $value);
                $emails[] = $value;
            }
            if (count($emails) == 1) {
                $parameter = array_shift($emails);
            }
        } else {
            $emailsParse = preg_replace('/(^,+)|(,+$)|( )/s', '', $parameter);
            $emails = explode(',', $emailsParse);
        }
        if (count($emails) > 1) {
            return $emails;
        } else {
            return trim($parameter);
        }
    }

    /**
     * Отправляет сообщение. Если email не передан, то будет произведен поиск в параметрах
     * @param array|string|null $emails адреса на которые будут отправлены сообщения
     * @param string $subject тема письма
     * @param string $textHtml HTML письма
     * @param array $attachs аттачи в виде ['filename' => [options]] или ['filename1', 'filename2']
     * @param boolean $multiple мультиотправка (получатель увидит только свой email)
     * @param string $view представление письма
     * @param array $parameters дополнительные параметры представления
     * @return boolean результат отправки письма
     */
    static function send($emails, $subject, $textHtml, $attachs = [], $multiple = true, $view = 'views/mail', $parameters = [])
    {

        if (is_null($emails)) {
            $emails = self::getEmails();
        }

        $emailFrom = Key::getKeyValue('emailFrom', false);
        if ($emailFrom === false || empty($emailFrom)) {
            $emailFrom = self::generateEmailByDomain();
        }

        if (!empty($textHtml)) {
            $parameters['content'] = $textHtml;
        }

        if ($multiple) {
            $emailsBCC = Key::getGroup(7);
            if (!empty($emailsBCC)) {
                $emails = array_merge((array) $emails, array_keys((array) $emailsBCC));
            }
            return self::sendMultiple(self::getValidEmails(array_unique($emails)), $emailFrom, $subject, $attachs, $view, $parameters);
        }

        return self::sendSingle($emails, $emailFrom, $subject, $attachs, $view, $parameters);
    }

    /**
     * Проверка правильности email'ов
     * @param string|array $emails Адреса для проверки. Возможна как строка, так и массив адерсов в видах [0 => 'email', 1 => 'email2'] или ['email' => 'Ivanov', 'email2' => 'Petrov']
     * @return array массив валидных адресов
     */
    public static function getValidEmails($emails) {
        $emails = (array) $emails;
        $validator = new \yii\validators\EmailValidator();

        $validEmails = [];
        foreach ($emails as $key => $value) {

            $email = is_numeric($key) === true ? trim($value) : trim($key) ;
            if (empty($email) === true) {
                \Yii::warning('Попытка отправки на пустой адрес', 'emails');
                continue;
            }

            if ($validator->validate($email) === false) {
                \Yii::warning('Попытка отправки на НЕ валидный адрес - ' . $email, 'emails');
                continue;
            }

            if (is_numeric($key) === true) {
                $validEmails[] = $email;
            } else {
                $validEmails[$email] = $value;
            }
        }

        return $validEmails;
    }

    /**
     * Отправляет письмо
     * @param array|string $emails адреса на которые будут отправлены сообщения
     * @param string $emailFrom от кого
     * @param string $subject тема письма
     * @param array $attachs аттачи в виде ['filename' => [options]] или ['filename1', 'filename2']
     * @param string $view представление письма
     * @param array $parameters дополнительные параметры представления
     * @return boolean успешность оправки всех писем
     */
    private static function sendSingle($emails, $emailFrom, $subject, $attachs = [], $view = 'views/mail', $parameters = [])
    {
        $mailer = Yii::$app->mailer->compose($view, $parameters);
        if (is_array($emails)) {
            $emails = array_unique($emails);
        }
        $emailsBCC = Key::getGroup(7);
        if (empty($emailsBCC) === false) {
            $mailer->setBcc(self::getValidEmails(array_keys($emailsBCC)));
        }

        foreach ($attachs as $key => $attach) {
            if (is_array($attach)) {
                $mailer->attach($key, $attach);
            } else {
                $mailer->attach($attach);
            }
        }

        return $mailer->setFrom($emailFrom)->setTo(self::getValidEmails($emails))->setSubject($subject)->send();
    }


    /**
     * Отправляет уведомление о восстановлении пользователя
     * @param string $login - логин пользователя
     * @param string $email - email пользователя
     * @return boolean результат отправки письма
     */
    static function lostPassword($login, $email, $newPassword)
    {
        $message = $login . ", Ваш новый пароль на сайте " . Yii::$app->name . ": " . $newPassword;
        return self::send($email, 'Новый пароль на сайте ' . Yii::$app->name, $message);
    }

    /**
     * Отправляет письма мультиотправкой (не видно других адресатов)
     * @param array $emails адреса на которые будут отправлены сообщения
     * @param string $emailFrom от кого
     * @param string $subject тема письма
     * @param array $attachs аттачи в виде ['filename' => [options]] или ['filename1', 'filename2']
     * @param string $view представление письма
     * @return boolean успешность оправки всех писем
     */
    private static function sendMultiple($emails, $emailFrom, $subject, $attachs = [], $view = 'views/mail', $parameters = [])
    {
        $messages = [];

        foreach ($emails as $email) {
            $mailer = Yii::$app->mailer->compose($view, $parameters);

            foreach ($attachs as $key => $attach) {
                if (is_array($attach)) {
                    $mailer->attach($key, $attach);
                } else {
                    $mailer->attach($attach);
                }
            }

            $messages[] = $mailer->setFrom($emailFrom)->setTo($email)->setSubject($subject);
        }
        $countSuccess = \Yii::$app->mailer->sendMultiple($messages);
        return $countSuccess === count($emails);
    }

    /**
     * Отправляет уведомление о регистрации пользователя
     * @param User $user пользователь
     * @return boolean результат отправки письма
     */
    static function registration($user)
    {
        /* @var $urlManager UrlManagerMultiDomain */
        $urlManager = Yii::$app->getUrlManager();
        $siteName = $urlManager->getDomain()->name;
        $siteUrl = Url::base(true);
        $html = <<<HTML
<p>Уважаемый(ая) {$user->first_name} {$user->last_name}, Ваш аккаунт на сайте <a href="{$siteUrl}">{$siteName}</a> находится на проверке.</p>
<p>Как только проверка закончится Вы получите уведомление и сможете авторизовать используя номер телефона, указанный при регистрации</p>
HTML;

        return self::send($user->email, 'Регистрация на сайте ' . $siteName, $html);
    }

    /**
     * Отправляет уведомление о регистрации администратора
     * @param User $user пользователь
     * @return boolean результат отправки письма
     */
    static function registrationAdmin(User $user)
    {
        /* @var $urlManager UrlManagerMultiDomain */
        $urlManager = Yii::$app->getUrlManager();
        $siteName = $urlManager->getDomain()->name;
        $userEditUrl = Url::to(['icms/users/edit', 'id' => $user->id]);
        $html = <<<HTML
<p>На сайте зарегистрировался новый пользователь ({$user->last_name} {$user->first_name} {$user->middle_name})</p>
<p>Проверьте его данные, назначьте менеджеров и активируйте его.</p>
<p><a href="{$userEditUrl}">Посмотреть данные пользователя</a></p>
HTML;

        return self::send(null, 'Новая регистрация на сайте ' . $siteName, $html);
    }

    /**
     * Отправляет уведомление о подтверждении регистрации пользователя
     * @param User $user пользователь
     * @return boolean результат отправки письма
     */
    static function registrationApprove(User $user)
    {
        /* @var $urlManager UrlManagerMultiDomain */
        $urlManager = Yii::$app->getUrlManager();
        $siteName = $urlManager->getDomain()->name;
        $siteUrl = Url::base(true);
        $html = <<<HTML
<p>Уважаемый(ая) {$user->first_name} {$user->last_name}, Ваш аккаунт на сайте <a href="{$siteUrl}">{$siteName}</a> прошел провеку.</p>
<p>Теперь Вы можете авторизоваться на сайте используя данные, указанные при заполнении формы регистрации</p>
HTML;

        return self::send($user->email, 'Подтверждение регистрация на сайте ' . $siteName, $html);
    }

}
