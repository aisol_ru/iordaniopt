<?php

namespace app\components;

use AmoCRM\Client\AmoCRMApiClient;
use app\models\Key;
use app\models\order\Order;
use AmoCRM\Models\CatalogElementModel;
use AmoCRM\Collections\CustomFieldsValuesCollection;
use AmoCRM\Models\CustomFieldsValues\TextCustomFieldValuesModel;
use AmoCRM\Models\CustomFieldsValues\ValueModels\TextCustomFieldValueModel;
use AmoCRM\Models\CustomFieldsValues\ValueCollections\TextCustomFieldValueCollection;

class AmoApi
{

    const AMO_BASE_DOMAIN = "iordanipr.amocrm.ru";
    const AMO_SECRET_CODE = "04zAWhO10mzbOrDpLvr7sUyxv1ILrktkQ2sWU3OCc3ZNeUwojD77dTQCfIgkc0vr";
    const AMO_ID = "64c726c1-0ba3-448e-a15f-93cb7540d25b";

    private $apiClient;

    public static function getApiClient()
    {
        return new AmoCRMApiClient(self::AMO_ID, self::AMO_SECRET_CODE, "https://opt-iordani.ru/auth/amo-crm-auth");
    }

    public static function saveToken(\League\OAuth2\Client\Token\AccessToken $accessToken)
    {
        $key = Key::findOne(15);
        $key->value = json_encode([
            'access_token' => $accessToken->getToken(),
            'refresh_token' => $accessToken->getRefreshToken(),
            'expires' => $accessToken->getExpires(),
        ]);
        $key->save(false);
    }

    public function __construct()
    {
        $this->apiClient = self::getApiClient();

        $accessToken = $this->getAccessToken();
        $this->apiClient
            ->setAccessToken($accessToken)
            ->setAccountBaseDomain(self::AMO_BASE_DOMAIN)
            ->onAccessTokenRefresh(
                function (\League\OAuth2\Client\Token\AccessTokenInterface $accessToken, string $baseDomain) {
                    \app\components\AmoApi::saveToken($accessToken);
                }
            );
    }

    public function addOrder(Order $order)
    {
        $catalog = $this->apiClient->catalogs()->getOne(5117);

        $catalogElement = new CatalogElementModel();
        $catalogElement->setName('Заказ №' . $order->id);


        $customFields = $this->generateOrderValues($order);

        $catalogElement->setCustomFieldsValues($customFields);

        $catalogElementsService = $this->apiClient->catalogElements($catalog->getId());

        try {
            $catalogElementsService->addOne($catalogElement);
        } catch (AmoCRMApiException $e) {
            printError($e);
            die;
        }
    }

    private function getAccessToken()
    {

        $oAuth = $this->apiClient->getOAuthClient();

        $accessToken = new AmoToken();

        if (empty($accessToken->getToken())) {
            $this->sendUserOnAuthUrl();
        }

        if ($accessToken->hasExpired()) {
            $accessToken = $oAuth->getAccessTokenByRefreshToken($accessToken);
            self::saveToken($accessToken);
        }

        return $accessToken;
    }

    private function sendUserOnAuthUrl()
    {
        $authorizationUrl = $this->apiClient->getOAuthClient()->getAuthorizeUrl([
            'state' => \Yii::$app->getSecurity()->generateRandomString(),
            'mode' => 'popup',
        ]);

        header('Location: ' . $authorizationUrl);
        die();
    }

    private function generateOrderValues(Order $order)
    {
        $customFields = new CustomFieldsValuesCollection();

        $setFieldId = function ($value) {
            return (new TextCustomFieldValuesModel())
                ->setFieldId($value);
        };

        $addText = function ($value) {
            return (new TextCustomFieldValueCollection())
                ->add((new TextCustomFieldValueModel())
                    ->setValue($value));
        };

        $clientField = $setFieldId(925189);
        $amountField = $setFieldId(1000511);
        $cityField = $setFieldId(925307);
        $innField = $setFieldId(971627);
        $ogrnField = $setFieldId(971629);
        $organizationField = $setFieldId(925245);
        $emailField = $setFieldId(925241);
        $phoneField = $setFieldId(925239);
        $countField = $setFieldId(911617);

        $clientField->setValues($addText($order->client->getFullName()));
        $amountField->setValues($addText($order->sum));
        $cityField->setValues($addText($order->client->city));
        $innField->setValues($addText($order->client->inn));
        $ogrnField->setValues($addText($order->client->ogrn));
        $organizationField->setValues($addText($order->client->organization));
        $emailField->setValues($addText($order->client->email));
        $phoneField->setValues($addText($order->client->phone));
        $countField->setValues($addText($order->good_count));


        $customFields->add($clientField);
        $customFields->add($amountField);
        $customFields->add($cityField);
        $customFields->add($innField);
        $customFields->add($ogrnField);
        $customFields->add($organizationField);
        $customFields->add($emailField);
        $customFields->add($phoneField);
        $customFields->add($countField);

        return $customFields;
    }

}
