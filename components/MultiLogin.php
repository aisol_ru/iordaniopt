<?php


namespace app\components;


use app\components\interfaces\UrlManagerMultiDomain;
use app\models\Domain;
use app\models\user\User;
use Yii;
use yii\helpers\Url;
use yii\web\ForbiddenHttpException;

class MultiLogin
{
    private $token;
    private $been = [];
    private $need = [];
    private $sessionId;
    private $userId;
    private $backUrl;
    private $duration;
    /**
     * @var UrlManagerMultiDomain&\yii\web\UrlManager
     */
    private $urlManager;
    private $loginRoute = ['/auth/domain-auth'];
    private $logoutRoute = ['/auth/domain-logout'];

    public function __construct($token = '', $duration = 0)
    {
        $this->token = $token;
        $this->duration = $duration;
        $this->urlManager = Yii::$app->get('urlManager');
    }

    public function goRedirectLogin($backUrl = false)
    {
        return $this->goRedirect($this->loginRoute, $backUrl);
    }

    public function goRedirectLogout($backUrl = false)
    {
        return $this->goRedirect($this->logoutRoute, $backUrl);
    }

    /**
     * @param $route
     * @param false $backUrl
     * @return false|string
     */
    private function goRedirect($route, $backUrl = false)
    {
        if ($backUrl !== false) {
            $this->backUrl = Url::to($backUrl, true);
        } else {
            $this->backUrl = Url::current([], true);
        }
        $currentDomain = $this->urlManager->getDomain();
        /* @var $needDomains Domain[] */
        $needDomains = Domain::find()
            ->andWhere(['!=', 'id', $currentDomain->id])
            ->all();
        foreach ($needDomains as $needDomain) {
            $this->need[] = [
                'id' => $needDomain->id,
                'domain' => $needDomain->domain,
            ];
        }
        if (empty($this->need) === true) {
            return Url::to($backUrl);
        }
        $this->been[] = [
            'id' => $currentDomain->id,
            'domain' => $currentDomain->domain,
        ];

        $this->sessionId = Yii::$app->getSession()->getId();
        $this->userId = Yii::$app->getUser()->getId();

        return $this->generateRedirectUrl($route);
    }

    public function auth()
    {
        $this->parseToken();
        if (Yii::$app->getSession()->getIsActive() === false) {
            Yii::$app->getSession()->open();
        }
        $sessionData = Yii::$app->getSession()->readSession($this->sessionId);
        session_decode($sessionData);
        $sessionUserId = Yii::$app->getSession()->get(Yii::$app->getUser()->idParam, null);
        if ($this->userId != $sessionUserId) {
            throw new ForbiddenHttpException('User error');
        }
        Yii::$app->getSession()->setId($this->sessionId);
        $user = User::find()
            ->andWhere(['id' => $this->userId])
            ->andWhere(['status' => User::STATUS_ACTIVE])
            ->one();
        Yii::$app->getUser()->login($user, $this->duration);

        $currentDomain = $this->urlManager->getDomain();
        $this->been[] = [
            'id' => $currentDomain->id,
            'domain' => $currentDomain->domain,
        ];

        $this->need = array_filter($this->need, function($domain) use ($currentDomain) {
            return $domain['id'] != $currentDomain->id;
        });

        if (empty($this->need) === true) {
            return $this->backUrl;
        }

        return $this->generateRedirectUrl($this->loginRoute);
    }

    public function logout()
    {
        $this->parseToken();

        if (Yii::$app->getUser()->isGuest === false) {
            Yii::$app->getUser()->logout();
        }

        $currentDomain = $this->urlManager->getDomain();
        $this->been[] = [
            'id' => $currentDomain->id,
            'domain' => $currentDomain->domain,
        ];

        $this->need = array_filter($this->need, function($domain) use ($currentDomain) {
            return $domain['id'] != $currentDomain->id;
        });

        if (empty($this->need) === true) {
            return $this->backUrl;
        }

        return $this->generateRedirectUrl($this->logoutRoute);
    }

    protected function generateRedirectUrl($route)
    {
        $token = $this->generateToken();
        $route['token'] = $token;

        $protocol = preg_replace('/:\/\/.*?$/', '', $this->urlManager->getHostInfo());

        return $protocol . '://' . $this->need[0]['domain'] . Url::to($route, false);
    }


    protected function parseToken()
    {
        $json = Yii::$app->getSecurity()->decryptByKey($this->token, Yii::$app->getRequest()->cookieValidationKey);
        $data = json_decode($json, true);
        $this->been = $data['been_login'];
        $this->need = $data['need_login'];
        $this->sessionId = $data['session_id'];
        $this->userId = $data['user_id'];
        $this->backUrl = $data['back_url'];
        $this->duration = $data['duration'];
    }

    protected function generateToken()
    {
        $data = [
            'been_login' => $this->been,
            'need_login' => $this->need,
            'session_id' => $this->sessionId,
            'user_id' => $this->userId,
            'back_url' => $this->backUrl,
            'duration' => $this->duration,
        ];
        return Yii::$app->getSecurity()->encryptByKey(json_encode($data), Yii::$app->getRequest()->cookieValidationKey);
    }

}
