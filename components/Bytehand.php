<?php

namespace app\components;

class Bytehand
{

    private static $_serviceKey = 'W4AcWemOPaZmHvjG9z4FVgBslfTTC7Oc';
    private static $_sender = 'SMS-INFO';

    public static function send($receiver, $text, $sendTime = 'now')
    {
        $phone = IcmsHelper::clearPhone($receiver);

        $parameters = [
            'sender' => self::$_sender,
            'receiver' => $phone,
            'text' => $text,
        ];

        if ($sendTime !== 'now') {
            $parameters['send_after'] = date(DATE_ATOM, is_numeric($sendTime) === true ? $sendTime : strtotime($sendTime));
        }

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, 'https://api.bytehand.com/v2/sms/messages');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);

        $parametersString = json_encode($parameters);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $parametersString);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_HTTPHEADER, [
            'Content-Type: application/json;charset=UTF-8',
            'X-Service-Key: ' . self::$_serviceKey,
        ]);

        $jsonString = curl_exec($curl);
        curl_close($curl);

        \app\models\log\Bytehand::add(self::$_sender, $parametersString, $jsonString, $sendTime);

        return json_decode($jsonString, true);
    }

}
