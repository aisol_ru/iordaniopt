<?php


namespace app\components\settings;


use Yii;
use yii\helpers\Json;

class Settings extends \pheme\settings\components\Settings
{
    public $modelClass = BaseSetting::class;

    public function get($key, $section = null, $default = null)
    {
        if (is_null($section)) {
            $pieces = explode('.', $key, 2);
            if (count($pieces) > 1) {
                $section = $pieces[0];
                $key = $pieces[1];
            } else {
                $section = '';
            }
        }

        $data = $this->getRawConfig();

        if (isset($data[$section][$key][0])) {
            if (in_array($data[$section][$key][1], ['object', 'double', 'boolean', 'bool', 'integer', 'int', 'float', 'string', 'array'])) {
                if ($this->autoDecodeJson && $data[$section][$key][1] === 'object') {
                    $value = Json::decode($data[$section][$key][0]);
                } else {
                    $value = $data[$section][$key][0];
                    settype($value, $data[$section][$key][1]);
                }
            }
        } else {
            $value = $default;
        }
        return $value;
    }

    public function getLang($key, $lang = null, $default = null, $section = null)
    {
        if (is_null($lang) === true) {
            $lang = Yii::$app->language;
        }

        $key .= '_' . $lang;

        return self::get($key, $section, $default);
    }

}