<?php

namespace app\components\settings;

use yii\helpers\Json;
use yii\base\InvalidParamException;

class BaseSetting extends \pheme\settings\models\BaseSetting
{

    /**
     * @inheritdoc
     */
    public function setSetting($section, $key, $value, $type = null)
    {
        $model = static::findOne(['section' => $section, 'key' => $key]);

        if ($model === null) {
            $model = new static();
            $model->active = 1;
        }
        $model->section = $section;
        $model->key = $key;
        $model->value = strval($value);

        if (empty($value) === false) {
            if ($type !== null) {
                $model->type = $type;
            } else {
                $t = gettype($value);
                if ($t == 'string') {
                    if (is_numeric($value) === true) {
                        $t = 'double';
                    } else {
                        $error = false;
                        try {
                            Json::decode($value);
                        } catch (InvalidParamException $e) {
                            $error = true;
                        }
                        if (!$error) {
                            $t = 'object';
                        }
                    }
                }
                $model->type = $t;
            }
        } else {
            $model->type = 'string';
        }

        return $model->save();
    }

}
