<?php

namespace app\components\db;

class ActiveQuery extends \yii\db\ActiveQuery
{
    /**
     * Получение установленного алиаса
     * @return string
     */
    public function getTableAlias()
    {
        list(,$alias) = $this->getTableNameAndAlias();
        return $alias;
    }

    /**
     * Добавляет в условие выборки self::STATUS_ACTIVE
     * @return $this
     */
    public function isActive()
    {
        $modelClass = $this->modelClass;
        $alias = $this->getTableAlias();
        return $this->andWhere([$alias . '.status' => $modelClass::STATUS_ACTIVE]);
    }

    /**
     * Сортировка по полю sort
     * @param integer $order SORT_ASC или SORT_DESC
     * @return $this
     */
    public function bySort($order = SORT_ASC)
    {
        $alias = $this->getTableAlias();
        return $this->orderBy([$alias . '.sort' => $order]);
    }

    /**
     * Получить в сгруппированном виде по указанному полю
     * @param string $groupField поле для группировки
     * @param boolean $saveKeys сохранять ли ключи
     * @return array массив сгуппированных моделей
     */
    public function asGroup($groupField, $saveKeys = false)
    {
        $objects = $this->all();
        $result = [];

        foreach ($objects as $key => $object) {
            if ($saveKeys === true) {
                $result[$object->{$groupField}][$key] = $object;
            } else {
                $result[$object->{$groupField}][] = $object;
            }
        }

        return $result;
    }

}
