<?php

namespace app\components\controller;

use app\components\View;
use Yii;
use app\components\IcmsHelper;
use app\models\structure\Tree;
use yii\web\Controller;

/**
 * Контроллер фронтэнда
 * @property array $menu Верхнее меню
 * @property array $menu_bottom Нижнее меню
 * @property array $bread Хлебные крошки
 * @property View $view Класс вьюхи
 */
class Frontend extends Controller
{

    public $bread;
    public $layout = 'text';

    public function beforeAction($action)
    {
        $this->view->h1 = Yii::$app->name;
        if (is_null(\Yii::$app->urlManager->pageId) === false) {
            /* @var $page Tree */
            $page = Tree::findOne(\Yii::$app->getUrlManager()->pageId);
            $this->view->tree = $page;

            $this->view->h1 = empty($page->seo_h1) === true ? $page->name_menu : $page->seo_h1;
            $this->view->title = empty($page->seo_title) === true ? $page->name_menu : $page->seo_title;
            $this->view->keywords = $page->seo_keywords;
            $this->view->description = $page->seo_description;

            $this->bread = IcmsHelper::getBreadCrumbsTree($page);
        }

        return parent::beforeAction($action);
    }

}
