<?php

namespace app\components\controller;


use Yii;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use yii\filters\ContentNegotiator;
use yii\filters\Cors;
use yii\filters\VerbFilter;
use yii\rest\Controller;
use yii\web\BadRequestHttpException;
use yii\web\Response;

class Api extends Controller
{

    public $enableCsrfValidation = false;

    public $noAuthentication = [];

    public function behaviors()
    {

        $behaviors = [
            'corsFilter' => [
                'class' => Cors::class,
                'cors' => [
                    'Origin' => ['*'],
                    'Access-Control-Request-Method' => ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS'],
                    'Access-Control-Request-Headers' => ['*'],
                    'Access-Control-Allow-Credentials' => null,
                    'Access-Control-Max-Age' => 86400,
                    'Access-Control-Expose-Headers' => [
                        'link',
                        'x-pagination-current-page',
                        'x-pagination-page-count',
                        'x-pagination-per-page',
                        'x-pagination-total-count'
                    ],
                ],
            ],
            'contentNegotiator' => [
                'class' => ContentNegotiator::class,
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
            'verbFilter' => [
                'class' => VerbFilter::class,
                'actions' => $this->verbs(),
            ],
            'authenticator' => [
                'class' => CompositeAuth::class,
                'authMethods' => [
                    QueryParamAuth::class,
                    HttpBearerAuth::class,
                ],
            ],
        ];
        if (in_array($this->action->id, $this->noAuthentication) !== false) {
            unset($behaviors['authenticator']);
        }
        return $behaviors;
    }

    /**
     * Получает тело запроса как JSON
     * @param bool $allowEmpty разрешен
     * @return array
     * @throws BadRequestHttpException
     */
    public function getRawBodyAsJson($allowEmpty = false)
    {
        $rowBody = Yii::$app->getRequest()->getRawBody();
        if (empty($rowBody) === true && $allowEmpty === false) {
            throw new BadRequestHttpException();
        } elseif(empty($rowBody) === true && $allowEmpty === true) {
            return [];
        }
        return json_decode(trim($rowBody), true);
    }

}
