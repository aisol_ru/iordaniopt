<?php

namespace app\components\traits;


use app\components\db\ActiveRecord;

trait ActiveJSON
{

    public $jsonData = [];

    /**
     * Получение значения из JSON-поля
     * @param string $name
     * @param mixed $default
     * @return mixed
     */
    public function jsonGet($name, $default = null)
    {
        $jsonText = $this->json;
        $json = [];
        if (empty($jsonText) !== true) {
            $json = json_decode($jsonText, true);
        }

        return isset($json[$name]) === true ? $json[$name] : $default;
    }

    /**
     * Установка значения JSON-поля
     * @param string $name
     * @param mixed $value
     */
    public function jsonSet($name, $value)
    {
        $jsonText = $this->json;
        $json = [];
        if (empty($jsonText) !== true) {
            $json = json_decode($jsonText, true);
        }

        $json[$name] = $value;
        $this->jsonData[$name] = $value;
        $this->json = json_encode($json);
    }

    /**
     * Удаление значения JSON-поля
     * @param string $name
     */
    public function jsonUnset($name)
    {
        $jsonText = $this->json;
        $json = [];
        if (empty($jsonText) !== true) {
            $json = json_decode($jsonText, true);
        }
        if (isset($json[$name]) === false) {
            return;
        }
        unset($json[$name]);
        unset($this->jsonData[$name]);

        $this->json = json_encode($json);
    }

    public function jsonDecode()
    {
        $jsonText = $this->json;
        $json = [];
        if (empty($jsonText) !== true) {
            $json = json_decode($jsonText, true);
        }
        $this->jsonData = $json;
    }

    public function jsonEncode()
    {
        $json = $this->jsonData;
        $this->json = json_encode($json);
    }

    public function init()
    {
        parent::init();

        $this->on(ActiveRecord::EVENT_AFTER_FIND, [$this, 'jsonDecode']);
        $this->on(ActiveRecord::EVENT_BEFORE_INSERT, [$this, 'jsonEncode']);
        $this->on(ActiveRecord::EVENT_BEFORE_UPDATE, [$this, 'jsonEncode']);
    }

}
