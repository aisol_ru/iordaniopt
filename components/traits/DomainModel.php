<?php

namespace app\components\traits;

use app\components\interfaces\UrlManagerMultiDomain;
use app\models\Domain;
use yii\base\Event;
use yii\base\InvalidConfigException;

/**
 * @property integer $domain_id
 *
 * @property Domain $domain
 */
trait DomainModel
{
    public function getDomainId()
    {
        return $this->domain_id;
    }

    public function setDomainId($domainId)
    {
        $this->domain_id = $domainId;
    }

    public function getDomain()
    {
        return $this->hasOne(Domain::class, ['id' => 'domain_id']);
    }

    public function init()
    {
        parent::init();

        if ($this->hasAttribute('domain_id') === false) {
            throw new InvalidConfigException('Modal "' . static::class . '" not have attribute "domain_id"');
        }

        $this->on(self::EVENT_BEFORE_INSERT, function($event) {
            /* @var $event Event */
            /* @var $model DomainModel */
            $model = $event->sender;
            /* @var $urlManager UrlManagerMultiDomain */
            $urlManager = \Yii::$app->getUrlManager();
            $domain = $urlManager->getDomain();
            if (is_null($domain) === false && $model->getDomainId() == false) {
                $model->setDomainId($domain->id);
            }
        });
    }

    public static function find($alias = '', $addDomain = true)
    {
        $activeQuery = parent::find();

        if (empty($alias) === true) {
            $alias = $activeQuery->getTableAlias();
        } else {
            $activeQuery->alias($alias);
        }
        if ($addDomain === true) {
            /* @var $urlManager UrlManagerMultiDomain */
            $urlManager = \Yii::$app->getUrlManager();
            $activeQuery->andWhere([$alias . '.domain_id' => $urlManager->getDomain()->id]);
        }

        return $activeQuery;
    }

}
