<?php


namespace app\components\interfaces;


use app\models\Domain;

interface UrlManagerMultiDomain
{

    /**
     * @return Domain|null
     */
    public function getDomain();

}
