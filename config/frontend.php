<?php

$config = [
    'modules' => [
        'ajax' => [
            'class' => 'app\modules\ajax\Module',
        ],
    ],
    'components' => [
        'user' => [
            'loginUrl' => ['account/login'],
        ],
        'urlManager' => [
            'class' => 'app\components\UrlManager',
            'rules' => [
                '/robots.txt' => 'site/robots-txt',
                '/sitemap.xml' => 'site/sitemap-xml',
                ['class' => 'app\components\url_rules\DefaultUrlRule'],
                [
                    'routes' => [
                        '' => 'catalog/list',
                        '<category_outer_code:[\w-]+>' => 'catalog/catalog-section',
                        '<category_outer_code:[\w-]+>/<alias:[\w-]+>' => 'catalog/good',
                    ],
                ],
                [
                    'routes' => [
                        '' => 'account/order-list',
                        '<number:[\d]+>' => 'account/order-view',
                    ],
                ],
            ]
        ],
        'view' => [
            'class' => 'app\components\View',

            'minifyOutput' => true,
            'minifyPath' => '@webroot/assets/minify',
            'forceCharset' => 'UTF-8',
            'compressOptions' => ['extra' => true, 'no-comments' => true],

        ],
    ],
];

if (YII_ENV === 'dev') {
    $config['components']['view']['enableMinify'] = false;
    $config['components']['assetManager']['appendTimestamp'] = true;
}

return $config;
