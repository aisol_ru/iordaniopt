<?php

namespace app\models;

use app\components\db\ActiveRecordFiles;
use app\components\traits\DomainModel;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * @property integer $id
 * @property integer $domain_id
 * @property string $name
 * @property string $image
 * @property string $link
 *
 * @property integer $sort
 * @property integer $status
 *
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $created_at
 * @property integer $updated_at
 */
class Event extends ActiveRecordFiles
{
    use DomainModel;

    public $imageFields = [
        'image' => ['events', 'event'],
    ];

    public static function tableName()
    {
        return '{{%event}}';
    }

    public function rules()
    {
        return [
            ['name', 'required', 'message' => 'Обязательное поле'],
            ['link', 'default', 'value' => ''],
            [
                '!image', 'file',
                'extensions' => ['png', 'jpg', 'gif'], 'wrongExtension' => 'Доступные форматы: {extensions}',
            ],
            ['sort', 'default', 'value' => 0],
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => array_keys(self::getStatuses())],
        ];
    }

    public function scenarios()
    {
        $defaultScenario = parent::scenarios()[self::SCENARIO_DEFAULT];
        return [
            self::SCENARIO_DEFAULT => $defaultScenario
        ];
    }

    public function behaviors()
    {
        return [
            'user' => BlameableBehavior::class,
            'timestamp' => TimestampBehavior::class,
        ];
    }

}
