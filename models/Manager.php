<?php

namespace app\models;

use app\components\db\ActiveRecord;
use app\components\traits\ActiveJSON;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * @property integer $id
 * @property string $name
 *
 * @property string $json
 *
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $created_at
 * @property integer $updated_at
 */
class Manager extends ActiveRecord
{
    use ActiveJSON;

    public static function tableName()
    {
        return '{{%manager}}';
    }

    public function rules()
    {
        return [
            ['name', 'required', 'message' => 'Обязательное поле'],
            ['jsonData', 'safe'],
        ];
    }

    public function scenarios()
    {
        $defaultScenario = parent::scenarios()[self::SCENARIO_DEFAULT];
        return [
            self::SCENARIO_DEFAULT => $defaultScenario
        ];
    }

    public function behaviors()
    {
        return [
            'user' => BlameableBehavior::class,
            'timestamp' => TimestampBehavior::class,
        ];
    }

}
