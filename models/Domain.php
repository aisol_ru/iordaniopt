<?php

namespace app\models;

use app\components\db\ActiveRecordFiles;
use app\components\traits\ActiveJSON;
use app\models\catalog\Brand;
use app\models\parameter\Parameter;
use app\models\structure\Tree;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;

/**
 * @property integer $id
 * @property string $name
 * @property string $domain
 * @property integer $brand_id
 * @property string $logo_preloader
 * @property string $logo_header
 * @property string $logo_footer
 * @property string $robots
 * @property string $sitemap
 * @property string $favicon
 * @property integer $is_premium
 * @property string $json
 *
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $created_at
 * @property integer $updated_at
 */
class Domain extends ActiveRecordFiles
{
    use ActiveJSON;

    const DOMAIN_DIAMOND_UNION = 1;
    const DOMAIN_ALVADA = 2;

    public $imageFields = [
        'logo_preloader' => ['domain/logos/preloader', 'logo'],
        'logo_header' => ['domain/logos/header', 'logo'],
        'logo_footer' => ['domain/logos/footer', 'logo'],
        'robots' => ['domain/seo', 'robots'],
        'sitemap' => ['domain/seo', 'sitemap'],
        'favicon' => ['domain/seo', 'favicon'],
    ];

    public static function tableName()
    {
        return '{{%domain}}';
    }

    public function rules()
    {
        return [
            ['name', 'required', 'message' => 'Обязательное поле'],
            ['domain', 'required', 'message' => 'Обязательное поле'],
            ['domain', 'unique', 'targetClass' => self::class, 'targetAttribute' => 'domain', 'message' => 'Такой домен уже создан'],
            ['brand_id', 'exist', 'targetClass' => Brand::class, 'targetAttribute' => 'id', 'skipOnEmpty' => true, 'isEmpty' => function($value) {
                return $value == false;
            }],
            [
                '!logo_preloader', 'file',
                'extensions' => ['png', 'jpg', 'gif', 'svg'], 'wrongExtension' => 'Доступные форматы: {extensions}',
            ],
            [
                '!logo_header', 'file',
                'extensions' => ['png', 'jpg', 'gif', 'svg'], 'wrongExtension' => 'Доступные форматы: {extensions}',
            ],
            [
                '!logo_footer', 'file',
                'extensions' => ['png', 'jpg', 'gif', 'svg'], 'wrongExtension' => 'Доступные форматы: {extensions}',
            ],
            [
                '!robots', 'file',
                'extensions' => ['txt'], 'wrongExtension' => 'Доступные форматы: {extensions}',
            ],
            [
                '!sitemap', 'file',
                'extensions' => ['xml'], 'wrongExtension' => 'Доступные форматы: {extensions}',
            ],
            [
                '!favicon', 'image',
                'minWidth' => 192,
                'minHeight' => 192,
                'extensions' => ['png'], 'wrongExtension' => 'Доступные форматы: {extensions}',
            ],
            ['is_premium', 'default', 'value' => 0],
            ['jsonData', 'safe'],
        ];
    }

    public function scenarios()
    {
        $defaultScenario = parent::scenarios()[self::SCENARIO_DEFAULT];
        return [
            self::SCENARIO_DEFAULT => $defaultScenario
        ];
    }

    public function behaviors()
    {
        return [
            'user' => BlameableBehavior::class,
            'timestamp' => TimestampBehavior::class,
        ];
    }

    public function checkReferences($checkDomainId)
    {
        $mainPage = Tree::find('', false)
            ->andWhere(['name' => ''])
            ->andWhere(['domain_id' => $checkDomainId])
            ->one();
        $currentMainPage = Tree::find('', false)
            ->andWhere(['name' => ''])
            ->andWhere(['domain_id' => $this->id])
            ->one();
        if (is_null($currentMainPage) === true) {
            $currentMainPage = new Tree();
            $currentMainPage->setAttributes($mainPage->getAttributes(), false);
            $currentMainPage->domain_id = $this->id;
            $currentMainPage->id = null;
            $currentMainPage->save(false);
        }
        $modules = Module::find('', false)
            ->indexBy('route')
            ->andWhere(['domain_id' => $checkDomainId])
            ->all();
        $currentModules = Module::find('', false)
            ->andWhere(['domain_id' => $this->id])
            ->indexBy('route')
            ->all();

        $pages = Tree::find('', false)
            ->indexBy('id')
            ->andWhere(['domain_id' => $checkDomainId])
            ->andWhere(['id' => ArrayHelper::map($modules, 'tree_id', 'tree_id')])
            ->all();
        $currentPages = Tree::find('', false)
            ->indexBy('id')
            ->andWhere(['domain_id' => $this->id])
            ->andWhere(['id' => ArrayHelper::map($currentModules, 'tree_id', 'tree_id')])
            ->all();
        $newPages = [];
        foreach ($modules as $module) {
            if (isset($currentModules[$module->route]) === false) {
                $newModule = new Module();
                $newModule->domain_id = $this->id;
                $newModule->name = $module->name;
                $newModule->url = $module->url;
                $newModule->route = $module->route;
                $newModule->save(false);

                if (isset($pages[$module->tree_id]) === true) {
                    $newPage = new Tree();
                    $newPage->setAttributes($pages[$module->tree_id]->getAttributes(), false);
                    $newPage->domain_id = $this->id;
                    $newPage->id = null;
                    if ($newPage->pid == $mainPage->id) {
                        $newPage->pid = $currentMainPage->id;
                    }
                    $newPage->save(false);
                    $newPages[] = $newPage;

                    $newModule->tree_id = $newPage->id;
                    $newModule->save(false);
                }
            } else {
                if (isset($currentPages[$currentModules[$module->route]->tree_id]) === false) {
                    $newPage = new Tree();
                    $newPage->setAttributes($pages[$module->tree_id]->getAttributes(), false);
                    $newPage->domain_id = $this->id;
                    $newPage->id = null;
                    if ($newPage->pid == $mainPage->id) {
                        $newPage->pid = $currentMainPage->id;
                    }
                    $newPage->save(false);
                    $newPages[] = $newPage;

                    $currentModules[$module->route]->tree_id = $newPage->id;
                    $currentModules[$module->route]->save(false);
                }
            }
        }

        foreach ($newPages as $page) {
            if ($page->pid == $currentMainPage->id) {
                continue;
            }
            $domainPage = Tree::find('', false)
                ->andWhere(['domain_id' => $checkDomainId])
                ->andWhere(['id' => $page->pid])
                ->one();
            $currentPage = Tree::find('', false)
                ->andWhere(['domain_id' => $this->id])
                ->andWhere(['name' => $domainPage->name])
                ->one();
            if (is_null($currentPage) === true) {
                $currentPage = self::createdTree($domainPage, $this->id, $checkDomainId, $mainPage->id, $currentMainPage->id);
            }
            $page->pid = $currentPage->id;
            $page->save(false);
        }

        /* @var $parameters Parameter[] */
        $parameters = Parameter::find('', false)
            ->indexBy('group')
            ->andWhere(['domain_id' => $checkDomainId])
            ->all();
        /* @var $currentParameters Parameter[] */
        $currentParameters = Parameter::find('', false)
            ->indexBy('group')
            ->andWhere(['domain_id' => $this->id])
            ->all();

        foreach ($parameters as $parameter) {
            if (isset($currentParameters[$parameter->group]) === true) {
                continue;
            }
            $newParameter = new Parameter();
            $newParameter->setAttributes($parameter->getAttributes(), false);
            $newParameter->domain_id = $this->id;
            if ($newParameter->type == Parameter::TYPE_FILE || $newParameter->type == Parameter::TYPE_IMAGE) {
                $newParameter->value = '';
            }
            $newParameter->id = null;
            $newParameter->save(false);
        }

    }

    private static function createdTree($domainPage, $domainId, $checkDomainId, $mainPageId, $currentMainPageId)
    {
        $page = new Tree();
        $page->setAttributes($domainPage->getAttributes(), false);
        $page->id = null;
        $page->domain_id = $domainId;
        if ($page->pid == $mainPageId) {
            $page->pid = $currentMainPageId;
        } else {
            $domainPage = Tree::find('', false)
                ->andWhere(['domain_id' => $checkDomainId])
                ->andWhere(['id' => $page->pid])
                ->one();
            $currentPage = self::createdTree($domainPage, $domainId, $checkDomainId, $mainPageId, $currentMainPageId);
            $page->pid = $currentPage->id;
        }
        $page->save(false);
        return $page;
    }

}
