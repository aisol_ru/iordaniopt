<?php

namespace app\models\user;

use app\components\db\ActiveQuery;
use app\components\db\ActiveRecordFiles;
use app\components\IcmsHelper;
use app\components\interfaces\UrlManagerMultiDomain;
use app\models\Manager;
use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\web\IdentityInterface;
use yii\helpers\ArrayHelper;

/**
 * @property integer $id
 * @property string $login
 * @property string $first_name
 * @property string $last_name
 * @property string $middle_name
 * @property string $phone
 * @property string $email
 * @property string $organization
 * @property string $inn
 * @property string $ogrn
 * @property string $city
 * @property string $certificate
 * @property string $document
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property integer $status
 *
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Manager[] $managers
 */
class User extends ActiveRecordFiles implements IdentityInterface
{

    public $fileFields = [
        'document' => ['certificate', self::NAME_ORIGINAL_ID],
    ];

    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 10;
    const DEFAULT_ROLE = 'user';

    public $role;
    public $password;
    public $password_repeat;

    public function scenarios()
    {
        $defaultScenario = parent::scenarios()[self::SCENARIO_DEFAULT];
        return [
            self::SCENARIO_DEFAULT => $defaultScenario,
            'filter' => ['login', 'email', 'last_name', 'first_name', 'middle_name', 'phone'],
            'edit' => [
                'login', 'email', 'phone', 'name',
                'status', 'role', 'last_name', 'first_name',
                'middle_name', 'phone', 'email', 'organization',
                'inn', 'ogrn', 'city', 'certificate',
            ],
            'editPassword' => ['password', 'password_repeat'],
            'lostPassword' => ['password'],
        ];
    }

    public function rules()
    {
        return [
            ['login', 'required', 'message' => 'Заполните поле Login', 'on' => ['default', 'edit']],
            ['login', 'unique', 'message' => 'Пользователь с таким логином уже зарегистрирован', 'on' => ['default', 'edit']],

            ['last_name', 'required', 'message' => 'Обязательное поле', 'on' => ['default', 'edit']],
            ['first_name', 'required', 'message' => 'Обязательное поле', 'on' => ['default', 'edit']],
            ['middle_name', 'default', 'value' => '', 'on' => ['default', 'edit']],

            ['phone', 'required', 'message' => 'Обязательное поле', 'on' => ['default', 'edit']],
            ['phone', 'filter', 'filter' => function($value) {
                return IcmsHelper::clearPhone(trim($value));
            }, 'on' => ['default', 'edit']],
            ['phone', 'unique', 'message' => 'Пользователь с таким телефоном уже зарегистрирован', 'on' => ['default', 'edit']],
            ['email', 'email', 'message' => 'Не является правильным E-Mail адресом', 'on' => ['default', 'edit']],
            ['email', 'required', 'message' => 'Заполните поле E-Mail', 'on' => ['default', 'edit']],

            ['organization', 'required', 'message' => 'Обязательное поле', 'on' => ['default', 'edit']],
            ['inn', 'required', 'message' => 'Обязательное поле', 'on' => ['default', 'edit']],
            ['ogrn', 'required', 'message' => 'Обязательное поле', 'on' => ['default', 'edit']],
            ['city', 'required', 'message' => 'Обязательное поле', 'on' => ['default', 'edit']],
            ['certificate', 'required', 'message' => 'Обязательное поле', 'on' => ['default', 'edit']],
            [
                '!document', 'file',
                'extensions' => ['doc', 'docx', 'pdf', 'jpg', 'png'], 'wrongExtension' => 'Доступные форматы: {extensions}',
            ],

            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DELETED], 'message' => 'Выберите правильное значение'],
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['role', 'default', 'value' => self::DEFAULT_ROLE],
            ['password', 'required', 'message' => 'Введите пароль'],
            ['password_repeat', 'required', 'message' => 'Повторите пароль'],
            ['password_repeat', 'compare', 'compareAttribute' => 'password', 'message' => 'Пароли не совпадают'],
        ];
    }

    static function getStatuses()
    {
        return [
            self::STATUS_ACTIVE => 'Включен',
            self::STATUS_DELETED => 'Отключен (требует проверки)'
        ];
    }

    static function getRolesAsArray()
    {
        $roles = ArrayHelper::map(Yii::$app->authManager->roles, 'name', 'description');
        if (Yii::$app->user->can('developer') === false) {
            unset($roles['developer']);
        }
        return $roles;
    }

    public static function getFirstRoleName($id)
    {
        $roles = Yii::$app->getAuthManager()->getRolesByUser($id);
        unset($roles['guest']);

        $role = array_pop($roles);

        if (is_null($role) === true) {
            return self::DEFAULT_ROLE;
        }

        return $role->name;
    }

    public static function tableName()
    {
        return '{{%user}}';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }

    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Поиск пользователя по логину
     * @param string $login - логин
     * @return /app/models/User|null - модель пользователя или null, если пользователя с таким логином не найдено
     */
    public static function findByLogin($login)
    {
        return static::findOne(['login' => $login, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Поиск пользователя по телефону
     * @param string $phone - телефон
     * @return self|null - модель пользователя или null, если пользователт с таким телефоном не найден
     */
    public static function findByPhone($phone)
    {
        return static::findOne(['phone' => $phone, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Поиск пользователя по токену сброса пароля
     * @param string $token токен для сброса пароля
     * @return /app/models/User|null - модель пользователя или null, если пользователя с таким токеном не существует
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Проверка токена на валидность
     * @param string $token токен для сброса пароля
     * @return boolean
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        $parts = explode('_', $token);
        $timestamp = (int)end($parts);
        return $timestamp + $expire >= time();
    }

    public function getId()
    {
        return $this->getPrimaryKey();
    }

    public function getAuthKey()
    {
        return $this->auth_key;
    }

    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Проверка пароля для текущего пользователя
     * @param string $password пароль
     * @return boolean
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Хеширование пароля и установка
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Генерация кода аутентификации для галочки "Запомнить"
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Генерация нового токена сброса пароля
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Удаление токена сброса пароля
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    public function getRoles()
    {
        $auth = Yii::$app->authManager;
        $roles = $auth->getRolesByUser($this->id);

        if (count($roles) > 0) {
            $rolesArr = [];
            foreach ($roles as $role) {
                $rolesArr[] = $role;
            }
            return $rolesArr;
        } else {
            return null;
        }
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        $auth = Yii::$app->authManager;

        $oldRole = $this->getFirstRoleName($this->id);

        if (($this->role !== $oldRole || $insert === true) && empty($this->role) === false) {
            $auth->revokeAll($this->id);

            $role = $auth->getRole($this->role);
            $auth->assign($role, $this->id);
        }

    }

    public function beforeSave($insert)
    {
        if (empty($this->password) === false) {
            $this->setPassword($this->password);
        }
        return parent::beforeSave($insert);
    }

    public function beforeDelete()
    {
        if (Yii::$app->user->can($this->role) === false) {
            return false;
        }
        Yii::$app->authManager->revokeAll($this->id);
        return parent::beforeDelete();
    }

    public function afterFind()
    {
        parent::afterFind();
        $this->role = self::getFirstRoleName($this->id);
    }

    public function getManagers($domainId = false)
    {
        if ($domainId === true) {
            /* @var $urlManager UrlManagerMultiDomain */
            $urlManager = \Yii::$app->getUrlManager();
            $domain = $urlManager->getDomain();
            $domainId = $domain->id;
        }
        $callback = null;
        if ($domainId !== false) {
            $callback = function ($query) use ($domainId) {
                /* @var $query ActiveQuery */
                $query->andWhere(['domain_id' => $domainId]);
            };
        }
        return $this->hasMany(Manager::class, ['id' => 'manager_id'])
            ->viaTable('{{%manager_to_user}}', ['user_id' => 'id'], $callback);
    }

}
