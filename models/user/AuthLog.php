<?php

namespace app\models\user;

use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

class AuthLog extends ActiveRecord
{

    public function rules()
    {
        return [
            [['user_id', 'name', 'ip'], 'safe']
        ];
    }

    public static function tableName()
    {
        return '{{%user_log}}';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }

    static function addLog($forDeveloper = false)
    {
        $log = new self();
        $user = \Yii::$app->user->identity;
        $log->user_id = $user->id;
        if (empty($user->first_name)) {
            $log->name = $user->login;
        } else {
            $log->name = $user->first_name;
        }
        $log->ip = \Yii::$app->request->getUserIp();
        $log->developer_only = $forDeveloper;
        $log->save();
    }

}
