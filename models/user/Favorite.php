<?php

namespace app\models\user;

use app\components\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use Yii;


/**
 * @property integer $id
 * @property integer $user_id
 * @property string $session
 * @property integer $link_id
 *
 * @property integer $created_at
 * @property integer $updated_at
 */
class Favorite extends ActiveRecord
{

    public static function tableName()
    {
        return '{{%user_favorite}}';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }

    public static function checkExists($id)
    {
        return is_null(self::search($id)) ===  false;
    }

    /**
     * @param integer $id
     * @return self
     */
    public static function search(int $id)
    {
        $query = self::find();
        if (Yii::$app->getUser()->isGuest === true) {
            $query->andWhere(['session' => Yii::$app->getSession()->getId()]);
        } else {
            $query->andWhere(['user_id' => Yii::$app->getUser()->getId()]);
        }

        $query->andWhere([
            'link_id' => $id,
        ]);

        return $query->one();
    }

    public static function add($id)
    {
        if (self::checkExists($id) === true) {
            return true;
        }

        $new = new self();
        $new->link_id = $id;

        if (Yii::$app->getUser()->isGuest === false) {
            $new->user_id = Yii::$app->getUser()->id;
        }

        $new->session = Yii::$app->getSession()->getId();

        return $new->save();
    }

    public static function remove($id)
    {
        $favorite = self::search($id);
        if (is_null($favorite) === true) {
            return true;
        }

        return $favorite->delete() !== false;
    }

    public static function flush()
    {
        if (Yii::$app->getUser()->isGuest === true) {
            return self::deleteAll(['session' => Yii::$app->getSession()->getId()]);
        } else {
            return self::deleteAll(['user_id' => Yii::$app->getUser()->getId()]);
        }
    }

    public static function getList($limit = false, $exceptId = false)
    {
        $query = self::find();
        if (Yii::$app->getUser()->isGuest === true) {
            $query->andWhere(['session' => Yii::$app->getSession()->getId()]);
        } else {
            $query->andWhere(['user_id' => Yii::$app->getUser()->getId()]);
        }

        if ($limit !== false) {
            $query->limit($limit);
        }

        if ($exceptId !== false) {
            $view = self::search($exceptId);
            if (is_null($view) === false) {
                $query->andWhere(['!=', 'id', $view->id]);
            }
        }

        $query->orderBy(['updated_at' => SORT_DESC]);

        return $query->all();
    }

    public static function getCount()
    {
        $query = self::find();
        if (Yii::$app->getUser()->isGuest === true) {
            $query->andWhere(['session' => Yii::$app->getSession()->getId()]);
        } else {
            $query->andWhere(['user_id' => Yii::$app->getUser()->getId()]);
        }

        return (int) $query->count() ?: 0;
    }

}