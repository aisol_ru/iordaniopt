<?php

namespace app\models\slider;

use app\components\db\ActiveRecordFiles;
use app\components\traits\DomainModel;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * @property integer $id
 * @property integer $slider_id
 * @property string $name
 * @property string $link
 * @property string $image
 * @property integer $sort
 * @property integer $status
 *
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Slider $slider
 */
class Slide extends ActiveRecordFiles
{
    use DomainModel;

    const SLIDER_MAIN = 1;
    const SLIDER_ACTION = 2;

    public $imageFields = [
        'image' => ['sliders', 'slider'],
    ];

    public function rules()
    {
        return [
            ['slider_id', 'exist', 'targetClass' => Slider::class, 'targetAttribute' => 'id'],
            ['name', 'required', 'message' => 'Обязательное поле'],
            ['link', 'default', 'value' => ''],
            [
                '!image', 'file',
                'extensions' => ['png', 'jpg', 'gif'], 'wrongExtension' => 'Доступные форматы: {extensions}',
            ],
            ['sort', 'default', 'value' => 0],
            ['sort', 'integer', 'message' => 'Введите целое число'],
            ['status', 'in', 'range' => [1, 2], 'message' => 'Выберите правильное значение', 'except' => 'filter'],
            ['status', 'default', 'value' => self::STATUS_ACTIVE, 'except' => 'filter'],
        ];
    }

    public function scenarios()
    {
        $defaultScenario = parent::scenarios()[self::SCENARIO_DEFAULT];
        return [
            self::SCENARIO_DEFAULT => $defaultScenario,
            'filter' => ['name', 'status'],
        ];
    }

    public function getSlider()
    {
        return $this->hasOne(Slider::class, ['id' => 'slider_id']);
    }

    public static function tableName()
    {
        return '{{%slide}}';
    }

    public function behaviors()
    {
        return [
            'user' => BlameableBehavior::class,
            'timestamp' => TimestampBehavior::class,
        ];
    }

}
