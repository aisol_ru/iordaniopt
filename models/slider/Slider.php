<?php

namespace app\models\slider;

use app\components\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;


/**
 * @property integer $id
 * @property string $name
 *
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Slide[] $slides
 */
class Slider extends ActiveRecord
{

    public function rules()
    {
        return [
            ['name', 'required', 'message' => 'Обязательное поле', 'on' => ['default']]
        ];
    }

    public function getSlides()
    {
        return $this->hasMany(Slide::class, ['slider_id' => 'id']);
    }

    public function scenarios()
    {
        $defaultScenario = parent::scenarios()[self::SCENARIO_DEFAULT];
        return [
            self::SCENARIO_DEFAULT => $defaultScenario,
            'filter' => ['name'],
        ];
    }

    public static function tableName()
    {
        return '{{%slider}}';
    }

    public function behaviors()
    {
        return [
            'user' => BlameableBehavior::class,
            'timestamp' => TimestampBehavior::class,
        ];
    }

    public function beforeDelete()
    {
        foreach ($this->banners as $banner) {
            $banner->delete();
        }
        return parent::beforeDelete();
    }

}
