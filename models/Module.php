<?php

namespace app\models;

use app\components\db\ActiveRecord;
use app\components\traits\DomainModel;
use app\models\structure\Tree;
use yii\helpers\ArrayHelper;

/**
 * @property integer $id
 * @property integer $domain_id
 * @property string $name
 * @property string $url
 * @property string $route
 * @property integer $tree_id
 */
class Module extends ActiveRecord
{
    use DomainModel;

    const DEFAULT_MODULE = 0;

    public function scenarios()
    {
        $defaultScenario = parent::scenarios()[self::SCENARIO_DEFAULT];
        return [
            self::SCENARIO_DEFAULT => $defaultScenario
        ];
    }

    public function rules()
    {
        return [
            ['name', 'required', 'message' => 'Введите название', 'on' => self::SCENARIO_DEFAULT],
            ['route', 'required', 'message' => 'Введите роут', 'when' => function ($model) {
                return $model->name !== 'Ajax';
            }],
            [['tree_id', 'url', 'route', 'name'], 'safe']
        ];
    }

    public static function tableName()
    {
        return '{{%module}}';
    }

    public function getPage()
    {
        return $this->hasOne(Tree::class, ['id' => 'tree_id']);
    }

    static function getNamesAsArray($fieldName = 'name', $sort = ['name' => SORT_ASC])
    {
        $primaryKeys = self::primaryKey();
        $primaryKey = array_shift($primaryKeys);
        $query = self::find()->andWhere(['!=', 'route', '']);
        if ($sort === false) {
            $query->orderBy($sort);
        }
        return [0 => 'Текстовая страница'] + ArrayHelper::map($query->all(), $primaryKey, $fieldName);
    }

    /**
     * Получение url'а по роуту из модулей
     * @param string $route роут
     * @param boolean $scheme абсолютный ли путь
     * @param array $parameters get параметры
     * @return string
     * @throws \yii\web\ServerErrorHttpException
     */
    static function getUrlByRoute($route, $scheme = false, $parameters = [])
    {
        $module = self::find()
            ->andWhere(['route' => $route])
            ->one();
        if (is_null($module) === true) {
            throw new \yii\web\ServerErrorHttpException('Не найден модуль по роуту ' . $route);
        }

        $url = '/' . preg_replace('/(\/$)/', '', $module->url);

        if (empty($parameters) === false) {
            $hash = ArrayHelper::remove($parameters, '#', false);

            if (empty($parameters) === false) {
                $url .= '?' . http_build_query($parameters);
            }

            if ($hash !== false) {
                $url .= '#' . $hash;
            }
        }
        return \yii\helpers\Url::to($url, $scheme);
    }

}
