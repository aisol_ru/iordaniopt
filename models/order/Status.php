<?php

namespace app\models\order;

use app\components\db\ActiveRecord;
use app\components\traits\ActiveJSON;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;


/**
 * @property integer $id
 * @property integer $order_id
 * @property integer $status
 * @property string $json
 *
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $created_at
 * @property integer $updated_at
 */
class Status extends ActiveRecord
{

    use ActiveJSON;


    public static function tableName()
    {
        return '{{%order_status}}';
    }

    public function behaviors()
    {
        return [
            'user' => BlameableBehavior::class,
            'timestamp' => TimestampBehavior::class,
        ];
    }

    public static function add($orderId, $status, $additional = [])
    {
        $new = new self();
        $new->order_id = $orderId;
        $new->status = $status;
        $new->json = json_encode($additional);
        $new->save();
        return $new;
    }

}
