<?php

namespace app\models\order;

use app\components\db\ActiveRecordFiles;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;


/**
 * @property integer $id
 * @property integer $order_id
 * @property integer $file
 * @property integer $status
 *
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $created_at
 * @property integer $updated_at
 * @property Order $order
 */
class ImageArchive extends ActiveRecordFiles
{

    const STATUS_GENERATED = 1;
    const STATUS_COMPLETED = 2;

    public $fileFields = [
        'file' => ['orders/archive', self::NAME_RANDOM],
    ];

    public static function tableName()
    {
        return '{{%order_archive}}';
    }

    public function getOrder()
    {
        return $this->hasOne(Order::class, ['id' => 'order_id']);
    }

    public function behaviors()
    {
        return [
            'user' => BlameableBehavior::class,
            'timestamp' => TimestampBehavior::class,
        ];
    }

}
