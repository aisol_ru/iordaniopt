<?php

namespace app\models\order;

use app\components\db\ActiveRecord;
use app\components\IcmsHelper;
use app\components\traits\ActiveJSON;
use app\models\catalog\Metal;
use app\models\catalog\Price;
use app\models\catalog\Size;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\web\ServerErrorHttpException;


/**
 * @property integer $id
 * @property integer $order_id
 * @property integer $good_id
 * @property integer $price_id
 * @property float $count
 * @property float $weight
 * @property float $sum
 * @property string $comment
 * @property string $good_insert
 * @property string $good_coating
 * @property string $json
 *
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Order $order
 * @property \app\models\catalog\Good $good
 */
class Good extends ActiveRecord
{

    use ActiveJSON;

    public static function tableName()
    {
        return '{{%order_good}}';
    }

    public function behaviors()
    {
        return [
            'user' => BlameableBehavior::class,
            'timestamp' => TimestampBehavior::class,
        ];
    }

    public function getOrder()
    {
        return $this->hasOne(Order::class, ['id' => 'order_id']);
    }

    public function getGood()
    {
        return $this->hasOne(\app\models\catalog\Good::class, ['id' => 'good_id']);
    }

    public function getCoating()
    {
        $size = Size::find()->andWhere(["coating_id" => $this->good_coating])->one();
        return $size->coating ?? null;
    }

    public function getInsert()
    {
        $size = Size::find()->andWhere(["insert_id" => $this->good_insert])->one();
        return $size->size_insert ?? null;
    }


    /**
     * @param integer $orderId идентификатор заказа
     * @param integer $goodId идентификатор товара
     * @param array|integer $sizesOrCount массив размеров [['size' => 15, 'count' => 2], ...] или количество
     * @param string $comment
     * @return self|false
     */
    public static function add($orderId, $goodId, $insertId, $coatingId, $sizesOrCount, $comment = "")
    {
        return self::edit($orderId, $goodId, $insertId, $coatingId, $sizesOrCount, $comment);
    }


    /**
     * @param integer $orderId идентификатор заказа
     * @param integer $goodId идентификатор товара
     * @param array|integer $sizesOrCount массив размеров [['size' => 15, 'count' => 2], ...] или количество
     * @param string $comment
     * @return self|false
     * @throws ServerErrorHttpException
     */
    public static function edit(int $orderId, int $goodId, $insertId, $coatingId, $sizesOrCount, $comment = "")
    {
        $insertId = $insertId ?? "";
        $coatingId = $coatingId ?? "";

        $good = self::find()
            ->andWhere([
                'order_id' => $orderId,
                'good_insert' => $insertId,
                'good_coating' => $coatingId,
                'good_id' => $goodId
            ])
            ->one();
        if (is_null($good) === true) {
            $good = new self();
            $good->order_id = $orderId;
            $good->good_id = $goodId;
            $good->good_insert = $insertId;
            $good->good_coating = $coatingId;
            $good->count = 0;
        }
        $good->comment = $comment;

        /* @var $catalogGood \app\models\catalog\Good */
        $catalogGood = $good->getGood()->one();
        if (is_null($catalogGood) === true) {
            throw new ServerErrorHttpException();
        }

        $good->jsonSet('name', $catalogGood->name);
        $good->jsonSet('attributes', $catalogGood->getAttributes());
        $price = $catalogGood->price;
        if (is_null($price) === true) {
            throw new ServerErrorHttpException('Price not found');
        }
        $good->jsonSet('weight', $catalogGood->weight_average);
        $good->jsonSet('price_unit', $price);

        $validSizes = $catalogGood->getSizesFromTable()
            ->andWhere(["insert_id" => $insertId])
            ->andWhere(["coating_id" => $coatingId])
            ->isActive()
            ->all();

        if (empty($validSizes) === false && is_array($sizesOrCount) === false) {
            throw new ServerErrorHttpException('Add to good without sizes');
        }
        if (empty($validSizes) === true && is_array($sizesOrCount) === true) {
            throw new ServerErrorHttpException('Add to good with sizes');
        }

        if (empty($validSizes) === false && is_array($sizesOrCount) === true) { // Товар с размерами
            $currentSizes = $good->jsonGet('sizes', []);
            $count = 0;
            $sum = 0;
            $sizes = [];
            $sizesWeight = 0;

            foreach ($validSizes as $size) {

                $newSize = IcmsHelper::arrayFind($sizesOrCount, function ($element) use ($size) {
                    return $element['size'] === (string)$size->size;
                });
                $currentSize = IcmsHelper::arrayFind($currentSizes, function ($element) use ($size) {
                    return $element['size'] === (string)$size->size;
                });

                if ($newSize === false && $currentSizes === false) {
                    $addSize = ['size' => (string)$size, 'count' => 0, 'sum' => 0, 'weight' => 0];
                } elseif ($newSize === false && $currentSize !== false) {
                    $addSize = $currentSize;
                } else {
                    $addSize = [
                        'size' => (string)$size->size,
                        'count' => $newSize["count"],
                        'sum' => $newSize["count"] * (double)$size->price,
                        'weight' => (double)$size->weight,
                    ];
                }
                $sum += $addSize['sum'];
                $count += $addSize['count'];
                $sizesWeight += $addSize["weight"] * $addSize["count"];
                $sizes[] = $addSize;

            }

            usort($sizes, function ($a, $b) {
                return (float)$a['size'] <=> (float)$b['size'];
            });

            $good->jsonSet('sizes', $sizes);


            $good->weight = $sizesWeight;
            $good->count = $count;
            $good->sum = $sum;
        } elseif (empty($validSizes) === true && is_array($sizesOrCount) === false) { // Товар без размеров
            $good->count = $sizesOrCount;
            $good->weight = $good->count * $catalogGood->weight_average;
            $good->sum = $good->count * $price;
        } else {
            throw new ServerErrorHttpException();
        }

        if ($good->save() === true) {
            return $good;
        }

        return false;
    }

    public static function remove($orderId, $goodId, $insertId, $coatingId)
    {
        $good = self::find()
            ->andWhere(['order_id' => $orderId, 'good_insert' => $insertId, 'good_coating' => $coatingId, 'good_id' => $goodId])
            ->one();
        if (is_null($good) === true) {
            return true;
        }
        $good->delete();
        return true;
    }

    public function getMetalName()
    {
        if (empty($this->jsonData['attributes']['metal_id']) === true || $this->jsonData['attributes']['metal_id'] == 0) {
            return '';
        }
        $metal = Metal::find()
            ->andWhere(['id' => $this->jsonData['attributes']['metal_id']])
            ->one();
        if (is_null($metal) === true) {
            return '';
        }

        return $metal->name;
    }

    public function canRepeat()
    {
        $checkGood = \app\models\catalog\Good::find()
                ->andWhere(['id' => $this->good_id])
                ->isActive()
                ->count() > 0;
        if ($checkGood === false) {
            return false;
        }

        $checkPrice = Price::find()
                ->andWhere(['id' => $this->price_id])
//            или
//            ->andWhere(['sample' => $this->jsonData['sample']])
                ->andWhere(['good_id' => $this->good_id])
                ->count() > 0;
        if ($checkPrice === false) {
            return false;
        }


        return true;
    }

}
