<?php

namespace app\models\order;

use app\components\AmoApi;
use app\components\db\ActiveRecord;
use app\components\interfaces\UrlManagerMultiDomain;
use app\components\Mailer;
use app\components\settings\Settings;
use app\components\traits\DomainModel;
use app\models\Manager;
use app\models\user\User;
use PhpOffice\PhpSpreadsheet\Cell\Cell;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Color;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\web\ServerErrorHttpException;
use ZipArchive;


/**
 * @property integer $id
 * @property integer $domain_id
 * @property integer $number
 * @property string $hash
 * @property integer $user_id
 * @property integer $status
 * @property integer $good_count
 * @property float $good_weight
 * @property float $good_sum
 * @property integer $sum
 *
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Good[] $goods
 * @property Client $client
 * @property Status[] $historyStatuses
 * @property ImageArchive $imageArchive
 */
class Order extends ActiveRecord
{

    use DomainModel;

    const STATUS_NOT_COMPLETED = 0;
    const STATUS_NEW = 1;
    const STATUS_PROCESSED = 2;
    const STATUS_COMPLETED = 3;
    const STATUS_CANCELED = 4;
    const STATUS_REMOVED = 5;


    public function rules()
    {
        return [
            ['status', 'in', 'range' => array_merge(array_keys(self::getStatuses()), [self::STATUS_NOT_COMPLETED])],
        ];
    }

    public function scenarios()
    {
        $defaultScenario = parent::scenarios()[self::SCENARIO_DEFAULT];
        return [
            self::SCENARIO_DEFAULT => $defaultScenario,
            'filter' => ['number'],
        ];
    }

    public static function tableName()
    {
        return '{{%order}}';
    }

    public function behaviors()
    {
        return [
            'user' => BlameableBehavior::class,
            'timestamp' => TimestampBehavior::class,
        ];
    }

    public static function getStatuses($all = false)
    {
        $statuses = [
            self::STATUS_NEW => 'Новый',
            self::STATUS_PROCESSED => 'Принят в обработку',
            self::STATUS_COMPLETED => 'Выполнен',
            self::STATUS_CANCELED => 'Отменен',
            self::STATUS_REMOVED => 'Удален',
        ];
        if ($all === true) {
            $statuses[self::STATUS_NOT_COMPLETED] = 'Создан, но не оформлен';
        }
        return $statuses;
    }

    public function getGoods()
    {
        return $this->hasMany(Good::class, ['order_id' => 'id']);
    }

    public function getClient()
    {
        return $this->hasOne(Client::class, ['order_id' => 'id']);
    }

    public function getHistoryStatuses()
    {
        return $this->hasMany(Status::class, ['order_id' => 'id']);
    }

    public function getImageArchive()
    {
        return $this->hasOne(ImageArchive::class, ['order_id' => 'id']);
    }

    /**
     * @param null|int $domainId
     * @return self
     */
    public static function create($domainId = null)
    {
        if (is_null($domainId) === true) {
            /* @var $urlManager UrlManagerMultiDomain */
            $urlManager = \Yii::$app->getUrlManager();
            $domainId = $urlManager->getDomain()->id;
        }
        $new = new self();
        $new->domain_id = $domainId;
        $new->status = self::STATUS_NOT_COMPLETED;
        $new->user_id = Yii::$app->getUser()->getId();
        $new->save(false);
        return $new;
    }

    /**
     * @param boolean $create
     * @param null|int $domainId
     * @return self|null
     */
    public static function getCurrent($create = false, $domainId = null)
    {
        if (is_null($domainId) === true) {
            /* @var $urlManager UrlManagerMultiDomain */
            $urlManager = \Yii::$app->getUrlManager();
            $domainId = $urlManager->getDomain()->id;
        }
        $query = self::find('', false)
            ->andWhere(['domain_id' => $domainId])
            ->orderBy(['updated_at' => SORT_DESC]);
        $query->andWhere(['user_id' => Yii::$app->getUser()->getId()]);
        $query->andWhere(['status' => self::STATUS_NOT_COMPLETED]);

        $order = $query->one();
        if (is_null($order) === true && $create === true) {
            $order = self::create($domainId);
        }

        return $order;
    }

    public static function flush($orderId)
    {
        $order = self::find('', false)->andWhere(['id' => $orderId])->one();
        foreach ($order->goods as $good) {
            Good::remove($order->id, $good->good_id, $good->price_id);
        }
    }

    public function recalculation()
    {
        $this->sum = $this->good_sum = $this->good_count = $this->good_weight = 0;
        foreach ($this->getGoods()->all() as $good) {
            $this->good_count += $good->count;
            $this->good_sum += $good->sum;
            $this->sum += $good->sum;
            $this->good_weight += $good->weight;
        }
    }


    public function send()
    {
        if ($this->status != self::STATUS_NOT_COMPLETED) {
            throw new ServerErrorHttpException();
        }
        $this->status = self::STATUS_NEW;
        $this->number = $this->getNextOrderNumber();
        $this->hash = $this->generateHash();
    }

    public function sendNewOrderEmailAdmin(array $attachs)
    {

        $client = $this->client;

        /* @var $orderedStatus Status */
        $orderedStatus = $this->getHistoryStatuses()
            ->andWhere(['status' => self::STATUS_NEW])
            ->one();
        $orderedDate = date('d.m.Y H:i:s', $orderedStatus->created_at);

        $domain = $this->domain;
        return Mailer::send(null, 'Клиент сделал заказ на сайте ' . $domain->name, '', $attachs, true, 'views/order-admin', [
            'domain' => $domain,
            'order' => $this,
            'client' => $client,
            'orderedDate' => $orderedDate,
        ]);
    }

    public function sendNewOrderEmailManager()
    {

        $client = $this->client;

        /* @var $orderedStatus Status */
        $orderedStatus = $this->getHistoryStatuses()
            ->andWhere(['status' => self::STATUS_NEW])
            ->one();
        $orderedDate = date('d.m.Y H:i:s', $orderedStatus->created_at);

        $domain = $this->domain;

        /* @var $user User */
        $user = User::find()
            ->andWhere(['id' => $this->user_id])
            ->one();
        /* @var $manager Manager */
        $manager = $user->getManagers($domain->id)->one();
        if (is_null($manager) === true) {
            return false;
        }
        $emails = $manager->jsonGet('emails', []);
        if (empty($emails) === true) {
            return false;
        }


        return Mailer::send($emails, 'Ваш клиент сделал заказ на сайте ' . $domain->name, '', [], true, 'views/order-manager', [
            'domain' => $domain,
            'order' => $this,
            'client' => $client,
            'orderedDate' => $orderedDate,
        ]);
    }

    public function sendNewOrderEmailClient()
    {

        $client = $this->client;

        /* @var $orderedStatus Status */
        $orderedStatus = $this->getHistoryStatuses()
            ->andWhere(['status' => self::STATUS_NEW])
            ->one();
        $orderedDate = date('d.m.Y H:i', $orderedStatus->created_at);

        $domain = $this->domain;

        /* @var $user User */
        $user = User::find()
            ->andWhere(['id' => $this->user_id])
            ->one();
        /* @var $manager Manager */
        $manager = $user->getManagers($domain->id)->one();

        return Mailer::send($client->email, 'Информация о Вашем заказе на сайте ' . $domain->name, '', [], true, 'views/order-client', [
            'domain' => $domain,
            'order' => $this,
            'client' => $client,
            'orderedDate' => $orderedDate,
            'manager' => $manager,
        ]);
    }

    public function generate1cXml()
    {
        $path = Yii::getAlias('@app/orders/');
        if (file_exists($path) === false) {
            mkdir($path, 0777, true);
        }
        $tempPath = Yii::getAlias('@runtime/orders/');
        if (file_exists($tempPath) === false) {
            mkdir($tempPath, 0777, true);
        }

        $currentTime = date('d.m.Y H:i:s');

        $fileName = $this->number . '.xml';

        /* @var $orderedStatus Status */
        $orderedStatus = $this->getHistoryStatuses()
            ->andWhere(['status' => self::STATUS_NEW])
            ->one();
        $orderedDate = date('d.m.Y H:i:s', $orderedStatus->created_at);
        $client = $this->client;
        $clientFullName = $client->getFullName();

        $xmlString = <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<КоммерческаяИнформация ДатаФормирования="{$currentTime}">
    <Документ>
        <НомерЗаказа>{$this->number}</НомерЗаказа>
        <Дата>{$orderedDate}</Дата>
        <ФИО>{$clientFullName}</ФИО>
        <Телефон>{$client->phone}</Телефон>
        <ЭлектроннаяПочта>{$client->email}</ЭлектроннаяПочта>
        <Организация>{$client->organization}</Организация>
        <ИНН>{$client->inn}</ИНН>
        <ОГРН>{$client->ogrn}</ОГРН>
        <Город>{$client->city}</Город>
        <Валюта>руб</Валюта>
        <СуммаДокумента>{$this->sum}</СуммаДокумента>
        <Товары>
XML;
        file_put_contents($tempPath . $fileName, $xmlString . PHP_EOL, FILE_APPEND);

        $goodsBySample = [];
        /* @var $good Good */
        foreach ($this->getGoods()->each() as $good) {
            $goodsBySample[$good->jsonGet('sample')][$good->good_id][] = $good;
        }

        foreach ($goodsBySample as $sample => $goodBySample) {
            file_put_contents($tempPath . $fileName, "            <Проба значение=\"{$sample}\">" . PHP_EOL, FILE_APPEND);
            foreach ($goodBySample as $goods) {
                $firstGood = $goods[0];

                $sum = 0;
                $catalogGood = $firstGood->jsonGet('attributes', []);
                $xmlString = <<<XML
                <Товар>
                    <ИдТовар>{$catalogGood['outer_code']}</ИдТовар>
                    <Артикул>{$catalogGood['vendor_code']}</Артикул>
                    <ТорговыеПредложения>
XML;

                foreach ($goods as $good) {
                    $sizes = $good->jsonGet('sizes', []);
                    if (empty($sizes) === false) {
                        foreach ($sizes as $size) {
                            if ($size['count'] == 0) {
                                continue;
                            }
                            $xmlString .= <<<XML

                        <Предложение>
                            <Размер>{$size['size']}</Размер>
                            <Количество>{$size['count']}</Количество>
                            <Цена>{$size['sum']}</Цена>
                        </Предложение>
XML;
                        }
                    } else {
                        $xmlString .= <<<XML

                        <Предложение>
                            <Количество>{$good->count}</Количество>
                            <Цена>{$good->sum}</Цена>
                        </Предложение>
XML;
                    }

                    $sum += $good->sum;
                }


                $xmlString .= PHP_EOL . "                    </ТорговыеПредложения>" . PHP_EOL . "                    <Сумма>{$sum}</Сумма>" . PHP_EOL . '                </Товар>';

                file_put_contents($tempPath . $fileName, $xmlString . PHP_EOL, FILE_APPEND);

            }
            file_put_contents($tempPath . $fileName, "            </Проба>" . PHP_EOL, FILE_APPEND);
        }


        $xmlString = '        </Товары>' . PHP_EOL . '    </Документ>' . PHP_EOL . '</КоммерческаяИнформация>';
        file_put_contents($tempPath . $fileName, $xmlString . PHP_EOL, FILE_APPEND);

        rename($tempPath . $fileName, $path . $fileName);

        return $path . $fileName;
    }

    /**
     * @return Xlsx
     */
    public function generateXlsx()
    {
        $domain = $this->domain;

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $sheet->mergeCells('A1:I1');
        $sheet->setCellValue('A1', 'Заказ на сайте ' . $domain->name);
        $sheet->getCell('A1')->getStyle()->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);

        $sheet->setCellValue('A2', 'Изображение');
        $sheet->setCellValue('B2', 'Наименование');
        $sheet->setCellValue('C2', 'Артикул');
        $sheet->setCellValue('D2', 'Вставка');
        $sheet->setCellValue('E2', 'Покрытие');
        $sheet->setCellValue('F2', 'Количество');
        $sheet->setCellValue('G2', 'Вес (гр.)');
        $sheet->setCellValue('H2', 'Цена (руб.)');
        $sheet->setCellValue('I2', 'Сумма (руб.)');
        $sheet->setCellValue('J2', 'Комментарий');
        $rowIterator = 2;

        foreach (range('A', 'J') as $columnID) {
            $cell = $sheet->getCell("{$columnID}{$rowIterator}")->getStyle();
            $cell->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
            $cell->getFont()->setBold(true);
            $cell->getBorders()->getAllBorders()->setBorderStyle(Border::BORDER_THIN);
            $cell->getBorders()->getAllBorders()->setColor(new Color(Color::COLOR_BLACK));
        }

        /* @var $good Good */
        foreach ($this->getGoods()->all() as $good) {
            $rowIterator++;
            $catalogGood = $good->good;
            $catalogImg = $catalogGood->getFullPath('image');
            if(!file_exists($catalogImg)) {
            $catalogImg = Yii::getAlias('@runtime/nophoto.jpg');
            }
            if (is_null($catalogGood) === false && empty($catalogGood->image) === false) {
                $image = new Drawing();
                $image->setName($catalogGood->name);
                $image->setPath($catalogImg);
                $image->setHeight(100);
                $image->setCoordinates("A{$rowIterator}");
                $image->setWorksheet($sheet);
                $sheet->getRowDimension($rowIterator)->setRowHeight(80);
                $sheet->getCell("A{$rowIterator}")->getStyle()->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
                $sheet->getCell("A{$rowIterator}")->getStyle()->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);
            }

            $sheet->setCellValue("B{$rowIterator}", $good->jsonGet('name'));
            $sheet->setCellValue("C{$rowIterator}", isset($good->jsonData['attributes']['vendor_code']) === true ? $good->jsonData['attributes']['vendor_code'] : '');
            $sheet->setCellValue("D{$rowIterator}", $good->getInsert() ?: "-");
            $sheet->setCellValue("E{$rowIterator}", $good->getCoating() ?: "-");
            $count = $good->count . ' шт.';
            if (empty($good->jsonData['sizes']) === false) {
                $sizes = [];
                foreach ($good->jsonData['sizes'] as $size) {
                    if ($size['count'] == 0) {
                        continue;
                    }
                    $sizes[] = "{$size['size']}: {$size['count']} шт.";
                }
                $count = implode(PHP_EOL, $sizes);
            }
            $sheet->setCellValue("F{$rowIterator}", $count);
            $sheet->setCellValue("G{$rowIterator}", $good->weight);
            $sheet->setCellValue("H{$rowIterator}", $good->jsonData['price_unit']);
            $sheet->setCellValue("I{$rowIterator}", $good->sum);
            $sheet->setCellValue("J{$rowIterator}", $good->comment);

            foreach (range('G', 'J') as $columnID) {
                $sheet->getCell("{$columnID}{$rowIterator}")->getStyle()->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_00);
            }
            foreach (range('A', 'J') as $columnID) {
                $borders = $sheet->getCell("{$columnID}{$rowIterator}")->getStyle()->getBorders()->getAllBorders();
                $borders->setBorderStyle(Border::BORDER_THIN);
                $borders->setColor(new Color(Color::COLOR_BLACK));
            }
        }
        $rowIterator++;

        $sheet->setCellValue("F{$rowIterator}", $this->good_count . ' шт.');
        $sheet->setCellValue("G{$rowIterator}", $this->good_weight);
        $sheet->setCellValue("I{$rowIterator}", $this->sum);

        foreach (range('F', 'I') as $columnID) {
            $cellStyle = $sheet->getCell("{$columnID}{$rowIterator}")->getStyle();
            $cellStyle->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_00);
            $cellStyle->getBorders()->getAllBorders()->setBorderStyle(Border::BORDER_THIN);
            $cellStyle->getBorders()->getAllBorders()->setColor(new Color(Color::COLOR_BLACK));
        }

        foreach (range('A', 'J') as $columnID) {
            $sheet->getColumnDimension($columnID)->setAutoSize(true);
        }

        return new Xlsx($spreadsheet);
    }

    /**
     * @return array
     */
    public function generateCsv()
    {

        $csvArr = [];

        foreach ($this->getGoods()->all() as $good) {
            $catalogGood = $good->good;


            $str = [
                $good->jsonGet('name'),
                isset($good->jsonData['attributes']['vendor_code']) === true ? $good->jsonData['attributes']['vendor_code'] : '',
                $good->jsonData['sample'],
                $good->getMetalName(),
            ];

            $count = $good->count . ' шт.';
            if (empty($good->jsonData['sizes']) === false) {
                $sizes = [];
                foreach ($good->jsonData['sizes'] as $size) {
                    if ($size['count'] == 0) {
                        continue;
                    }
                    $sizes[] = "{$size['size']}: {$size['count']} шт.";
                }
                $count = implode(PHP_EOL, $sizes);
            }

            $str[] = $count;
            $str[] = $good->weight;
            $str[] = $good->jsonData['price_unit'];
            $str[] = $good->sum;

            $csvArr[] = $str;
        }

        return $csvArr;

    }

    public function generateImageArchive($path = null)
    {
        if (is_null($path) === true) {
            $path = Yii::getAlias('@runtime/') . md5(time() . $this->number) . '.zip';
        }

        $zip = new ZipArchive();
        $t = $zip->open($path, ZipArchive::CREATE);
        $query = \app\models\catalog\Good::find()
            ->andWhere(['id' => $this->getGoods()->select(['good_id'])])
            ->andWhere(['!=', 'image', '']);

        /* @var $good \app\models\catalog\Good */
        foreach ($query->each() as $good) {
            $extension = $good->getFileExtention('image');
            $zip->addFile($good->getFullPath('image'), $good->vendor_code . '.' . $extension);
        }

        $zip->close();

        return $path;
    }

    public function getNextOrderNumber()
    {
        /* @var $settings Settings */
        $settings = Yii::$app->get('settings');
        $number = (integer)$settings->get('number', 'order_numbers', 0);

        $number++;

        $settings->set('number', $number, 'order_numbers');

        return $number;
    }

    public function generateHash()
    {
        return Yii::$app->getSecurity()->generateRandomString(32);
    }


    public function afterSave($insert, $changedAttributes)
    {
        if ($insert === true || isset($changedAttributes['status']) === true && $this->status != $changedAttributes['status']) {
            Status::add($this->id, $this->status);
        }

        parent::afterSave($insert, $changedAttributes);
    }

    public function canRepeat()
    {
        /* @var $good Good */
        foreach ($this->goods as $good) {
            if ($good->canRepeat() === true) {
                return true;
            }
        }
        return false;
    }

    public function sendAmo()
    {
        $amo = new AmoApi();
        $amo->addOrder($this);
    }

}
