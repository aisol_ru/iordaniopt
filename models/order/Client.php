<?php

namespace app\models\order;

use app\components\db\ActiveRecord;
use app\models\user\User;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;


/**
 * @property integer $id
 * @property integer $order_id
 * @property string $first_name
 * @property string $last_name
 * @property string $middle_name
 * @property string $phone
 * @property string $email
 * @property string $organization
 * @property string $inn
 * @property string $ogrn
 * @property string $city
 * @property string $certificate
 * @property string $comment
 *
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Order $order
 */
class Client extends ActiveRecord
{

    public function rules()
    {
        return [
            ['order_id', 'exist', 'targetClass' => Order::class, 'targetAttribute' => 'id'],
            ['first_name', 'required', 'message' => 'Обязательное поле'],
            ['last_name', 'required', 'message' => 'Обязательное поле'],
            ['middle_name', 'default', 'value' => ''],
            ['phone', 'required', 'message' => 'Обязательное поле'],
            ['email', 'required', 'message' => 'Обязательное поле'],
            ['email', 'email', 'message' => 'Проверьте правильность Email-адреса'],
            ['organization', 'required', 'message' => 'Обязательное поле'],
            ['inn', 'required', 'message' => 'Обязательное поле'],
            ['ogrn', 'required', 'message' => 'Обязательное поле'],
            ['city', 'required', 'message' => 'Обязательное поле'],
            ['certificate', 'required', 'message' => 'Обязательное поле'],
            ['comment', 'default', 'value' => ''],
            ['comment', 'filter', 'filter' => function($value) {
                return trim(strip_tags($value));
            }],
        ];
    }

    public static function tableName()
    {
        return '{{%order_client}}';
    }

    public function behaviors()
    {
        return [
            'user' => BlameableBehavior::class,
            'timestamp' => TimestampBehavior::class,
        ];
    }


    public function getOrder()
    {
        return $this->hasOne(Order::class, ['id' => 'order_id']);
    }

    public function getFullName()
    {
        $pieces = [];
        if (empty($this->last_name) === false) {
            $pieces[] = $this->last_name;
        }
        if (empty($this->first_name) === false) {
            $pieces[] = $this->first_name;
        }
        if (empty($this->middle_name) === false) {
            $pieces[] = $this->middle_name;
        }

        return implode(' ', $pieces);
    }

    /**
     * @param $user User
     */
    public function fill($user)
    {
        $this->first_name = $user->first_name;
        $this->last_name = $user->last_name;
        $this->middle_name = $user->middle_name;
        $this->phone = $user->phone;
        $this->email = $user->email;
        $this->organization = $user->organization;
        $this->inn = $user->inn;
        $this->ogrn = $user->ogrn;
        $this->city = $user->city;
        $this->certificate = $user->certificate;
    }

}
