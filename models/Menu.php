<?php

namespace app\models;

use app\components\db\ActiveRecord;
use app\components\traits\DomainModel;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * @property integer $id
 * @property integer $domain_id
 * @property string $name
 * @property string $url
 * @property integer $is_premium
 * @property integer $show_guest
 * @property integer $show_user
 *
 * @property integer $sort
 * @property integer $status
 *
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $created_at
 * @property integer $updated_at
 */
class Menu extends ActiveRecord
{
    use DomainModel;

    public static function tableName()
    {
        return '{{%menu}}';
    }

    public function rules()
    {
        return [
            ['name', 'required', 'message' => 'Обязательное поле'],
            ['url', 'required', 'message' => 'Обязательное поле'],
            ['is_premium', 'default', 'value' => 0],
            ['show_guest', 'default', 'value' => 0],
            ['show_user', 'default', 'value' => 0],
            ['sort', 'default', 'value' => 0],
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => array_keys(self::getStatuses())],
        ];
    }

    public function scenarios()
    {
        $defaultScenario = parent::scenarios()[self::SCENARIO_DEFAULT];
        return [
            self::SCENARIO_DEFAULT => $defaultScenario
        ];
    }

    public function behaviors()
    {
        return [
            'user' => BlameableBehavior::class,
            'timestamp' => TimestampBehavior::class,
        ];
    }

}
