<?php

namespace app\models\banner;

use app\components\db\ActiveRecordFiles;
use app\components\traits\DomainModel;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * @property integer $id
 * @property integer $category_id
 * @property string $name
 * @property string $link
 * @property string $image
 * @property integer $is_premium
 * @property integer $sort
 * @property integer $status
 *
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Category $category
 */
class Banner extends ActiveRecordFiles
{
    use DomainModel;

    const CATEGORY_MAIN = 1;
    const CATEGORY_NEW_COLLECTIONS = 2;

    public $imageFields = [
        'image' => ['banners', 'banner'],
    ];

    public function rules()
    {
        return [
            ['category_id', 'exist', 'targetClass' => Category::class, 'targetAttribute' => 'id'],
            ['name', 'required', 'message' => 'Обязательное поле'],
            ['link', 'default', 'value' => ''],
            [
                '!image', 'file',
                'extensions' => ['png', 'jpg', 'gif', 'svg'], 'wrongExtension' => 'Доступные форматы: {extensions}',
            ],
            ['is_premium', 'default', 'value' => 0],
            ['sort', 'default', 'value' => 0],
            ['sort', 'integer', 'message' => 'Введите целое число'],
            ['status', 'in', 'range' => [1, 2], 'message' => 'Выберите правильное значение', 'except' => 'filter'],
            ['status', 'default', 'value' => self::STATUS_ACTIVE, 'except' => 'filter'],
        ];
    }

    public function scenarios()
    {
        $defaultScenario = parent::scenarios()[self::SCENARIO_DEFAULT];
        return [
            self::SCENARIO_DEFAULT => $defaultScenario,
            'filter' => ['name', 'status'],
        ];
    }

    public function getCategory()
    {
        return $this->hasOne(Category::class, ['id' => 'category_id']);
    }

    public static function tableName()
    {
        return '{{%banner}}';
    }

    public function behaviors()
    {
        return [
            'user' => BlameableBehavior::class,
            'timestamp' => TimestampBehavior::class,
        ];
    }

}
