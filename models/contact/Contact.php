<?php

namespace app\models\contact;

use app\components\db\ActiveRecord;
use app\components\traits\DomainModel;
use app\models\map\Map;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * @property integer $id
 * @property integer $domain_id
 *
 * @property string $address
 * @property string $working_hours
 * @property string $phone
 * @property string $email
 * @property string $map_id
 *
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Map $map
 */
class Contact extends ActiveRecord
{
    use DomainModel;

    public static function tableName()
    {
        return '{{%contact}}';
    }

    public function rules()
    {
        return [
            ['address', 'default', 'value' => ''],
            ['working_hours', 'default', 'value' => ''],
            ['phone', 'default', 'value' => ''],
            ['email', 'default', 'value' => ''],
            ['map_id', 'default', 'value' => 0],
            ['map_id', 'exist', 'targetClass' => Map::class, 'targetAttribute' => 'id', 'skipOnEmpty' => true, 'isEmpty' => function($value) {
                return $value == false;
            }],
        ];
    }

    public function scenarios()
    {
        $defaultScenario = parent::scenarios()[self::SCENARIO_DEFAULT];
        return [
            self::SCENARIO_DEFAULT => $defaultScenario
        ];
    }

    public function behaviors()
    {
        return [
            'user' => BlameableBehavior::class,
            'timestamp' => TimestampBehavior::class,
        ];
    }

    public function getMap()
    {
        return $this->hasOne(Map::class, ['id' => 'map_id']);
    }

}
