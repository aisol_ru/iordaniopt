<?php

namespace app\models\catalog;

use app\components\db\ActiveRecord;
use app\components\db\ActiveRecordFiles;
use app\components\traits\ActiveJSON;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * @property integer $id
 * @property string $outer_code
 * @property string $name
 *
 * @property integer $sort
 * @property integer $status
 *
 * @property string $json
 *
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Good[] $goods
 */
class Category extends ActiveRecordFiles
{

    public $imageFields = [
        'image' => ['catalog_category', 'category'],
    ];

    public static function tableName()
    {
        return '{{%catalog_category}}';
    }

    use ActiveJSON;

    public function rules()
    {
        return [
            ['name', 'required', 'message' => 'Обязательное поле'],
            [
                '!image', 'file',
                'extensions' => ['png', 'jpg', 'gif'], 'wrongExtension' => 'Доступные форматы: {extensions}',
            ],
            ['sort', 'default', 'value' => 0],
            ['sort', 'integer', 'message' => 'Введите целое число'],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DISABLE], 'message' => 'Выберите правильное значение',],
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['jsonData', 'safe'],
        ];
    }

    public function scenarios()
    {
        $defaultScenario = parent::scenarios()[self::SCENARIO_DEFAULT];
        return [
            self::SCENARIO_DEFAULT => $defaultScenario,
            'filter' => ['name'],
        ];
    }

    public function getGoods()
    {
        return $this->hasMany(Good::class, ['category_id' => 'outer_code']);
    }

    public function getChildren()
    {
        return $this->hasMany(self::class, ['parent_outercode' => 'outer_code']);
    }

    public function behaviors()
    {
        return [
            'user' => BlameableBehavior::class,
            'timestamp' => TimestampBehavior::class,
        ];
    }

    public function beforeDelete()
    {

        /* @var $good Good */
        foreach ($this->getGoods()->all() as $good) {
            $good->delete();
        }

        return parent::beforeDelete();
    }

    public function getSizes()
    {
        return $this->jsonGet('sizes', []);
    }

}
