<?php

namespace app\models\catalog;

use app\components\IcmsHelper;
use app\components\traits\ActiveJSONGood;
use app\models\catalog\Category;
use app\models\user\Favorite;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use app\components\db\ActiveRecordFiles;
use yii\helpers\ArrayHelper;

/**
 * @property integer $id
 * @property string $outer_code
 * @property integer $category_id
 * @property string $alias
 * @property string $name
 * @property string $vendor_code
 *
 * @property float $weight_average
 * @property float $price
 *
 * @property float $width
 * @property float $height
 * @property string $prop_code
 * @property string $specification
 * @property string $view
 * @property integer $brand_id
 * @property integer $metal_id
 * @property integer $insert_id
 *
 * @property string $content
 * @property string $image
 *
 * @property integer $sort
 * @property integer $status
 *
 * @property string $seo_title
 * @property string $seo_description
 * @property string $seo_keywords
 *
 * @property string $json
 *
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Category $category
 * @property Image[] $images
 * @property Price[] $prices
 * @property Brand $brand
 * @property Metal $metal
 * @property Insert $insert
 * @property Collection[] $collections
 */
class Good extends ActiveRecordFiles
{
    use ActiveJSONGood;

    public $imageFields = [
        'image' => ['catalog/good', 'good'],
    ];

    public static function tableName()
    {
        return '{{%catalog_good}}';
    }

    public function rules()
    {
        return [
            ['category_id', 'required', 'message' => 'Обязательное поле'],
            ['category_id', 'exist', 'targetClass' => Category::class, 'targetAttribute' => 'id', 'skipOnEmpty' => true],
            ['alias', 'required', 'message' => 'Обязательное поле'],
            ['alias', 'unique', 'message' => 'Должен быть уникальным'],
            ['alias', 'match', 'pattern' => '/[a-zA-Z0-9_-]+$/', 'message' => 'Только латинские буквы, цифры, "_" и "-"'],
            ['name', 'required', 'message' => 'Обязательное поле'],
            ['vendor_code', 'default', 'value' => ''],

            ['weight_average', 'default', 'value' => 0],
            ['price', 'default', 'value' => 0],
            ['width', 'default', 'value' => 0],
            ['height', 'default', 'value' => 0],
            ['has_emale', 'default', 'value' => 0],
            ['specification', 'default', 'value' => ''],
            ['view', 'default', 'value' => ''],
            ['brand_id', 'required', 'message' => 'Обязательное поле'],
            ['brand_id', 'exist', 'targetClass' => Brand::class, 'targetAttribute' => 'id', 'skipOnEmpty' => true],
            ['metal_id', 'exist', 'targetClass' => Metal::class, 'targetAttribute' => 'id', 'skipOnEmpty' => true, 'isEmpty' => function ($value) {
                return $value == false;
            }],
            ['insert_id', 'exist', 'targetClass' => Insert::class, 'targetAttribute' => 'id', 'skipOnEmpty' => true, 'isEmpty' => function ($value) {
                return $value == false;
            }],

            ['content', 'default', 'value' => ''],
            [
                '!image', 'file',
                'extensions' => ['png', 'jpg', 'gif'], 'wrongExtension' => 'Доступные форматы: {extensions}',
            ],

            ['sort', 'default', 'value' => 0],
            ['sort', 'integer', 'message' => 'Введите целое число'],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DISABLE], 'message' => 'Выберите правильное значение',],
            ['status', 'default', 'value' => self::STATUS_ACTIVE],

            [['seo_title', 'seo_description', 'seo_keywords'], 'default', 'value' => ''],

            [['jsonData', "prop_code"], 'safe'],
        ];
    }

    public function scenarios()
    {
        $defaultScenario = parent::scenarios()[self::SCENARIO_DEFAULT];
        return [
            self::SCENARIO_DEFAULT => $defaultScenario,
            'filter' => ['name'],
        ];
    }

    public function getWeightForGoodCard()
    {

        $sizes = $this->getSizesFromTable();
        switch ($this->getNameTreeCategories()) {
            case "Кольца":
                $size = $sizes->andWhere(["size" => "18"])->one();
                if (is_null($size) === true) {
                    return false;
                }
                return "Вес на 18 размер: " . $size->weight;
            case "Браслеты":
                $size = $sizes->andWhere(["size" => "19"])->one();
                if (is_null($size) === true) {
                    return false;
                }
                return "Вес на 19 размер: " . $size->weight;
            case "Цепи":
                $size = $sizes->andWhere(["size" => "55"])->one();
                if (is_null($size) === true) {
                    return false;
                }
                return "Вес на 55 размер: " . $size->weight;
            default:
                return false;
        }
    }

    private function getNameTreeCategories()
    {

        $mainKolcaId = "4b01c459-8c82-11e7-85d9-3010b33baceb";
        $mainBrasletyId = "514b1f7b-8c82-11e7-85d9-3010b33baceb";
        $mainCepiId = "4b01c50a-8c82-11e7-85d9-3010b33baceb";

        $arrKolca = IcmsHelper::map(
            Category::find()
                ->andWhere(["outer_code" => $mainKolcaId])
                ->one()
                ->getChildren()
                ->all(), "outer_code", "outer_code");
        $arrBraslety = IcmsHelper::map(
            Category::find()
                ->andWhere(["outer_code" => $mainBrasletyId])
                ->one()
                ->getChildren()
                ->all(), "outer_code", "outer_code");
        $arrCepi = IcmsHelper::map(
            Category::find()
                ->andWhere(["outer_code" => $mainCepiId])
                ->one()
                ->getChildren()
                ->all(), "outer_code", "outer_code");

        $arrKolca[] = $mainKolcaId;
        $arrBraslety[] = $mainBrasletyId;
        $arrCepi[] = $mainCepiId;

        if (in_array($this->category_id, $arrKolca)) {
            return "Кольца";
        } else if (in_array($this->category_id, $arrBraslety)) {
            return "Браслеты";
        } else if (in_array($this->category_id, $arrCepi)) {
            return "Цепи";
        }

        return false;
    }

    public function getSizesFromTable()
    {
        return $this->hasMany(Size::class, ["catalog_id" => "outer_code"]);
    }

    public function getFirstCoating()
    {
        $sizeWithCoating = $this->getSizesFromTable()->andWhere(["!=", "coating_id", ""])->one();
        if (is_null($sizeWithCoating) === true) {
            return false;
        }
        return $sizeWithCoating->coating;
    }

    public function getAllCoatings(): array
    {
        return IcmsHelper::map($this->getSizesFromTable()->isActive()->andWhere(["!=", "coating_id", ""])->all(), "coating_id", "coating");
    }

    public function getAllInserts(): array
    {
        $inserts = IcmsHelper::map($this->getSizesFromTable()->andWhere(["!=", "insert_id", ""])->all(), "insert_id", "size_insert");

        foreach ($inserts as $insertId => &$insert) {
            preg_match_all("/(\d)/", $insertId, $insertCounts);
            $insertCounts = $insertCounts[1];
            array_shift($insertCounts);

            $insertArr = explode(", ", $insert);
            foreach ($insertArr as $key => &$item) {
                $nameInsert = $item;
                $itemArr = mb_str_split($nameInsert);
                $firstChar = array_shift($itemArr);
                $item = mb_strtoupper($firstChar) . join("", $itemArr);

//                $item = $insertCounts[$key] . " " . $item;
            }
            $insert = join(", ", $insertArr);
        }

        return $inserts;
    }

    public function getSizes()
    {
        return json_decode($this->sizes_json, true);
    }

    public function getSizesPrice()
    {
        return $this->jsonGet('price', []);
    }

    public function getCategory()
    {
        return $this->hasOne(Category::class, ['id' => 'category_id']);
    }

    public function getImages()
    {
        return $this->hasMany(Image::class, ['good_id' => 'id']);
    }

    public function getPrices()
    {
        return $this->hasMany(Price::class, ['good_id' => 'id']);
    }

    public function getBrand()
    {
        return $this->hasOne(Brand::class, ['id' => 'brand_id']);
    }

    public function getMetal()
    {
        return $this->hasOne(Metal::class, ['id' => 'metal_id']);
    }

    public function getInsert()
    {
        return $this->hasOne(Insert::class, ['id' => 'insert_id']);
    }

    public function getCollections()
    {
        return $this->hasMany(Collection::class, ['id' => 'collection_id'])
            ->viaTable('{{%catalog_good_to_collection}}', ['good_id' => 'id']);
    }

    public function isNew()
    {
        return $this->created_at > (time() - 3600 * 24 * 60);
    }

    public function isFavorite()
    {
        return Favorite::checkExists($this->id);
    }

    public function behaviors()
    {
        return [
            'user' => BlameableBehavior::class,
            'timestamp' => TimestampBehavior::class,
        ];
    }

    public function beforeDelete()
    {
        foreach ($this->getImages()->all() as $image) {
            $image->delete();
        }
        $this->unlinkAll('prices', true);
        $this->unlinkAll('collections', true);

        return parent::beforeDelete();
    }

}
