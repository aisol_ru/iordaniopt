<?php

namespace app\models\catalog;

use app\components\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * @property integer $id
 * @property string $outer_code
 * @property string $name
 *
 * @property integer $sort
 *
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Good[] $goods
 */
class Metal extends ActiveRecord
{

    public static function tableName()
    {
        return '{{%catalog_metal}}';
    }

    public function rules()
    {
        return [
            ['name', 'required', 'message' => 'Обязательное поле'],

            ['sort', 'default', 'value' => 0],
            ['sort', 'integer', 'message' => 'Введите целое число'],
        ];
    }

    public function scenarios()
    {
        $defaultScenario = parent::scenarios()[self::SCENARIO_DEFAULT];
        return [
            self::SCENARIO_DEFAULT => $defaultScenario,
            'filter' => ['name'],
        ];
    }

    public function getGoods()
    {
        return $this->hasMany(Good::class, ['metal_id' => 'id']);
    }

    public function behaviors()
    {
        return [
            'user' => BlameableBehavior::class,
            'timestamp' => TimestampBehavior::class,
        ];
    }

}
