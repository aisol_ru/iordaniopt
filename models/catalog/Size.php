<?php


namespace app\models\catalog;

/**
 * Class Size
 * @param integer id
 * @param string outer_code
 * @param string catalog_id
 * @param double price
 * @param double size
 * @package app\models\catalog
 */
class Size extends \app\components\db\ActiveRecord
{
    public static function tableName()
    {
        return '{{%catalog_sizes}}';
    }

    public function getGood()
    {
        return $this->hasOne(Good::class, ["outer_code" => "catalog_id"]);
    }
}