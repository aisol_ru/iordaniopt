<?php

namespace app\models\catalog;

use app\components\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * @property integer $id
 * @property integer $good_id
 * @property string $sample
 *
 * @property float $price
 *
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Good $good
 */
class Price extends ActiveRecord
{

    public static function tableName()
    {
        return '{{%catalog_good_price}}';
    }

    public function rules()
    {
        return [
            ['good_id', 'required', 'message' => 'Обязательное поле'],
            ['good_id', 'exist', 'targetClass' => Good::class, 'targetAttribute' => 'id'],

            ['sample', 'required', 'message' => 'Обязательное поле'],

            ['price', 'number', 'min' => 0, 'message' => 'Должно быть числом'],
        ];
    }

    public function scenarios()
    {
        $defaultScenario = parent::scenarios()[self::SCENARIO_DEFAULT];
        return [
            self::SCENARIO_DEFAULT => $defaultScenario,
            'filter' => ['name'],
        ];
    }

    public function getGood()
    {
        return $this->hasOne(Good::class, ['id' => 'good_id']);
    }

    public function behaviors()
    {
        return [
            'user' => BlameableBehavior::class,
            'timestamp' => TimestampBehavior::class,
        ];
    }

}
