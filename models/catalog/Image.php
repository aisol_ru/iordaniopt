<?php

namespace app\models\catalog;

use app\components\db\ActiveRecordFiles;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * @property integer $id
 * @property integer $good_id
 * @property string $name
 * @property string $image
 *
 * @property integer $sort
 *
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $created_at
 * @property integer $updated_at
 */
class Image extends ActiveRecordFiles
{

    public $imageFields = [
        'image' => ['catalog/good/images', 'good'],
    ];

    public static function tableName()
    {
        return '{{%catalog_good_image}}';
    }

    public function rules()
    {
        return [
            ['good_id', 'exist', 'targetClass' => Good::class, 'targetAttribute' => 'id'],
            ['name', 'required', 'message' => 'Введите название', 'on' => ['default']],
            [
                '!image', 'file',
                'extensions' => ['png', 'jpg', 'gif'], 'wrongExtension' => 'Доступные форматы: {extensions}',
                'maxSize' => 2097152, 'tooBig' => 'Не больше 2Мб',
                'skipOnEmpty' => false, 'message' => 'Выберите файл (файл не загружен)'
            ],
            ['sort', 'default', 'value' => 0],
            ['sort', 'integer', 'message' => 'Введите целое число'],
        ];
    }

    public function behaviors()
    {
        return [
            'user' => BlameableBehavior::class,
            'timestamp' => TimestampBehavior::class,
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => 'Название',
            'sort' => 'Сортировка',
        ];
    }

}
