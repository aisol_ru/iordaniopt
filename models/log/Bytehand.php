<?php

namespace app\models\log;

use yii\behaviors\TimestampBehavior;

/**
 * @property integer $id
 * @property string $name
 * @property string $request
 * @property string $response
 * @property integer $datetime
 *
 * @property integer $created_at
 * @property integer $updated_at
 */
class Bytehand extends \app\components\db\ActiveRecord
{

    public static function tableName()
    {
        return '{{%log_bytehand}}';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }

    public static function add($senderName, $requestString, $responseString, $sendTime = 'now')
    {
        $log = new self();
        $log->name = $senderName;
        $log->request = $requestString;
        $log->response = $responseString;
        $log->datetime = is_numeric($sendTime) === true ? $sendTime : time();
        return [$log->save(), $log->id];
    }

}
