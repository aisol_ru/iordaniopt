<?php
/** @noinspection ALL */

namespace app\commands;

use app\components\db\ActiveRecordFiles;
use app\components\IcmsHelper;
use app\models\catalog\Brand;
use app\models\catalog\Category;
use app\models\catalog\Collection;
use app\models\catalog\Good;
use app\models\catalog\Image;
use app\models\catalog\Insert;
use app\models\catalog\Metal;
use app\models\catalog\Price;
use app\models\catalog\Size;
use MongoDB\Driver\Exception\ExecutionTimeoutException;
use yii\console\ExitCode;
use yii\db\Exception;
use yii\helpers\ArrayHelper;
use yii\helpers\Console;
use yii\web\NotFoundHttpException;

/**
 * Импорт товаров из XML
 */
class ImportController extends \app\components\controller\Console
{

    const ACTIVE_KOSTIL_FOR_ACTIVE_CATALOG = 2;

    public $defaultAction = 'go';

    private $categoryIdsForNotUse = [];
    private $goodIdsForNotUse = [];
    private $sizesIdsForNotUse = [];
    private $importImagePath; //Путь до картинок


    /**
     * @param string $fileNameImport
     * @return int
     * @throws \yii\web\HttpException
     * @throws \yii\web\ServerErrorHttpException
     */
    public function actionGo($fileNameImport = 'import.xml', $fileNameOffers = "offers.xml")
    {

        $this->log('Импортируем...');
        $importPath = \Yii::getAlias('@app/exchange/');
        $this->importImagePath = \Yii::getAlias('@app/exchange/'); //Путь до картинок

        $path = \Yii::getAlias($importPath . $fileNameImport);
        $processingPath = \Yii::getAlias($importPath . 'import.processing.xml');
        $lastProcessingPath = \Yii::getAlias($importPath . 'import.last.processing.xml');
        if (file_exists($processingPath) === true) {
            unlink($processingPath);
        }
        if (file_exists($path) === false) {
            $this->log('Файл с остатками не найден!');
            return ExitCode::OK;
        }
        copy($path, $processingPath);
        copy($processingPath, $lastProcessingPath);


        $pathOffers = \Yii::getAlias($importPath . $fileNameOffers);
        $processingPathOffers = \Yii::getAlias($importPath . 'offers.processing.xml');
        $lastProcessingPathOffers = \Yii::getAlias($importPath . 'offers.last.processing.xml');
        if (file_exists($processingPathOffers) === true) {
            unlink($processingPathOffers);
        }
        if (file_exists($pathOffers) === false) {
            $this->log('Файл с sku не найден!');
            return ExitCode::OK;
        }
        copy($pathOffers, $processingPathOffers);
        copy($processingPathOffers, $lastProcessingPathOffers);

        $memTest = memory_get_usage();

        $xml = simplexml_load_file($processingPath);
        $xmlOffers = simplexml_load_file($processingPathOffers);

        $this->importRun($xml, $xmlOffers);

        unlink($path);
        unlink($pathOffers);
        unlink($processingPath);
        unlink($processingPathOffers);

        $this->log('Использовано памяти: ' . str_replace('&nbsp;', '', IcmsHelper::getSymbolByQuantity(memory_get_usage() - $memTest)));

        $this->log('Конец.');

        return ExitCode::OK;
    }

    private function importRun($importXml, $offersXml)
    {
        $this->log('Импортируем категории...');
        $xmlCategories = $importXml->xpath("//Группа");

        if ($this->log === true) {
            $iter = 0;
            $total = count($xmlCategories);
            Console::startProgress(0, $total);
        }

        foreach ($xmlCategories as $xmlCategory) {
            if ($this->log === true) {
                Console::updateProgress($iter, $total);
                $iter++;
            }

            try {

                $this->addImportCategory($xmlCategory);

            } catch (\Exception $e) {
                var_dump($xmlCategory);
                var_dump($e->getLine());
                var_dump($e->getMessage());
                return;
            }
        }

        if ($this->log === true) {
            Console::endProgress();
        }
        $this->log('Категории выгружены.');


        $this->log('Импортируем товары...');
        $xmlGoods = $importXml->xpath("//Товар");

        if ($this->log === true) {
            $iter = 0;
            $total = count($xmlGoods);
            Console::startProgress($iter, $total);
        }

        foreach ($xmlGoods as $xmlGood) {
            if ($this->log === true) {
                Console::updateProgress($iter, $total);
                $iter++;
            }
            try {

                $this->addImportGood($xmlGood);

            } catch (\Exception $e) {
                var_dump($xmlGood);
                var_dump($e->getLine());
                var_dump($e->getMessage());
                return;
            }
        }

        if ($this->log === true) {
            Console::endProgress();
        }
        $this->log('Товары выгружены.');


        $this->log('Загрузка размеров.');
        $offers = $offersXml->xpath("//Предложение");

        if ($this->log === true) {
            $iter = 0;
            $total = count($offers);
            Console::startProgress($iter, $total);
        }
        foreach ($offers as $offer) {
            if ($this->log === true) {
                Console::updateProgress($iter, $total);
                $iter++;
            }

            try {

                $this->addSize($offer);

            } catch (\Exception $e) {
                var_dump($offer);
                var_dump($e->getLine());
                var_dump($e->getMessage());
                return;
            }

        }
        if ($this->log === true) {
            Console::endProgress();
        }
        $this->log('Размеры загружены.');


        $this->log('Деактивация сущностей, которых нет в выгрузке.');
        try {

            $this->deactivationEntities();

        } catch (\Exception $e) {
            var_dump($e->getLine());
            var_dump($e->getMessage());
            return;
        }

        $this->log('Конец деактивации сущностей.');


        $this->log('Добавление средней цены.');
        try {

            $this->addGoodPrices();

        } catch (\Exception $e) {
            var_dump($e->getLine());
            var_dump($e->getMessage());
            return;
        }

        $this->log('Конец добавления средней цены.');


        $this->log('Добавление размеров в json.');
        try {

            $this->addGoodJsonSizes();

        } catch (\Exception $e) {
            var_dump($e->getLine());
            var_dump($e->getMessage());
            return;
        }

        $this->log('Конец добавления размеров в json.');

    }

    private function addImportCategory(\SimpleXMLElement $xmlCategory): void
    {
        $categoryId = $xmlCategory->Ид;
        $categoryName = $xmlCategory->Наименование;
        $categoryGroups = $xmlCategory->Группы->Группа;

        $category = Category::find()
            ->andWhere(["outer_code" => $categoryId])
            ->one();

        if (is_null($category) === true) {
            $category = new Category();
            $category->outer_code = $categoryId;
        }

        $category->name = $categoryName;

        if ($category->save(false) === true) {
            $this->categoryIdsForNotUse[] = $category->id;
            if (!empty($categoryGroups)) {
                $this->getAllCategoryRecursion($categoryId, $categoryGroups);
            }
        } else {
            throw new Exception("Категория $categoryId не сохранена");

        }
    }

    private function getAllCategoryRecursion(string $id, \SimpleXMLElement $categoryGroups): void
    {
        if (!empty($categoryGroups)) {
            foreach ($categoryGroups as $categoryGroup) {

                $categoryId = $categoryGroup->Ид;
                $categoryName = $categoryGroup->Наименование;

                $category = Category::find()
                    ->andWhere(["outer_code" => $categoryId])
                    ->one();

                if (is_null($category) === true) {
                    $category = new Category();
                    $category->outer_code = $categoryId;
                }

                $category->parent_outercode = $id;
                $category->name = $categoryName;

                if ($category->save(false) === true) {
                    $this->categoryIdsForNotUse[] = $category->id;
                    if (!empty($categoryGroup->Группы->Группа)) {
                        $this->getAllCategoryRecursion($categoryId, $categoryGroup->Группы->Группа);
                    }
                } else {
                    throw new Exception("Категория $categoryId не сохранена");
                }
            }
        }
    }

    private function addImportGood(\SimpleXMLElement $xmlGood): void
    {
        $good = Good::find()
            ->andWhere(['outer_code' => (string)$xmlGood->Ид])
            ->one();

        if (is_null($good) === true) {
            $good = new Good();
            $good->outer_code = (string)$xmlGood->Ид;
            if ($good->save(false) === false) {
                throw new Exception("Товар $good->outer_code не сохранен");
            };
        }

        $good->brand_id = self::ACTIVE_KOSTIL_FOR_ACTIVE_CATALOG;

        $good->category_id = (string)$xmlGood->Группы->Ид;

        $good->vendor_code = (string)$xmlGood->Артикул;
        $good->name = (string)$xmlGood->Наименование;
        $good->alias = (string)IcmsHelper::transliterate($good->name);
        $good->content = (string)$xmlGood->Описание;

        preg_match("/\.(.*)/", (string)$xmlGood->Картинка, $fileExtension);
        $isCoruptedFileName = preg_match("/(\/)/", (string)$xmlGood->Картинка) + preg_match("/(:)/", (string)$xmlGood->Картинка);

        if (empty($fileExtension) || $isCoruptedFileName > 0) {
            $good->image = "";
        } else if (file_exists($this->importImagePath . (string)$xmlGood->Картинка)) {

            try {

                $good->saveFileFromPath("image", $this->importImagePath . (string)$xmlGood->Картинка, ActiveRecordFiles::TYPE_FILE_IMAGE);
                unlink($this->importImagePath . (string)$xmlGood->Картинка);

            } catch (\yii\web\ServerErrorHttpException $e) {
            };

        }


        if (!is_null($xmlGood->ЗначенияРеквизитов->ЗначениеРеквизита)) {
            foreach ($xmlGood->ЗначенияРеквизитов->ЗначениеРеквизита as $goodWidth) {
                switch ((string)$goodWidth->Наименование) {
                    case "Вес":
                        $good->weight_average = str_replace(",", ".", (string)$goodWidth->Значение);
                        break;
                    case "Код":
                        $good->prop_code = (string)$goodWidth->Значение;
                        break;
                }
            }
        }

        if ($good->save(false) === true) {
            $this->goodIdsForNotUse[] = $good->id;
        } else {
            throw new Exception("Товар $good->outer_code не сохранен");
        }
    }

    private function addSize(\SimpleXMLElement $offer): void
    {
        $arrIds = explode("#", $offer->Ид);
        list($catalogId, $sizeId, $coatingId, $insertId) = $arrIds;

        $size = Size::find()
            ->andWhere(["catalog_id" => $catalogId])
            ->andWhere(["outer_code" => $sizeId])
            ->andWhere(["coating_id" => $coatingId])
            ->andWhere(["insert_id" => $insertId])
            ->one();

        if (is_null($size) === true) {
            $size = new Size();
            $size->outer_code = $sizeId;
            $size->catalog_id = $catalogId;
            $size->coating_id = $coatingId;
            $size->insert_id = $insertId;
        }


        if (!is_null($offer->Цены->Цена)) {
            $size->price = str_replace(",", ".", $offer->Цены->Цена->ЦенаЗаЕдиницу);
        }
        $size->count = $offer->Количество;

        if (!is_null($offer->ХарактеристикиТовара)) {
            foreach ($offer->ХарактеристикиТовара->ХарактеристикаТовара as $propSize) {
                switch ($propSize->Наименование) {
                    case "Размер":
                        $size->size = str_replace(",", ".", $propSize->Значение);
                        break;
                    case "Вес":
                        $size->weight = str_replace(",", ".", $propSize->Значение);
                        break;
                    case "Покрытие":
                        switch ($propSize->Значение) {
                            case "яудалитьОксид":
                                $size->coating = "Оксид";
                                break;
                            default:
                                $size->coating = $propSize->Значение;
                                break;
                        }
                        break;
                    case "Камни":
                        $size->size_insert = $propSize->Значение;
                        break;
                }
            }
        }

        if ($size->save(false) === true) {
            $this->sizesIdsForNotUse[] = $size->id;
        } else {
            throw new Exception("Не сохранился размер $size->outer_code товара с id: $size->catalog_id");
        }
    }

    private function deactivationEntities(): void
    {

        $goods = Good::find();
        $categories = Category::find();
        $sizes = Size::find();

        if ($this->log === true) {
            $iter = 0;
            $total = array_sum([$goods->count(), $categories->count(), $sizes->count()]);
            Console::startProgress($iter, $total);
        }

        foreach ($goods->each() as $good) {
            if ($this->log === true) {
                Console::updateProgress($iter, $total);
                $iter++;
            }
            if (in_array($good->id, $this->goodIdsForNotUse) === false) {
                $good->status = Good::STATUS_DISABLE;
            } else {
                $good->status = Good::STATUS_ACTIVE;
            }
            $good->save(false);

        }

        foreach ($categories->each() as $category) {
            if ($this->log === true) {
                Console::updateProgress($iter, $total);
                $iter++;
            }
            if (in_array($category->id, $this->categoryIdsForNotUse) === false) {
                $category->status = Category::STATUS_DISABLE;
            } else {
                $category->status = Category::STATUS_ACTIVE;
            }
            $category->save(false);

        }

        foreach ($sizes->each() as $size) {
            if ($this->log === true) {
                Console::updateProgress($iter, $total);
                $iter++;
            }
            if (in_array($size->id, $this->sizesIdsForNotUse) === false) {
                $size->status = Size::STATUS_DISABLE;
            } else {
                $size->status = Size::STATUS_ACTIVE;
            }
            $size->save(false);
        }

        if ($this->log === true) {
            Console::endProgress();
        }

    }

    private function addGoodPrices()
    {
        $sizes = Size::find()
            ->orderBy("size")
            ->isActive();

        $arrForGoodPrices = [];
        $arrForGoodCoatings = [];
        foreach ($sizes->each() as $size) {

            $arrForGoodPrices[$size->catalog_id][] = $size->price;
            $arrForGoodCoatings[$size->catalog_id][] = $size->coating;

        }

        foreach ($arrForGoodPrices as $catalogId => $arrPrices) {

            $good = Good::find()
                ->andWhere(["outer_code" => $catalogId])
                ->one();

            if (is_null($good)) {
                continue;
            }

            $price_unit = array_sum($arrPrices);
            $countSizes = count($arrPrices);

            $good->price = $price_unit / $countSizes;
            $good->save(false);
        }

        foreach ($arrForGoodCoatings as $catalogId => $arrCoatings) {

            $good = Good::find()
                ->andWhere(["outer_code" => $catalogId])
                ->one();
            if (is_null($good)) {
                continue;
            }

            $strCoatings = join("", $arrCoatings);

            if (mb_stripos($strCoatings, "эмаль") !== false){
                $good->has_emale = 1;
                $good->save(false);
            }
        }
    }

    private function addGoodJsonSizes()
    {

        $sizes = Size::find()
            ->orderBy("size")
            ->isActive();

        $arrForGoodJsonSizes = [];
        foreach ($sizes->each() as $size) {

            $arrForGoodJsonSizes[$size->catalog_id][] = [
                "insert_id" => $size->insert_id,
                "coating_id" => $size->coating_id,
                "size" => $size->size,
                "price" => $size->price,
                "count" => $size->count,
                "weight" => $size->weight,
            ];

        }


        $arrForGenerateJson = [];
        foreach ($arrForGoodJsonSizes as $catalogId => $arrSizes) {

            foreach ($arrSizes as $size) {
                $arrForGenerateJson[$catalogId][] = [
                    "insert_id" => $size["insert_id"],
                    "coating_id" => $size["coating_id"],
                    "sizes" => [],
                ];
            }

            $array = array_map('json_encode', $arrForGenerateJson[$catalogId]);
            $array = array_unique($array);
            $arrForGenerateJson[$catalogId] = array_map(function ($elem) {
                return json_decode($elem, true);
            }, $array);
        }

        foreach ($arrForGenerateJson as $catalogId => &$arrSizesJson) {
            foreach ($arrSizesJson as &$sizeJson) {

                foreach ($arrForGoodJsonSizes[$catalogId] as $size) {
                    if ($sizeJson["insert_id"] === $size["insert_id"] && $sizeJson["coating_id"] === $size["coating_id"]) {
                        $sizeJson["sizes"][] = [
                            "size" => $size["size"],
                            "price" => $size["price"],
                            "count" => $size["count"],
                            "weight" => $size["weight"],
                        ];
                    }
                }
            }
        }

        $goods = Good::find();
        foreach ($goods->each() as $good) {
            if (key_exists($good->outer_code, $arrForGenerateJson)) {
                $good->sizes_json = json_encode($arrForGenerateJson[$good->outer_code]);
            } else {
                $good->sizes_json = "[]";
            }
            $good->save(false);
        }

    }

}
