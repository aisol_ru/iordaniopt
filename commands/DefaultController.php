<?php

namespace app\commands;

use app\components\Mailer;
use app\models\catalog\Good;
use app\models\order\ImageArchive;
use app\models\order\Order;
use yii\console\ExitCode;
use yii\helpers\Console;

class DefaultController extends \app\components\controller\Console
{

    public function actionIndex($message = 'hello world')
    {
        echo $message . "\n";
    }

    public function actionOrderToXml($orderId)
    {
        $order = Order::find('', false)->andWhere(['id' => $orderId])->one();
        if (is_null($order) === true) {
            $this->log("Заказ с ID {$orderId} не найден");
            return ExitCode::UNSPECIFIED_ERROR;
        }

        $filePath = $order->generate1cXml();
        $this->log('XML файл создан - ' . $filePath);

        return ExitCode::OK;
    }

    public function actionOrderToXlsx($orderId)
    {
        $order = Order::find('', false)->andWhere(['id' => $orderId])->one();
        if (is_null($order) === true) {
            $this->log("Заказ с ID {$orderId} не найден");
            return ExitCode::UNSPECIFIED_ERROR;
        }

        $filePath = $order->generateXlsx();
        $this->log('Xlsx файл создан - ' . $filePath);

        return ExitCode::OK;
    }

    public function actionOrderImageArchive()
    {
        $this->log('Создание архивов изображений товаров в заказах');

        $query = ImageArchive::find()
            ->andWhere(['status' => ImageArchive::STATUS_GENERATED]);

        $totalCount = $query->count() ?: 0;
        if ($this->log === true) {
            Console::startProgress(0, $totalCount);
        }

        $iterator = 0;
        /* @var $archive ImageArchive */
        foreach ($query->each() as $archive) {
            $iterator++;
            if ($this->log === true) {
                Console::updateProgress($iterator, $totalCount);
            }
            /* @var $archive Order */
            $order = Order::find('', false)->andWhere(['id' => $archive->order_id])->one();
            if (is_null($order) === true) {
                $archive->delete();
                continue;
            }

            $path = $order->generateImageArchive();
            $archive->saveFileFromPath('file', $path);
            $archive->status = ImageArchive::STATUS_COMPLETED;
            $archive->save();
            unlink($path);
        }

        if ($this->log === true) {
            Console::endProgress();
        }

        $this->log('Закончили.');
        return ExitCode::OK;
    }

    public function actionOrderImageArchiveClear()
    {
        $this->log('Удаление старых архивов с изображениями');

        $query = ImageArchive::find()
            ->andWhere(['<', 'created_at', time() - 86400])
            ->andWhere(['status' => ImageArchive::STATUS_COMPLETED]);

        $totalCount = $query->count() ?: 0;
        if ($this->log === true) {
            Console::startProgress(0, $totalCount);
        }

        $iterator = 0;
        /* @var $archive ImageArchive */
        foreach ($query->each() as $archive) {
            $iterator++;
            if ($this->log === true) {
                Console::updateProgress($iterator, $totalCount);
            }
            $archive->delete();
        }

        if ($this->log === true) {
            Console::endProgress();
        }

        $this->log('Закончили.');
        return ExitCode::OK;
    }

    public function actionCatalogNoImage()
    {
        $this->log('Список товаров без изображений');

        $query = Good::find()
            ->andWhere(['=', 'image', '']);

        $totalCount = $query->count() ?: 0;
        if ($this->log === true) {
            Console::startProgress(0, $totalCount);
        }

        $message = <<<HTML
<h1>Товары без изображений (Всего: {$totalCount})</h1>
HTML;

        $iterator = 0;
        /* @var $good Good */
        foreach ($query->each() as $good) {
            $iterator++;
            if ($this->log === true) {
                Console::updateProgress($iterator, $totalCount);
            }
            $message .= <<<HTML
{$good->vendor_code}<br>
HTML;

        }

        $result = false;
        if ($totalCount > 0) {
            $result = Mailer::send('diamond.union.lp@yandex.ru', 'Товары без изображений', $message);
        }

        if ($this->log === true) {
            Console::endProgress();
        }

        if ($totalCount > 0) {
            if ($result === true) {
                $this->log('Письмо отправлено');
            } else {
                $this->log('Ошибка отправки письма');
            }
        }

        $this->log('Закончили.');
        return ExitCode::OK;
    }

}
