<?php

use yii\db\Migration;

class m201002_140446_order_weight extends Migration
{

    public function safeUp()
    {
        $this->addColumn('{{%order}}', 'good_weight', $this->decimal(11, 2)->after('good_count')->defaultValue(0)->notNull()->comment('Вес товаров'));
        $this->addColumn('{{%order_good}}', 'weight', $this->decimal(11, 2)->after('count')->defaultValue(0)->notNull()->comment('Вес'));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%order_good}}', 'weight');
        $this->dropColumn('{{%order}}', 'good_weight');
    }

}
