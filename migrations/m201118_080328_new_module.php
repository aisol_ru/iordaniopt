<?php

use yii\db\Migration;

class m201118_080328_new_module extends Migration
{

    public function safeUp()
    {

        $this->insert('{{%module}}', [
            'domain_id' => 1,
            'name' => 'Новинки',
            'url' => '',
            'route' => 'site/new-list',
            'tree_id' => 0,
        ]);
    }

    public function safeDown()
    {
        $this->delete('{{%module}}', ['route' => 'site/new-list']);
    }

}
