<?php

use yii\db\Migration;

class m201106_094942_order_image_archive extends Migration
{

    public function safeUp()
    {
        $this->createTable('{{%order_archive}}', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer()->defaultValue(0)->unsigned()->notNull()->comment('Идентификатор заказа'),
            'file' => $this->string(255)->defaultValue('')->notNull()->comment('Файл'),
            'status' => $this->integer(1)->defaultValue(0)->unsigned()->notNull()->comment('Статус'),

            'created_by' => $this->integer(11)->defaultValue(null)->null()->comment('Создатель'),
            'updated_by' => $this->integer(11)->defaultValue(null)->null()->comment('Редактор'),
            'created_at' => $this->integer()->defaultValue(0)->unsigned()->comment('Дата создания'),
            'updated_at' => $this->integer()->defaultValue(0)->unsigned()->comment('Дата изменения'),
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('{{%order_archive}}');
    }

}
