<?php

use yii\db\Migration;

class m201122_124011_favicon extends Migration
{

    public function safeUp()
    {
        $this->addColumn('{{%domain}}', 'favicon', $this->string(255)->after('sitemap')->defaultValue('')->notNull()->comment('Favicon'));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%domain}}', 'favicon');
    }

}
