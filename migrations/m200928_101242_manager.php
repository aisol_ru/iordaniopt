<?php

use yii\db\Migration;

class m200928_101242_manager extends Migration
{

    public function safeUp()
    {
        $this->createTable('{{%manager}}', [
            'id' => $this->primaryKey(),

            'name' => $this->string(255)->defaultValue('')->notNull()->comment('ФИО'),

            'json' => $this->text()->comment('JSON'),

            'created_by' => $this->integer(11)->defaultValue(null)->null()->comment('Создатель'),
            'updated_by' => $this->integer(11)->defaultValue(null)->null()->comment('Редактор'),
            'created_at' => $this->integer(11)->defaultValue(0)->notNull()->comment('Дата создания'),
            'updated_at' => $this->integer(11)->defaultValue(0)->notNull()->comment('Дата изменения'),
        ]);


        $this->createTable('{{%manager_to_user}}', [
            'domain_id' => $this->integer()->defaultValue(null)->notNull()->comment('Идентификатор домена'),
            'manager_id' => $this->integer()->defaultValue(null)->notNull()->comment('Идентификатор менеджера'),
            'user_id' => $this->integer()->defaultValue(null)->notNull()->comment('Идентификатор пользователя'),
        ]);

        $this->addPrimaryKey('manager-to-user_pk', '{{%manager_to_user}}', ['domain_id', 'manager_id', 'user_id']);

        $this->insert('{{%admin_menu}}', [
            'pid' => 0,
            'controller' => 'managers',
            'route' => 'managers/list',
            'title' => 'Менеджеры',
            'isActive' => 1,
            'in_button' => 0,
            'icon_class' => 'icon_nav_structure',
            'sort' => 30,
            'role' => 'manager',

            'created_at' => time(),
            'updated_at' => time(),
        ]);
        $insertId = $this->getDb()->getLastInsertID();

        $this->insert('{{%admin_menu}}', [
            'pid' => $insertId,
            'controller' => 'managers',
            'route' => 'managers/add',
            'title' => 'Менеджер',
            'isActive' => 1,
            'in_button' => 1,
            'icon_class' => 'icon_nav_structure',
            'sort' => 0,
            'role' => 'manager',

            'created_at' => time(),
            'updated_at' => time(),
        ]);
    }

    public function safeDown()
    {
        $this->delete('{{%admin_menu}}', ['route' => 'managers/add']);
        $this->delete('{{%admin_menu}}', ['route' => 'managers/list']);
        $this->dropTable('{{%manager_to_user}}');
        $this->dropTable('{{%manager}}');
    }

}
