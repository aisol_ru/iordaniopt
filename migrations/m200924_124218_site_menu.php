<?php

use yii\db\Migration;

class m200924_124218_site_menu extends Migration
{

    public function safeUp()
    {
        $this->createTable('{{%menu}}', [
            'id' => $this->primaryKey(),
            'domain_id' => $this->integer()->defaultValue(0)->notNull()->comment('Идентификатор домена'),
            'name' => $this->string(255)->defaultValue('')->notNull()->comment('Название'),
            'url' => $this->string(2000)->defaultValue('')->notNull()->comment('URL'),

            'is_premium' => $this->integer(1)->defaultValue(0)->notNull()->comment('Флаг Промо'),
            'show_guest' => $this->integer(1)->defaultValue(0)->notNull()->comment('Флаг Отображения НЕ авторизованным пользователям'),
            'show_user' => $this->integer(1)->defaultValue(0)->notNull()->comment('Флаг Отображения авторизованным пользователям'),

            'sort' => $this->integer()->defaultValue(0)->notNull()->comment('Сортировка'),
            'status' => $this->integer()->defaultValue(0)->notNull()->comment('Статус'),

            'created_by' => $this->integer(11)->defaultValue(null)->null()->comment('Создатель'),
            'updated_by' => $this->integer(11)->defaultValue(null)->null()->comment('Редактор'),
            'created_at' => $this->integer(11)->defaultValue(0)->notNull()->comment('Дата создания'),
            'updated_at' => $this->integer(11)->defaultValue(0)->notNull()->comment('Дата изменения'),
        ]);


        $this->insert('{{%admin_menu}}', [
            'pid' => 0,
            'controller' => 'site-menu',
            'route' => 'site-menu/list',
            'title' => 'Меню',
            'isActive' => 1,
            'in_button' => 0,
            'icon_class' => 'icon_nav_structure',
            'sort' => 200,
            'role' => 'manager',

            'created_at' => time(),
            'updated_at' => time(),
        ]);
    }

    public function safeDown()
    {
        $this->delete('{{%admin_menu}}', ['route' => 'site-menu/list']);
        $this->dropTable('{{%menu}}');
    }

}
