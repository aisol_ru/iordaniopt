<?php

use yii\db\Migration;

class m200930_083005_user_info extends Migration
{

    public function safeUp()
    {
        $this->renameColumn('{{%user}}', 'name', 'first_name');
        $this->addColumn('{{%user}}', 'last_name', $this->string(255)->after('first_name')->defaultValue('')->notNull()->comment('Фамилия'));
        $this->addColumn('{{%user}}', 'middle_name', $this->string(255)->after('last_name')->defaultValue('')->notNull()->comment('Отчество'));
        $this->addColumn('{{%user}}', 'organization', $this->string(255)->after('email')->defaultValue('')->notNull()->comment('Организация'));
        $this->addColumn('{{%user}}', 'inn', $this->string(255)->after('organization')->defaultValue('')->notNull()->comment('ИНН'));
        $this->addColumn('{{%user}}', 'ogrn', $this->string(255)->after('inn')->defaultValue('')->notNull()->comment('ОГРН'));
        $this->addColumn('{{%user}}', 'city', $this->string(255)->after('ogrn')->defaultValue('')->notNull()->comment('Город'));
        $this->addColumn('{{%user}}', 'certificate', $this->string(255)->after('city')->defaultValue('')->notNull()->comment('Сертификат'));
        $this->addColumn('{{%user}}', 'document', $this->string(255)->after('certificate')->defaultValue('')->notNull()->comment('Сертификат'));
    }

    public function safeDown()
    {
        $this->renameColumn('{{%user}}', 'first_name', 'name');

        $this->dropColumn('{{%user}}', 'document');
        $this->dropColumn('{{%user}}', 'certificate');
        $this->dropColumn('{{%user}}', 'city');
        $this->dropColumn('{{%user}}', 'ogrn');
        $this->dropColumn('{{%user}}', 'inn');
        $this->dropColumn('{{%user}}', 'organization');
        $this->dropColumn('{{%user}}', 'middle_name');
        $this->dropColumn('{{%user}}', 'last_name');
    }

}
