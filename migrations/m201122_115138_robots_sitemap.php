<?php

use yii\db\Migration;

class m201122_115138_robots_sitemap extends Migration
{

    public function safeUp()
    {
        $this->addColumn('{{%domain}}', 'robots', $this->string(255)->after('logo_footer')->defaultValue('')->notNull()->comment('Файл robots.txt'));
        $this->addColumn('{{%domain}}', 'sitemap', $this->string(255)->after('robots')->defaultValue('')->notNull()->comment('Файл sitemap.xml'));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%domain}}', 'sitemap');
        $this->dropColumn('{{%domain}}', 'robots');
    }

}
