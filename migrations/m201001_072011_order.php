<?php

use yii\db\Migration;

class m201001_072011_order extends Migration
{

    public function safeUp()
    {
        $this->createTable('{{%order}}', [
            'id' => $this->primaryKey(),
            'domain_id' => $this->integer(11)->defaultValue(0)->notNull()->comment('Идентификатор домена'),
            'number' => $this->integer(11)->defaultValue(null)->null()->unsigned()->comment('Номер'),
            'user_id' => $this->integer(11)->defaultValue(0)->notNull()->comment('Идентификатор пользователя'),
            'status' => $this->integer(1)->defaultValue(0)->unsigned()->notNull()->comment('Статус'),
            'good_count' => $this->integer(11)->defaultValue(0)->unsigned()->notNull()->comment('Количество товаров'),
            'good_sum' => $this->decimal(11, 2)->defaultValue(0)->unsigned()->notNull()->comment('Стоимость товаров'),
            'sum' => $this->decimal(11, 2)->defaultValue(0)->unsigned()->notNull()->comment('Итоговая стоимость'),

            'created_by' => $this->integer(11)->defaultValue(null)->null()->comment('Создатель'),
            'updated_by' => $this->integer(11)->defaultValue(null)->null()->comment('Редактор'),
            'created_at' => $this->integer()->defaultValue(0)->unsigned()->comment('Дата создания'),
            'updated_at' => $this->integer()->defaultValue(0)->unsigned()->comment('Дата изменения'),
        ]);

        $this->createTable('{{%order_good}}', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer()->defaultValue(0)->notNull()->comment('Идентификатор заказа'),
            'good_id' => $this->integer()->defaultValue(0)->notNull()->comment('Идентификатор товара'),
            'price_id' => $this->integer(1)->defaultValue(0)->notNull()->comment('Тип цены'),
            'count' => $this->integer(11)->defaultValue(0)->unsigned()->notNull()->comment('Количество'),
            'sum' => $this->decimal(11, 2)->defaultValue(0)->unsigned()->notNull()->comment('Стоимость'),
            'json' => $this->text()->null()->defaultValue(null)->comment('JSON'),

            'created_by' => $this->integer(11)->defaultValue(null)->null()->comment('Создатель'),
            'updated_by' => $this->integer(11)->defaultValue(null)->null()->comment('Редактор'),
            'created_at' => $this->integer()->defaultValue(0)->unsigned()->comment('Дата создания'),
            'updated_at' => $this->integer()->defaultValue(0)->unsigned()->comment('Дата изменения'),
        ]);

        $this->createTable('{{%order_client}}', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer()->defaultValue(0)->notNull()->comment('Идентификатор заказа'),
            'last_name' => $this->string(255)->defaultValue('')->notNull()->comment('Фамилия'),
            'first_name' => $this->string(255)->defaultValue('')->notNull()->comment('Имя'),
            'middle_name' => $this->string(255)->defaultValue('')->notNull()->comment('Отчество'),
            'phone' => $this->string(255)->defaultValue('')->notNull()->comment('Телефон'),
            'email' => $this->string(255)->defaultValue('')->notNull()->comment('E-mail'),
            'organization' => $this->string(255)->defaultValue('')->notNull()->comment('Организация'),
            'inn' => $this->string(255)->defaultValue('')->notNull()->comment('ИНН'),
            'ogrn' => $this->string(255)->defaultValue('')->notNull()->comment('ОГРН'),
            'city' => $this->string(255)->defaultValue('')->notNull()->comment('Город'),
            'certificate' => $this->string(255)->defaultValue('')->notNull()->comment('Сертификат'),

            'comment' => $this->text()->comment('Комментарий'),

            'created_by' => $this->integer(11)->defaultValue(null)->null()->comment('Создатель'),
            'updated_by' => $this->integer(11)->defaultValue(null)->null()->comment('Редактор'),
            'created_at' => $this->integer()->defaultValue(0)->unsigned()->comment('Дата создания'),
            'updated_at' => $this->integer()->defaultValue(0)->unsigned()->comment('Дата изменения'),
        ]);

        $this->createTable('{{%order_status}}', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer()->defaultValue(0)->notNull()->comment('Идентификатор заказа'),
            'status' => $this->integer(1)->defaultValue(0)->unsigned()->notNull()->comment('Статус'),
            'json' => $this->text()->null()->defaultValue(null)->comment('JSON'),

            'created_by' => $this->integer(11)->defaultValue(null)->null()->comment('Создатель'),
            'updated_by' => $this->integer(11)->defaultValue(null)->null()->comment('Редактор'),
            'created_at' => $this->integer()->defaultValue(0)->unsigned()->comment('Дата создания'),
            'updated_at' => $this->integer()->defaultValue(0)->unsigned()->comment('Дата изменения'),
        ]);
    }


    public function safeDown()
    {
        $this->dropTable('{{%order_status}}');
        $this->dropTable('{{%order_client}}');
        $this->dropTable('{{%order_good}}');
        $this->dropTable('{{%order}}');
    }

}
