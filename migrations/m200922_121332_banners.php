<?php

use yii\db\Migration;

class m200922_121332_banners extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%banner_category}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->defaultValue('')->notNull()->comment('Название'),

            'created_by' => $this->integer(11)->defaultValue(null)->null()->comment('Создатель'),
            'updated_by' => $this->integer(11)->defaultValue(null)->null()->comment('Редактор'),
            'created_at' => $this->integer(11)->defaultValue(0)->notNull()->comment('Дата создания'),
            'updated_at' => $this->integer(11)->defaultValue(0)->notNull()->comment('Дата изменения'),
        ]);

        $this->createTable('{{%banner}}', [
            'id' => $this->primaryKey(),
            'category_id' => $this->integer()->defaultValue(0)->notNull()->comment('Идентификатор категории'),
            'name' => $this->string(255)->defaultValue('')->notNull()->comment('Название'),
            'link' => $this->string(255)->defaultValue('')->notNull()->comment('Ссылка'),
            'image' => $this->string(255)->defaultValue('')->notNull()->comment('Изображение'),
            'is_premium' => $this->integer(1)->unsigned()->defaultValue(0)->notNull()->comment('Флаг премиума'),

            'sort' => $this->integer(11)->defaultValue(0)->notNull()->comment('Сортировка'),
            'status' => $this->integer(11)->defaultValue(0)->notNull()->comment('Статус'),

            'created_by' => $this->integer(11)->defaultValue(null)->null()->comment('Создатель'),
            'updated_by' => $this->integer(11)->defaultValue(null)->null()->comment('Редактор'),
            'created_at' => $this->integer(11)->defaultValue(0)->notNull()->comment('Дата создания'),
            'updated_at' => $this->integer(11)->defaultValue(0)->notNull()->comment('Дата изменения'),
        ]);



        $this->insert('{{%admin_menu}}', [
            'pid' => 0,
            'controller' => 'banners',
            'route' => 'banners/list',
            'title' => 'Баннеры',
            'isActive' => 1,
            'in_button' => 0,
            'icon_class' => 'icon_nav_structure',
            'sort' => 10,
            'role' => 'manager',

            'created_at' => time(),
            'updated_at' => time(),
        ]);
        $insertId = $this->getDb()->getLastInsertID();

        $this->insert('{{%admin_menu}}', [
            'pid' => $insertId,
            'controller' => 'banners',
            'route' => 'banners/category-add',
            'title' => 'Категорию',
            'isActive' => 1,
            'in_button' => 1,
            'icon_class' => 'icon_nav_structure',
            'sort' => 0,
            'role' => 'manager',

            'created_at' => time(),
            'updated_at' => time(),
        ]);

        $this->insert('{{%admin_menu}}', [
            'pid' => $insertId,
            'controller' => 'banners',
            'route' => 'banners/banner-add',
            'title' => 'Баннер',
            'isActive' => 1,
            'in_button' => 1,
            'icon_class' => 'icon_nav_structure',
            'sort' => 10,
            'role' => 'manager',
            'parentName' => 'category_id',

            'created_at' => time(),
            'updated_at' => time(),
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('{{%banner}}');
        $this->dropTable('{{%banner_category}}');

        $this->delete('{{%admin_menu}}', ['route' => 'banners/banner-add']);
        $this->delete('{{%admin_menu}}', ['route' => 'banners/category-add']);
        $this->delete('{{%admin_menu}}', ['route' => 'banners/list']);
    }
}
