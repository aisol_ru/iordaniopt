<?php

use yii\db\Migration;

class m201118_081348_action_module extends Migration
{

    public function safeUp()
    {

        $this->insert('{{%module}}', [
            'domain_id' => 1,
            'name' => 'Акции',
            'url' => '',
            'route' => 'site/action-list',
            'tree_id' => 0,
        ]);
    }

    public function safeDown()
    {
        $this->delete('{{%module}}', ['route' => 'site/action-list']);
    }

}
