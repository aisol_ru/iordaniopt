<?php

use yii\db\Migration;

class m200925_063523_catalog extends Migration
{

    public function safeUp()
    {
        $this->createTable('{{%catalog_category}}', [
            'id' => $this->primaryKey(),

            'outer_code' => $this->string(255)->defaultValue('')->notNull()->comment('Внешний код'),

            'name' => $this->string(255)->defaultValue('')->notNull()->comment('Название'),

            'sort' => $this->integer(1)->defaultValue(0)->notNull()->comment('Сортировка'),
            'status' => $this->integer(1)->defaultValue(0)->notNull()->unsigned()->comment('Статус'),

            'json' => $this->text()->comment('JSON'),

            'created_by' => $this->integer(11)->defaultValue(null)->null()->comment('Создатель'),
            'updated_by' => $this->integer(11)->defaultValue(null)->null()->comment('Редактор'),
            'created_at' => $this->integer(11)->defaultValue(0)->notNull()->comment('Дата создания'),
            'updated_at' => $this->integer(11)->defaultValue(0)->notNull()->comment('Дата изменения'),
        ]);

        $this->createTable('{{%catalog_metal}}', [
            'id' => $this->primaryKey(),

            'outer_code' => $this->string(255)->defaultValue('')->notNull()->comment('Внешний код'),

            'name' => $this->string(255)->defaultValue('')->notNull()->comment('Название'),

            'sort' => $this->integer(1)->defaultValue(0)->notNull()->comment('Сортировка'),

            'created_by' => $this->integer(11)->defaultValue(null)->null()->comment('Создатель'),
            'updated_by' => $this->integer(11)->defaultValue(null)->null()->comment('Редактор'),
            'created_at' => $this->integer(11)->defaultValue(0)->notNull()->comment('Дата создания'),
            'updated_at' => $this->integer(11)->defaultValue(0)->notNull()->comment('Дата изменения'),
        ]);

        $this->createTable('{{%catalog_insert}}', [
            'id' => $this->primaryKey(),

            'outer_code' => $this->string(255)->defaultValue('')->notNull()->comment('Внешний код'),

            'name' => $this->string(255)->defaultValue('')->notNull()->comment('Название'),

            'sort' => $this->integer(1)->defaultValue(0)->notNull()->comment('Сортировка'),

            'created_by' => $this->integer(11)->defaultValue(null)->null()->comment('Создатель'),
            'updated_by' => $this->integer(11)->defaultValue(null)->null()->comment('Редактор'),
            'created_at' => $this->integer(11)->defaultValue(0)->notNull()->comment('Дата создания'),
            'updated_at' => $this->integer(11)->defaultValue(0)->notNull()->comment('Дата изменения'),
        ]);

        $this->createTable('{{%catalog_collection}}', [
            'id' => $this->primaryKey(),

            'outer_code' => $this->string(255)->defaultValue('')->notNull()->comment('Внешний код'),

            'name' => $this->string(255)->defaultValue('')->notNull()->comment('Название'),

            'sort' => $this->integer(1)->defaultValue(0)->notNull()->comment('Сортировка'),

            'created_by' => $this->integer(11)->defaultValue(null)->null()->comment('Создатель'),
            'updated_by' => $this->integer(11)->defaultValue(null)->null()->comment('Редактор'),
            'created_at' => $this->integer(11)->defaultValue(0)->notNull()->comment('Дата создания'),
            'updated_at' => $this->integer(11)->defaultValue(0)->notNull()->comment('Дата изменения'),
        ]);

        $this->createTable('{{%catalog_brand}}', [
            'id' => $this->primaryKey(),

            'outer_code' => $this->string(255)->defaultValue('')->notNull()->comment('Внешний код'),

            'name' => $this->string(255)->defaultValue('')->notNull()->comment('Название'),

            'sort' => $this->integer(1)->defaultValue(0)->notNull()->comment('Сортировка'),

            'created_by' => $this->integer(11)->defaultValue(null)->null()->comment('Создатель'),
            'updated_by' => $this->integer(11)->defaultValue(null)->null()->comment('Редактор'),
            'created_at' => $this->integer(11)->defaultValue(0)->notNull()->comment('Дата создания'),
            'updated_at' => $this->integer(11)->defaultValue(0)->notNull()->comment('Дата изменения'),
        ]);

        $this->createTable('{{%catalog_good}}', [
            'id' => $this->primaryKey(),

            'outer_code' => $this->string(255)->defaultValue('')->notNull()->comment('Внешний код'),

            'category_id' => $this->integer()->defaultValue(0)->notNull()->comment('Идентификатор категории'),
            'alias' => $this->string(255)->defaultValue('')->notNull()->comment('Alias'),
            'name' => $this->string(255)->defaultValue('')->notNull()->comment('Название'),

            'vendor_code' => $this->string(255)->defaultValue('')->notNull()->comment('Артикул'),

            'weight_average' => $this->decimal(11, 2)->defaultValue(0)->notNull()->comment('Средний вес'),
            'price' => $this->decimal(11, 2)->defaultValue(0)->notNull()->comment('Цена'),

            'width' => $this->decimal(11, 2)->defaultValue(0)->notNull()->unsigned()->comment('Ширина'),
            'height' => $this->decimal(11, 2)->defaultValue(0)->notNull()->unsigned()->comment('Высота'),
            'specification' => $this->text()->comment('Спецификация'),
            'view' => $this->string(255)->defaultValue('')->comment('Сопоставление'),
            'brand_id' => $this->integer()->defaultValue(0)->notNull()->comment('Идентификатор бренда'),
            'metal_id' => $this->integer()->defaultValue(0)->notNull()->comment('Идентификатор метала'),
            'insert_id' => $this->integer()->defaultValue(0)->notNull()->comment('Идентификатор вставки'),

            'content' => $this->text()->comment('Текст'),
            'image' => $this->string(255)->defaultValue('')->notNull()->comment('Изображение'),

            'sort' => $this->integer(1)->defaultValue(0)->notNull()->comment('Сортировка'),
            'status' => $this->integer(1)->defaultValue(0)->notNull()->unsigned()->comment('Статус'),

            'seo_title' => $this->text()->comment('SEO Title'),
            'seo_description' => $this->text()->comment('SEO Description'),
            'seo_keywords' => $this->text()->comment('SEO Keywords'),

            'json' => $this->text()->comment('JSON'),

            'created_by' => $this->integer(11)->defaultValue(null)->null()->comment('Создатель'),
            'updated_by' => $this->integer(11)->defaultValue(null)->null()->comment('Редактор'),
            'created_at' => $this->integer(11)->defaultValue(0)->notNull()->comment('Дата создания'),
            'updated_at' => $this->integer(11)->defaultValue(0)->notNull()->comment('Дата изменения'),
        ]);

        $this->createTable('{{%catalog_good_image}}', [
            'id' => $this->primaryKey(),
            'good_id' => $this->integer()->defaultValue(0)->notNull()->comment('Идентификатор товара'),

            'name' => $this->string(255)->defaultValue('')->notNull()->comment('Название'),
            'image' => $this->string(255)->defaultValue('')->notNull()->comment('Изображение'),

            'sort' => $this->integer(1)->defaultValue(0)->notNull()->comment('Сортировка'),

            'created_by' => $this->integer(11)->defaultValue(null)->null()->comment('Создатель'),
            'updated_by' => $this->integer(11)->defaultValue(null)->null()->comment('Редактор'),
            'created_at' => $this->integer(11)->defaultValue(0)->notNull()->comment('Дата создания'),
            'updated_at' => $this->integer(11)->defaultValue(0)->notNull()->comment('Дата изменения'),
        ]);

        $this->createTable('{{%catalog_good_price}}', [
            'id' => $this->primaryKey(),
            'good_id' => $this->integer()->defaultValue(0)->notNull()->comment('Идентификатор товара'),
            'sample' => $this->string(255)->defaultValue('')->notNull()->comment('Проба'),

            'price' => $this->decimal(11, 2)->defaultValue(0)->notNull()->unsigned()->comment('Цена'),

            'created_by' => $this->integer(11)->defaultValue(null)->null()->comment('Создатель'),
            'updated_by' => $this->integer(11)->defaultValue(null)->null()->comment('Редактор'),
            'created_at' => $this->integer(11)->defaultValue(0)->notNull()->comment('Дата создания'),
            'updated_at' => $this->integer(11)->defaultValue(0)->notNull()->comment('Дата изменения'),
        ]);

        $this->createTable('{{%catalog_good_to_collection}}', [
            'good_id' => $this->integer()->defaultValue(null)->notNull()->comment('Идентификатор товара'),
            'collection_id' => $this->integer()->defaultValue(null)->notNull()->comment('Идентификатор коллекции'),
        ]);

        $this->addPrimaryKey('catalog-good-to-collection_pk', '{{%catalog_good_to_collection}}', ['good_id', 'collection_id']);
    }

    public function safeDown()
    {
        $this->dropTable('{{%catalog_good_to_collection}}');
        $this->dropTable('{{%catalog_good_price}}');
        $this->dropTable('{{%catalog_good_image}}');
        $this->dropTable('{{%catalog_good}}');
        $this->dropTable('{{%catalog_brand}}');
        $this->dropTable('{{%catalog_collection}}');
        $this->dropTable('{{%catalog_insert}}');
        $this->dropTable('{{%catalog_metal}}');
        $this->dropTable('{{%catalog_category}}');
    }

}
