<?php

use yii\db\Migration;

class m201002_122243_basket_module extends Migration
{

    public function safeUp()
    {
        $this->insert('{{%module}}', [
            'domain_id' => 1,
            'name' => 'Корзина',
            'url' => '',
            'route' => 'basket/main',
            'tree_id' => 0,
        ]);
    }

    public function safeDown()
    {
        $this->delete('{{%module}}', ['route' => 'basket/main']);
    }

}
