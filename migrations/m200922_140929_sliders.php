<?php

use yii\db\Migration;

class m200922_140929_sliders extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%slider}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->defaultValue('')->notNull()->comment('Название'),

            'created_by' => $this->integer(11)->defaultValue(null)->null()->comment('Создатель'),
            'updated_by' => $this->integer(11)->defaultValue(null)->null()->comment('Редактор'),
            'created_at' => $this->integer(11)->defaultValue(0)->notNull()->comment('Дата создания'),
            'updated_at' => $this->integer(11)->defaultValue(0)->notNull()->comment('Дата изменения'),
        ]);

        $this->createTable('{{%slide}}', [
            'id' => $this->primaryKey(),
            'slider_id' => $this->integer()->defaultValue(0)->notNull()->comment('Идентификатор слайдера'),
            'name' => $this->string(255)->defaultValue('')->notNull()->comment('Название'),
            'link' => $this->string(255)->defaultValue('')->notNull()->comment('Ссылка'),
            'image' => $this->string(255)->defaultValue('')->notNull()->comment('Изображение'),

            'sort' => $this->integer(11)->defaultValue(0)->notNull()->comment('Сортировка'),
            'status' => $this->integer(11)->defaultValue(0)->notNull()->comment('Статус'),

            'created_by' => $this->integer(11)->defaultValue(null)->null()->comment('Создатель'),
            'updated_by' => $this->integer(11)->defaultValue(null)->null()->comment('Редактор'),
            'created_at' => $this->integer(11)->defaultValue(0)->notNull()->comment('Дата создания'),
            'updated_at' => $this->integer(11)->defaultValue(0)->notNull()->comment('Дата изменения'),
        ]);



        $this->insert('{{%admin_menu}}', [
            'pid' => 0,
            'controller' => 'sliders',
            'route' => 'sliders/list',
            'title' => 'Слайдеры',
            'isActive' => 1,
            'in_button' => 0,
            'icon_class' => 'icon_nav_structure',
            'sort' => 20,
            'role' => 'manager',

            'created_at' => time(),
            'updated_at' => time(),
        ]);
        $insertId = $this->getDb()->getLastInsertID();

        $this->insert('{{%admin_menu}}', [
            'pid' => $insertId,
            'controller' => 'sliders',
            'route' => 'sliders/slider-add',
            'title' => 'Слайдер',
            'isActive' => 1,
            'in_button' => 1,
            'icon_class' => 'icon_nav_structure',
            'sort' => 0,
            'role' => 'developer',

            'created_at' => time(),
            'updated_at' => time(),
        ]);

        $this->insert('{{%admin_menu}}', [
            'pid' => $insertId,
            'controller' => 'sliders',
            'route' => 'sliders/slide-add',
            'title' => 'Слайд',
            'isActive' => 1,
            'in_button' => 1,
            'icon_class' => 'icon_nav_structure',
            'sort' => 10,
            'role' => 'manager',
            'parentName' => 'slider_id',

            'created_at' => time(),
            'updated_at' => time(),
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('{{%slider}}');
        $this->dropTable('{{%slide}}');

        $this->delete('{{%admin_menu}}', ['route' => 'sliders/slider-add']);
        $this->delete('{{%admin_menu}}', ['route' => 'sliders/slide-add']);
        $this->delete('{{%admin_menu}}', ['route' => 'sliders/list']);
    }
}
