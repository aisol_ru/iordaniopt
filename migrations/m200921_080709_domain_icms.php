<?php

use yii\db\Migration;

class m200921_080709_domain_icms extends Migration
{

    public function safeUp()
    {

        $this->insert('{{%admin_menu}}', [
            'pid' => 0,
            'controller' => 'domains',
            'route' => 'domains/list',
            'title' => 'Домены',
            'isActive' => 1,
            'in_button' => 0,
            'icon_class' => 'icon_nav_structure',
            'sort' => 100,
            'role' => 'admin',

            'created_at' => time(),
            'updated_at' => time(),
        ]);
        $insertId = $this->getDb()->getLastInsertID();

        $this->insert('{{%admin_menu}}', [
            'pid' => $insertId,
            'controller' => 'domains',
            'route' => 'domains/add',
            'title' => 'Домен',
            'isActive' => 1,
            'in_button' => 1,
            'icon_class' => 'icon_nav_structure',
            'sort' => 0,
            'role' => 'developer',

            'created_at' => time(),
            'updated_at' => time(),
        ]);
    }

    public function safeDown()
    {
        $this->delete('{{%admin_menu}}', ['route' => 'domains/add']);
        $this->delete('{{%admin_menu}}', ['route' => 'domains/list']);
    }

}
