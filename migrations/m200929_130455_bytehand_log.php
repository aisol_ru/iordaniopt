<?php

use yii\db\Migration;

class m200929_130455_bytehand_log extends Migration
{

    public function safeUp()
    {
        $this->createTable('{{%log_bytehand}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->comment('Подпись отправителя'),
            'request' => $this->text()->comment('Тело запроса'),
            'response' => $this->text()->comment('Тело ответа'),
            'datetime' => $this->integer()->unsigned()->notNull()->comment('Время отправки'),
            'created_at' => $this->integer()->unsigned()->comment('Дата создания'),
            'updated_at' => $this->integer()->unsigned()->comment('Дата изменения'),
        ]);

    }

    public function safeDown()
    {
        $this->dropTable('{{%log_bytehand}}');
    }

}
