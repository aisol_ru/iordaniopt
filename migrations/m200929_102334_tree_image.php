<?php

use yii\db\Migration;

class m200929_102334_tree_image extends Migration
{

    public function safeUp()
    {
        $this->addColumn('{{%tree}}', 'image', $this->string(255)->after('nofollow')->defaultValue('')->notNull()->comment('Изображение'));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%tree}}', 'image');
    }

}
