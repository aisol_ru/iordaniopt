<?php

use yii\db\Migration;

class m200923_082341_module_page_domain extends Migration
{

    public function safeUp()
    {
        $this->addColumn('{{%tree}}', 'domain_id', $this->integer(11)->after('id')->defaultValue(0)->notNull()->comment('Идентификатор домена'));
        $this->addColumn('{{%module}}', 'domain_id', $this->integer(11)->after('id')->defaultValue(0)->notNull()->comment('Идентификатор домена'));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%module}}', 'domain_id');
        $this->dropColumn('{{%tree}}', 'domain_id');
    }

}
