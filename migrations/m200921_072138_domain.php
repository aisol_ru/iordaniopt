<?php

use yii\db\Migration;

class m200921_072138_domain extends Migration
{

    public function safeUp()
    {
        $this->createTable('{{%domain}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->defaultValue('')->notNull()->comment('Название'),
            'domain' => $this->string(255)->defaultValue('')->notNull()->comment('Домен'),
            'logo_preloader' => $this->string(255)->defaultValue('')->notNull()->comment('Логотип - прелоадер'),
            'logo_header' => $this->string(255)->defaultValue('')->notNull()->comment('Логотип - хедер'),
            'logo_footer' => $this->string(255)->defaultValue('')->notNull()->comment('Логотип - футер'),

            'json' => $this->text()->comment('JSON'),

            'created_by' => $this->integer(11)->defaultValue(null)->null()->comment('Создатель'),
            'updated_by' => $this->integer(11)->defaultValue(null)->null()->comment('Редактор'),
            'created_at' => $this->integer(11)->defaultValue(0)->notNull()->comment('Дата создания'),
            'updated_at' => $this->integer(11)->defaultValue(0)->notNull()->comment('Дата изменения'),
        ]);

        $this->insert('{{%domain}}', [
            'id' => 1,
            'name' => 'Diamond Union',
            'domain' => 'diamond.local',
            'json' => '{"class": "diamond-site"}',
            'created_by' => 4,
            'updated_by' => 4,
            'created_at' => time(),
            'updated_at' => time(),
        ]);
        $this->insert('{{%domain}}', [
            'id' => 2,
            'name' => 'Alvada',
            'domain' => 'alvada.local',
            'json' => '{"class": "alvada-site"}',
            'created_by' => 4,
            'updated_by' => 4,
            'created_at' => time(),
            'updated_at' => time(),
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('{{%domain}}');
    }

}
