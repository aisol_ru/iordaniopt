<?php

use yii\db\Migration;

class m201105_063948_user_favorite extends Migration
{

    public function safeUp()
    {
        $this->createTable('{{%user_favorite}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->defaultValue(0)->notNull()->unsigned()->comment('Идентификатор пользователя'),
            'session' => $this->string(64)->defaultValue('')->notNull()->comment('Идентификатор сессии'),
            'link_id' => $this->integer()->defaultValue(0)->notNull()->unsigned()->comment('Идентификатор товара'),

            'created_at' => $this->integer(11)->defaultValue(0)->notNull()->comment('Дата создания'),
            'updated_at' => $this->integer(11)->defaultValue(0)->notNull()->comment('Дата изменения'),
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('{{%user_favorite}}');
    }

}
