<?php

use yii\db\Migration;

class m200923_080712_banner_domain_id extends Migration
{

    public function safeUp()
    {
        $this->addColumn('{{%banner}}', 'domain_id', $this->integer(11)->after('id')->defaultValue(0)->notNull()->comment('Идентификатор домена'));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%banner}}', 'domain_id');
    }

}
