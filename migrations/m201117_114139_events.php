<?php

use yii\db\Migration;

class m201117_114139_events extends Migration
{

    public function safeUp()
    {
        $this->createTable('{{%event}}', [
            'id' => $this->primaryKey(),
            'domain_id' => $this->integer(11)->defaultValue(0)->notNull()->comment('Идентификатор домена'),
            'name' => $this->string(255)->defaultValue('')->notNull()->comment('Название'),
            'image' => $this->string(255)->defaultValue('')->notNull()->comment('Изображение'),
            'link' => $this->string(500)->defaultValue('')->notNull()->comment('Ссылка'),

            'sort' => $this->integer()->defaultValue(0)->notNull()->comment('Сортировка'),
            'status' => $this->integer()->defaultValue(0)->notNull()->comment('Статус'),

            'created_by' => $this->integer(11)->defaultValue(null)->null()->comment('Создатель'),
            'updated_by' => $this->integer(11)->defaultValue(null)->null()->comment('Редактор'),
            'created_at' => $this->integer()->defaultValue(0)->unsigned()->comment('Дата создания'),
            'updated_at' => $this->integer()->defaultValue(0)->unsigned()->comment('Дата изменения'),
        ]);


        $this->insert('{{%admin_menu}}', [
            'pid' => 0,
            'controller' => 'events',
            'route' => 'events/list',
            'title' => 'События',
            'isActive' => 1,
            'in_button' => 0,
            'icon_class' => 'icon_nav_structure',
            'sort' => 260,
            'role' => 'manager',

            'created_at' => time(),
            'updated_at' => time(),
        ]);
        $parentId = $this->getDb()->getLastInsertID();
        $this->insert('{{%admin_menu}}', [
            'pid' => $parentId,
            'controller' => 'events',
            'route' => 'events/add',
            'title' => 'Событие',
            'isActive' => 1,
            'in_button' => 1,
            'icon_class' => 'icon_nav_structure',
            'sort' => 0,
            'role' => 'manager',

            'created_at' => time(),
            'updated_at' => time(),
        ]);

        $this->insert('{{%module}}', [
            'domain_id' => 1,
            'name' => 'События',
            'url' => '',
            'route' => 'site/event-list',
            'tree_id' => 0,
        ]);
    }

    public function safeDown()
    {
        $this->delete('{{%module}}', ['route' => 'site/event-list']);
        $this->delete('{{%admin_menu}}', ['route' => 'events/add']);
        $this->delete('{{%admin_menu}}', ['route' => 'events/list']);
        $this->dropTable('{{%event}}');
    }

}
