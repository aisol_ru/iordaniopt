<?php

use yii\db\Migration;

class m201118_082802_brand extends Migration
{

    public function safeUp()
    {
        $this->createTable('{{%brand}}', [
            'id' => $this->primaryKey(),
            'domain_id' => $this->integer(11)->defaultValue(0)->notNull()->comment('Идентификатор домена'),
            'content' => $this->text()->comment('Контент'),
            'image' => $this->string(255)->defaultValue('')->notNull()->comment('Изображение'),

            'sort' => $this->integer()->defaultValue(0)->notNull()->comment('Сортировка'),
            'status' => $this->integer()->defaultValue(0)->notNull()->comment('Статус'),

            'created_by' => $this->integer(11)->defaultValue(null)->null()->comment('Создатель'),
            'updated_by' => $this->integer(11)->defaultValue(null)->null()->comment('Редактор'),
            'created_at' => $this->integer()->defaultValue(0)->unsigned()->comment('Дата создания'),
            'updated_at' => $this->integer()->defaultValue(0)->unsigned()->comment('Дата изменения'),
        ]);


        $this->insert('{{%admin_menu}}', [
            'pid' => 0,
            'controller' => 'brands',
            'route' => 'brands/list',
            'title' => 'Бренд',
            'isActive' => 1,
            'in_button' => 0,
            'icon_class' => 'icon_nav_structure',
            'sort' => 270,
            'role' => 'manager',

            'created_at' => time(),
            'updated_at' => time(),
        ]);
        $parentId = $this->getDb()->getLastInsertID();
        $this->insert('{{%admin_menu}}', [
            'pid' => $parentId,
            'controller' => 'brands',
            'route' => 'brands/add',
            'title' => 'Запись',
            'isActive' => 1,
            'in_button' => 1,
            'icon_class' => 'icon_nav_structure',
            'sort' => 0,
            'role' => 'manager',

            'created_at' => time(),
            'updated_at' => time(),
        ]);

        $this->insert('{{%module}}', [
            'domain_id' => 1,
            'name' => 'Бренд',
            'url' => '',
            'route' => 'site/brand',
            'tree_id' => 0,
        ]);
    }

    public function safeDown()
    {
        $this->delete('{{%module}}', ['route' => 'site/brand']);
        $this->delete('{{%admin_menu}}', ['route' => 'brands/add']);
        $this->delete('{{%admin_menu}}', ['route' => 'brands/list']);
        $this->dropTable('{{%brand}}');
    }

}
