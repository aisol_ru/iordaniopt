<?php

use yii\db\Migration;

class m200925_115208_domain_brand_id extends Migration
{

    public function safeUp()
    {
        $this->addColumn('{{%domain}}', 'brand_id', $this->integer()->after('domain')->defaultValue(0)->notNull()->comment('Идентификатор домена'));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%domain}}', 'brand_id');
    }

}
