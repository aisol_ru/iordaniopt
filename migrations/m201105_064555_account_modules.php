<?php

use yii\db\Migration;

class m201105_064555_account_modules extends Migration
{

    public function safeUp()
    {
        $this->insert('{{%module}}', [
            'domain_id' => 1,
            'name' => 'Аккаунт',
            'url' => '',
            'route' => 'account/main',
            'tree_id' => 0,
        ]);
        $this->insert('{{%module}}', [
            'domain_id' => 1,
            'name' => 'Аккаунт - Персональные данные',
            'url' => '',
            'route' => 'account/personal-data',
            'tree_id' => 0,
        ]);
        $this->insert('{{%module}}', [
            'domain_id' => 1,
            'name' => 'Аккаунт - Заказы',
            'url' => '',
            'route' => 'account/order-list',
            'tree_id' => 0,
        ]);
        $this->insert('{{%module}}', [
            'domain_id' => 1,
            'name' => 'Аккаунт - Отложенные украшения',
            'url' => '',
            'route' => 'account/favorite-list',
            'tree_id' => 0,
        ]);
    }

    public function safeDown()
    {
        $this->delete('{{%module}}', ['route' => 'account/favorite-list']);
        $this->delete('{{%module}}', ['route' => 'account/order-list']);
        $this->delete('{{%module}}', ['route' => 'account/personal-data']);
        $this->delete('{{%module}}', ['route' => 'account/main']);
    }

}
