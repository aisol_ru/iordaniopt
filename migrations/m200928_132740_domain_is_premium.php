<?php

use yii\db\Migration;

class m200928_132740_domain_is_premium extends Migration
{

    public function safeUp()
    {
        $this->addColumn('{{%domain}}', 'is_premium', $this->integer(1)->after('logo_footer')->defaultValue(0)->notNull()->comment('Флаг премиальности'));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%domain}}', 'is_premium');
    }

}
