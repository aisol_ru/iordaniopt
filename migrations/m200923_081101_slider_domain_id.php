<?php

use yii\db\Migration;

class m200923_081101_slider_domain_id extends Migration
{

    public function safeUp()
    {
        $this->addColumn('{{%slide}}', 'domain_id', $this->integer(11)->after('id')->defaultValue(0)->notNull()->comment('Идентификатор домена'));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%slide}}', 'domain_id');
    }

}
