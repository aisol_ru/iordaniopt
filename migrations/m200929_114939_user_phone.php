<?php

use yii\db\Migration;

class m200929_114939_user_phone extends Migration
{

    public function safeUp()
    {
        $this->addColumn('{{%user}}', 'phone', $this->string(255)->after('name')->defaultValue('')->notNull()->comment('Телефон'));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%user}}', 'phone');
    }

}
