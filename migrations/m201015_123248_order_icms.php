<?php

use yii\db\Migration;

class m201015_123248_order_icms extends Migration
{

    public function safeUp()
    {
        $this->insert('{{%admin_menu}}', [
            'pid' => 0,
            'controller' => 'orders',
            'route' => 'orders/list',
            'title' => 'Заказы',
            'isActive' => 1,
            'in_button' => 0,
            'icon_class' => 'icon_nav_structure',
            'sort' => 250,
            'role' => 'manager',

            'created_at' => time(),
            'updated_at' => time(),
        ]);
    }

    public function safeDown()
    {
        $this->delete('{{%admin_menu}}', ['route' => 'orders/list']);
    }

}
