<?php

use yii\db\Migration;

class m200924_121124_parameter_domain extends Migration
{

    public function safeUp()
    {
        $this->addColumn('{{%parameter}}', 'group', $this->integer()->after('id')->defaultValue(0)->unsigned()->notNull()->comment('Группа'));
        $this->addColumn('{{%parameter}}', 'domain_id', $this->integer()->after('id')->defaultValue(0)->unsigned()->notNull()->comment('Идентификатор домена'));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%parameter}}', 'domain_id');
        $this->dropColumn('{{%parameter}}', 'group');
    }

}
