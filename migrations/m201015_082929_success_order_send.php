<?php

use yii\db\Migration;

class m201015_082929_success_order_send extends Migration
{

    public function safeUp()
    {
        $this->insert('{{%module}}', [
            'domain_id' => 1,
            'name' => 'Корзина - успешное оформление заказа',
            'url' => '',
            'route' => 'basket/success',
            'tree_id' => 0,
        ]);

        $this->addColumn('{{%order}}', 'hash', $this->string(32)->after('number')->defaultValue('')->notNull()->comment('Хэш заказа'));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%order}}', 'hash');
        $this->delete('{{%module}}', ['route' => 'basket/success']);
    }

}
