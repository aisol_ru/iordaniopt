<?php

use yii\db\Migration;

class m201117_122821_contacts extends Migration
{

    public function safeUp()
    {
        $this->createTable('{{%contact}}', [
            'id' => $this->primaryKey(),
            'domain_id' => $this->integer(11)->defaultValue(0)->notNull()->comment('Идентификатор домена'),

            'address' => $this->string(255)->defaultValue('')->notNull()->comment('Адрес'),
            'working_hours' => $this->string(255)->defaultValue('')->notNull()->comment('Режим работы'),
            'phone' => $this->string(255)->defaultValue('')->notNull()->comment('Телефон'),
            'email' => $this->string(255)->defaultValue('')->notNull()->comment('Email'),
            'map_id' => $this->integer()->defaultValue(0)->unsigned()->comment('Email'),

            'created_by' => $this->integer(11)->defaultValue(null)->null()->comment('Создатель'),
            'updated_by' => $this->integer(11)->defaultValue(null)->null()->comment('Редактор'),
            'created_at' => $this->integer()->defaultValue(0)->unsigned()->comment('Дата создания'),
            'updated_at' => $this->integer()->defaultValue(0)->unsigned()->comment('Дата изменения'),
        ]);

        $this->createTable('{{%contact_social}}', [
            'id' => $this->primaryKey(),
            'domain_id' => $this->integer(11)->defaultValue(0)->notNull()->comment('Идентификатор домена'),

            'name' => $this->string(255)->defaultValue('')->notNull()->comment('Название'),
            'link' => $this->string(255)->defaultValue('')->notNull()->comment('Ссылка'),
            'image' => $this->string(255)->defaultValue('')->notNull()->comment('Изображение'),

            'sort' => $this->integer()->defaultValue(0)->notNull()->comment('Сортировка'),
            'status' => $this->integer()->defaultValue(0)->notNull()->comment('Статус'),

            'created_by' => $this->integer(11)->defaultValue(null)->null()->comment('Создатель'),
            'updated_by' => $this->integer(11)->defaultValue(null)->null()->comment('Редактор'),
            'created_at' => $this->integer()->defaultValue(0)->unsigned()->comment('Дата создания'),
            'updated_at' => $this->integer()->defaultValue(0)->unsigned()->comment('Дата изменения'),
        ]);


        $this->insert('{{%module}}', [
            'domain_id' => 1,
            'name' => 'Контакты',
            'url' => '',
            'route' => 'site/contact',
            'tree_id' => 0,
        ]);

    }

    public function safeDown()
    {
        $this->delete('{{%module}}', ['route' => 'site/contact']);
        $this->dropTable('{{%contact_social}}');
        $this->dropTable('{{%contact}}');
    }

}
