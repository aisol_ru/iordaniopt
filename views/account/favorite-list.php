<?php
/* @var $this \app\components\View */
/* @var $goods \app\models\catalog\Good[] */
/* @var $filter \app\forms\CatalogFilter */
/* @var $sort \yii\data\Sort */
/* @var $order \app\models\order\Order */
/* @var $pagination \yii\data\Pagination */

use app\assets\MainAsset;
use app\models\user\Favorite;
use app\widgets\SortSelect;
use yii\helpers\Url;

?>

<?php if (empty($goods) === false) { ?>
    <section class="catalog-section">
        <div class="catalog-section-wrap page-wrapper">
            <div class="catalog-sort-row">
                <div class="sr-basket-list">
                    <div class="sr-basket-info-unit">
                        <div class="sr-info-title">Отложено</div>
                        <span><?= Favorite::getCount() ?> шт.</span>
                    </div>
                    <div class="sr-basket-info-unit">
                        <button class="btn btn-white js-user-favorite-flush-button">Очистить список</button>
                    </div>
                </div>
                <div class="catalog-sort-unit-wrap">
                    <div class="mobile-filter-btn js-mobile-filter-btn">
                        <img src="<?= MainAsset::path('img/filter-ico.svg') ?>" alt="">
                        <span>Фильтр <div class="mobile-filter-counter active"><?= $pagination->totalCount ?></div></span>
                    </div>
                    <div class="for-mobile-search-row">
                        <div class="filter-search-block">
                            <div class="input-row">
                                <input type="text" placeholder="Поиск по каталогу">
                                <label for=""></label>
                                <img src="<?= MainAsset::path('img/search-icon.svg') ?>" alt="">
                            </div>
                        </div>
                    </div>
                    <div class="catalog-sort-unit">
                        Сортировать:
                        <?= SortSelect::widget(['sort' => $sort, 'id' => 'catalog-list-sort']) ?>
                    </div>
                </div>
            </div>
            <div class="catalog-list-filter-panel">
                <div class="catalog-filter-col">
                    <?= \app\widgets\catalog\Filter::widget([
                            'filter' => $filter,
                            'route' => ['account/favorite-list'],
                            'parametersQuery' => Favorite::find()
                                ->select(['link_id'])
                                ->andWhere(['user_id' => Yii::$app->getUser()->getId()]),
                    ]) ?>
                </div>
                <div class="catalog-list-col">
                    <div class="catalog-list js-catalog-list">
                        <?php foreach ($goods as $good) { ?>
                            <?= \app\widgets\catalog\Good::widget(['good' => $good, 'urlAddScheme' => true]) ?>
                        <?php } ?>
                    </div>
                    <?php if ($pagination->pageCount > 1) { ?>
                        <div class="btn-row">
                            <button
                                type="button"
                                class="btn show-more-btn btn-ly js-catalog-pagination"
                                data-page="<?= $pagination->page + 1 ?>"
                                data-total-page="<?= $pagination->pageCount ?>"
                                data-page-param="<?= $pagination->pageParam ?>"
                                data-sort-param="<?= $sort->sortParam ?>"
                            >Показать еще</button>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </section>
<?php } else { ?>
    <section class="text-panel-section">

        <div class="text-panel-wrap page-wrapper">
            <div class="catalog-item-title-row">
                <div class="section-title"><?= $this->h1 ?></div>
            </div>
            <div class="text-panel">
                <div class="notification">
                    У Ваш нет отложенных товаров. Чтобы их добавить, воспользуйтесь <a href="<?= Url::to(['catalog/list']) ?>">каталогом</a>
                </div>
            </div>
        </div>
    </section>
<?php } ?>
