<?php
/* @var $this \app\components\View */
/* @var $orders Order[] */
/* @var $domains Domain[] */

use app\components\IcmsHelper;
use app\models\Domain;
use app\models\order\ImageArchive;
use app\models\order\Order;
use yii\helpers\Url;

?>


<section class="account-section">
    <div class="account-section-wrap page-wrapper">
        <div class="catalog-item-title-row">
            <div class="section-title">Заказы</div>
        </div>
        <div class="account-panel">
            <?= \app\widgets\AccountMenu::widget() ?>
            <div class="account-content-col">
                <div class="account-order-list">
                  <?php if(!empty($orders)) { ?>
                    <?php foreach ($orders as $order) { ?>
                        <div class="account-order-unit <?= $order->domain_id == Domain::DOMAIN_ALVADA ? 'aou-alvada' : '' ?>">
                            <div class="order-number-row">
                                <div class="order-status"><?= Order::getStatuses(true)[$order->status] ?></div>
                                <div class="order-number">
                                    Заказ № <?= $order->number ?>
                                    <?php if ($order->domain_id == Domain::DOMAIN_ALVADA) { ?>
                                        <span class="aou-alvada-label">Alvada</span>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="account-order-unit-inner-wrap">
                                <div class="account-order-unit-inner-list">
                                    <div class="account-order-unit-inner-list-unit">
                                        <div class="aou-inlist-title">Дата создания:</div>
                                        <div class="aou-inlist-val"><?= IcmsHelper::dateTimeFormat('d.m.Y', $order->getHistoryStatuses()->andWhere(['status' => Order::STATUS_NEW])->one()->created_at) ?></div>
                                    </div>
                                    <div class="account-order-unit-inner-list-unit">
                                        <div class="aou-inlist-title">Кол-во:</div>
                                        <div class="aou-inlist-val"><?= $order->good_count ?> шт.</div>
                                    </div>
                                    <div class="account-order-unit-inner-list-unit">
                                        <div class="aou-inlist-title">Общий вес:</div>
                                        <div class="aou-inlist-val"><?= number_format($order->good_weight, 2, '.', ' ') ?> гр.</div>
                                    </div>
                                    <div class="account-order-unit-inner-list-unit">
                                        <div class="aou-inlist-title">Сумма заказа:</div>
                                        <div class="aou-inlist-val"><?= number_format($order->sum, 0, '.', ' ') ?> р.</div>
                                    </div>
                                </div>
                                <div class="btn-row">
                                    <a href="//<?= $domains[$order->domain_id]->domain ?><?= Url::to(['account/order-view', 'number' => $order->number]) ?>" class="btn btn-ly">Подробнее о заказе</a>
                                    <button
                                            <?= $order->canRepeat() === false ? 'disabled' : '' ?>
                                            type="button"
                                            class="btn js-order-repeat"
                                            data-number="<?= $order->number ?>"
                                    >Повторить</button>
                                    <button
                                            data-number="<?= $order->number ?>"
                                            type="button"
                                            class="btn btn-preloader js-order-image-archive"
                                            data-status="<?= is_null($order->imageArchive) === false ? $order->imageArchive->status : '0' ?>"
                                            <?= is_null($order->imageArchive) === false && $order->imageArchive->status == ImageArchive::STATUS_GENERATED ? 'disabled' : '' ?>
                                            data-file="<?= is_null($order->imageArchive) === false && $order->imageArchive->status == ImageArchive::STATUS_COMPLETED ? $order->imageArchive->getPath('file')  : '' ?>"
                                    >
                                        <span>
                                            Скачать фото
                                        </span>
                                        <svg <?= is_null($order->imageArchive) === true || $order->imageArchive->status != ImageArchive::STATUS_GENERATED ? 'style="display:none;"' : '' ?> class="btn-preloader-animate js-btn-preloader" xmlns:svg="http://www.w3.org/2000/svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.0" width="24px" height="24px" viewBox="0 0 128 128" xml:space="preserve"><g><circle cx="16" cy="64" r="16" fill="#ffffff" fill-opacity="1"/><circle cx="16" cy="64" r="14.344" fill="#ffffff" fill-opacity="1" transform="rotate(45 64 64)"/><circle cx="16" cy="64" r="12.531" fill="#ffffff" fill-opacity="1" transform="rotate(90 64 64)"/><circle cx="16" cy="64" r="10.75" fill="#ffffff" fill-opacity="1" transform="rotate(135 64 64)"/><circle cx="16" cy="64" r="10.063" fill="#ffffff" fill-opacity="1" transform="rotate(180 64 64)"/><circle cx="16" cy="64" r="8.063" fill="#ffffff" fill-opacity="1" transform="rotate(225 64 64)"/><circle cx="16" cy="64" r="6.438" fill="#ffffff" fill-opacity="1" transform="rotate(270 64 64)"/><circle cx="16" cy="64" r="5.375" fill="#ffffff" fill-opacity="1" transform="rotate(315 64 64)"/><animateTransform attributeName="transform" type="rotate" values="0 64 64;315 64 64;270 64 64;225 64 64;180 64 64;135 64 64;90 64 64;45 64 64" calcMode="discrete" dur="1040ms" repeatCount="indefinite"></animateTransform></g></svg>
                                    </button>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                  <?php }else{ ?>
                    <h4>Заказов нет</h4>
                  <?php } ?>
                </div>
            </div>
        </div>
    </div>
</section>
