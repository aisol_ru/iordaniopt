<?php

/* @var $this \app\components\View */
/* @var $order \app\models\order\Order */
/* @var $goods \app\models\order\Good[] */

use app\forms\CatalogFilter;
use yii\helpers\Html;
use yii\helpers\Url;

?>

<section class="basket-section">
    <div class="basket-section-wrap page-wrapper">
        <div class="catalog-item-title-row">
            <a href="<?= Url::to(['account/order-list']) ?>" class="title-row-getback-link">К заказам</a>
            <div class="section-title">Заказ № <?= $order->number ?></div>
        </div>
        <div class="basket-panel">
            <div class="basket-list-block">
                <div class="basket-list-th-row">
                    <div class="basket-list-unit blu-photo">Фото</div>
                    <div class="basket-list-unit blu-spec">Характеристики</div>
                    <div class="basket-list-unit blu-quant">Количество</div>
                    <div class="basket-list-unit blu-weight">Вес</div>
                    <div class="basket-list-unit blu-price">Цена</div>
                    <div class="basket-list-unit blu-summ">Сумма</div>
                    <div class="basket-list-unit blu-btns"></div>
                </div>
                <div class="basket-mobile-top-text-row">Украшения в корзине</div>
                <div class="basket-td-row-list">
                    <?php foreach ($goods as $good) { ?>
                        <div class="basket-td-row">
                            <div class="basket-list-unit blu-photo">
                                <?php if (is_null($good->good) === false && empty($good->good->image) === false) { ?>
                                    <img
                                            src="http://<?= $order->domain->domain ?><?= $good->good->getResizeCache('image', 100, 100, 5) ?>"
                                            alt="<?= Html::encode($good->jsonData['name']) ?>"
                                    >
                                <?php } ?>
                            </div>
                            <div class="basket-list-unit blu-spec">
                                <div class="for-mobile-blu-title">Характеристики</div>
                                <div class="catalog-item-scu-table">
                                    <div class="catalog-item-scu-row">
                                        <div class="catalog-item-scu-name">Наименование</div>
                                        <div class="catalog-item-scu-val">
                                            <?php if ($good->canRepeat() === true) { ?>
                                                <a href="<?= Url::to(['catalog/good', 'alias' => $good->jsonData['attributes']['alias']]) ?>">
                                                    <?= $good->jsonData['name'] ?>
                                                </a>
                                            <?php } else { ?>
                                                <?= $good->jsonData['name'] ?>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <?php if ($good->canRepeat() === true && empty($good->good->collections) === false) { ?>
                                        <div class="catalog-item-scu-row">
                                            <div class="catalog-item-scu-name">Коллекция</div>
                                            <div class="catalog-item-scu-val">
                                                <?php foreach ($good->good->collections as $collection) { ?>
                                                    <a href="<?= Url::to(['catalog/list', (new CatalogFilter())->formName() => ['collections' => [$collection->id]]]) ?>"><?= $collection->name ?></a>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <?php if (empty($good->jsonData['attributes']['vendor_code']) === false) { ?>
                                        <div class="catalog-item-scu-row">
                                            <div class="catalog-item-scu-name">Артикул</div>
                                            <div class="catalog-item-scu-val"><?= $good->jsonData['attributes']['vendor_code'] ?></div>
                                        </div>
                                    <?php } ?>
                                    <?php if ($good->jsonData['attributes']['weight_average'] > 0) { ?>
                                        <div class="catalog-item-scu-row">
                                            <div class="catalog-item-scu-name">Средний вес</div>
                                            <div class="catalog-item-scu-val"><?= $good->jsonData['attributes']['weight_average'] ?> г.</div>
                                        </div>
                                    <?php } ?>
                                    <?php if (empty($good->getMetalName()) === false) { ?>
                                        <div class="catalog-item-scu-row">
                                            <div class="catalog-item-scu-name">Цвет металла</div>
                                            <div class="catalog-item-scu-val"><?= $good->getMetalName() ?></div>
                                        </div>
                                    <?php } ?>
                                    <?php if ($good->jsonData['attributes']['width'] > 0 && $good->jsonData['attributes']['height'] > 0) { ?>
                                        <div class="catalog-item-scu-row">
                                            <div class="catalog-item-scu-name">Габаритные размеры</div>
                                            <div class="catalog-item-scu-val"><?= $good->jsonData['attributes']['width'] ?>х<?= $good->jsonData['attributes']['height'] ?></div>
                                        </div>
                                    <?php } ?>
                                    <?php if (empty($good->jsonData['attributes']['specification']) === false) { ?>
                                        <div class="catalog-item-scu-row">
                                            <div class="catalog-item-scu-name">Основная спецификация</div>
                                            <div class="catalog-item-scu-val"><?= $good->jsonData['attributes']['specification'] ?></div>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="basket-list-unit blu-quant">
                                <div class="for-mobile-blu-title">Количество</div>
                                <?php if (empty($good->jsonData['sizes']) === false) { ?>
                                    <div class="catalog-item-scu-table">
                                        <?php foreach ($good->jsonData['sizes'] as $size) {
                                            if ($size['count'] == 0) {
                                                continue;
                                            }
                                            ?>
                                            <div class="catalog-item-scu-row">
                                                <div class="catalog-item-scu-name"><?= number_format($size['size'], 1, '.', '') ?></div>
                                                <div class="catalog-item-scu-val"><?= $size['count'] ?> шт.</div>
                                            </div>
                                        <?php } ?>
                                        <div class="catalog-item-scu-row basket-total-quant-row">
                                            <div class="catalog-item-scu-name">Итого</div>
                                            <div class="catalog-item-scu-val"><?= $good->count ?> шт.</div>
                                        </div>
                                    </div>
                                <?php } else { ?>
                                    <?= $good->count ?> шт.
                                <?php } ?>
                            </div>
                            <div class="basket-list-unit blu-weight">
                                <div class="for-mobile-blu-title">Вес</div>
                                <p><?= number_format($good->weight, 2, '.', ' ') ?> г</p>
                            </div>
                            <div class="basket-list-unit blu-price">
                                <div class="for-mobile-blu-title">Цена</div>
                                <p><?= number_format($good->jsonData['price_unit'], 0, '.', ' ') ?> р.</p>
                            </div>
                            <div class="basket-list-unit blu-summ">
                                <div class="for-mobile-blu-title">Сумма</div>
                                <p><?= number_format($good->sum, 0, '.', ' ') ?> р.</p>
                            </div>
                            <div class="basket-list-unit blu-btns">
                                <div class="btn-row">
                                    <button type="button" class="btn btn-ly js-order-repeat-good"
                                        <?= $good->canRepeat() === false ? 'disabled' : '' ?>
                                        data-id="<?= $good->good_id ?>"
                                        data-price-id="<?= $good->price_id ?>"
                                        data-count='<?= isset($good->jsonData['sizes']) === true ? json_encode($good->jsonData['sizes']) : $good->count ?>'
                                    >В новый заказ</button>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
                <div class="basket-list-total-row o-total-row">
                    <div class="basket-list-total-block">
                        <div class="blt-unit">Итого <span><?= $order->good_count ?> шт</span></div>
                        <div class="blt-unit">Общий вес <span><?= number_format($order->good_weight, 2, '.', ' ') ?> гр.</span></div>
                        <div class="blt-unit">Сумма заказа <span><?= number_format($order->sum, 2, '.', ' ') ?> р.</span></div>
                    </div>
                    <div class="basket-total-btn-block">
                        <div class="btn-row">
                            <a href="<?= Url::to(['account/order-create-xlsx', 'number' => $order->number]) ?>" class="btn btn-white print-btn"><svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M6 9V2H18V9" stroke="#c6022d" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                                    <path d="M6 18H4C3.46957 18 2.96086 17.7893 2.58579 17.4142C2.21071 17.0391 2 16.5304 2 16V11C2 10.4696 2.21071 9.96086 2.58579 9.58579C2.96086 9.21071 3.46957 9 4 9H20C20.5304 9 21.0391 9.21071 21.4142 9.58579C21.7893 9.96086 22 10.4696 22 11V16C22 16.5304 21.7893 17.0391 21.4142 17.4142C21.0391 17.7893 20.5304 18 20 18H18" stroke="#c6022d" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                                    <path d="M18 14H6V22H18V14Z" stroke="#c6022d" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                                </svg>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="basket-comment-block">
                <div class="basket-comment-title">Дополнительна информация</div>
                <div class="input-row">
                    <textarea disabled cols="30" rows="10" placeholder="Комментарий к заказу"><?= $order->client->comment ?></textarea>
                </div>
                <div class="btn-row">
                    <button
                            <?= $order->canRepeat() === false ? 'disabled' : '' ?>
                            type="button"
                            class="btn js-order-repeat"
                            data-number="<?= $order->number ?>"
                    >Повторить заказ</button>
                </div>
            </div>
        </div>
    </div>
</section>
