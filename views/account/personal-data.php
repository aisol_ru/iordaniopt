<?php
/* @var $this \app\components\View */
/* @var $model \app\forms\PersonalData */

use app\widgets\input\File;
use app\widgets\AccountMenu;
use yii\widgets\ActiveForm;
use yii\widgets\MaskedInput;
use app\widgets\AjaxSubmitButton;
?>


<section class="account-section">
    <div class="account-section-wrap page-wrapper">
        <div class="catalog-item-title-row">
            <div class="section-title"><?= $this->h1 ?></div>
        </div>
        <div class="account-panel">
            <?= AccountMenu::widget() ?>
            <div class="account-content-col">
                <div class="account-content-panel">
                    <?php $form = ActiveForm::begin([
                        'options' => [
                            'enctype' => 'multipart/form-data',
                        ],
                        'fieldConfig' => [
                            'template' => "{input}\n{label}\n{hint}\n{error}",
                            'options' => [
                                'class' => 'input-row',
                            ],
                        ]
                    ]) ?>
                        <div class="account-form-col">
                            <div class="account-inner-title">Данные пользователя</div>
                            <?= $form->field($model, 'lastName')->textInput()->label('Фамилия') ?>
                            <?= $form->field($model, 'firstName')->textInput()->label('Имя') ?>
                            <?= $form->field($model, 'middleName')->textInput()->label('Отчество') ?>
                            <?= $form->field($model, 'phone')->widget(MaskedInput::class, [
                                'mask' => '+7 (999) 999-99-99',
                                'options' => ['disabled' => true],
                            ])->label('Телефон') ?>
                            <?= $form->field($model, 'email')->textInput()->label('E-mail') ?>
                        </div>
                        <div class="account-form-col">
                            <div class="account-inner-title">Данные организации</div>
                            <?= $form->field($model, 'organization')->textInput()->label('Организация') ?>
                            <?= $form->field($model, 'inn')->textInput()->label('ИНН') ?>
                            <?= $form->field($model, 'ogrn')->textInput()->label('ОГРН') ?>
                            <?= $form->field($model, 'city')->textInput()->label('Город') ?>
                            <?= $form->field($model, 'certificate')->textInput()->label('Адрес доставки') ?>
                        </div>
                        <div class="btn-row">
                          <?=
                          AjaxSubmitButton::widget([
                              'useWithActiveForm' => $form->getId(),
                              'label' => 'Сохранить изменения',
                              'options' => ['class' => 'btn'],
                              'ajaxOptions' => [
                                  'type' => 'POST',
                                  'url' => '/ajax/account/personalnye-dannye',
                                  'dataType' => 'json',
                                  'success' => new \yii\web\JsExpression("function(data){
                                        if(data.success){
                                          $.growl.notice({ title: 'Данные', message: 'Сохранение произведено', duration: 5000});
                                        }
                                  }"),
                              ],
                          ])
                          ?>
                        </div>
                    <?php $form::end() ?>
                </div>
            </div>
        </div>
    </div>
</section>
