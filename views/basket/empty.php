<?php
/* @var $this \app\components\View */

use yii\helpers\Url;

?>


<section class="text-panel-section">

    <div class="text-panel-wrap page-wrapper">
        <div class="catalog-item-title-row">
            <div class="section-title"><?= $this->h1 ?></div>
        </div>
        <div class="text-panel">
            <div class="notification">
                Ваша корзина пуста. Чтобы добавить в неё товары, воспользуйтесь <a href="<?= Url::to(['catalog/list']) ?>">каталогом</a>
            </div>
        </div>
    </div>
</section>
