<?php
/* @var $this \app\components\View */
/* @var $order \app\models\order\Order */
/* @var $basketOrder \app\forms\BasketOrder */

/* @var $goods \app\models\order\Good[] */

use app\assets\MainAsset;
use app\components\Pjax;
use app\forms\CatalogFilter;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

?>

<section class="basket-section">
    <div class="basket-section-wrap page-wrapper">
        <div class="catalog-item-title-row">
            <div class="section-title"><?= $this->h1 ?></div>
        </div>
        <div class="basket-panel">
            <div class="basket-list-block">
                <div class="basket-list-th-row">
                    <div class="basket-list-unit blu-photo">Фото</div>
                    <div class="basket-list-unit blu-spec">Характеристики</div>
                    <div class="basket-list-unit blu-quant">Количество</div>
                    <div class="basket-list-unit blu-weight">Вставка</div>
                    <div class="basket-list-unit blu-weight">Покрытие</div>
                    <div class="basket-list-unit blu-weight">Вес</div>
                    <div class="basket-list-unit blu-price">Ср. цена</div>
                    <div class="basket-list-unit blu-summ">Сумма по размерам</div>
                    <div class="basket-list-unit blu-summ">Комментарий</div>
                    <div class="basket-list-unit blu-btns"></div>
                </div>
                <div class="basket-mobile-top-text-row">Украшения в корзине</div>
                <?php $pjax = Pjax::begin([
                    'options' => [
                        'id' => 'basket-pjax',
                        'class' => 'basket-td-row-list',
                    ],
                    'timeout' => 5000,
                ]) ?>
                <?php foreach ($goods as $good) { ?>
                    <div class="basket-td-row js-basket-row">
                        <div class="basket-list-unit blu-photo">
                            <?php if (empty($good->jsonData['sample']) === false) { ?>
                                <div class="metal-quality-tag"><?= $good->jsonData['sample'] ?></div>
                            <?php } ?>
                            <?php if (is_null($good->good) === false && empty($good->good->image) === false) { ?>
                                <img src="<?= $good->good->getResizeCache('image', 100, 100, 5) ?>">
                            <?php } ?>
                        </div>
                        <div class="basket-list-unit blu-spec">
                            <div class="for-mobile-blu-title">Характеристики</div>
                            <div class="catalog-item-scu-table">
                                <div class="catalog-item-scu-row">
                                    <div class="catalog-item-scu-name">Наименование</div>
                                    <div class="catalog-item-scu-val">
                                        <a href="<?= Url::to(['catalog/good', 'alias' => $good->good->alias]) ?>">
                                            <?= $good->good->name ?>
                                        </a>
                                    </div>
                                </div>
                                <?php if (empty($good->good->collections) === false) { ?>
                                    <div class="catalog-item-scu-row">
                                        <div class="catalog-item-scu-name">Коллекция</div>
                                        <div class="catalog-item-scu-val">
                                            <?php foreach ($good->good->collections as $collection) { ?>
                                                <a href="<?= Url::to(['catalog/list', (new CatalogFilter())->formName() => ['collections' => [$collection->id]]]) ?>"><?= $collection->name ?></a>
                                            <?php } ?>
                                        </div>
                                    </div>
                                <?php } ?>
                                <?php if (empty($good->good->vendor_code) === false) { ?>
                                    <div class="catalog-item-scu-row">
                                        <div class="catalog-item-scu-name">Артикул</div>
                                        <div class="catalog-item-scu-val"><?= $good->good->vendor_code ?></div>
                                    </div>
                                <?php } ?>
                                <?php if ($good->good->weight_average > 0) { ?>
                                    <div class="catalog-item-scu-row">
                                        <div class="catalog-item-scu-name">Средний вес</div>
                                        <div class="catalog-item-scu-val"><?= $good->good->weight_average ?> г.</div>
                                    </div>
                                <?php } ?>
                                <?php if ($good->good->metal_id != 0 && is_null($good->good->metal) === false) { ?>
                                    <div class="catalog-item-scu-row">
                                        <div class="catalog-item-scu-name">Цвет металла</div>
                                        <div class="catalog-item-scu-val"><?= $good->good->metal->name ?></div>
                                    </div>
                                <?php } ?>
                                <?php if ($good->good->width > 0 && $good->good->height > 0) { ?>
                                    <div class="catalog-item-scu-row">
                                        <div class="catalog-item-scu-name">Габаритные размеры</div>
                                        <div class="catalog-item-scu-val"><?= $good->good->width ?>
                                            х<?= $good->good->height ?></div>
                                    </div>
                                <?php } ?>
                                <?php if (empty($good->good->specification) === false) { ?>
                                    <div class="catalog-item-scu-row">
                                        <div class="catalog-item-scu-name">Основная спецификация</div>
                                        <div class="catalog-item-scu-val"><?= $good->good->specification ?></div>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="basket-list-unit blu-quant">
                            <div class="for-mobile-blu-title">Количество</div>
                            <?php if (empty($good->jsonData['sizes']) === false) { ?>
                                <div class="catalog-item-scu-table">
                                    <?php foreach ($good->jsonData['sizes'] as $size) {
                                        if ($size['count'] == 0) {
                                            continue;
                                        }
                                        ?>
                                        <div class="catalog-item-scu-row">
                                            <div class="catalog-item-scu-name"><?= $size['size'] ?></div>
                                            <div class="catalog-item-scu-val"><?= $size['count'] ?> шт.</div>
                                        </div>
                                    <?php } ?>
                                    <div class="catalog-item-scu-row basket-total-quant-row">
                                        <div class="catalog-item-scu-name">Итого</div>
                                        <div class="catalog-item-scu-val"><?= $good->count ?> шт.</div>
                                    </div>
                                </div>
                            <?php } else { ?>
                                <?= $good->count ?> шт.
                            <?php } ?>
                        </div>
                        <div class="basket-list-unit">
                            <div class="for-mobile-blu-title">Вставка</div>
                            <p><?= $good->getInsert() ?: "-" ?></p>
                        </div>
                        <div class="basket-list-unit">
                            <div class="for-mobile-blu-title">Покрытие</div>
                            <p><?= $good->getCoating() ?: "-" ?></p>
                        </div>
                        <div class="basket-list-unit blu-weight">
                            <div class="for-mobile-blu-title">Вес</div>
                            <p><?= number_format($good->weight, 2, '.', ' ') ?> г</p>
                        </div>
                        <div class="basket-list-unit blu-price">
                            <div class="for-mobile-blu-title">Ср. цена</div>
                            <p><?= number_format($good->jsonData['price_unit'], 0, '.', ' ') ?> р.</p>
                        </div>
                        <div class="basket-list-unit blu-summ">
                            <div class="for-mobile-blu-title">Сумма по размерам</div>
                            <p><?= number_format($good->sum, 0, '.', ' ') ?> р.</p>
                        </div>
                        <div class="basket-list-unit blu-summ">
                            <div class="for-mobile-blu-title">Комментарий</div>
                            <p><?= $good->comment ?></p>
                        </div>
                        <div class="basket-list-unit blu-btns">
                            <div class="btn-row">
                                <a href="javascript:void(0);"
                                   class="btn btn-ly js-basket-show-edit-panel">Редактировать</a>
                                <a
                                        href="javascript:void(0);"
                                        class="btn btn-brown js-basket-remove"
                                        data-good-id="<?= $good->good_id ?>"
                                        data-insert-id="<?= $good->good_insert ?>"
                                        data-coating-id="<?= $good->good_coating ?>"
                                >Удалить</a>
                            </div>
                        </div>

                        <div class="floatin-size-panel js-basket-edit-panel" style="display: none;">
                            <div class="close-floating-size-panel js-basket-edit-panel-close">
                                <img src="<?= MainAsset::path('img/close-header.svg') ?>">
                            </div>
                            <div class="floating-size-panel-wrap">
                                <div class="fsp-title">
                                    Редактирование
                                    <p><?php if (empty($good->good->vendor_code) === false) { ?>
                                            арт. <?= $good->good->vendor_code ?>
                                        <?php } ?></p>
                                </div>
                                <div class="catalog-item-scu-size-list">
                                    <?php if (empty($good->jsonData['sizes']) === false) { ?>
                                        <?php foreach ($good->jsonData['sizes'] as $size) { ?>
                                            <div class="catalog-item-scu-size-unit">
                                                <div class="catalog-unit-scu-size-title"><?= $size['size'] ?></div>
                                                <div class="quant-block js-input-number">
                                                    <div class="quant-minus js-input-number-button" data-action="-">
                                                        <svg width="12" height="2" viewBox="0 0 12 2" fill="none"
                                                             xmlns="http://www.w3.org/2000/svg">
                                                            <rect width="12" height="2" fill="#eaeaea"></rect>
                                                        </svg>
                                                    </div>
                                                    <input
                                                            type="text"
                                                            autocomplete="off"
                                                            data-size="<?= $size['size'] ?>"
                                                            data-default="<?= $size['count'] ?: '' ?>"
                                                            data-size-weight="<?= $size['weight'] ?>"
                                                            value="<?= $size['count'] ?: '' ?>"
                                                            placeholder="0"
                                                            tabindex="-1"
                                                            class="js-basket-input-quant js-basket-edit-good-count"
                                                    >
                                                    <div class="quant-plus js-input-number-button" data-action="+">
                                                        <svg width="12" height="12" viewBox="0 0 12 12" fill="none"
                                                             xmlns="http://www.w3.org/2000/svg">
                                                            <rect y="5" width="12" height="2" fill="#eaeaea"></rect>
                                                            <rect x="5" y="12" width="12" height="2"
                                                                  transform="rotate(-90 5 12)" fill="#eaeaea"></rect>
                                                        </svg>

                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    <?php } else { ?>
                                        <div class="catalog-item-scu-size-unit">
                                            <div class="catalog-unit-scu-size-title">Кол-во</div>
                                            <div class="quant-block js-input-number">
                                                <div class="quant-minus js-input-number-button" data-action="-">
                                                    <svg width="12" height="2" viewBox="0 0 12 2" fill="none"
                                                         xmlns="http://www.w3.org/2000/svg">
                                                        <rect width="12" height="2" fill="#eaeaea"></rect>
                                                    </svg>
                                                </div>
                                                <input
                                                        type="text"
                                                        autocomplete="off"
                                                        data-default="<?= $good->count ?: '' ?>"
                                                        value="<?= $good->count ?>"
                                                        data-size-weight="<?= $good->weight ?>"
                                                        placeholder="0"
                                                        tabindex="-1"
                                                        class="js-basket-input-quant js-basket-edit-good-count"
                                                >
                                                <div class="quant-plus js-input-number-button" data-action="+">
                                                    <svg width="12" height="12" viewBox="0 0 12 12" fill="none"
                                                         xmlns="http://www.w3.org/2000/svg">
                                                        <rect y="5" width="12" height="2" fill="#eaeaea"></rect>
                                                        <rect x="5" y="12" width="12" height="2"
                                                              transform="rotate(-90 5 12)" fill="#eaeaea"></rect>
                                                    </svg>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>

                                </div>
                                <div class="btn-row">
                                    <button
                                            type="button"
                                            data-has-sizes="<?= empty($good->jsonData['sizes']) === false ? 1 : 0 ?>"
                                            data-id="<?= $good->good_id ?>"
                                            data-insert-id="<?= $good->good_insert ?>"
                                            data-coating-id="<?= $good->good_coating ?>"
                                            class="btn js-basket-edit"
                                    >Сохранить
                                    </button>
                                </div>
                            </div>
                        </div>

                    </div>
                <?php } ?>

                <?php $pjax::end() ?>
                <div class="basket-list-total-row">
                    <div class="basket-list-total-block">
                        <div class="blt-unit">Итого <span class="js-basket-count"><?= $order->good_count ?></span><span>шт</span>
                        </div>
                        <div class="blt-unit">Общий вес <span
                                    class="js-basket-weight"><?= number_format($order->good_weight, 2, '.', ' ') ?></span><span>гр.</span>
                        </div>
                        <div class="blt-unit">Сумма заказа <span
                                    class="js-basket-sum"><?= number_format($order->sum, 0, '.', ' ') ?></span><span>р.</span>
                        </div>
                    </div>
                    <div class="basket-total-btn-block">
                        <div class="btn-row">
                            <a href="javascript:void(0);" class="btn btn-white js-basket-flush">Очистить корзину</a>
                            <a href="<?= Url::to(['basket/create-xlsx']) ?>" class="btn btn-white print-btn">
                                <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                     xmlns="http://www.w3.org/2000/svg">
                                    <path d="M6 9V2H18V9" stroke="#c6022d" stroke-width="1.5" stroke-linecap="round"
                                          stroke-linejoin="round"/>
                                    <path d="M6 18H4C3.46957 18 2.96086 17.7893 2.58579 17.4142C2.21071 17.0391 2 16.5304 2 16V11C2 10.4696 2.21071 9.96086 2.58579 9.58579C2.96086 9.21071 3.46957 9 4 9H20C20.5304 9 21.0391 9.21071 21.4142 9.58579C21.7893 9.96086 22 10.4696 22 11V16C22 16.5304 21.7893 17.0391 21.4142 17.4142C21.0391 17.7893 20.5304 18 20 18H18"
                                          stroke="#c6022d" stroke-width="1.5" stroke-linecap="round"
                                          stroke-linejoin="round"/>
                                    <path d="M18 14H6V22H18V14Z" stroke="#c6022d" stroke-width="1.5"
                                          stroke-linecap="round" stroke-linejoin="round"/>
                                </svg>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <?php $form = ActiveForm::begin([
                'fieldConfig' => [
                    'options' => ['class' => 'input-row'],
                ],
                'options' => ['class' => 'basket-comment-block'],
            ]) ?>
            <div class="basket-comment-title">Дополнительна информация</div>
            <?= $form->field($basketOrder, 'comment')->textarea([
                'cols' => 30,
                'rows' => 10,
                'placeholder' => 'Комментарий к заказу',
            ])->label(false) ?>
            <div class="btn-row">
                <button type="submit" class="btn">оформить заказ</button>
            </div>
            <?php $form::end() ?>
        </div>
    </div>
</section>
