<?php
/* @var $this \app\components\View */
/* @var $order \app\models\order\Order|null */

use yii\helpers\Url;

?>


<div class="basket-thankyou-section">
    <div class="basket-thankyou-block">
        <div class="catalog-item-title-row">
            <div class="section-title">Спасибо за заказ</div>
        </div>
        <div class="basket-thank-order-number">Ваш заказ № <?= $order->number ?></div>
        <p>Информацию о состоянии заказа Вы можете получить в личном кабинете или у персонального менеджера</p>
        <div class="btn-row">
            <a href="<?= Url::to(['catalog/list']) ?>" class="btn">Перейти в каталог</a>
        </div>
    </div>
</div>
