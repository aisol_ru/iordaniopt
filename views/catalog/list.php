<?php
/* @var $this \app\components\View */
/* @var $goods \app\models\catalog\Good[] */
/* @var $filter \app\forms\CatalogFilter */
/* @var $sort \yii\data\Sort */
/* @var $order \app\models\order\Order */

/* @var $pagination \yii\data\Pagination */

use app\assets\MainAsset;
use yii\helpers\Html;

?>


<section class="catalog-section">
    <div class="catalog-section-wrap page-wrapper">
        <div class="catalog-sort-row">
            <?php if (is_null($order) === false) { ?>
                <div class="sr-basket-list">
                    <div class="sr-basket-info-unit">
                        <div class="sr-info-title">В корзине</div>
                        <span><?= $order->good_count ?> шт.</span>
                    </div>
                    <div class="sr-basket-info-unit">
                        <div class="sr-info-title">Общий вес</div>
                        <span><?= number_format($order->good_weight, 2, '.', ' ') ?> гр.</span>
                    </div>
                    <div class="sr-basket-info-unit">
                        <div class="sr-info-title">Сумма заказа</div>
                        <span><?= number_format($order->sum, 2, '.', ' ') ?> р.</span>
                    </div>
                </div>
            <?php } ?>
            <div class="catalog-sort-unit-wrap">
                <div class="mobile-filter-btn js-mobile-filter-btn">
                    <img src="<?= MainAsset::path('img/filter-ico.svg') ?>" alt="">
                    <span>
                        Фильтр
                        <!--                        <div class="mobile-filter-counter active">-->
                        <? //= $pagination->totalCount ?><!--</div>-->
                    </span>
                </div>
<!--                <div class="for-mobile-search-row">-->
<!--                    <div class="filter-search-block">-->
<!--                        <div class="input-row">-->
<!--                            --><?//= Html::activeTextInput($filter, 'word', ['id' => 'search-catalog-mobile', 'autocomplete' => 'off', 'placeholder' => 'Артикул/Название']) ?>
<!--                            <label for=""></label>-->
<!--                            <img src="--><?//= MainAsset::path('img/search-icon.svg') ?><!--" alt="">-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->

                <div class="catalog-filter-unit search-cf-unit">
                    <!--                    <div class="catalog-filter-unit-title">Поиск по артикулу</div>-->
                    <div class="filter-search-block">
                        <div class="input-row">
                            <?= Html::activeTextInput($filter, 'word', ['autocomplete' => 'off', 'placeholder' => 'Артикул/Название', 'form' => 'catalog-list-filter']) ?>
                            <label for=""></label>
                            <img src="<?= MainAsset::path('img/search-icon.svg') ?>" alt="">
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <?php \app\components\Pjax::begin([
            'formSelector' => '#catalog-list-filter',
            'timeout' => 5000,
            "id" => "catalog-pjax",
        ]); ?>
        <div class="catalog-list-filter-panel">
            <div class="catalog-filter-col">
                <?= \app\widgets\catalog\Filter::widget(['filter' => $filter, 'brandId' => \Yii::$app->getUrlManager()->getDomain()->brand_id]) ?>
            </div>
            <div class="catalog-list-col">
                <div class="catalog-list js-catalog-list">
                    <?php foreach ($goods as $good) { ?>
                        <?= \app\widgets\catalog\Good::widget(['good' => $good]) ?>
                    <?php } ?>
                </div>
                <?php if ($pagination->pageCount > 1) { ?>
                    <div class="btn-row">
                        <button
                                type="button"
                                class="btn show-more-btn btn-ly js-catalog-pagination"
                                data-category-outer-code="<?= $categoryOuterCode ?>"
                                data-is-filter="<?= $isFilter === true ? "true" : "false" ?>"
                                data-page="<?= $pagination->page + 1 ?>"
                                data-total-page="<?= $pagination->pageCount ?>"
                                data-page-param="<?= $pagination->pageParam ?>"
                                data-sort-param="sort"
                        >Показать еще
                        </button>
                    </div>
                <?php } ?>
            </div>
        </div>
        <?php \app\components\Pjax::end(); ?>
    </div>
</section>
