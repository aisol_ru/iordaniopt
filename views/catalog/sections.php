<?php
/* @var $this \app\components\View */
/* @var $categorys \app\models\catalog\Good[] */
/* @var $filter \app\forms\CatalogFilter */
/* @var $sort \yii\data\Sort */
/* @var $order \app\models\order\Order */

/* @var $pagination \yii\data\Pagination */

use app\assets\MainAsset;
use app\widgets\SortSelect;
use yii\helpers\Html;

?>


<section class="catalog-section">
    <div class="catalog-section-wrap page-wrapper">
        <div class="catalog-sort-row">
            <?php if (is_null($order) === false) { ?>
                <div class="sr-basket-list">
                    <div class="sr-basket-info-unit">
                        <div class="sr-info-title">В корзине</div>
                        <span><?= $order->good_count ?> шт.</span>
                    </div>
                    <div class="sr-basket-info-unit">
                        <div class="sr-info-title">Общий вес</div>
                        <span><?= number_format($order->good_weight, 2, '.', ' ') ?> гр.</span>
                    </div>
                    <div class="sr-basket-info-unit">
                        <div class="sr-info-title">Сумма заказа</div>
                        <span><?= number_format($order->sum, 2, '.', ' ') ?> р.</span>
                    </div>
                </div>
            <?php } ?>
            <div class="catalog-sort-unit-wrap">
                <?php $form = \app\modules\icms\widgets\ActiveFormIcms::begin([
                        "method" => "GET"
                ]); ?>
<!--                <div class="for-mobile-search-row">-->
<!--                    <div class="filter-search-block">-->
<!--                        <div class="input-row">-->
<!--                            --><?//= Html::activeTextInput($filter, 'word', ['id' => 'search-catalog-on-main-mobile', 'autocomplete' => 'off', 'placeholder' => 'Артикул/Название']) ?>
<!--                            <label for=""></label>-->
<!--                            <img src="--><?//= MainAsset::path('img/search-icon.svg') ?><!--" alt="">-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->

                <div class="catalog-filter-unit search-cf-unit">
                    <div class="filter-search-block">
                        <div class="input-row">
                            <?= Html::activeTextInput($filter, 'word', ['id' => 'search-catalog-on-main', 'autocomplete' => 'off', 'placeholder' => 'Артикул/Название']) ?>
                            <label for=""></label>
                            <img src="<?= MainAsset::path('img/search-icon.svg') ?>" alt="">
                        </div>
                    </div>
                </div>

                <?php $form::end(); ?>
            </div>
        </div>
        <div class="catalog-list-filter-panel">
            <div class="catalog-list-col">
                <div class="catalog-slider js-main-page-slider">
                    <?php foreach ($slides as $slide) { ?>
                        <div class="main-page-slider-unit">
                            <a href="<?= !empty($slide->link) ? $slide->link : "javascript:void(0);" ?>">
                                <img src="<?= $slide->getResizePath('image', 1024, 428) ?>"
                                     alt="<?= Html::encode($slide->name) ?>">
                            </a>
                        </div>
                    <?php } ?>
                </div>
                <div class="catalog-list catalog-list-category">
                    <?php foreach ($categories as $category) { ?>
                        <div class="catalog-list-unit catalog-list-unit-category">
                            <a href="<?= \yii\helpers\Url::to(["catalog/catalog-section", "category_outer_code" => $category->outer_code]) ?>"
                               class="cli-wrap">
                                <?php if (empty($category->image) === false) { ?>
                                    <div class="clu-pic-holder">
                                        <img src="<?= $category->getResizeCache('image', 460, 460, 5, true) ?>"
                                             alt="<?= Html::encode($category->name) ?>">
                                    </div>
                                <?php } else { ?>
                                    <div class="clu-pic-holder">
                                        <img src="<?= MainAsset::path('img/nophoto.jpg') ?>"
                                             alt="<?= Html::encode($category->name) ?>">
                                    </div>
                                <?php } ?>
                                <!--                                <div class="clu-title">-->
                                <? //= $category->name ?><!--</div>-->
                            </a>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</section>
