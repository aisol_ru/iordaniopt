<?php
/* @var $this \app\components\View */
/* @var $good  \app\models\catalog\Good */
/* @var $images  \app\models\catalog\Image[] */
/* @var $variantMetals  \app\models\catalog\Good[] */
/* @var $variantInserts  \app\models\catalog\Good[] */
/* @var $headset  \app\models\catalog\Good[] */
/* @var $similar  \app\models\catalog\Good[] */
/* @var $prices  \app\models\catalog\Price[] */

/* @var $sizes array */

use app\forms\CatalogFilter;
use yii\helpers\Html;
use yii\helpers\Url;
use app\assets\MainAsset;

?>


<section class="catalog-item-section">
    <div class="catalog-item-section-wrap page-wrapper">
        <div class="catalog-item-title-row">
            <a href="<?= Url::to(Yii::$app->request->referrer ?: ['catalog/catalog-section', 'category_outer_code' => $good->category_id]) ?>"
               class="title-row-getback-link js-history-back">К украшениям</a>
            <div class="section-title"><?= $this->h1 ?></div>
        </div>
        <div class="catalog-item-info-panel">
            <div class="catalog-item-pic-col">
                <div class="catalog-item-pic-col-wrap">

                    <?php if ($imagesCount > 1) { ?>
                        <div class="catalog-item-pic-small-col">
                            <div class="catalog-item-pic-small-carousel js-catalog-item-pic-small-carousel">
                                <?php if (empty($good->image) === false) { ?>
                                    <div class="catalog-item-pic-small-slide">
                                        <img src="<?= $good->getResizeCache('image', 400, 400, 1, true) ?>"
                                             alt="<?= Html::encode($good->name) ?>">
                                    </div>
                                <?php } else { ?>
                                    <div class="catalog-item-pic-small-slide">
                                        <img src="<?= MainAsset::path('img/nophoto.jpg') ?>"
                                             alt="<?= Html::encode($good->name) ?>">
                                    </div>
                                <?php } ?>
                                <?php foreach ($images as $image) { ?>
                                    <div class="catalog-item-pic-small-slide">
                                        <img src="<?= $image->getResizeCache('image', 400, 400, 1, true) ?>"
                                             alt="<?= Html::encode($image->name) ?>">
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    <?php } ?>

                    <div class="catalog-item-pic-big-col">
                        <?php if ($good->isNew() === true) { ?>
                            <div class="clu-label-row">
                                <div class="clu-label-unit">New</div>
                            </div>
                        <?php } ?>
                        <!-- <div class="clu-fav js-user-favorite-button <?= $good->isFavorite() === true ? 'active' : '' ?>" data-id="<?= $good->id ?>">
                            <svg width="28" height="24" viewBox="0 0 28 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M14 22.3571L27 7.45681M14 22.3571L1 7.45681M14 22.3571L20.0636 7.92257C20.1866 7.62969 20.1632 7.29577 20.0004 7.02296L16.4074 1M14 22.3571L7.93556 7.9206C7.813 7.62882 7.83578 7.29626 7.99698 7.02392L11.5627 1M27 7.45681L22.0038 1.3658C21.8138 1.13424 21.5301 1 21.2306 1H16.4074M27 7.45681H1M1 7.45681L5.99624 1.3658C6.18619 1.13424 6.46991 1 6.76941 1H11.5627M16.4074 1H11.5627" stroke="#C4C4C4" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                            </svg>
                        </div> -->
                        <div class="catalog-item-pic-big-carousel<?= $imagesCount > 1 ? " js-catalog-item-pic-big-carousel" : "" ?>">
                            <?php if (empty($good->image) === false) { ?>
                                <div class="catalog-item-pic-big-slide js-magnifier"
                                     data-image="<?= $good->getResizeCache('image', false, false, 1, true) ?>">
                                    <img src="<?= $good->getResizeCache('image', 400, 400, 1, true) ?>"
                                         alt="<?= Html::encode($good->name) ?>">
                                </div>
                            <?php } else { ?>
                                <div class="catalog-item-pic-big-slide js-magnifier"
                                     data-image="<?= $good->getPath('image') ?>">
                                    <img src="<?= MainAsset::path('img/nophoto.jpg') ?>"
                                         alt="<?= Html::encode($good->name) ?>">
                                </div>
                            <?php } ?>
                            <?php foreach ($images as $image) { ?>
                                <div class="catalog-item-pic-small-slide js-magnifier"
                                     data-image="<?= $image->getResizeCache('image', false, false, 1, true) ?>">
                                    <img src="<?= $image->getResizeCache('image', 400, 400, 1, true) ?>"
                                         alt="<?= Html::encode($image->name) ?>">
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <div class="catalog-item-content-wrap">
                    <?= $good->content ?>
                </div>
            </div>
            <div class="catalog-item-scu-col">
                <div class="catalog-item-scu-col-wrap">
                    <div class="catalog-item-scu-panel">
                        <div class="catalog-item-scu-block">
                            <div class="catalog-item-scu-title">Характеристики украшения</div>
                            <div class="catalog-item-scu-cont">
                                <div class="catalog-item-scu-table">
                                    <div class="catalog-item-scu-row">
                                        <div class="catalog-item-scu-name">Наименование</div>
                                        <div class="catalog-item-scu-val"><?= $good->name ?></div>
                                    </div>
                                    <?php if (empty($good->collections) === false) { ?>
                                        <div class="catalog-item-scu-row">
                                            <div class="catalog-item-scu-name">Коллекция</div>
                                            <div class="catalog-item-scu-val">
                                                <?php foreach ($good->collections as $collection) { ?>
                                                    <a href="<?= Url::to(['catalog/list', (new CatalogFilter())->formName() => ['collections' => [$collection->id]]]) ?>"><?= $collection->name ?></a>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <?php if (empty($good->vendor_code) === false) { ?>
                                        <div class="catalog-item-scu-row">
                                            <div class="catalog-item-scu-name">Артикул</div>
                                            <div class="catalog-item-scu-val"><?= $good->vendor_code ?></div>
                                        </div>
                                    <?php } ?>
                                    <?php if ($good->weight_average > 0) { ?>
                                        <div class="catalog-item-scu-row">
                                            <div class="catalog-item-scu-name">Средний вес</div>
                                            <div class="catalog-item-scu-val"><?= $good->weight_average ?> г.</div>
                                        </div>
                                    <?php } ?>
                                    <?php if ($good->metal_id != 0 && is_null($good->metal) === false) { ?>
                                        <div class="catalog-item-scu-row">
                                            <div class="catalog-item-scu-name">Цвет металла</div>
                                            <div class="catalog-item-scu-val"><?= $good->metal->name ?></div>
                                        </div>
                                    <?php } ?>
                                    <?php if ($good->width > 0 && $good->height > 0) { ?>
                                        <div class="catalog-item-scu-row">
                                            <div class="catalog-item-scu-name">Габаритные размеры</div>
                                            <div class="catalog-item-scu-val"><?= $good->width ?>
                                                х<?= $good->height ?></div>
                                        </div>
                                    <?php } ?>
                                    <?php if (empty($good->specification) === false) { ?>
                                        <div class="catalog-item-scu-row">
                                            <div class="catalog-item-scu-name">Основная спецификация</div>
                                            <div class="catalog-item-scu-val"><?= $good->specification ?></div>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                        <?php if (empty($price) === false) { ?>
                            <div class="catalog-item-scu-block">
                                <div class="catalog-item-scu-cont">
                                    <div class="catalog-item-scu-price-row">
                                        <div class="catalog-item-scu-price-block">Средняя цена: <span
                                                    class="js-good-price-unit"
                                                    data-price="<?= $price ?>">0</span><span> р.</span>
                                        </div>
                                        <div class="ciu-price-announce">Цена указана ориентировочная, без учета
                                            индивидуальных условий сотрудничества и подлежит корректировке Вашим ведущим
                                            менеджером
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="catalog-item-scu-inserts-wrap js-select-insert">
                                <?php
                                $isIssetEmptyInsert = false;
                                foreach ($sizes as $sku) {
                                    if ($sku["insert_id"] === "") {
                                        $isIssetEmptyInsert = true;
                                        break;
                                    }
                                } ?>

                                <?php if ($isIssetEmptyInsert) { ?>
                                    <button data-insert="" class="btn active">Без вставки</button>
                                <?php } ?>

                                <?php
                                $i = 0;
                                foreach ($good->getAllInserts() as $insertId => $insert) { ?>
                                    <button data-insert="<?= $insertId ?>"
                                            class="btn <?= ($isIssetEmptyInsert === false && $i === 0) ? "active" : "" ?>"><?= $insert ?></button>
                                    <?php
                                    $i++;
                                } ?>
                            </div>

                            <div class="catalog-item-scu-coating-wrap">
                                <?php if ($coatings = $good->getAllCoatings()) { ?>
                                    <label for="catalog-select-coating">
                                        Выберите покрытие
                                    </label>
                                    <select id="catalog-select-coating">
                                        <?php foreach ($coatings as $insertId => $insert) { ?>
                                            <option value="<?= $insertId ?>"><?= $insert ?></option>
                                        <?php } ?>
                                        <option value="">Без покрытия</option>
                                    </select>
                                <?php } ?>
                            </div>

                            <div class="catalog-item-comment">
                                <label for="catalog-item-comment">Комментарий</label>
                                <textarea id="catalog-item-comment" cols="30" rows="2"></textarea>
                            </div>

                            <div class="catalog-item-scu-block">
                                <!-- <div class="catalog-item-scu-title">Проба</div> -->
                                <div class="catalog-item-scu-cont">
                                    <div class="catalog-item-probe-select-row">
                                        <?php if (empty($sizes) === true) { ?>
                                            <div class="catalog-item-scu-size-unit">
                                                <div class="catalog-unit-scu-size-title">Кол-во</div>
                                                <div class="quant-block js-input-number">
                                                    <div class="quant-minus js-input-number-button" data-action="-">
                                                        <svg width="12" height="2" viewBox="0 0 12 2" fill="none"
                                                             xmlns="http://www.w3.org/2000/svg">
                                                            <rect width="12" height="2" fill="#eaeaea"></rect>
                                                        </svg>
                                                    </div>
                                                    <input type="text"
                                                           autocomplete="off"
                                                           value=""
                                                           placeholder="0"
                                                           tabindex="-1"
                                                           class="js-basket-input-quant js-good-count"
                                                           data-price="<?= $price ?>">
                                                    <div class="quant-plus js-input-number-button" data-action="+">
                                                        <svg width="12" height="12" viewBox="0 0 12 12" fill="none"
                                                             xmlns="http://www.w3.org/2000/svg">
                                                            <rect y="5" width="12" height="2" fill="#eaeaea"></rect>
                                                            <rect x="5" y="12" width="12" height="2"
                                                                  transform="rotate(-90 5 12)" fill="#eaeaea"></rect>
                                                        </svg>

                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                            <?php if (empty($sizes) === false) { ?>
                                <div class="catalog-item-scu-block">
                                    <div class="catalog-item-scu-cont">
                                        <p class="ciu-size-announce">Размерный ряд может быть скорректирован Вашим
                                            ведущим менеджером, так как зависит от модели украшения</p>
                                        <div class="catalog-item-scu-size-list">

                                            <?php foreach ($sizes as $sku) { ?>
                                                <?php foreach ($sku["sizes"] as $size) { ?>

                                                    <div data-insert="<?= $sku["insert_id"] ?>"
                                                         data-coating="<?= $sku["coating_id"] ?>"
                                                         class="catalog-item-scu-size-unit js-catalog-item-size">
                                                        <div class="catalog-unit-scu-size-title"><?= $size["size"] !== 0 ? $size["size"] : "Кол-во" ?></div>
                                                        <div class="quant-block js-input-number">
                                                            <div class="quant-minus js-input-number-button"
                                                                 data-action="-">
                                                                <svg width="12" height="2" viewBox="0 0 12 2"
                                                                     fill="none"
                                                                     xmlns="http://www.w3.org/2000/svg">
                                                                    <rect width="12" height="2" fill="#eaeaea"/>
                                                                </svg>
                                                            </div>

                                                            <input type="text" autocomplete="off" value=""
                                                                   placeholder="0"
                                                                   readonly data-size="<?= $size["size"] ?>"
                                                                   data-count="<?= $size["count"] ?>"
                                                                   data-price="<?= $size["price"] ?>"
                                                                   data-size-weight="<?= $size["weight"] ?>"
                                                                   tabindex="-1"
                                                                   class="js-basket-input-quant js-good-count">

                                                            <div class="quant-plus js-input-number-button"
                                                                 data-action="+"
                                                                 data-count="<?= $size["count"] ?>">
                                                                <svg width="12" height="12" viewBox="0 0 12 12"
                                                                     fill="none"
                                                                     xmlns="http://www.w3.org/2000/svg">
                                                                    <rect y="5" width="12" height="2"
                                                                          fill="#eaeaea"/>
                                                                    <rect x="5" y="12" width="12" height="2"
                                                                          transform="rotate(-90 5 12)"
                                                                          fill="#eaeaea"/>
                                                                </svg>
                                                            </div>
                                                        </div>
                                                    </div>

                                                <?php } ?>
                                            <?php } ?>

                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                            <div class="catalog-item-total-price-block">
                                <div class="catalog-item-total-price">
                                    Сумма: <span class="js-good-sum" data-price="<?= $price ?>"
                                                 data-count="<?= $price ?>">0</span><span> р.</span>
                                </div>
                                <div class="btn-row">
                                    <button type="button" data-has-sizes="<?= empty($sizes) === false ? 1 : 0 ?>"
                                            data-id="<?= $good->id ?>" class="btn js-good-add">Добавить в заказ
                                    </button>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

