<?php
/* @var $this \app\components\View */
/* @var $brands \app\models\Brand[] */
/* @var $youtubeLink string */
/* @var $youtubeId string */

use yii\helpers\Html;

?>


<div class="brands-section">
    <div class="brands-panel page-wrapper">
        <div class="section-title"><?= $this->h1 ?></div>
        <div class="brands-panel-list">

            <?php foreach ($brands as $brand) { ?>
                <div class="brand-text-unit">
                    <div class="brand-text-pic">
                        <img src="<?= $brand->getResizeCache('image', 645, 400) ?>">
                    </div>
                    <div class="brand-text-block">
                        <?= $brand->content ?>
                    </div>
                </div>
            <?php } ?>

        </div>
        <?php if (empty($youtubeId) === false) { ?>
            <div class="brand-video-panel js-brand-video-panel">
                <a href="<?= $youtubeLink ?>" data-fancybox class="video-poster js-video-poster" style="background-image: url('http://i3.ytimg.com/vi/<?= $youtubeId ?>/maxresdefault.jpg')">
                </a>
            </div>
        <?php } ?>
    </div>
</div>