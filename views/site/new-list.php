<?php
/* @var $this \app\components\View */
/* @var $banners \app\models\banner\Banner[] */

use yii\helpers\Html;

?>


<div class="new-collection-section">
    <div class="new-collection-panel page-wrapper">
        <div class="section-title"><?= $this->h1 ?></div>
        <div class="collection-list">

            <?php foreach ($banners as $banner) { ?>
                <a href="<?= $banner->link ?: 'javascript:void(0);' ?>" class="collection-unit">
                    <div class="collection-unit-pic-holder">
                        <img src="<?= $banner->getResizeCache('image', 420, 300) ?>" alt="<?= Html::encode($banner->name) ?>">
                    </div>
                    <div class="collection-unit-title">
                        <?= $banner->name ?>
                    </div>
                </a>
            <?php } ?>

        </div>
    </div>
</div>