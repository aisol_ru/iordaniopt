<?php
/* @var $this \app\components\View */
/* @var $events \app\models\Event[] */

use yii\helpers\Html;

?>


<div class="main-page-slider-section">
    <div class="main-page-slider-panel page-wrapper">
        <div class="section-title"><?= $this->h1 ?></div>
        <?php if (empty($events) === false) { ?>
            <div class="main-page-slider js-main-page-slider">
                <?php foreach ($events as $event) { ?>
                    <a href="<?= $event->link ?: 'javascript:void(0);' ?>" class="main-page-slider-unit">
                        <img src="<?= $event->getPath('image') ?>" alt="<?= Html::encode($event->name) ?>">
                    </a>
                <?php } ?>
            </div>
          <?php }else{ ?>
              <h4>Событий нет</h4>
          <?php } ?>
    </div>
</div>
