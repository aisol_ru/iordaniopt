<?php
/* @var $this \app\components\View */
/* @var $slides \app\models\slider\Slide[] */

use yii\helpers\Html;


?>

<div class="main-page-slider-section">
    <div class="main-page-slider-panel page-wrapper">
        <div class="section-title"><?= $this->h1 ?></div>
        <div class="main-page-slider js-main-page-slider">

            <?php if(!empty($slides)) { ?>
                <?php foreach ($slides as $slide) { ?>
                    <a href="<?= $slide->link ?: 'javascript:void(0);' ?>" class="main-page-slider-unit">
                        <img src="<?= $slide->getPath('image') ?>" alt="<?= Html::encode($slide->name) ?>">
                    </a>
                <?php } ?>
            <?php }else{ ?>
                <h4>Акций нет</h4>
            <?php } ?>

        </div>
    </div>
</div>
