<?php
/* @var $this app\components\View */
/* @var $exception \yii\web\HttpException|\Exception */
/* @var $handler \yii\web\ErrorHandler */

switch ($exception->statusCode) {
    case 403:
        $this->title = 'Доступ запрещен [ошибка 403]';
        break;
    case 404:
        $this->title = 'Страница не существует [ошибка 404]';
        break;
    case 500:
        $this->title = 'Ошибка сервера [ошибка 500]';
        break;
    default:
        $this->title = $exception->getName() . ' [error ' . $exception->statusCode . ']';
        break;
}

$message = $exception->getMessage();
?>

<section class="text-panel-section">

    <div class="text-panel-wrap page-wrapper">
        <div class="catalog-item-title-row">
            <div class="section-title"><?= $this->title ?></div>
        </div>
        <div class="text-panel">
            <?php if (empty($message) === false) { ?>
                <p><?= $message ?></p>
            <?php } ?>
        </div>
    </div>
</section>
