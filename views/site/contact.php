<?php
/* @var $this \app\components\View */
/* @var $contact \app\models\contact\Contact */
/* @var $socials \app\models\contact\Social[] */

use app\widgets\YandexMap;
use yii\helpers\Html;

?>

<div class="contacts-section">
    <div class="contacts-panel page-wrapper">
        <div class="section-title">Контакты</div>
        <div class="contacts-block">
            <div class="contacts-text-col">
                <div class="contacts-text-panel">
                    <?php if (empty($contact->address) === false) { ?>
                        <p><b>Адрес:</b><br></p>
                        <p><?= $contact->address ?></p>
                        <br>
                        <br>
                    <?php } ?>
                    <?php if (empty($contact->working_hours) === false) { ?>
                        <p><b>Прием звонков:</b><br></p>
                        <p><?= $contact->working_hours ?></p>
                        <br>
                        <br>
                    <?php } ?>

                    <?php if (empty($contact->phone) === false) { ?>
                        <p><b>Телефон:</b><br></p>
                        <p><?= $contact->phone ?></p>

                        <br>
                        <br>
                    <?php } ?>

                    <p><b>Ольга</b><br></p>
                    <a href="tel:+7-910-958-2507">+7-910-958-2507</a>

                    <p><b>Фёдор</b><br></p>
                    <a href="tel:+7-999-740-0951">+7-999-740-0951</a>

                    <p><b>Мария</b><br></p>
                    <a href="tel:+7-962-186-69-44">+7-962-186-69-44</a>
                    <br>
                    <br>
                    <?php if (empty($contact->email) === false) { ?>
                        <p><b>E-mail:</b><br></p>
                        <p><?= $contact->email ?></p>
                        <br>
                        <br>
                    <?php } ?>
                </div>
                <div class="contact-text-soc-row">
                    <div class="btn-row">
                        <button type="button" class="btn btn-white js-open-feedback-popup">Отправить сообщение</button>
                    </div>
                    <?php if (empty($socials) === false) { ?>
                        <div class="contact-soc-row">
                            <?php foreach ($socials as $social) { ?>
                                <a target="_blank" href="<?= $social->link ?: 'javascript:void(0);' ?>">
                                    <img src="<?= $social->getPath('image') ?>" alt="<?= Html::encode($social->name) ?>">
                                </a>
                            <?php } ?>
                        </div>
                    <?php } ?>
                </div>
            </div>
            <div class="contacts-map-col">
                <?php if (is_null($contact->map) === false) { ?>
                    <div class="contact-map-block">
                        <?= YandexMap::widget([
                            'map' => $contact->map,
                            'marks' => $contact->map->getMarks()->all(),
                        ]) ?>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
