<?php
/* @var $this app\components\View */
/* @var $banners \app\models\banner\Banner[] */
/* @var $slides \app\models\slider\Slide[] */

use yii\helpers\Html;

?>
<?php if (Yii::$app->getUser()->isGuest === true) { ?>
    <div class="main-page-slider-section">
          <div class="main-page-slider-panel page-wrapper">
              <div class="main-page-slider js-main-page-slider">
                  <?php if(!empty($slides)) { ?>
                      <?php foreach ($slides as $slide) { ?>
                          <div class="main-page-slider-unit">
                              <img src="<?= $slide->getResizePath('image', 800, 560) ?>" alt="<?= Html::encode($slide->name) ?>">
                          </div>
                      <?php } ?>
                    <?php }else{ ?>
                      <h4>Контент еще не завезли :)</h4>
                    <?php } ?>
              </div>
          </div>
      </div>
<?php } else { ?>
    <div class="logged-main-section">
        <div class="new-collection-panel page-wrapper">
            <div class="collection-list">
                <?php foreach ($banners as $key => $banner) { ?>
                    <a href="<?= $banner->link ?: 'javascript:void(0);' ?>" class="collection-unit <?= $key >= 3 ? 'w50' : '' ?>">
                        <div class="collection-unit-pic-holder">
                            <img src="<?= $banner->getPath('image') ?>" alt="<?= Html::encode($banner->name) ?>">
                        </div>
                        <div class="collection-unit-title">
                            <?= $banner->name ?>
                            <?php if ($banner->is_premium == 1) { ?>
                                <span class="prem-tag">Premium</span>
                            <?php } ?>
                        </div>
                    </a>
                <?php } ?>
            </div>
        </div>
    </div>
<?php } ?>
