<?php
/* @var $this app\components\View */
?>
<section class="text-panel-section">

    <div class="text-panel-wrap page-wrapper">
        <div class="catalog-item-title-row">
            <div class="section-title"><?= $this->h1 ?></div>
        </div>
        <div class="text-panel">
            <?= $this->tree->getContent() ?>
        </div>
    </div>
</section>
