<?php
/* @var $this app\components\View */
/* @var $content string */


use app\assets\MainAsset;
use app\models\parameter\Parameter;
use app\widgets\Header;
use app\widgets\popup\Auth;
use app\widgets\popup\Feedback;
use app\widgets\Preloader;
use app\widgets\TopMenu;
use yii\helpers\Html;
use app\widgets\BasketButton;

MainAsset::register($this);
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">

<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <title><?= Html::encode($this->title) ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <meta name="yandex-verification" content="001eb7091c2ddd2e" />
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<?= Preloader::widget() ?>

<?= Auth::widget() ?>

<?= Feedback::widget() ?>

<div style="display: none;" id="feedback-modal">
    <div class="modal-win">
        <div class="modal-win-cont">
            <div class="modal-title">Обратная связь</div>
            <div class="input-row">
                <input type="text" placeholder="Имя">
            </div>
            <div class="input-row">
                <input type="text" placeholder="Телефон*">
            </div>
            <div class="input-row">
                <input type="text" placeholder="Комментарий*">
            </div>
            <div class="checkbox-row agreement-row">
                <input type="checkbox" id="agreement-check">
                <label for="agreement-check">Даю согласие на обработку <a href="javascript:void(0);">персональных
                        данных</a></label>
            </div>
        </div>
        <div class="btn-row">
            <a href="javascript:void(0);" class="btn">Получить код</a>
        </div>
    </div>
</div>

<div class="fade-wrapper"></div>
<div class="site-wrapper <?= $this->getDomain()->jsonGet('class', '') ?>">
    <header class="header <?= Yii::$app->getUser()->isGuest === false ? 'logged-header' : '' ?>">
        <div class="mobile-header page-wrapper">
            <div class="burger-btn js-burger-btn">
                <span></span>
                <span></span>
                <span></span>
            </div>
            <a href="/" class="header-logo">
                <img src="<?= $this->getDomain()->getPath('logo_header') ?>" alt="<?= $this->getDomain()->name ?>">
            </a>
            <div class="mobile-header-account-block">
                <div class="auth-block">
                    <a href="javascript:void(0);" class="header-log-link js-auth-button">
                        <svg width="26" height="26" viewBox="0 0 26 26" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M19.1915 9.6932C20.2547 4.02449 15.3962 -0.348882 11.2413 1.38379" stroke="#c6022d" stroke-width="1.5" stroke-linecap="round"/>
                            <path d="M16.6748 10.1305C16.6748 13.7264 14.308 16.3779 11.683 16.3779C9.05799 16.3779 6.69119 13.7264 6.69119 10.1305C6.69119 6.53464 9.05799 3.88312 11.683 3.88312C14.308 3.88312 16.6748 6.53464 16.6748 10.1305Z" stroke="#c6022d" stroke-width="1.5" stroke-linecap="round"/>
                            <path d="M19.1915 14.0666C21.6943 14.3581 25.5548 16.27 24.9333 21.5013" stroke="#c6022d" stroke-width="1.5" stroke-linecap="round"/>
                            <path d="M6.82455 17.5652C4.45677 17.7032 0.800529 19.1607 1.0085 24.1173C1.02965 24.6215 1.46166 25 1.96632 25H21.646C22.0347 25 22.3964 24.7769 22.5103 24.4052C23.1414 22.3451 22.7408 18.8322 16.9831 17.5652" stroke="#c6022d" stroke-width="1.5" stroke-linecap="round"/>
                            <path d="M9 19L12 22L15 19" stroke="#c6022d" stroke-width="1.5"/>
                        </svg>
                    </a>
                    <!-- <a href="javascript:void(0);" class="header-fav-link">
                        <svg width="28" height="24" viewBox="0 0 28 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M14 22.3571L27 7.45681M14 22.3571L1 7.45681M14 22.3571L20.0636 7.92257C20.1866 7.62969 20.1632 7.29577 20.0004 7.02296L16.4074 1M14 22.3571L7.93556 7.9206C7.813 7.62882 7.83578 7.29626 7.99698 7.02392L11.5627 1M27 7.45681L22.0038 1.3658C21.8138 1.13424 21.5301 1 21.2306 1H16.4074M27 7.45681H1M1 7.45681L5.99624 1.3658C6.18619 1.13424 6.46991 1 6.76941 1H11.5627M16.4074 1H11.5627" stroke="#c6022d" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                        </svg>
                        <span class="header-link-counter">0</span>
                    </a> -->
                    <?= BasketButton::widget() ?>
                </div>
            </div>
        </div>
        <div class="desktop-header">

            <?= Header::widget() ?>

            <div class="header-bot-row">
                <div class="header-bot-wrap page-wrapper">
                    <?= TopMenu::widget() ?>
                </div>
            </div>
        </div>
    </header>
    <div class="page-content">
        <?= $content ?>
    </div>
    <footer class="footer">
        <div class="footer-row page-wrapper">
            <!-- <a href="/" class="footer-logo-block">
                <img src="<?= $this->getDomain()->getPath('logo_footer') ?>" alt="<?= $this->getDomain()->name ?>">
            </a> -->
            <div class="footer-info-row">
                <?php if (empty(Parameter::getValue(2, false, false)) === false) { ?>
                    <p><?= Parameter::getValue(2, false, false) ?></p>
                <?php } ?>
                <!-- <?php if (empty(Parameter::getValue(3, false, true)) === false) { ?>
                    <p>ИНН <?= Parameter::getValue(3, false, true) ?></p>
                <?php } ?>
                <?php if (empty(Parameter::getValue(4, false, true)) === false) { ?>
                    <p>ОГРН <?= Parameter::getValue(4, false, true) ?></p>
                <?php } ?>
                <?php if (empty(Parameter::getValue(5, false, true)) === false) { ?>
                    <p>Учетный номер в гос. инспекции пробирного надзора: <?= Parameter::getValue(5, false, true) ?></p>
                <?php } ?>
                <?php if (empty(Parameter::getValue(6, true, true)) === false) { ?>
                    <a target="_blank" href="<?= Parameter::getValue(6, true, true) ?>">Политика обработки ПД</a>
                <?php } ?> -->
            </div>
        </div>
    </footer>
</div>
<?php $this->endBody() ?>
</body>

</html>
<?php $this->endPage() ?>
