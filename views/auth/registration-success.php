<?php
/* @var $this app\components\View */
?>
<section class="text-panel-section">

    <div class="text-panel-wrap page-wrapper">
        <div class="catalog-item-title-row">
            <div class="section-title"><?= $this->h1 ?></div>
        </div>
        <div class="text-panel">
            <div class="notification">
                Вы успешно зарегистрировались на сайте. После проверки заполненных данных администратом Вы сможете использовать их для входа на сайт
            </div>
        </div>
    </div>
</section>
