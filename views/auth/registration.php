<?php
/* @var $this \app\components\View */
/* @var $model \app\forms\Registration */

use app\widgets\input\File;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\MaskedInput;

?>


<section class="registration-section">
    <div class="registration-panel page-wrapper">
        <div class="catalog-item-title-row">
            <div class="section-title"><?= $this->h1 ?></div>
        </div>
        <div class="registration-panel-row">
            <div class="registration-text-col">
                <div class="registration-text">
                    <?= $this->tree->getContent() ?>
                </div>
                <?php if (empty($this->tree->image) === false) { ?>
                    <img src="<?= $this->tree->getPath('image') ?>" alt="<?= Html::encode($this->tree->name_menu) ?>">
                <?php } ?>
            </div>
            <div class="registration-form-col">
                <?php $form = ActiveForm::begin([
                    'options' => [
                        'enctype' => 'multipart/form-data',
                    ],
                    'fieldConfig' => [
                        'template' => "{input}\n{label}\n{hint}\n{error}",
                        'options' => [
                            'class' => 'input-row',
                        ],
                    ]
                ]) ?>
                    <div class="account-form-col">
                        <div class="account-inner-title">Данные пользователя</div>
                        <?= $form->field($model, 'lastName')->textInput()->label('Фамилия') ?>
                        <?= $form->field($model, 'firstName')->textInput()->label('Имя') ?>
                        <?= $form->field($model, 'middleName')->textInput()->label('Отчество') ?>
                        <?= $form->field($model, 'phone')->widget(MaskedInput::class, [
                            'mask' => '+7 (999) 999-99-99',
                            'options' => ['disabled' => true],
                        ])->label('Телефон') ?>
                        <?= $form->field($model, 'email')->textInput()->label('E-mail') ?>
                    </div>
                    <div class="account-form-col">
                        <div class="account-inner-title">Данные организации</div>
                        <?= $form->field($model, 'organization')->textInput()->label('Организация') ?>
                        <?= $form->field($model, 'inn')->textInput()->label('ИНН') ?>
                        <?= $form->field($model, 'ogrn')->textInput()->label('ОГРН') ?>
                        <?= $form->field($model, 'city')->textInput()->label('Город') ?>
                        <?= $form->field($model, 'certificate')->textInput()->label('Свидетельство о постановке на спецучет') ?>
                    </div>

                    <div class="account-form-files-row">
                        <div class="account-inner-title">Документы</div>

                        <?= $form->field($model, 'document')->widget(File::class, [
                            'options' => [
                                'label' => 'Свидетельство о постановке на спецучет',
                                'button' => 'Загрузить документ',
                            ],
                        ])->label(false) ?>

                    </div>

                    <?= $form->field($model, 'agreement', ['options' => ['class' => 'checkbox-row agreement-row']])->checkbox([], false)->label('Даю согласие на обработку <a href="javascript:void(0);">персональных данных</a>') ?>

                    <div class="btn-row">
                        <button type="submit" class="btn">Зарегистрироваться</button>
                    </div>
                <?php $form::end() ?>
            </div>
        </div>
    </div>
</section>
