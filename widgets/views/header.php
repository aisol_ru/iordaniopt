<?php
/* @var $this \app\components\View */
/* @var $manager \app\models\Manager */

use app\assets\MainAsset;
use app\widgets\BasketButton;
use app\widgets\FavoriteButton;
use yii\helpers\Url;
?>

<div class="header-top-row">
    <div class="close-desktop-header js-close-desktop-header">
        <img src="<?= MainAsset::path('img/close-header.svg') ?>" alt="">
    </div>
    <div class="header-top-row-wrap page-wrapper">
        <div class="header-sides-block hsb-right"></div>
        <div class="header-logo-block">
            <a href="/" class="header-logo">
                <img src="<?= $this->getDomain()->getPath('logo_header') ?>" alt="<?= $this->getDomain()->name ?>">
            </a>
        </div>
        <!-- <div class="header-managers-row">
            <?php if (is_null($manager) === false) { ?>

                <div class="header-manager-block-wrap">
                    <div class="header-manager-block js-header-manager-block">
                        <div class="h-manager-title">Ваш персональный менеджер</div>
                        <div class="h-manager-shop">
                            <?= $this->getDomain()->name ?>
                            <?php if ($this->getDomain()->is_premium == 1) { ?>
                                <span class="prem-tag">PREMIUM</span>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="h-manager-info-wrap">
                        <div class="h-manager-info">
                            <div class="manager-info-name"><?= $manager->name ?></div>
                            <div class="manager-info-list">
                                <div class="manager-info-list-unit">
                                    <?php foreach ($manager->jsonGet('phones') as $phone) { ?>
                                        <a href="tel:+<?= preg_replace( '/[\D]/', '', $phone) ?>"><?= $phone ?></a>
                                    <?php } ?>
                                    <?php foreach ($manager->jsonGet('emails') as $email) { ?>
                                        <a href="mailto:+<?= $email ?>"><?= $email ?></a>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            <?php } ?>
        </div> -->
        <div class="header-sides-block hsb-right">
            <div class="auth-block">
                <a href="<?= Url::to(['account/main']) ?>" class="header-log-link">
                    <img src="<?= MainAsset::path('img/user.svg') ?>" width="30" height="30">
                </a>
                <!-- <?= FavoriteButton::widget() ?> -->
                <?= BasketButton::widget() ?>
            </div>
        </div>
    </div>
</div>
