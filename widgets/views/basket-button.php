<?php
/* @var $this \app\components\View */
/* @var $count int */

use yii\helpers\Url;
use app\assets\MainAsset;
?>

<a href="<?= Url::to(['basket/main']) ?>" class="header-cart-link">
    <img src="<?= MainAsset::path('img/shopping-cart.svg') ?>" width="30" height="30">
    <span class="header-link-counter js-menu-basket-count"><?= $count ?></span>
</a>
