<?php
/* @var $this \app\components\View */
?>

<style>
    .preloader-wrap {
        z-index: 10000;
        position: fixed;
        top: 0;
        bottom: 0;
        left: 0;
        right: 0;
        background: #fff;
        display: flex;
        justify-content: center;
        align-items: center;
    }

    .preloader {
        /* width: 340px; */
        height: 130px;
        display: flex;
        justify-content: center;
        align-items: center;
        position: relative;
    }

    .preloader img {
        width: 560px;
        animation: preloader-animation 3s forwards;
    }

    @keyframes preloader-animation {
        0% {
            opacity: 0;
            transform: scale(0.6);
        }

        30% {
            opacity: 0;
            transform: scale(0.6);
        }

        90% {
            opacity: 1;
            transform: scale(1);
        }
    }

    @media screen and (max-width: 768px) {
        .preloader {
            width: 200px;
            height: 200px;
        }

        .preloader img {
            width: 100%;
            max-width: 100%;
        }
    }

    @media screen and (max-width: 480px) {
        .preloader {
            width: 120px;
            height: 120px;
        }
    }
</style>
<div class="preloader-wrap js-preloader">
    <div class="preloader">
        <img src="<?= $this->getDomain()->getPath('logo_preloader') ?>" alt="<?= $this->getDomain()->name ?>">
    </div>
</div>
