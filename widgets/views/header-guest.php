<?php
/* @var $this \app\components\View */
use app\assets\MainAsset;
?>

<div class="header-top-row">
    <div class="close-desktop-header js-close-desktop-header">
        <img src="<?= MainAsset::path('img/close-header.svg') ?>" alt="">
    </div>
    <div class="header-top-row-wrap page-wrapper">
        <div class="header-sides-block hsb-left">
<!--            <a href="/" class="header-shop-link">интернет-магазин</a>-->
        </div>
        <div class="header-logo-block">
            <a href="/" class="header-logo">
                <img src="<?= $this->getDomain()->getPath('logo_header') ?>" alt="<?= $this->getDomain()->name ?>">
            </a>
        </div>
        <div class="header-sides-block hsb-right">
            <div class="auth-block">
                <a href="javascript:void(0);" class="header-log-link js-auth-button">
                    <img src="<?= MainAsset::path('img/user.svg') ?>" width="30" height="30">
                    Вход / Регистрация
                </a>
            </div>
        </div>
    </div>
</div>
