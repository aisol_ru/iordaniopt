<?php
/* @var $this \app\components\View */
/* @var $menu \string[][] */
/* @var $currentRoute \string */

use yii\helpers\Url;

?>

<div class="account-links-list-col">
    <div class="account-links-list">
        <?php foreach ($menu as $element) { ?>
            <a href="<?= Url::to([$element['route']]) ?>" class="<?= $element['route'] === $currentRoute ? 'active' : '' ?>">
                <?= $element['name'] ?>
            </a>
        <?php } ?>
    </div>
</div>
