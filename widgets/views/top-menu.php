<?php
/* @var $this \app\components\View */
/* @var $elements \app\models\Menu[] */

?>

<div class="nav-wrapper">
    <a href="javascript:void(0);" class="header-shop-link">Интернет магазин</a>
    <nav>
        <ul>
            <?php foreach ($elements as $element) { ?>
                <li>
                    <a class="<?= $element->id === 10 && \Yii::$app->user->isGuest === true ? "js-auth-button" : ""?><?= is_null($this->tree) === false && $this->tree->url === $element->url ? 'active' : '' ?>" href="<?= $element->url ?>">
                        <?= $element->name ?>
                        <?php if ($element->is_premium) { ?>
                            <span class="prem-tag">PREMIUM</span>
                        <?php } ?>
                    </a>
                </li>
            <?php } ?>
        </ul>
    </nav>
</div>
