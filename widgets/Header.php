<?php


namespace app\widgets;


use app\models\Domain;
use app\models\user\User;
use Yii;
use yii\base\Widget;

class Header extends Widget
{

    public function run()
    {
        if (Yii::$app->getUser()->isGuest === true) {
            return $this->render('header-guest', []);
        }

        /* @var $user User */
        $user = Yii::$app->getUser()->getIdentity();
        $manager = $user->getManagers(true)->one();

        return $this->render('header', [
            'manager' => $manager,
        ]);
    }

}
