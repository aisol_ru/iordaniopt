<?php


namespace app\widgets\input;


use yii\helpers\ArrayHelper;
use yii\widgets\InputWidget;

class File extends InputWidget
{

    public $multiple = false;

    public function run()
    {
        $options = $this->options;
        $label = ArrayHelper::remove($options, 'label', false);
        $button = ArrayHelper::remove($options, 'button', false);

        $this->registerJs();

        return $this->render('file', [
            'label' => $label,
            'button' => $button,
            'model' => $this->model,
            'attribute' => $this->attribute,
            'multiple' => $this->multiple,
        ]);
    }

    private function registerJs()
    {
        $js = <<<JS
$('.js-file-input').on('change', 'input[type="file"]', function() {
    var input = $(this);
    var block = input.closest('.js-file-input');
    var fileListBlock = block.find('.js-file-list');
    fileListBlock.find('.js-file').remove();
    var removeUrl = input.data('remove-url') ;
    var image = input.data('image') ;
    var files = input.get(0).files;
    for (var i = 0; i < files.length; i++) {
        var file = files[i];
        var fileHtml = `
<div class="upload-doc-link-label js-file">
    \${file.name}
</div>
            `;
        fileListBlock.append(fileHtml);
    }
});
JS;

        $this->getView()->registerJs($js);
    }

}
