<?php
/* @var $this \app\components\View */
/* @var $label string|null */
/* @var $button string|null */
/* @var $model \yii\base\Model */
/* @var $attribute string */
/* @var $multiple boolean */

use app\assets\MainAsset;
use yii\helpers\Html;

?>

<div class="js-file-input file-input">
    <div class="upload-doc-list">
        <div class="upload-doc-list-unit js-file-list">
            <?php if (empty($label) === false) { ?>
                <p><?= $label ?></p>
            <?php } ?>
            <?php if (empty($model->{$attribute}) === false) { ?>
                <div class="upload-doc-link-label js-file">
                    <?= $model->{$attribute} ?>
                </div>
            <?php } ?>
        </div>
    </div>
    <label class="upload-doc-btn" for="<?= Html::getInputId($model, $attribute) ?>">
        <img src="<?= MainAsset::path('img/upload-doc-icon.svg') ?>"><?= $button ?>
    </label>

    <?= Html::activeFileInput($model, $attribute, [
        'hidden' => true,
        'multiple' => $multiple,
        'data-image' => MainAsset::path('img/close-header.svg'),
    ]) ?>
</div>
