<?php

namespace app\widgets;

use app\models\Menu;
use yii\base\Widget;

class TopMenu extends Widget
{

    public function run()
    {
        $query = Menu::find()
            ->isActive()
            ->bySort();

        if (\Yii::$app->getUser()->can('user') === true) {
            $query->andWhere(['show_user' => 1]);
        } else {
            $query->andWhere(['show_guest' => 1]);
        }

        return $this->render('top-menu', [
            'elements' => $query->all(),
        ]);
    }

}
