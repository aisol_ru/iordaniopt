<?php


namespace app\widgets;


use app\models\order\Order;
use yii\base\Widget;

class BasketButton extends Widget
{

    public function run()
    {
        $count = 0;
        $order = Order::getCurrent(false);
        if (is_null($order) === false) {
            $count = $order->good_count;
        }

        return $this->render('basket-button', [
            'count' => $count
        ]);
    }

}
