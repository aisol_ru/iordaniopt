<?php


namespace app\widgets;


use yii\base\Widget;

class AccountMenu extends Widget
{

    private function getMenu()
    {
        return [
            ['route' => 'account/personal-data', 'name' => 'Персональные данные'],
            ['route' => 'account/order-list', 'name' => 'Заказы'],
            //['route' => 'account/favorite-list', 'name' => 'Отложенные украшения'],
            ['route' => 'basket/main', 'name' => 'Корзина'],
            ['route' => 'auth/logout', 'name' => 'Выйти'],
        ];
    }

    public function run()
    {
        return $this->render('account-menu', [
            'menu' => $this->getMenu(),
            'currentRoute' => \Yii::$app->controller->getRoute(),
        ]);
    }

}
