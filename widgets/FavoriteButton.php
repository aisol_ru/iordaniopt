<?php


namespace app\widgets;


use app\models\user\Favorite;
use yii\base\Widget;

class FavoriteButton extends Widget
{

    public function run()
    {



        return $this->render('favorite-button', [
            'count' => Favorite::getCount(),
        ]);
    }

}