<?php


namespace app\widgets\popup;


use yii\base\Widget;

class Feedback extends Widget
{

    public function run()
    {

        $this->registerJs();

        return $this->render('feedback', [
            'model' => new \app\forms\Feedback(),
        ]);
    }

    private function registerJs()
    {
        $js = <<<JS
$(document).on('click', '.js-open-feedback-popup', function() {
    $.fancybox.open({
    src: '#feedback-modal',
    type : 'inline',
    afterClose: function() {
        $('#feedback-modal').find('form input[type="text"]').val('');
    }
});
});
JS;

        $this->getView()->registerJs($js);
    }

}