<?php
/* @var $this \app\components\View */
use app\widgets\AjaxSubmitButton;
use yii\widgets\ActiveForm;
?>

<div style="display: none;" id="login-modal">
  <?php $form = ActiveForm::begin([]) ?>
    <div class="modal-win">
        <div class="modal-win-cont">
            <div class="modal-title">Войти</div>
            <?= $form->field($model, 'login', ['options' => ['class' => 'input-row']])->textInput(['placeholder' => 'Логин *'])->label(false) ?>
            <?= $form->field($model, 'password', ['options' => ['class' => 'input-row']])->passwordInput(['placeholder' => 'Пароль *'])->label(false) ?>
        </div>
        <div class="btn-row">
          <?=
          AjaxSubmitButton::widget([
              'useWithActiveForm' => $form->getId(),
              'label' => 'Войти',
              'options' => ['class' => 'btn'],
              'ajaxOptions' => [
                  'type' => 'POST',
                  'url' => '/ajax/auth/auth',
                  'dataType' => 'json',
                  'success' => new \yii\web\JsExpression("function(data){
                      if(data.success){
                        $.growl.notice({ title: 'Авторизация', message: 'Происходит авторизация!', duration: 5000});
                        setTimeout(function () {
                          location.href = data.redirect;
                        },1500);
                      }
                      if(data.error == 1)
                      {
                        $.growl.error({ title: 'Авторизация', message: 'Неверно введен пароль', duration: 5000});
                      }
                      if(data.error == 2)
                      {
                        $.growl.error({ title: 'Авторизация', message: 'Пользователь не найден', duration: 5000});
                      }
                      if(data.error == 3)
                      {
                        $.growl.error({ title: 'Авторизация', message: 'Аккаунт не активирован. Ожидайте активацию', duration: 5000});
                      }
                  }"),
              ],
          ])
          ?>
        </div>
        <div class="btn-row" style="padding: 0;">
          <a href="javascript:void(0)" class="js-reg-button">Регистрация</a>
          <a href="javascript:void(0)" class="btn-row js-restore-button">Забыли пароль?</a>
        </div>
    </div>
    <?php $form->end() ?>
</div>


<div style="display: none;" id="reg-modal">
  <?php $form = ActiveForm::begin([]) ?>
    <div class="modal-win">
        <div class="modal-win-cont">
            <div class="modal-title">Регистрация</div>
            <?= $form->field($reg, 'name', ['options' => ['class' => 'input-row']])->textInput(['placeholder' => 'Имя *'])->label(false) ?>
            <?= $form->field($reg, 'middle_name', ['options' => ['class' => 'input-row']])->textInput(['placeholder' => 'Отчество *'])->label(false) ?>
            <?= $form->field($reg, 'lastname', ['options' => ['class' => 'input-row']])->textInput(['placeholder' => 'Фамилия *'])->label(false) ?>
            <?= $form->field($reg, 'inn', ['options' => ['class' => 'input-row']])->textInput(['placeholder' => 'ИНН *'])->label(false) ?>
            <?= $form->field($reg, 'ogrn', ['options' => ['class' => 'input-row']])->textInput(['placeholder' => 'ОГРН *'])->label(false) ?>
            <?= $form->field($reg, 'nameOrg', ['options' => ['class' => 'input-row']])->textInput(['placeholder' => 'Название организации *'])->label(false) ?>
            <?= $form->field($reg, 'city', ['options' => ['class' => 'input-row']])->textInput(['placeholder' => 'Город *'])->label(false) ?>
            <?= $form->field($reg, 'certificate', ['options' => ['class' => 'input-row']])->textInput(['placeholder' => 'Адрес доставки *'])->label(false) ?>
            <?= $form->field($reg, 'email', ['options' => ['class' => 'input-row']])->textInput(['placeholder' => 'Email *'])->label(false) ?>
            <?= $form->field($reg, 'phone', ['options' => ['class' => 'input-row']])->widget(\yii\widgets\MaskedInput::class, ['mask' => '+7 (999) 999-99-99', 'options'=>['placeholder' => 'Телефон *']])->label(false) ?>
            <?= $form->field($reg, 'login', ['options' => ['class' => 'input-row']])->textInput(['placeholder' => 'Логин *'])->label(false) ?>
            <?= $form->field($reg, 'password', ['options' => ['class' => 'input-row']])->passwordInput(['placeholder' => 'Пароль *'])->label(false) ?>
        </div>
        <div class="btn-row">
          <?=
          AjaxSubmitButton::widget([
              'useWithActiveForm' => $form->getId(),
              'label' => 'Зарегистрироваться',
              'options' => ['class' => 'btn'],
              'ajaxOptions' => [
                  'type' => 'POST',
                  'url' => '/ajax/auth/reg',
                  'dataType' => 'json',
                  'success' => new \yii\web\JsExpression("function(data){
                      if(data.success){
                        $.growl.notice({ title: 'Регистрация', message: 'Регистрация завершена. Ожидайте подтверждения учетной записи', duration: 5000});
                        $.fancybox.close();
                        form.yiiActiveForm('resetForm');
                        form[0].reset();
                      }
                      if(data.error == 1)
                      {
                        $.growl.error({ title: 'Регистрация', message: 'Такой логин уже зарегистрирован', duration: 5000});
                      }
                  }"),
              ],
          ])
          ?>
        </div>
    </div>
    <?php $form->end() ?>
</div>

<div style="display: none;" id="restore-modal">
  <?php $form = ActiveForm::begin([]) ?>
    <div class="modal-win">
        <div class="modal-win-cont">
            <div class="modal-title">Восстановить пароль</div>
            <?= $form->field($restore, 'login', ['options' => ['class' => 'input-row']])->textInput(['placeholder' => 'Логин *'])->label(false) ?>
        </div>
        <div class="btn-row">
          <?=
          AjaxSubmitButton::widget([
              'useWithActiveForm' => $form->getId(),
              'label' => 'Восстановить',
              'options' => ['class' => 'btn'],
              'ajaxOptions' => [
                  'type' => 'POST',
                  'url' => '/ajax/auth/restore',
                  'dataType' => 'json',
                  'success' => new \yii\web\JsExpression("function(data){
                      if(data.success){
                        $.growl.notice({ title: 'Восстановление', message: 'На вашу почту отправлен новый пароль', duration: 5000});
                        $.fancybox.close();
                        form.yiiActiveForm('resetForm');
                        form[0].reset();
                      }
                      if(data.error == 1)
                      {
                        $.growl.error({ title: 'Восстановление', message: 'Пользователь не найден', duration: 5000});
                      }
                  }"),
              ],
          ])
          ?>
        </div>
    </div>
    <?php $form->end() ?>
</div>
