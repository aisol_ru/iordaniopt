<?php
/* @var $this \app\components\View */
/* @var $model \app\forms\Feedback */

use app\widgets\AjaxSubmitButton;
use yii\widgets\ActiveForm;


?>

<div style="display: none;" id="feedback-modal">
    <?php $form = ActiveForm::begin([
        'options' => ['class' => 'modal-win'],
        'fieldConfig' => ['options' => ['class' => 'input-row']],
    ]) ?>
    <div class="modal-win-cont">
        <div class="modal-title">Обратная связь</div>

        <?= $form->field($model, 'name')->textInput(['placeholder' => 'Имя*'])->label(false) ?>
        <?= $form->field($model, 'phone')->textInput(['placeholder' => 'Телефон*'])->label(false) ?>
        <?= $form->field($model, 'comment')->textInput(['placeholder' => 'Комментарий*'])->label(false) ?>

        <?= $form->field($model, 'agreement', [
            'options' => ['class' => 'checkbox-row agreement-row'],
            'template' => "{input}\n{label}\n{hint}\n{error}",
        ])->checkbox([], false)->label('Даю согласие на обработку <a href="javascript:void(0);">персональных данных</a>') ?>

    </div>
    <div class="btn-row">
        <?=
        AjaxSubmitButton::widget([
            'useWithActiveForm' => $form->getId(),
            'label' => 'Отправить',
            'options' => ['class' => 'btn'],
            'ajaxOptions' => [
                'type' => 'POST',
                'url' => '/ajax/default/feedback',
                'dataType' => 'json',
                'success' => new \yii\web\JsExpression("function(data){
                    $.growl.notice({ title: 'Отправка сообщения', message: 'Ваше сообщение получено.<br>Мы свяжемся с Вами в ближайшее время.', duration: 5000});
                    $.fancybox.close();
                    form.find('input[type=\"text\"]').val('');
                    form.yiiActiveForm('resetForm');
                }"),
            ],
        ])
        ?>
    </div>
    <?php $form->end() ?>
</div>
