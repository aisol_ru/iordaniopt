<?php


namespace app\widgets\popup;

use app\forms\AuthUser;
use app\forms\Reg;
use app\forms\Restore;
use yii\base\Widget;

class Auth extends Widget
{

    public function run()
    {
        $model = new AuthUser;
        $reg = new Reg;
        $restore = new Restore;
        return $this->render('auth', [
          'model' => $model,
          'reg' => $reg,
          'restore' => $restore
        ]);
    }

}
