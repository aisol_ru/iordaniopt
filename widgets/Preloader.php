<?php


namespace app\widgets;


use Yii;
use yii\base\Widget;

class Preloader extends Widget
{

    public function run()
    {
        $showPreloader = Yii::$app->getSession()->get('show-preloader', true);
        if ($showPreloader !== true) {
            return '';
        }

        Yii::$app->getSession()->set('show-preloader', false);

        return $this->render('preloader');
    }

}
