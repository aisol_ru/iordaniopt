<?php


namespace app\widgets;


use Yii;
use yii\data\Sort;

class SortSession extends Sort
{
    public $sessionParam = 'sort';

    public function loadDefault($default)
    {
        $currentSort = Yii::$app->getRequest()->get($this->sortParam, false);
        if ($currentSort !== false) {
            Yii::$app->getSession()->set($this->sessionParam, $currentSort);
            return;
        }

        $sessionSort = Yii::$app->getSession()->get($this->sessionParam, false);
        $sessionCheck = true;
        if ($sessionSort !== false) {
            foreach ($this->parseSortParam($sessionSort) as $attribute) {
                $descending = false;
                if (strncmp($attribute, '-', 1) === 0) {
                    $descending = true;
                    $attribute = substr($attribute, 1);
                }
                if (isset($this->attributes[$attribute]) === false) {
                    $sessionCheck = false;
                    break;
                }
                $this->setAttributeOrders([$attribute => $descending === true ? SORT_DESC : SORT_ASC]);
                $this->defaultOrder = $attribute;
            }
        } else {
            $sessionCheck = false;
        }

        if ($sessionCheck === false) {
            $this->defaultOrder = $default;
        }
    }

}
