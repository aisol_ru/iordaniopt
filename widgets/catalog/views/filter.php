<?php
/* @var $this \app\components\View */
/* @var $filter \app\forms\CatalogFilter */
/* @var $categories \app\models\catalog\Category[] */
/* @var $metals \app\models\catalog\Metal[] */
/* @var $inserts \app\models\catalog\Insert[] */
/* @var $collections \app\models\catalog\Collection[] */

/* @var $route array */

use app\assets\MainAsset;
use app\widgets\CheckboxList;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\widgets\SortSelect;

?>

<?php $form = ActiveForm::begin([
    'id' => 'catalog-list-filter',
    'method' => 'GET',
    'action' => $route,
    'options' => [
        'class' => 'catalog-filter',
    ],
]) ?>
<div class="mobile-filter-head-row">
    <div class="close-mobile-filter js-close-mobile-filter">
        <img src="<?= MainAsset::path('img/close-header.svg') ?>" alt="">
    </div>
    <a href="javascript:void(0);" class="search-result-filter-btn">К результатам</a>
</div>
<div class="catalog-filter-units-wrap">
    <div class="catalog-sort-unit">
        <?= SortSelect::widget(['sort' => $sort, 'form' => $form, 'filter' => $filter]) ?>
    </div>
    <div class="catalog-filter-unit">
        <div class="catalog-filter-unit-checks-list">
            <div class="checkbox-row-block">
                <?= $form->field($filter, 'isNew', [
                    'options' => ['class' => 'checkbox-row'],
                    'template' => "{input}\n{label}\n{hint}\n{error}",
                ])->checkbox(['uncheck' => false], false)->label('Новинка') ?>
            </div>
        </div>
    </div>
    <div class="catalog-filter-unit">
        <div class="catalog-filter-unit-title">Тип украшения</div>

        <?php foreach ($categories as $category) { ?>
            <div class="checkbox-row-block">
                <div class="checkbox-row">
                    <input class="js-input-filter-catalog" <?= Yii::$app->request->get("category_outer_code") === $category["outer_code"] || isset(Yii::$app->request->get("CatalogFilter", [])["categories"]) && in_array($category["outer_code"], Yii::$app->request->get("CatalogFilter", [])["categories"]) ? "checked" : "" ?>
                           id="catalogfilter-categories_<?= $category["outer_code"] ?>" type="checkbox"
                           name="CatalogFilter[categories][]" value="<?= $category["outer_code"] ?>">
                    <label for="catalogfilter-categories_<?= $category["outer_code"] ?>"><?= $category["name"] ?></label>

                </div>
                <?php if (!is_null($category["children"])) { ?>
                    <?= $form
                        ->field($filter, 'categories[]', [
                            'options' => [
                                'class' => 'catalog-filter-unit-checks-list js-filter-category',
                                'style' => $category["isShow"] ? "display:block" : "display:none"],
                        ])
                        ->widget(CheckboxList::class, [
                            'items' => $category["children"]["items"],
                            'itemsOptions' => $category["children"]['itemsOptions'],
                        ])
                        ->label(false) ?>
                <?php } ?>
            </div>
        <?php } ?>

    </div>
    <!-- <div class="catalog-filter-unit">
            <div class="catalog-filter-unit-title">Цвет металла</div>

            <?= $form->field($filter, 'metals', [
        'options' => ['class' => 'catalog-filter-unit-checks-list'],
    ])->widget(CheckboxList::class, [
        'items' => $metals,
    ])->label(false) ?>

        </div> -->
    <!-- <div class="catalog-filter-unit">
            <div class="catalog-filter-unit-title">Основная вставка</div>

            <?= $form->field($filter, 'inserts', [
        'options' => ['class' => 'catalog-filter-unit-checks-list'],
    ])->widget(CheckboxList::class, [
        'items' => $inserts,
    ])->label(false) ?>

        </div>
        <div class="catalog-filter-unit">
            <div class="catalog-filter-unit-title">Коллекции</div>

            <?= $form->field($filter, 'collections', [
        'options' => ['class' => 'catalog-filter-unit-checks-list'],
    ])->widget(CheckboxList::class, [
        'items' => $collections,
    ])->label(false) ?>

        </div> -->
    <div class="catalog-filter-unit">
        <div class="catalog-filter-unit-title">Стоимость изделия</div>
        <div class="price-slider-wrap">
            <div class="price-slider-input-row">
                <div class="psi-input-row">
                    <div class="psi-text-for-input">от</div>
                    <?= Html::activeTextInput($filter, 'priceMin', ['id' => 'price-slider-input1',]) ?>
                </div>
                <div class="psi-input-row">
                    <div class="psi-text-for-input">до</div>
                    <?= Html::activeTextInput($filter, 'priceMax', ['id' => 'price-slider-input2']) ?>
                </div>
            </div>
            <div
                    class="price-slider-block"
                    data-min="<?= $filter->defaultPriceMin ?>"
                    data-max="<?= $filter->defaultPriceMax ?>"
                    id="price-slider-block"
            ></div>
        </div>
    </div>
</div>
<!--<div class="btn-row">-->
<!--    <a href="--><?//= Url::to($route) ?><!--" class="btn btn-white reset-filter">Сбросить</a>-->
<!--    <button type="submit" class="btn btn-white">Применить</button>-->
<!--</div>-->
<?php $form::end() ?>
