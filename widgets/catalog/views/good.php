<?php
/* @var $this \app\components\View */
/* @var $good \app\models\catalog\Good */
/* @var $url string */

/* @var $decorationClass string */

use yii\helpers\Html;
use app\assets\MainAsset;

?>

<?php if (true /* file_exists($good->getFullPath('image')) && $good->price > 0 */) { ?>
    <div class="catalog-list-unit <?= $decorationClass ?>">
        <?php if ($good->isNew() === true) { ?>
            <div class="clu-label-row">
                <div class="clu-label-unit">New</div>
            </div>
        <?php } ?>
        <!-- <div class="clu-fav js-user-favorite-button <?= $good->isFavorite() === true ? 'active' : '' ?>" data-id="<?= $good->id ?>">
          <svg width="28" height="24" viewBox="0 0 28 24" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path d="M14 22.3571L27 7.45681M14 22.3571L1 7.45681M14 22.3571L20.0636 7.92257C20.1866 7.62969 20.1632 7.29577 20.0004 7.02296L16.4074 1M14 22.3571L7.93556 7.9206C7.813 7.62882 7.83578 7.29626 7.99698 7.02392L11.5627 1M27 7.45681L22.0038 1.3658C21.8138 1.13424 21.5301 1 21.2306 1H16.4074M27 7.45681H1M1 7.45681L5.99624 1.3658C6.18619 1.13424 6.46991 1 6.76941 1H11.5627M16.4074 1H11.5627" stroke="#C4C4C4" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
          </svg>
      </div> -->
        <a href="<?= $url ?>" class="cli-wrap">
            <?php if (empty($good->image) === false) { ?>
                <div class="clu-pic-holder">
                    <img src="<?= $good->getResizeCache('image', 160, 160, 5, true) ?>"
                         alt="<?= Html::encode($good->name) ?>">
                </div>
            <?php } else { ?>
                <div class="clu-pic-holder">
                    <img src="<?= MainAsset::path('img/nophoto.jpg') ?>" alt="<?= Html::encode($good->name) ?>">
                </div>
            <?php } ?>

            <div class="clu-title">
                <?= $good->name ?>
            </div>

            <!--          --><?php //if ($good->weight_average > 0) { ?>
            <!--              <span class="clu-weight">Ср. вес: -->
            <? //= number_format($good->weight_average, 2, '.', '') ?><!-- гр.</span>-->
            <!--          --><?php //} ?>

            <?php if ($good->getWeightForGoodCard() !== false) { ?>
                <span class="clu-weight"><?= $good->getWeightForGoodCard() ?></span>
            <?php } ?>

            <!--            --><?php //if (($firstCoating = $good->getFirstCoating()) !== false) { ?>
            <!--                <span class="clu-weight">П: --><? //= $firstCoating ?><!--</span>-->
            <!--            --><?php //} ?>


            <?php if (!empty($allCoating = $good->getAllCoatings())) { ?>
                <span class="clu-weight">П:
                    <?php foreach ($allCoating as $coating) { ?>
                        <u><?= $coating ?></u>;
                    <?php } ?>
                </span>
            <?php } ?>

            <?php if ($good->price > 0) { ?>
                <span class="clu-weight">Ср. цена: <?= number_format($good->price, 0, ", ", " ") ?> р.</span>
            <?php } ?>
        </a>
        <div class="clu-incart-btn">
            <div class="btn-row">
                <a href="<?= $url ?>" class="btn btn-ly">Купить</a>
            </div>
        </div>
    </div>
<?php } ?>
