<?php


namespace app\widgets\catalog;


use app\forms\CatalogFilter;
use app\models\catalog\Category;
use app\models\catalog\Collection;
use app\models\catalog\Insert;
use app\models\catalog\Metal;
use app\models\Domain;
use Yii;
use yii\base\Widget;
use yii\helpers\ArrayHelper;
use app\widgets\SortSession;

class Filter extends Widget
{

    /**
     * @var CatalogFilter
     */
    public $filter;
    public $route = ['catalog/catalog-section'];
    public $brandId = null;
    public $parametersQuery = null;

    public function run()
    {

        $mainCategories = Category::find()
            ->distinct()
            ->joinWith('goods good', false)
//            ->andWhere(['>', 'good.price', 0])
            ->andWhere(["parent_outercode" => ""])
            ->orderBy(["name" => SORT_ASC])
            ->isActive()
            ->all();

        $categories = [];
        foreach ($mainCategories as $category) {

            $arrWithChildes = [
                "outer_code" => $category->outer_code,
                "name" => $category->name,
                "children" => null,
            ];

            if (!empty($category->children)) {

                $elems = [
                    'items' => [],
                    'itemsOptions' => [],
                ];

                $isShow = false;
                $getCategories = Yii::$app->request->get("CatalogFilter", [])["categories"] ?? [];
                if (!empty($getCategories)) {
                    foreach ($category->children as $child) {
                        $elems["items"][$child["outer_code"]] = $child->name;

                        if (in_array($category->outer_code, $getCategories)) {
                            $isShow = true;
                            $elems['itemsOptions'][$child->outer_code] = ["checked" => "checked", "disabled" => "disabled"];
                        } elseif (in_array($child->outer_code, $getCategories)) {
                            $isShow = true;
                            $elems['itemsOptions'][$child->outer_code] = ["checked" => "checked"];
                        } else {
                            $elems['itemsOptions'][$child->outer_code] = [];
                        }

                    }
                } else {
                    foreach ($category->children as $child) {

                        $elems["items"][$child["outer_code"]] = $child->name;

                        $getMainCategory = Yii::$app->request->get("category_outer_code");
                        if ($getMainCategory === $category->outer_code) {
                            $isShow = true;
                            $elems['itemsOptions'][$child->outer_code] = ["checked" => "checked", "disabled" => "disabled"];
                        } else {
                            $elems['itemsOptions'][$child->outer_code] = [];
                        }
                    }
                }

                $arrWithChildes["children"] = $elems;
                $arrWithChildes["isShow"] = $isShow;

            }

            $categories[] = $arrWithChildes;
        }

        $metals = Metal::find()
            ->distinct()
            ->joinWith('goods good', false)
            ->andFilterWhere(['good.brand_id' => $this->brandId])
            ->andFilterWhere(['good.id' => $this->parametersQuery])
            ->all();
        $inserts = Insert::find()
            ->distinct()
            ->joinWith('goods good', false)
            ->andFilterWhere(['good.brand_id' => $this->brandId])
            ->andFilterWhere(['good.id' => $this->parametersQuery])
            ->all();
        $collections = Collection::find()
            ->distinct()
            ->joinWith('goods good', false)
            ->andFilterWhere(['good.brand_id' => $this->brandId])
            ->andFilterWhere(['good.id' => $this->parametersQuery])
            ->all();

        $sort = new SortSession([
            'sortParam' => 'sort',
            'sessionParam' => 'catalog-list-sort',
            'attributes' => [
//                 'price_asc' => [
//                     'asc' => ['price' => SORT_ASC],
//                     'desc' => ['price' => SORT_DESC],
//                     'default' => SORT_ASC,
//                     'label' => 'По цене (убывание)',
//                 ],
//                 'price_desc' => [
//                     'asc' => ['price' => SORT_ASC],
//                     'desc' => ['price' => SORT_DESC],
//                     'default' => SORT_DESC,
//                     'label' => 'По цене (возрастание)',
//                 ],
                'vendor_code' => [
                    'asc' => ['t.vendor_code' => SORT_ASC],
                    'desc' => ['t.vendor_code' => SORT_DESC],
                    'default' => SORT_DESC,
                    'label' => 'По артикулу',
                ],
            ],
        ]);

        return $this->render('filter', [
            'filter' => $this->filter,
            'categories' => $categories,
            'metals' => ArrayHelper::map($metals, 'id', 'name'),
            'inserts' => ArrayHelper::map($inserts, 'id', 'name'),
            'collections' => ArrayHelper::map($collections, 'id', 'name'),
            'route' => $this->route,
            'sort' => $sort,
        ]);
    }

}
