<?php

namespace app\widgets\catalog;

use app\models\Domain;
use yii\base\Widget;
use yii\helpers\Url;

class Good extends Widget
{
    /**
     * @var \app\models\catalog\Good
     */
    public $good;

    public $urlAddScheme = false;

    public function run()
    {
        $domain = Domain::find()
            ->andWhere(['brand_id' => $this->good->brand_id])
            ->one();
        $decorationClass = '';
        if (is_null($domain) === false) {
            $decorationClass = $domain->jsonGet('class', '');
        }

        $url = Url::to(['catalog/good', 'alias' => $this->good->alias, 'category_outer_code' => $this->good->category_id]);
        if ($this->urlAddScheme === true && is_null($domain) === false) {
            $url = '//' . $domain->domain . $url;
        }

        return $this->render('good', [
            'good' => $this->good,
            'url' => $url,
            'decorationClass' => $decorationClass,
        ]);
    }

}
