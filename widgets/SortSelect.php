<?php

namespace app\widgets;

use Yii;
use yii\base\Widget;
use yii\helpers\Html;

class SortSelect extends Widget
{

    /**
     * @var SortSession
     */
    public $sort = null;
    public $options = [];
    public $form;
    public $filter;

//    public function init()
//    {
//        if (!isset($this->options['id'])) {
//            $this->options['id'] = $this->getId();
//        }
//
//        $js = <<<JS
//$('#{$this->options['id']}').on('change', function() {
//    url = $(this).find(':checked').data('url');
//    window.location = url;
//});
//JS;
//
//        $this->view->registerJs($js);
//    }

    public function run()
    {
        $items = [];
        foreach ($this->sort->attributes as $attributeName => $parameters) {
            if (!isset($parameters['label'])) {
                continue;
            }
            $items[$attributeName] = $parameters['label'];
            $this->options['options'][$attributeName] = ['data-url' => $this->sort->createUrl($attributeName)];
        }

        $currentSort = Yii::$app->getRequest()->get($this->sort->sortParam, false);
        if ($currentSort === false) {
            $currentSort = $this->sort->defaultOrder;
        } else {
            if (strncmp($currentSort, '-', 1) === 0) {
                $descending = true;
                $currentSort = substr($currentSort, 1);
            }
        }

        return $this->form->field($this->filter, "sort")
            ->dropDownList($items, $this->options)
            ->label(false);
    }

}
