<?php

namespace app\widgets;

use yii\widgets\InputWidget;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

class CheckboxList extends InputWidget
{

    public $items = [];
    public $options = [];
    public $select = null;
    public $itemsOptions = [];
    public $blockOptions = [];
    public $defaultItemOptions = [];

    public function run()
    {
        $options = $this->options;
        if (isset($options['id']) === false) {
            $options['id'] = Html::getInputId($this->model, $this->attribute);
        }
        $name = isset($options['name']) ? $options['name'] : Html::getInputName($this->model, $this->attribute);
        if (substr($name, -2) !== '[]') {
            $name .= '[]';
        }
        $lines = [];
        if ($this->hasModel() === true) {
            $selection = Html::getAttributeValue($this->model, $this->attribute);
        } else {
            $selection = [$this->select];
        }

        Html::addCssClass($this->blockOptions, 'checkbox-row');

        foreach ($this->items as $key => $labelName) {
            $itemOptions = ArrayHelper::merge($this->defaultItemOptions, ArrayHelper::getValue($this->itemsOptions, $key, []));
            $itemOptions['value'] = $key;
            $itemOptions['id'] = $options['id'] . '_' . $key;

            $checked = $selection !== null &&
                    (!is_array($selection) && !strcmp($key, $selection) || is_array($selection) && in_array($key, $selection));

            $line = Html::checkbox($name, $checked, $itemOptions);
            $label = Html::label($labelName, $itemOptions['id']);

            $checkBlock = Html::tag('div', $line . $label, $this->blockOptions);
            $lines[] = Html::tag('div', $checkBlock, ['class' => 'checkbox-row-block']);
        }

        return implode("\n", $lines);
    }

}
