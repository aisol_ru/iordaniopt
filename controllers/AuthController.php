<?php

namespace app\controllers;

use app\components\AmoApi;
use app\components\controller\Frontend;
use app\components\Mailer;
use app\components\MultiLogin;
use app\forms\Registration;
use Yii;
use yii\web\ForbiddenHttpException;
use yii\web\UploadedFile;

class AuthController extends Frontend
{

    public function actionDomainAuth($token)
    {
        $multiLogin = new MultiLogin($token);
        $redirectUrl = $multiLogin->auth();

        return $this->redirect($redirectUrl);
    }

    public function actionDomainLogout($token)
    {
        $multiLogin = new MultiLogin($token);
        $redirectUrl = $multiLogin->logout();

        return $this->redirect($redirectUrl);
    }

    public function actionRegistration()
    {
        $authSession = Yii::$app->getSession()->get('auth-phone-code', false);

        if ($authSession === false) {
            throw new ForbiddenHttpException();
        }

        $model = new Registration();
        $model->phone = $authSession['phone'];
        $model->document = UploadedFile::getInstance($model, 'document');
        if ($model->load(Yii::$app->getRequest()->post()) === true && $model->validate() === true) {

            $user = $model->save();

            Yii::$app->getSession()->remove('auth-phone-code');

            Mailer::registration($user);
            Mailer::registrationAdmin($user);

            return $this->render('registration-success', [

            ]);
        }



        return $this->render('registration', [
            'model' => $model,
        ]);
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();
        $multiLogin = new MultiLogin();
        $redirectUrl = $multiLogin->goRedirectLogout('/');
        return $this->redirect($redirectUrl);
    }

    public function actionAmoCrmAuth()
    {
        $apiClient = AmoApi::getApiClient();

        if (!empty(Yii::$app->request->get("referer", ""))) {
            $apiClient->setAccountBaseDomain(Yii::$app->request->get("referer", ""));
        }

        try {
            $accessToken = $apiClient
                ->getOAuthClient()
                ->getAccessTokenByCode(Yii::$app->request->get("code", ""));

            if (!$accessToken->hasExpired()) {
                AmoApi::saveToken($accessToken);
            }
        } catch (Exception $e) {
            die((string)$e);
        }

        $ownerDetails = $apiClient->getOAuthClient()->getResourceOwner($accessToken);

        printf('Привет, %s!', $ownerDetails->getName());
    }
}
