<?php

namespace app\controllers;

use app\components\controller\Frontend;
use app\components\IcmsHelper;
use app\components\interfaces\UrlManagerMultiDomain;
use app\models\banner\Banner;
use app\models\Brand;
use app\models\catalog\Size;
use app\models\contact\Contact;
use app\models\Event;
use app\models\parameter\Parameter;
use app\models\slider\Slide;
use app\models\order\Order;
use app\models\contact\Social;
use yii\web\ErrorAction;
use yii\web\NotFoundHttpException;
use Yii;

class SiteController extends Frontend
{

    public function actions()
    {
        return [
            'error' => [
                'class' => ErrorAction::class,
            ],
        ];
    }

    public function actionIndex()
    {

        if (Yii::$app->getUser()->isGuest === false) {
            return Yii::$app->getResponse()->redirect('/katalog');
        }

        $this->layout = 'index';

        $banners = Banner::find()
            ->andWhere(['category_id' => Banner::CATEGORY_MAIN])
            ->isActive()
            ->bySort()
            ->limit(5)
            ->all();

        $slides = Slide::find()
            ->andWhere(['slider_id' => Slide::SLIDER_MAIN])
            ->isActive()
            ->bySort()
            ->all();
        return $this->render('index', [
            'banners' => $banners,
            'slides' => $slides,
        ]);
    }

    public function actionPage()
    {
        return $this->render('text');
    }

    public function actionSitemap()
    {
        return $this->render('sitemap');
    }

    public function actionEventList()
    {
        $events = Event::find()->isActive()->bySort()->all();


        return $this->render('event-list', [
            'events' => $events,
        ]);
    }

    public function actionContact()
    {
        $contact = Contact::find()->one();
        $socials = Social::find()->isActive()->bySort()->all();

        return $this->render('contact', [
            'contact' => $contact,
            'socials' => $socials,
        ]);
    }

    public function actionNewList()
    {
        $banners = Banner::find()
            ->andWhere(['category_id' => Banner::CATEGORY_NEW_COLLECTIONS])
            ->isActive()
            ->bySort()
            ->all();

        return $this->render('new-list', [
            'banners' => $banners,
        ]);
    }

    public function actionActionList()
    {
        $slides = Slide::find()
            ->andWhere(['slider_id' => Slide::SLIDER_ACTION])
            ->isActive()
            ->bySort()
            ->all();

        return $this->render('action-list', [
            'slides' => $slides,
        ]);
    }

    public function actionBrand()
    {
        $brands = Brand::find()
            ->isActive()
            ->bySort()
            ->all();

        $youtubeLink = Parameter::getValue(7, false, true);
        $youtubeId = IcmsHelper::getYoutubeEmbedUrl($youtubeLink);

        return $this->render('brand', [
            'brands' => $brands,
            'youtubeLink' => $youtubeLink,
            'youtubeId' => $youtubeId,
        ]);
    }

    public function actionRobotsTxt()
    {
        /* @var $urlManager UrlManagerMultiDomain */
        $urlManager = \Yii::$app->getUrlManager();
        $domain = $urlManager->getDomain();

        if (empty($domain->robots) === true) {
            throw new NotFoundHttpException();
        }

        return \Yii::$app->getResponse()->sendFile($domain->getFullPath('robots'), 'robots.txt', [
            'inline' => true,
        ]);
    }

    public function actionSitemapXml()
    {
        /* @var $urlManager UrlManagerMultiDomain */
        $urlManager = \Yii::$app->getUrlManager();
        $domain = $urlManager->getDomain();

        if (empty($domain->sitemap) === true) {
            throw new NotFoundHttpException();
        }

        return \Yii::$app->getResponse()->sendFile($domain->getFullPath('sitemap'), 'sitemap.xml', [
            'inline' => true,
        ]);
    }

    public function actionTest()
    {
        $id = Yii::$app->request->get("id", "");

        $order = Order::findOne($id);

        $xlsx = $order->generateXlsx();
        $tempPath = tempnam(sys_get_temp_dir(), 'order-xml');
        $xlsx->save($tempPath);
        Yii::$app->getResponse()->sendFile($tempPath, 'order ' . $order->id . '.xlsx');
        unlink($tempPath);
    }

}
