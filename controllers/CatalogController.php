<?php

namespace app\controllers;

use app\components\controller\Frontend;
use app\components\UrlManager;
use app\components\View;
use app\forms\CatalogFilter;
use app\forms\SearchCatalogOnMainForm;
use app\models\catalog\Category;
use app\models\catalog\Good;
use app\models\catalog\Image;
use app\models\catalog\Size;
use app\models\order\Order;
use app\models\slider\Slide;
use app\widgets\SortSession;
use Props\NotFoundException;
use Yii;
use yii\data\Pagination;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class CatalogController extends Frontend
{
    public $defaultAction = false;

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['list', 'good', 'catalog-section'],
                        'allow' => true,
                        'roles' => ['user'],
                    ],
                ],
            ],
        ];
    }

    public function actionList()
    {

        $categories = Category::find()
            ->andWhere(["parent_outercode" => ""])
            ->isActive()
            ->all();

        $slides = Slide::find()
            ->andWhere(["slider_id" => 3])
            ->all();

        $filter = new CatalogFilter();

        if ($filter->load(Yii::$app->request->get())) {
            return $this->redirect(["catalog/catalog-section", "CatalogFilter[word]" => $filter->word]);
        }

        return $this->render("sections", [
            'slides' => $slides,
            'categories' => $categories,
            'order' => Order::getCurrent(false),
            'filter' => $filter,
        ]);

    }

    public function actionCatalogSection()
    {

        $categoryOuterCode = Yii::$app->request->get("category_outer_code");
        $isFilter = true;

        $query = Good::find()
            ->alias('t')
            ->isActive();

        /* @var $view View */
        $view = $this->getView();
        $view->h1 = $view->title = "Каталог";

        if (!is_null($categoryOuterCode)) {

            $isFilter = false;

            $category = Category::find()
                ->andWhere(["outer_code" => $categoryOuterCode])
                ->one();

            if (is_null($category)) {
                throw new NotFoundException("Категория не найдена");
            }

            $view->h1 = $category->name;
            $view->title = $category->name;

            if (!empty($category->children)) {

                $child = ArrayHelper::map($category->children, "outer_code", "outer_code");
                $child[$categoryOuterCode] = $categoryOuterCode;

                $query->andFilterWhere(['t.category_id' => $child]);

            } else {
                $query->andWhere(["t.category_id" => $categoryOuterCode]);
            }

        }

        if (Yii::$app->getUser()->can('manager') === false) {
            $query->andWhere(['!=', 't.image', '']);
        }

        /* @var $urlManager UrlManager */
        $urlManager = Yii::$app->getUrlManager();
        if ($urlManager->getDomain()->brand_id) {
            $query->andWhere(['t.brand_id' => $urlManager->getDomain()->brand_id]);
        }

        $sort = new SortSession([
            'sortParam' => 'sort',
            'sessionParam' => 'catalog-list-sort',
            'attributes' => [
                'has_emale',
//                 'price_asc' => [
//                     'asc' => ['price' => SORT_ASC],
//                     'desc' => ['price' => SORT_DESC],
//                     'default' => SORT_ASC,
//                     'label' => 'По цене (убывание)',
//                 ],
//                 'price_desc' => [
//                     'asc' => ['price' => SORT_ASC],
//                     'desc' => ['price' => SORT_DESC],
//                     'default' => SORT_DESC,
//                     'label' => 'По цене (возрастание)',
//                 ],
                'vendor_code' => [
                    'asc' => ['t.vendor_code' => SORT_ASC],
                    'desc' => ['t.vendor_code' => SORT_DESC],
                    'default' => SORT_DESC,
                    'label' => 'По артикулу',
                ],
            ],
            'defaultOrder' => ['has_emale' => SORT_ASC]
        ]);

        $filter = new CatalogFilter();
        $filter->getDefaultPrices($query);

        $query->distinct();

        if ($filter->load(Yii::$app->getRequest()->get()) === true && $filter->validate() === true) {
            $filter->apply($query);
            $filter->priceMin = $filter->priceMin ?: $filter->defaultPriceMin;
            $filter->priceMax = $filter->priceMax ?: $filter->defaultPriceMax;
        } else {
            $filter->priceMin = $filter->defaultPriceMin;
            $filter->priceMax = $filter->defaultPriceMax;
        }

        $query->orderBy($sort->orders);

        $pagination = new Pagination([
            'defaultPageSize' => 50,
            'totalCount' => $query->count(),
        ]);
        
        $query
            ->limit($pagination->getLimit())
            ->offset($pagination->getOffset());

        if (Yii::$app->getRequest()->get('get-list', false) !== false) {
            Yii::$app->getResponse()->format = Response::FORMAT_RAW;
            $html = '';
            /* @var $good Good */
            foreach ($query->each() as $good) {
                $html .= \app\widgets\catalog\Good::widget(['good' => $good]);
            }
            return $html;
        }

        return $this->render('list', [
            'goods' => $query->all(),
            'filter' => $filter,
            'order' => Order::getCurrent(false),
            'pagination' => $pagination,
            'isFilter' => $isFilter,
            'categoryOuterCode' => $categoryOuterCode,
        ]);
    }


    public function actionGood($alias)
    {
        /* @var $good Good */
        $good = Good::find()
            ->andWhere(['alias' => $alias])
            ->isActive()
            ->one();

        /* @var $urlManager UrlManager */
        $urlManager = Yii::$app->getUrlManager();
        if ($good->brand_id != 0 && $urlManager->getDomain()->brand_id !== $good->brand_id) {
            throw new NotFoundHttpException();
        }

        if (is_null($good) === true) {
            throw new NotFoundHttpException();
        }

        /* @var $view View */
        $view = $this->getView();
        $view->h1 = $good->name;
        $view->title = $good->seo_title ?: $good->name;
        $view->description = $good->seo_description;
        $view->keywords = $good->seo_keywords;

        /* @var $images Image[] */
        $images = $good->getImages()
            ->bySort()
            ->all();

        $imagesCount = count($images);
        if (!empty($good->image)) $imagesCount++;

        $price = $good->price;

        $sizes = json_decode($good->sizes_json, true);

        return $this->render('good', [
            'good' => $good,
            'images' => $images,
            'price' => $price,
            'imagesCount' => $imagesCount,
            'sizes' => $sizes,
        ]);
    }

}
