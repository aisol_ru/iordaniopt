<?php

namespace app\controllers;

use app\components\controller\Frontend;
use app\components\UrlManager;
use app\forms\CatalogFilter;
use app\forms\PersonalData;
use app\models\catalog\Good;
use app\models\Domain;
use app\models\order\Order;
use app\models\user\Favorite;
use app\models\user\User;
use app\widgets\SortSession;
use Yii;
use yii\data\Pagination;
use yii\filters\AccessControl;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\UploadedFile;

class AccountController extends Frontend
{

    public $defaultAction = false;

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['main', 'personal-data', 'order-list', 'order-view', 'order-create-xlsx', 'favorite-list'],
                        'allow' => true,
                        'roles' => ['user'],
                    ],
                ],
            ],
        ];
    }

    public function actionMain()
    {

        return $this->redirect(['account/personal-data']);
    }

    public function actionPersonalData()
    {
        $model = new PersonalData();
        /* @var $currentUser User */
        $currentUser = Yii::$app->getUser()->getIdentity();
        $model->fill($currentUser);
        // $uploadedFile = UploadedFile::getInstance($model, 'document');
        // if (is_null($uploadedFile) === false) {
        //     $model->document = $uploadedFile;
        // }

        if ($model->load(Yii::$app->getRequest()->post()) === true && $model->validate() === true) {
            $model->save();
            return $this->refresh();
        }

        return $this->render('personal-data', [
            'model' => $model,
        ]);
    }

    public function actionOrderList()
    {

        $query = Order::find('', false)
            ->andWhere(['user_id' => Yii::$app->getUser()->getId()])
            ->andWhere(['!=', 'status', Order::STATUS_NOT_COMPLETED])
            ->orderBy(['number' => SORT_DESC]);

        $domains = Domain::find()
            ->indexBy('id')
            ->all();

        return $this->render('order/list', [
            'orders' => $query->all(),
            'domains' => $domains,
        ]);
    }

    public function actionOrderView($number)
    {
        /* @var $order Order|null */
        $order = Order::find()
            ->andWhere(['number' => $number])
            ->one();
        if (is_null($order) === true) {
            throw new NotFoundHttpException();
        }

        if ($order->user_id != Yii::$app->getUser()->getId()) {
            throw new ForbiddenHttpException();
        }

        return $this->render('order/view', [
            'order' => $order,
            'goods' => $order->getGoods()->all(),
        ]);
    }

    public function actionOrderCreateXlsx($number)
    {
        /* @var $order Order|null */
        $order = Order::find()
            ->andWhere(['number' => $number])
            ->one();
        if (is_null($order) === true) {
            throw new NotFoundHttpException();
        }

        if ($order->user_id != Yii::$app->getUser()->getId()) {
            throw new ForbiddenHttpException();
        }

        $writer = $order->generateXlsx();

        $tempPath = tempnam(sys_get_temp_dir(), 'order-xml');
        $writer->save($tempPath);

        Yii::$app->getResponse()->sendFile($tempPath, "order-{$order->number}.xlsx");

        unlink($tempPath);
    }

    public function actionFavoriteList()
    {
        $query = Good::find()
            ->alias('t')
            ->joinWith('prices price')
            ->andWhere(['t.id' => Favorite::find()
                ->select(['link_id'])
                ->andWhere(['user_id' => Yii::$app->getUser()->getId()])
            ])
            ->isActive();

        $filter = new CatalogFilter();
        $filter->getDefaultPrices($query);
        $query->distinct();
        if ($filter->load(Yii::$app->getRequest()->get()) === true && $filter->validate() === true) {
            $filter->apply($query);
            $filter->priceMin = $filter->priceMin ?: $filter->defaultPriceMin;
            $filter->priceMax = $filter->priceMax ?: $filter->defaultPriceMax;
        } else {
            $filter->priceMin = $filter->defaultPriceMin;
            $filter->priceMax = $filter->defaultPriceMax;
        }

        $sort = new SortSession([
            'sortParam' => 'sort',
            'sessionParam' => 'catalog-list-sort',
            'attributes' => [
                'price_asc' => [
                    'asc' => ['price.price' => SORT_ASC],
                    'desc' => ['price.price' => SORT_DESC],
                    'default' => SORT_ASC,
                    'label' => 'По цене (убывание)',
                ],
                'price_desc' => [
                    'asc' => ['price.price' => SORT_ASC],
                    'desc' => ['price.price' => SORT_DESC],
                    'default' => SORT_DESC,
                    'label' => 'По цене (возрастание)',
                ],
                'vendor_code' => [
                    'asc' => ['t.vendor_code' => SORT_ASC],
                    'desc' => ['t.vendor_code' => SORT_DESC],
                    'default' => SORT_DESC,
                    'label' => 'По артикулу',
                ],
            ],
        ]);
        $sort->loadDefault(['price_asc' => SORT_ASC]);
        $query->orderBy($sort->orders);

        $pagination = new Pagination([
            'defaultPageSize' => 50,
            'totalCount' => $query->count(),
        ]);
        $query
            ->limit($pagination->getLimit())
            ->offset($pagination->getOffset());

        if (Yii::$app->getRequest()->get('get-list', false) !== false) {
            Yii::$app->getResponse()->format = Response::FORMAT_RAW;
            $html = '';
            /* @var $good Good */
            foreach ($query->each() as $good) {
                $html .= \app\widgets\catalog\Good::widget(['good' => $good]);
            }
            return $html;
        }

        return $this->render('favorite-list', [
            'goods' => $query->all(),
            'filter' => $filter,
            'sort' => $sort,
            'order' => Order::getCurrent(false),
            'pagination' => $pagination,
        ]);
    }


}
