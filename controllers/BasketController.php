<?php

namespace app\controllers;

use app\components\controller\Frontend;
use app\components\UrlManager;
use app\components\View;
use app\forms\BasketOrder;
use app\forms\CatalogFilter;
use app\models\catalog\Good;
use app\models\catalog\Image;
use app\models\order\Client;
use app\models\order\Order;
use app\models\user\User;
use app\widgets\SortSession;
use Yii;
use yii\data\Pagination;
use yii\filters\AccessControl;
use yii\web\BadRequestHttpException;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class BasketController extends Frontend
{

    public $defaultAction = false;

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['main', 'success', 'create-xlsx', 'create-csv'],
                        'allow' => true,
                        'roles' => ['user'],
                    ],
                ],
            ],
        ];
    }

    public function actionMain()
    {
        $order = Order::getCurrent();

        if (is_null($order) === true || $order->good_count == 0) {

            return $this->render('empty');
        }
        $basketOrder = new BasketOrder();

        if ($basketOrder->load(Yii::$app->getRequest()->post()) === true && $basketOrder->validate() === true) {

            /* @var $client Client */
            $client = $order->getClient()->one() ?: new Client();

            /* @var $currentUser User */
            $currentUser = Yii::$app->getUser()->getIdentity();
            $client->fill($currentUser);
            $client->order_id = $order->id;
            $client->comment = $basketOrder->comment;
            $client->save(false);
            $order->send();
            $order->save(false);

            $xlsx = $order->generateXlsx();
            $xlsxPath = sys_get_temp_dir() . "/order" . $order->id . ".xlsx";
            $xlsx->save($xlsxPath);

            $order->sendNewOrderEmailAdmin([$order->generate1cXml(), $xlsxPath]);
            $order->sendNewOrderEmailManager();
            $order->sendNewOrderEmailClient();
            $order->generate1cXml();
            $order->sendAmo();

            unlink($xlsxPath);

            return $this->redirect(['basket/success', 'h' => $order->hash]);
        }

        return $this->render('main', [
            'order' => $order,
            'basketOrder' => $basketOrder,
            'goods' => $order->getGoods()->orderBy(['good_id' => SORT_ASC])->all(),
        ]);
    }

    public function actionSuccess($h)
    {
        /* @var $order Order|null */
        $order = Order::find()
            ->andWhere(['hash' => $h])
            ->one();
        if (is_null($order) === true) {
            throw new BadRequestHttpException();
        }

        if ($order->status != Order::STATUS_NEW) {
            throw new ForbiddenHttpException();
        }

        return $this->render('success', [
            'order' => $order,
        ]);
    }

    public function actionCreateXlsx()
    {
        $order = Order::getCurrent(true);
        $writer = $order->generateXlsx();

        $tempPath = tempnam(sys_get_temp_dir(), 'order-xml');
        $writer->save($tempPath);

        Yii::$app->getResponse()->sendFile($tempPath, 'order.xlsx');

        unlink($tempPath);
    }

    public function actionCreateCsv()
    {
        $order = Order::getCurrent(true);

        $writer = $order->generateCsv();

        $tempPath = tempnam(sys_get_temp_dir(), 'order-csv');

        $fp = fopen($tempPath, 'w');

        foreach ($writer as $csvStr) {
            fputcsv($fp, $csvStr, ";");
        }

        Yii::$app->getResponse()->sendFile($tempPath, 'order.csv');

        unlink($tempPath);
    }

}
