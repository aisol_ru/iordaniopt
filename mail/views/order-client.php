<?php
/* @var $domain \app\models\Domain */
/* @var $order \app\models\order\Order */
/* @var $client \app\models\order\Client */
/* @var $orderedDate string */
/* @var $manager \app\models\Manager */

use yii\helpers\Html;

?>

<h1>Заказ №<?= $order->number ?> от <?= $orderedDate ?></h1>

<?php if (is_null($manager) === false) { ?>
    <h2>Ваш персональный менеджер - <?= $manager->name ?></h2>
    <?php if (empty($manager->jsonGet('phones', [])) === false) { ?>
        <p>Телефон: <?= implode(', ', $manager->jsonData['phones']) ?></p>
    <?php } ?>
    <?php if (empty($manager->jsonGet('emails', [])) === false) { ?>
        <p>Email: <?= implode(', ', $manager->jsonData['emails']) ?></p>
    <?php } ?>
<?php } ?>



<h2>Товары в заказе</h2>
<table border="box" style="width: 100%;">
    <thead>
        <tr>
            <th></th>
            <th>Наименование</th>
            <th>Артикул</th>
            <th>Проба</th>
            <th>Количество (шт.)</th>
            <th>Размеры</th>
            <th>Вес (гр.)</th>
            <th>Итоговый вес (гр.)</th>
            <th>Цена (руб.)</th>
            <th>Сумма (руб.)</th>
        </tr>
    </thead>
    <tbody>
        <?php
        /* @var $good \app\models\order\Good */
        foreach ($order->getGoods()->each() as $good) { ?>

            <tr>
                <td style="text-align: center; vertical-align: middle">
                    <?php if (empty($good->good->image) === false) { ?>
                        <img
                            src="http://<?= $domain->domain ?><?= $good->good->getResizeCache('image', 100, 100, 5) ?>"
                            alt="<?= Html::encode($good->jsonData['name']) ?>"
                        >
                    <?php } ?>
                </td>
                <td>
                    <?= $good->jsonData['name'] ?>
                </td>
                <td>
                    <?= $good->good->vendor_code ?>
                </td>
                <td>
                    <?= $good->count ?>
                </td>
                <td>
                    <?php if (empty($good->jsonData['sizes']) === false) { ?>
                        <?php foreach ($good->jsonData['sizes'] as $key => $size) {
                            if ($size['count'] == 0) {
                                continue;
                            }
                            ?>
                            <?= $size['size'] ?>: <?= $size['count'] ?> шт.;
                        <?php } ?>
                    <?php } else { ?>
                        ---
                    <?php } ?>
                </td>
                <td>
                    <?= $good->jsonData['weight'] ?>
                </td>
                <td>
                    <?= $good->weight ?>
                </td>
                <td>
                    <?= number_format($good->jsonData['price_unit'], 2, '.', '&nbsp;') ?>
                </td>
                <td>
                    <?= number_format($good->sum, 2, '.', '&nbsp;') ?>
                </td>
            </tr>

        <?php } ?>
        <tr>
            <td colspan="4" style="text-align: right">Итого:</td>
            <td colspan="2"><?= $order->good_count ?> шт.</td>
            <td colspan="2"><?= $order->good_weight ?></td>
            <td colspan="2"><?= number_format($order->good_sum, 2, '.', '&nbsp;') ?></td>
        </tr>
    </tbody>
</table>
