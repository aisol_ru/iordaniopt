<?php
/* @var $domain \app\models\Domain */
/* @var $order \app\models\order\Order */
/* @var $client \app\models\order\Client */
/* @var $orderedDate string */

use yii\helpers\Html;

?>

<h1>Заказ №<?= $order->number ?></h1>
<p><strong>Сайт:</strong> <?= $domain->name ?></p>
<p><strong>Дата:</strong> <?= $orderedDate ?></p>
<?php if (empty($client->comment) === false) { ?>
    <p><strong>Комментрий:</strong><br><?= nl2br($client->comment) ?></p>
<?php } ?>

<h2>Информация о клиенте</h2>
<p><strong>ФИО:</strong> <?= $client->getFullName() ?></p>
<p><strong>Телефон:</strong> <?= $client->phone ?></p>
<p><strong>Email:</strong> <?= $client->email ?></p>
<p><strong>Огранизация:</strong> <?= $client->organization ?></p>
<p><strong>ИНН:</strong> <?= $client->inn ?></p>
<p><strong>ОГРН:</strong> <?= $client->ogrn ?></p>
<p><strong>Город:</strong> <?= $client->city ?></p>

<h2>Товары в заказе</h2>
<table border="box" style="width: 100%;">
    <thead>
        <tr>
            <th></th>
            <th>Наименование</th>
            <th>Артикул</th>
            <th>Проба</th>
            <th>Количество (шт.)</th>
            <th>Размеры</th>
            <th>Вес (гр.)</th>
            <th>Итоговый вес (гр.)</th>
            <th>Цена (руб.)</th>
            <th>Сумма (руб.)</th>
        </tr>
    </thead>
    <tbody>
        <?php
        /* @var $good \app\models\order\Good */
        foreach ($order->getGoods()->each() as $good) { ?>

            <tr>
                <td style="text-align: center; vertical-align: middle">
                    <?php if (empty($good->good->image) === false) { ?>
                        <img
                            src="http://<?= $domain->domain ?><?= $good->good->getResizeCache('image', 100, 100, 5) ?>"
                            alt="<?= Html::encode($good->jsonData['name']) ?>"
                        >
                    <?php } ?>
                </td>
                <td>
                    <?= $good->jsonData['name'] ?>
                </td>
                <td>
                    <?= $good->good->vendor_code ?>
                </td>
                <td>
                    <?= $good->count ?>
                </td>
                <td>
                    <?php if (empty($good->jsonData['sizes']) === false) { ?>
                        <?php foreach ($good->jsonData['sizes'] as $key => $size) {
                            if ($size['count'] == 0) {
                                continue;
                            }
                            ?>
                            <?= $size['size'] ?>: <?= $size['count'] ?> шт.;
                        <?php } ?>
                    <?php } else { ?>
                        ---
                    <?php } ?>
                </td>
                <td>
                    <?= $good->jsonData['weight'] ?>
                </td>
                <td>
                    <?= $good->weight ?>
                </td>
                <td>
                    <?= number_format($good->jsonData['price_unit'], 2, '.', '&nbsp;') ?>
                </td>
                <td>
                    <?= number_format($good->sum, 2, '.', '&nbsp;') ?>
                </td>
            </tr>

        <?php } ?>
        <tr>
            <td colspan="4" style="text-align: right">Итого:</td>
            <td colspan="2"><?= $order->good_count ?> шт.</td>
            <td colspan="2"><?= $order->good_weight ?></td>
            <td colspan="2"><?= number_format($order->good_sum, 2, '.', '&nbsp;') ?></td>
        </tr>
    </tbody>
</table>

<br>
<br>
<br>
<a href="http://<?= $domain->domain ?>/icms/orders/view?number=<?= $order->number ?>">Посмотреть заказ в CMS</a>
