<?php

namespace app\modules\ajax\controllers;

use app\components\controller\Ajax;
use app\models\user\Favorite;
use Yii;
use yii\web\BadRequestHttpException;
use yii\web\ForbiddenHttpException;

class FavoriteController extends Ajax
{

    public function beforeAction($action)
    {
        if (Yii::$app->getUser()->can('user') === false) {
            throw new ForbiddenHttpException();
        }

        return parent::beforeAction($action);
    }

    public function actionSet()
    {

        $goodId = Yii::$app->getRequest()->post('good_id', false);

        if ($goodId == false) {
            throw new BadRequestHttpException();
        }

        if (Favorite::checkExists($goodId) === true) {
            Favorite::remove($goodId);
        } else {
            Favorite::add($goodId);
        }

        return [
            'success' => true,
            'count' => Favorite::getCount(),
        ];
    }

    public function actionFlush()
    {
        Favorite::flush();

        return [
            'success' => true,
        ];
    }

}