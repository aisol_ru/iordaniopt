<?php

namespace app\modules\ajax\controllers;

use app\components\controller\Ajax;
use app\models\catalog\Good;
use yii\web\BadRequestHttpException;

class DefaultController extends Ajax
{

    public function actionFeedback()
    {

        $model = new \app\forms\Feedback();
        $result = ['success' => false];
        if ($model->load(\Yii::$app->request->post()) === true && $model->validate() === true) {
            $result['success'] = true;

            $isSend = $model->send();

            if ($isSend === false) {
                $result['success'] = false;
                $result['error'] = 2;
            }
        } else {
            $result['error'] = 1;
        }

        return $result;
    }

    public function actionFindSelectCatalog($value)
    {
        if (is_null($value)) {
            throw new BadRequestHttpException();
        }

        $catalogs1 = Good::find()
            ->alias("g")
            ->andFilterWhere(['LIKE', 'g.vendor_code', $value . '%', false])
            ->all();

        $catalogs2 = Good::find()
            ->alias("g")
            ->andFilterWhere(['LIKE', 'g.name', $value])
            ->all();

        $result = [];

        foreach ($catalogs1 as $catalog){
            $result[] = $catalog->vendor_code;
        }

        foreach ($catalogs2 as $catalog){
            $result[] = $catalog->name;
        }

        $result = array_unique($result, SORT_STRING);
        shuffle($result);

        return $result;
    }

}
