<?php

namespace app\modules\ajax\controllers;

use app\components\Bytehand;
use app\components\controller\Ajax;
use app\components\IcmsHelper;
use app\components\MultiLogin;
use app\models\user\User;
use Yii;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\ForbiddenHttpException;

class AuthController extends Ajax
{

    public function actionGetCode()
    {
        $phone = Yii::$app->getRequest()->post('phone', null);
        if (empty($phone) === true) {
            throw new BadRequestHttpException();
        }

        $currentCode = Yii::$app->getSession()->get('auth-phone-code', false);

        $user = User::find()
            ->andWhere(['phone' => IcmsHelper::clearPhone($phone)])
            ->one();
        if (is_null($user) === false && $user->status == User::STATUS_DELETED) {
            return ['success' => false, 'error' => 'Аккаунт находится на проверке'];
        }


        if ($currentCode === false || (($currentCode['created_at'] + 60) < time())) {

            $code = IcmsHelper::generateNumber(6);

            Bytehand::send($user->phone, 'Код для входа на сайт: ' . $code);

            Yii::$app->getSession()->set('auth-phone-code', [
                'phone' => IcmsHelper::clearPhone($phone),
                'code' => $code,
                'checked' => false,
                'count' => 0,
                'created_at' => time(),
            ]);

        }

        return ['success' => true];
    }

    public function actionCheckCode()
    {
        $requestCode = trim(Yii::$app->getRequest()->post('code', ''));
        if (empty($requestCode) === true) {
            throw new BadRequestHttpException();
        }

        $currentCode = Yii::$app->getSession()->get('auth-phone-code', false);
        if ($currentCode === false) {
            throw new ForbiddenHttpException();
        }
        $result = [];

        if ($currentCode['code'] == $requestCode) {
            $result['success'] = true;

            $user = User::find()
                ->andWhere(['phone' => $currentCode['phone']])
                ->one();

            if (is_null($user) === false && $user->status == User::STATUS_DELETED) {
                return [
                    'success' => false,
                    'error' => 'Аккаунт находится на проверке',
                ];
            }

            $currentCode['checked'] = true;
            Yii::$app->getSession()->set('auth-phone-code', $currentCode);

            if (is_null($user) === false) {
                $duration = 2592000;
                Yii::$app->user->login($user, $duration);
                $multiLogin = new MultiLogin('', $duration);
                $result['redirect'] = $multiLogin->goRedirectLogin('/');
            } else {
                $result['redirect'] = Url::to(['/auth/registration']);
            }

        } else {
            $result['success'] = false;
            $currentCode['count']++;
            if ($currentCode['count'] === 10) {
                Yii::$app->getSession()->remove('auth-phone-code');
                $result['end'] = true;
            } else {
                Yii::$app->getSession()->set('auth-phone-code', $currentCode);
            }
        }


        return $result;
    }

    public function actionAuth()
    {
      $model = new \app\forms\AuthUser();
      $result['success'] = false;
      if ($model->load(\Yii::$app->request->post()) === true && $model->validate() === true) {
        $user = User::find()
            ->andWhere(['login' => $model->login])
            ->one();

        if (is_null($user) === false) {
          if($user->validatePassword($model->password) === true)
          {
            if($user->status == User::STATUS_ACTIVE)
            {
                $result['success'] = true;
                $duration = 2592000;
                Yii::$app->user->login($user, $duration);
                $multiLogin = new MultiLogin('', $duration);
                $result['redirect'] = $multiLogin->goRedirectLogin('/');
            }else{
                $result['error'] = 3;
            }
          }else{
            $result['error'] = 1;
          }
        }else{
          $result['error'] = 2;
        }
      }
      return $result;
    }

    public function actionReg()
    {
      $model = new \app\forms\Reg();
      $result['success'] = false;
      if ($model->load(\Yii::$app->request->post()) === true && $model->validate() === true) {
        $userFind = User::find()
          ->andWhere(['login' => $model->login])
          ->one();

        if(!empty($userFind))
        {
            $result['error'] = 1;
        }else{
            $user = new User;
            $user->login = $model->login;
            $user->email = $model->email;
            $user->middle_name = $model->middle_name;
            $user->phone = $model->phone;
            $user->first_name = $model->name;
            $user->last_name = $model->lastname;
            $user->organization = $model->nameOrg;
            $user->inn = $model->inn;
            $user->ogrn = $model->ogrn;
            $user->city = $model->city;
            $user->certificate = $model->certificate;
            $user->password = $user->setPassword($model->password);
            $user->status = User::STATUS_DELETED;
            $user->generateAuthKey();
            $user->save(false);

            $auth = Yii::$app->authManager;
            $authorRole = $auth->getRole(User::DEFAULT_ROLE);
            $auth->assign($authorRole, $user->getId());

            $result['success'] = true;
            $model->send();
        }
      }
      return $result;
    }

    public function actionRestore()
    {
      $model = new \app\forms\Restore();
      $result['success'] = false;
      if ($model->load(\Yii::$app->request->post()) === true && $model->validate() === true) {
        $user = User::find()
            ->andWhere(['login' => $model->login])
            ->one();

        if (is_null($user) === false) {
          $new_password = IcmsHelper::generatePassword(10);
          $user->password = $user->setPassword($new_password);
          $user->save(false);
          $model->send($new_password, $user->email);
          $result['success'] = true;
        }else{
          $result['error'] = 1;
        }
      }
      return $result;
    }

}
