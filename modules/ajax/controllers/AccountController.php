<?php

namespace app\modules\ajax\controllers;

use app\components\controller\Ajax;
use app\models\order\ImageArchive;
use app\models\order\Order;
use Yii;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;

class AccountController extends Ajax
{

    public function beforeAction($action)
    {
        if (Yii::$app->getUser()->can('user') === false) {
            throw new ForbiddenHttpException();
        }

        return parent::beforeAction($action);
    }

    public function actionOrderGetArchive($number)
    {
        /* @var $order Order|null */
        $order = Order::find()
            ->andWhere(['!=', 'status', Order::STATUS_NOT_COMPLETED])
            ->andWhere(['number' => $number])
            ->one();
        if (is_null($order) === true) {
            throw new NotFoundHttpException();
        }

        if ($order->user_id != Yii::$app->getUser()->getId()) {
            throw new ForbiddenHttpException();
        }

        $archive = $order->getImageArchive()->one();
        if (is_null($archive) === true) {
            $archive = new ImageArchive();
            $archive->order_id = $order->id;
            $archive->status = ImageArchive::STATUS_GENERATED;
            $archive->save();
        }

        $result = [
            'success' => true,
            'file' => false,
        ];
        if ($archive->status == ImageArchive::STATUS_COMPLETED) {
            $result['file'] = $archive->getPath('file');
        }

        return $result;
    }
    public function actionPersonalnyeDannye()
    {
      $model = new \app\forms\PersonalData();
      if ($model->load(\Yii::$app->request->post()) === true && $model->validate() === true) {
          $result = ['success' => true];
          $model->save();
          return $result;
      }
    }
}
