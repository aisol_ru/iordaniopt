<?php

namespace app\modules\ajax\controllers;

use app\components\controller\Ajax;
use app\components\interfaces\UrlManagerMultiDomain;
use app\models\Domain;
use app\models\order;
use app\widgets\catalog\Good;
use kcfinder\dir;
use Yii;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;

class OrderController extends Ajax
{

    public function beforeAction($action)
    {
        if (Yii::$app->getUser()->can('user') === false) {
            throw new ForbiddenHttpException();
        }

        return parent::beforeAction($action);
    }

    public function actionAdd()
    {
        $goodId = Yii::$app->getRequest()->post('good_id', false);
        $insertId = Yii::$app->getRequest()->post('insert_id');
        $coatingId = Yii::$app->getRequest()->post('coating_id');
        $count = Yii::$app->getRequest()->post('count', false);
        $comment = Yii::$app->getRequest()->post('comment', "");

        $order = order\Order::getCurrent(true);

        order\Good::add($order->id, $goodId, $insertId, $coatingId, $count, $comment);

        $order->recalculation();
        $order->save(false);

        return [
            'success' => true,
            'good_count' => $order->good_count,
            'good_sum' => $order->good_sum,
            'good_weight' => $order->good_weight,
            'sum' => $order->sum,
        ];
    }

    public function actionUpdate()
    {
        $goodId = Yii::$app->getRequest()->post('good_id', false);
        $insertId = Yii::$app->getRequest()->post('insert_id');
        $coatingId = Yii::$app->getRequest()->post('coating_id');
        $count = Yii::$app->getRequest()->post('count', false);
        $comment = Yii::$app->getRequest()->post('comment', "");

        $order = order\Order::getCurrent(true);
        $good = order\Good::edit($order->id, $goodId, $insertId, $coatingId, $count, $comment);
        $order->recalculation();
        $order->save(false);

        $goodNewSum = 0;
        if ($good !== false) {
            $goodNewSum = $good->sum;
        }

        return [
            'success' => true,
            'good_count' => $order->good_count,
            'good_id' => $good->id,
            'good_sum' => $order->good_sum,
            'good_weight' => $order->good_weight,
            'good_new_sum' => $goodNewSum,
            'sum' => $order->sum,
        ];
    }

    public function actionRemove()
    {
        $goodId = Yii::$app->getRequest()->post('good_id', false);
        $insertId = Yii::$app->getRequest()->post('insert_id');
        $coatingId = Yii::$app->getRequest()->post('coating_id');

        $order = order\Order::getCurrent(true);
        order\Good::remove($order->id, $goodId, $insertId, $coatingId);

        $order->recalculation();
        $order->save(false);

        return [
            'success' => true,
            'good_count' => $order->good_count,
            'good_sum' => $order->good_sum,
            'good_weight' => $order->good_weight,
            'sum' => $order->sum,
        ];
    }

    public function actionFlush()
    {
        $order = order\Order::getCurrent(true);
        order\Order::flush($order->id);
        $order->recalculation();
        $order->save(false);

        return [
            'success' => true,
            'good_count' => $order->good_count,
            'good_sum' => $order->good_sum,
            'good_weight' => $order->good_weight,
            'sum' => $order->sum,
        ];
    }

    public function actionRepeat()
    {
        $number = Yii::$app->getRequest()->post('number', null);
        if (empty($number) === true) {
            throw new BadRequestHttpException();
        }

        $oldOrder = order\Order::find('', false)
            ->andWhere(['number' => $number])
            ->one();

        if (is_null($oldOrder) === true) {
            throw new NotFoundHttpException('Order not found');
        }

        if ($oldOrder->user_id != Yii::$app->getUser()->getId() || $oldOrder->status == order\Order::STATUS_NOT_COMPLETED) {
            throw new ForbiddenHttpException();
        }

        $order = order\Order::getCurrent(true);
        order\Order::flush($order->id);
        $order->recalculation();
        $order->save(false);

        /* @var $good order\Good */
        foreach ($oldOrder->getGoods()->all() as $good) {
            if (is_null($good->good) === true) {
                continue;
            }

            $sizes = $good->jsonGet('sizes', $good->count);

            order\Good::add($order->id, $good->good_id, $good->price_id, $sizes);
        }

        $order->recalculation();
        $order->save(false);

        $redirectUrl = Url::to(['/basket/main']);

        return [
            'success' => true,
            'good_count' => $order->good_count,
            'good_sum' => $order->good_sum,
            'good_weight' => $order->good_weight,
            'sum' => $order->sum,
            'redirect' => $redirectUrl,
        ];
    }

}
