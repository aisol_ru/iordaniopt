<?php

namespace app\modules\icms\forms;

use yii\base\Model;

class Order extends Model
{

    public $status;

    public function rules()
    {
        return [
            ['status', 'required', 'message' => 'Заполните поле'],
            ['status', 'in', 'range' => array_keys(\app\models\order\Order::getStatuses())],
        ];
    }

}
