<?php

namespace app\modules\icms\controllers;

use app\components\controller\Backend;
use Yii;
use yii\filters\AccessControl;
use app\models\Manager;
use app\modules\icms\widgets\GreenLine;
use yii\web\NotFoundHttpException;

class ManagersController extends Backend
{

    public $defaultAction = 'list';

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['manager'],
                    ],
                ],
            ],
        ];
    }

    public function actionList()
    {
        $this->layout = 'innerPjax';
        Yii::$app->view->params['breadCrumbs']['crumbs'] = [
            ['url' => ['managers/list'], 'title' => 'Менеджеры'],
        ];

        return $this->render('list');
    }

    public function actionAdd()
    {
        $model = new Manager();
        Yii::$app->view->params['breadCrumbs']['crumbs'] = [
            ['url' => ['managers/list'], 'title' => 'Менеджеры'],
            ['title' => 'Добавление менеджера'],
        ];
        if ($model->load(Yii::$app->request->post()) === true && $model->save() === true) {
            GreenLine::show();
            return $this->redirect(['managers/edit', 'id' => $model->id]);
        } else {
            $model->jsonData['phones'] = [];
            $model->jsonData['emails'] = [];
        }

        return $this->render('edit', ['model' => $model]);
    }

    public function actionEdit($id)
    {
        $model = Manager::find()->andWhere(['id' => $id])->one();
        if (is_null($model) === true) {
            throw new NotFoundHttpException();
        }
        Yii::$app->view->params['breadCrumbs']['crumbs'] = [
            ['url' => ['managers/list'], 'title' => 'Менеджеры'],
            ['title' => 'Редактирование менеджера'],
        ];
        if ($model->load(Yii::$app->request->post()) === true && $model->save() === true) {
            GreenLine::show();
            return $this->refresh();
        } else {
            $model->jsonData['phones'] = empty($model->jsonData['phones']) === false ? $model->jsonData['phones'] : [];
            $model->jsonData['emails'] = empty($model->jsonData['emails']) === false ? $model->jsonData['emails'] : [];
        }
        return $this->render('edit', ['model' => $model]);
    }

}
