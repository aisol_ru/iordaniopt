<?php

namespace app\modules\icms\controllers;

use app\components\controller\Backend;
use Yii;
use yii\filters\AccessControl;
use app\models\banner\Category;
use app\models\banner\Banner;
use app\modules\icms\widgets\GreenLine;
use yii\web\NotFoundHttpException;

class BannersController extends Backend
{

    public $defaultAction = 'list';

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['manager'],
                    ],
                ],
            ],
        ];
    }

    public function actionList()
    {
        $this->layout = 'innerPjax';
        Yii::$app->view->params['breadCrumbs']['crumbs'] = [
            ['title' => 'Категории баннеров'],
        ];

        return $this->render('category/list');
    }

    public function actionCategory($id)
    {
        $this->layout = 'innerPjax';
        $category = Category::findOne($id);

        if (is_null($category) === true) {
            throw new NotFoundHttpException();
        }

        Yii::$app->view->params['adminMenuDropDown'] = ['pids' => ['category_id' => $category->id]];
        Yii::$app->view->params['breadCrumbs']['crumbs'] = [
            ['url' => ['banners/list'], 'title' => 'Категории баннеров'],
            ['title' => $category->name],
        ];
        return $this->render('banner/list');
    }

    public function actionCategoryAdd()
    {
        $model = new Category();
        Yii::$app->view->params['breadCrumbs']['crumbs'] = [
            ['url' => ['banners/list'], 'title' => 'Категории баннеров'],
            ['title' => 'Создание категории'],
        ];
        if ($model->load(Yii::$app->request->post()) === true && $model->save() === true) {
            GreenLine::show();
            return $this->redirect(['banners/category-edit', 'id' => $model->id]);
        }

        return $this->render('category/edit', ['model' => $model]);
    }

    public function actionCategoryEdit($id)
    {
        $model = Category::findOne($id);
        if (is_null($model) === true) {
            throw new NotFoundHttpException();
        }
        Yii::$app->view->params['breadCrumbs']['crumbs'] = [
            ['url' => ['banners/list'], 'title' => 'Категории баннеров'],
            ['title' => 'Редактирование категории'],
        ];
        Yii::$app->view->params['adminMenuDropDown'] = ['pids' => ['category_id' => $model->id]];
        if ($model->load(Yii::$app->request->post()) === true) {
            GreenLine::show();
            $model->save();
            return $this->refresh();
        }
        return $this->render('category/edit', ['model' => $model]);
    }

    public function actionBannerAdd()
    {
        Yii::$app->view->params['breadCrumbs']['crumbs'] = [
            ['url' => ['banners/list'], 'title' => 'Категории баннеров'],
            ['title' => 'Создание баннера'],
        ];
        $model = new Banner();

        if ($model->load(Yii::$app->request->post()) === true && $model->save() === true) {
            $model->saveFiles();
            GreenLine::show();
            return $this->redirect(['banners/banner-edit', 'id' => $model->id]);
        }

        $model->category_id = $model->category_id ?: Yii::$app->getRequest()->get('category_id', 0);

        return $this->render('banner/edit', ['model' => $model]);
    }

    public function actionBannerEdit($id)
    {
        $banner = Banner::findOne($id);
        if (is_null($banner) === true) {
            throw new NotFoundHttpException();
        }

        Yii::$app->view->params['adminMenuDropDown'] = ['pids' => ['category_id' => $banner->category->id]];
        Yii::$app->view->params['breadCrumbs']['crumbs'] = [
            ['url' => ['banners/list'], 'title' => 'Категории баннеров'],
            ['title' => 'Редактирование баннера'],
        ];
        if ($banner->load(Yii::$app->request->post()) === true && $banner->save() === true) {
            $banner->saveFiles();
            GreenLine::show();
            return $this->refresh();
        }
        return $this->render('banner/edit', ['model' => $banner]);
    }

}
