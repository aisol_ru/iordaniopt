<?php

namespace app\modules\icms\controllers;

use app\components\controller\Backend;
use app\models\Menu;
use app\modules\icms\widgets\GreenLine;
use Yii;
use yii\filters\AccessControl;

class SiteMenuController extends Backend
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['list'],
                        'allow' => true,
                        'roles' => ['manager'],
                    ],
                ],
            ],
        ];
    }

    public function actionList()
    {
        $menu = new Menu();
        if ($menu->load(Yii::$app->getRequest()->post()) === true && $menu->save() === true) {
            GreenLine::show('Пункт меню добавлен');
            return $this->refresh();
        }
        $this->layout = 'innerPjax';
        Yii::$app->view->params['breadCrumbs']['crumbs'] = [
            ['url' => ['site-manu/list'], 'title' => 'Список пунктов меню'],
        ];

        return $this->render('list', [
            'model' => $menu,
        ]);
    }

}
