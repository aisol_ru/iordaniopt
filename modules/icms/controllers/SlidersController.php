<?php

namespace app\modules\icms\controllers;

use app\components\controller\Backend;
use app\models\slider\Slide;
use app\models\slider\Slider;
use Yii;
use yii\filters\AccessControl;
use app\models\banner\Category;
use app\models\banner\Banner;
use app\modules\icms\widgets\GreenLine;
use yii\web\NotFoundHttpException;

class SlidersController extends Backend
{

    public $defaultAction = 'list';

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['manager'],
                    ],
                ],
            ],
        ];
    }

    public function actionList()
    {
        $this->layout = 'innerPjax';
        Yii::$app->view->params['breadCrumbs']['crumbs'] = [
            ['title' => 'Слайдеры'],
        ];

        return $this->render('slider/list');
    }

    public function actionSlider($id)
    {
        $this->layout = 'innerPjax';
        $slider = Slider::findOne($id);

        if (is_null($slider) === true) {
            throw new NotFoundHttpException();
        }

        Yii::$app->view->params['adminMenuDropDown'] = ['pids' => ['slider_id' => $slider->id]];
        Yii::$app->view->params['breadCrumbs']['crumbs'] = [
            ['url' => ['sliders/list'], 'title' => 'Слайдеры'],
            ['title' => $slider->name],
        ];
        return $this->render('slide/list');
    }

    public function actionSliderAdd()
    {
        $model = new Slider();
        Yii::$app->view->params['breadCrumbs']['crumbs'] = [
            ['url' => ['sliders/list'], 'title' => 'Слайдеры'],
            ['title' => 'Создание слайдера'],
        ];
        if ($model->load(Yii::$app->request->post()) === true && $model->save() === true) {
            GreenLine::show();
            return $this->redirect(['sliders/slider-edit', 'id' => $model->id]);
        }

        return $this->render('slider/edit', ['model' => $model]);
    }

    public function actionSliderEdit($id)
    {
        $model = Slider::findOne($id);
        if (is_null($model) === true) {
            throw new NotFoundHttpException();
        }
        Yii::$app->view->params['breadCrumbs']['crumbs'] = [
            ['url' => ['sliders/list'], 'title' => 'Слайдеры'],
            ['title' => 'Редактирование слайдера'],
        ];
        Yii::$app->view->params['adminMenuDropDown'] = ['pids' => ['slider_id' => $model->id]];
        if ($model->load(Yii::$app->request->post()) === true) {
            GreenLine::show();
            $model->save();
            return $this->refresh();
        }
        return $this->render('slider/edit', ['model' => $model]);
    }

    public function actionSlideAdd()
    {
        Yii::$app->view->params['breadCrumbs']['crumbs'] = [
            ['url' => ['sliders/list'], 'title' => 'Слайдеры'],
            ['title' => 'Создание слайда'],
        ];
        $model = new Slide();

        if ($model->load(Yii::$app->request->post()) === true && $model->save() === true) {
            $model->saveFiles();
            GreenLine::show();
            return $this->redirect(['sliders/slide-edit', 'id' => $model->id]);
        }

        $model->slider_id = \Yii::$app->getRequest()->get('slider_id', 0);

        return $this->render('slide/edit', ['model' => $model]);
    }

    public function actionSlideEdit($id)
    {
        $model = Slide::findOne($id);
        if (is_null($model) === true) {
            throw new NotFoundHttpException();
        }

        Yii::$app->view->params['adminMenuDropDown'] = ['pids' => ['slider_id' => $model->slider->id]];
        Yii::$app->view->params['breadCrumbs']['crumbs'] = [
            ['url' => ['sliders/list'], 'title' => 'Слайдеры'],
            ['title' => 'Редактирование слайда'],
        ];
        if ($model->load(Yii::$app->request->post()) === true && $model->save() === true) {
            $model->saveFiles();
            GreenLine::show();
            return $this->refresh();
        }
        return $this->render('slide/edit', ['model' => $model]);
    }

}
