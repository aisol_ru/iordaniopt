<?php

namespace app\modules\icms\controllers;

use app\components\controller\Backend;
use app\models\contact\Contact;
use app\models\Domain;
use app\models\contact\Social;
use Yii;
use yii\filters\AccessControl;
use app\modules\icms\widgets\GreenLine;
use yii\web\NotFoundHttpException;

class DomainsController extends Backend
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['list', 'add', 'edit'],
                        'allow' => true,
                        'roles' => ['developer'],
                    ],
                ],
            ],
        ];
    }

    public function actionList()
    {
        $this->layout = 'innerPjax';
        Yii::$app->view->params['breadCrumbs']['crumbs'] = [
            ['url' => ['domains/list'], 'title' => 'Список доменов'],
        ];

        return $this->render('list');
    }

    public function actionAdd()
    {
        Yii::$app->view->params['breadCrumbs']['crumbs'] = [
            ['url' => ['domains/list'], 'title' => 'Список доменов'],
            ['title' => 'Создание домена'],
        ];
        $model = new Domain();

        if ($model->load(Yii::$app->request->post()) === true && $model->save() === true) {
            $model->saveFiles();
            $model->checkReferences(1);
            $contact = new Contact();
            $contact->save(false);
            GreenLine::show();
            return $this->redirect(['domains/edit', 'id' => $model->id]);
        }

        return $this->render('edit', ['model' => $model]);
    }

    public function actionEdit($id)
    {
        Yii::$app->view->params['breadCrumbs']['crumbs'] = [
            ['url' => ['domains/list'], 'title' => 'Список доменов'],
            ['title' => 'Редактирование домена'],
        ];
        $model = Domain::find()->andWhere(['id' => $id])->one();
        if (is_null($model) === true) {
            throw new NotFoundHttpException();
        }

        if ($model->load(Yii::$app->request->post()) === true && $model->save() === true) {
            $model->saveFiles();
            if ($model->id != 1) {
                $model->checkReferences(1);
            }
            GreenLine::show();
            return $this->refresh();
        }

        $contact = Contact::find()->one() ?: new Contact();
        $social = new Social();

        return $this->render('edit', [
            'model' => $model,
            'contact' => $contact,
            'social' => $social,
        ]);
    }

}
