<?php

namespace app\modules\icms\controllers;

use app\components\Mailer;
use app\models\Domain;
use app\models\Manager;
use Yii;
use app\models\user\User;
use yii\filters\AccessControl;
use app\modules\icms\widgets\GreenLine;
use yii\helpers\ArrayHelper;

class UsersController extends \app\components\controller\Backend
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['index', 'add', 'edit', 'edit_password'],
                'rules' => [
                    [
                        'actions' => ['index', 'add', 'edit', 'edit_password'],
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $this->layout = 'innerPjax';
        Yii::$app->view->params['breadCrumbs']['crumbs'] = [
            ['url' => ['users/index'], 'title' => 'Список пользователей'],
        ];

        return $this->render('index');
    }

    public function actionAdd()
    {
        Yii::$app->view->params['breadCrumbs']['crumbs'] = [
            ['url' => ['users/index'], 'title' => 'Список пользователей'],
            ['url' => '', 'title' => 'Создание нового пользователя'],
        ];
        $model = new User(['scenario' => 'default']);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            GreenLine::show();
            $model->saveFiles();
            return $this->redirect(['users/edit', 'id' => $model->id]);
        }

        return $this->render('add', ['roles' => User::getRolesAsArray(), 'model' => $model]);
    }

    public function actionImport()
    {
        Yii::$app->view->params['breadCrumbs']['crumbs'] = [
            ['url' => ['users/index'], 'title' => 'Список пользователей'],
            ['url' => '', 'title' => 'Импорт пользователей'],
        ];

        $csv = \yii\web\UploadedFile::getInstanceByName('csv');

        if($csv)
        {
            if (($handle = fopen($csv->tempName, 'r')) !== false) {
                $i = 0;
                while (($row = fgetcsv($handle, 1000, ';')) !== false) {
                    $i++;
                    if ($i === 1) {
                        continue;
                    }

                    $usr = new User();

                    $usr->email = $row[1];
                    $usr->login = $row[2];
                    $usr->first_name = $row[3];
                    $usr->last_name = $row[4];
                    $usr->organization = $row[8];
                    $usr->phone = $row[9];
                    $usr->certificate = $row[15];
                    $usr->inn = $row[19];
                    $usr->save(false);
                }
            }
            fclose($handle);
            GreenLine::show();
        }

        return $this->render('import');
    }

    public function actionEdit($id)
    {
        $model = User::findOne($id);
        Yii::$app->view->params['breadCrumbs']['crumbs'] = [
            ['url' => ['users/index'], 'title' => 'Список пользователей'],
            ['url' => '', 'title' => 'Редактирование пользователя'],
        ];
        if (is_null($model) === true) {
            return $this->redirect(['users/add']);
        }
        $model->scenario = 'edit';

        /* @var $domains Domain[] */
        $domains = Domain::find()->indexBy('id')->all();
        $managers = [];
        foreach ($domains as $domain) {
            /* @var $manager Manager */
            $manager = $model->getManagers($domain->id)->one();
            $managers[$domain->id] = is_null($manager) === false ? $manager->id : 0;
        }

        $currentStatus = $model->status;
        if ($model->load(Yii::$app->request->post()) === true && $model->save() === true) {
            $model->saveFiles();
            $newManagerIds = Yii::$app->getRequest()->post('managers', []);
            $model->unlinkAll('managers', true);

            foreach ($newManagerIds as $domainId => $newManagerId) {
                if ($newManagerId == 0) {
                    continue;
                }
                $manager = Manager::find()
                    ->andWhere(['id' => $newManagerId])
                    ->one();
                if (is_null($manager) === true) {
                    continue;
                }

            }

            if ($model->status != $currentStatus && $model->status == User::STATUS_ACTIVE) {
                Mailer::registrationApprove($model);
            }

            GreenLine::show();
            return $this->refresh();
        }

        return $this->render('edit', [
            'model' => $model,
            'roles' => User::getRolesAsArray(),
            'domains' => ArrayHelper::map($domains, 'id', 'name'),
            'managers' => $managers,
        ]);
    }

    public function actionEdit_password($id)
    {
        $user = User::findOne($id);
        Yii::$app->view->params['breadCrumbs']['crumbs'] = [
            ['url' => ['users/index'], 'title' => 'Список пользователей'],
            ['url' => ['users/edit', 'id' => $user->id], 'title' => $user->first_name ?: $user->login],
            ['url' => '', 'title' => 'Редактирование пароля'],
        ];
        if (is_null($user) === true) {
            return $this->redirect(['users/index']);
        }
        $user->scenario = 'editPassword';

        if ($user->load(Yii::$app->request->post()) && $user->save()) {
            GreenLine::show('Пароль сохранен');
            return $this->redirect(['users/edit', 'id' => $user->id]);
        }
        return $this->render('password_edit', ['model' => $user]);
    }

}
