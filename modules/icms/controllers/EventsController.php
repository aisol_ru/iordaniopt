<?php

namespace app\modules\icms\controllers;

use app\components\controller\Backend;
use app\models\Event;
use Yii;
use yii\filters\AccessControl;
use app\modules\icms\widgets\GreenLine;
use yii\web\NotFoundHttpException;

class EventsController extends Backend
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['list', 'add', 'edit'],
                        'allow' => true,
                        'roles' => ['manager'],
                    ],
                ],
            ],
        ];
    }

    public function actionList()
    {
        $this->layout = 'innerPjax';
        Yii::$app->view->params['breadCrumbs']['crumbs'] = [
            ['url' => ['events/list'], 'title' => 'Список событий'],
        ];

        return $this->render('list');
    }

    public function actionAdd()
    {
        Yii::$app->view->params['breadCrumbs']['crumbs'] = [
            ['url' => ['events/list'], 'title' => 'Список событий'],
            ['title' => 'Создание события'],
        ];
        $model = new Event();

        if ($model->load(Yii::$app->request->post()) === true && $model->save() === true) {
            $model->saveFiles();
            GreenLine::show();
            return $this->redirect(['events/edit', 'id' => $model->id]);
        }

        return $this->render('edit', ['model' => $model]);
    }

    public function actionEdit($id)
    {
        Yii::$app->view->params['breadCrumbs']['crumbs'] = [
            ['url' => ['events/list'], 'title' => 'Список событий'],
            ['title' => 'Создание события'],
        ];
        $model = Event::find()->andWhere(['id' => $id])->one();
        if (is_null($model) === true) {
            throw new NotFoundHttpException();
        }

        if ($model->load(Yii::$app->request->post()) === true && $model->save() === true) {
            $model->saveFiles();
            GreenLine::show();
            return $this->refresh();
        }
        return $this->render('edit', ['model' => $model]);
    }

}
