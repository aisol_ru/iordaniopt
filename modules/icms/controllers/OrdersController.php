<?php

namespace app\modules\icms\controllers;

use app\components\controller\Backend;
use Yii;
use yii\filters\AccessControl;
use app\models\order\Order;
use app\modules\icms\widgets\GreenLine;
use yii\web\NotFoundHttpException;

class OrdersController extends Backend
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['manager'],
                    ],
                ],
            ],
        ];
    }

    public function actionList()
    {
        $this->layout = 'innerPjax';
        Yii::$app->view->params['breadCrumbs']['crumbs'] = [
            ['title' => 'Заказы'],
        ];

        return $this->render('list');
    }

    public function actionView($number)
    {
        $order = Order::find()->andWhere(['number' => $number])->one();

        if (is_null($order) === true) {
            throw new NotFoundHttpException();
        }

        Yii::$app->view->params['breadCrumbs']['crumbs'] = [
            ['url' => ['orders/list'], 'title' => 'Заказы'],
            ['title' => 'Заказ №' . $order->number],
        ];

        $model = new \app\modules\icms\forms\Order();

        if ($model->load(Yii::$app->request->post()) === true && $model->validate() === true) {
            $order->status = $model->status;
            $order->save();
            GreenLine::show();
            return $this->refresh();
        }

        $model->status = $order->status;
        return $this->render('view', [
            'order' => $order,
            'newStatus' => $order->getHistoryStatuses()->andWhere(['status' => Order::STATUS_NEW])->one(),
            'model' => $model,
        ]);
    }

}
