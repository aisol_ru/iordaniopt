<?php
/* @var $this \yii\web\View */
/* @var $domains \app\models\Domain[] */

?>

<div class="domain-selector-block">
<?php foreach ($domains as $domain) { ?>
    <a href="http://<?= $domain->domain ?>/icms/" class="button <?= Yii::$app->getUrlManager()->domain->id == $domain->id ? 'acitve' : '' ?>">
        <?= $domain->name ?>
    </a>
<?php } ?>
</div>
