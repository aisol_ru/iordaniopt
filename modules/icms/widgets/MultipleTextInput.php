<?php


namespace app\modules\icms\widgets;


use yii\helpers\Html;
use yii\widgets\InputWidget;

class MultipleTextInput extends InputWidget
{


    public function run()
    {
        $values = Html::getAttributeValue($this->model, $this->attribute);
        $fieldName = Html::getInputName($this->model, $this->attribute) . '[]';

        $html = "<div class='js-multiple-input-block'>";
        $html .= "<div class='js-multiple-inputs'>";
        foreach ($values as $value) {
            $html .= <<<HTML
<div class="form-group js-multiple-input">
    <fieldset>
        <div class="multiple-input">
            <input type="text" name="{$fieldName}" value="{$value}" class="form-control">
            <button class="button js-multiple-input-remove" type="button">-</button>
        </div>
    </fieldset>
</div>
HTML;

        }
        $html .= '</div>';

        $html .= <<<HTML
<div class="form-group">
    <fieldset>
        <div class="multiple-input">
            <input type="text" placeholder="Добавить" class="form-control">
            <button class="button js-multiple-input-add" data-name="{$fieldName}" type="button">+</button>
        </div>
    </fieldset>
</div>
HTML;

        $html .= '</div>';


        $this->registerJs();
        return $html;
    }

    private function registerJs()
    {
        $js = <<<JS
$('.js-multiple-input-block').on('click', '.js-multiple-input-remove', function() {
    var button = $(this);
    button.closest('.js-multiple-input').remove();
});
$('.js-multiple-input-block').on('click', '.js-multiple-input-add', function() {
    var button = $(this);
    var addInput = button.siblings('input');
    var name = button.data('name');
    var inputBlock = button.closest('.js-multiple-input-block').find('.js-multiple-inputs');
    var input = `
<div class="form-group js-multiple-input">
    <fieldset>
        <div class="multiple-input">
            <input type="text" name="\${name}" value="\${addInput.val()}" class="form-control">
            <button class="button js-multiple-input-remove" type="button">-</button>
        </div>
    </fieldset>
</div>
`;
    inputBlock.append(input);
    addInput.val('');
});
JS;

        $this->getView()->registerJs($js);

    }



}
