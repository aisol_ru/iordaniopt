<?php


namespace app\modules\icms\widgets;


use app\models\Domain;
use yii\base\Widget;

class DomainSelector extends Widget
{

    public function run()
    {
        $domains = Domain::find()->all();

        return $this->render('domain-selector', [
            'domains' => $domains,
        ]);
    }

}
