<?php
/* @var $this \yii\web\View */

use app\models\Event;
use app\modules\icms\widgets\grid\GridActionColumn;
use app\modules\icms\widgets\grid\GridFormatColumn;
use app\modules\icms\widgets\grid\GridView;
use app\modules\icms\widgets\NotificationGritter;

?>
<div class="data">

    <?= NotificationGritter::widget(['preset' => 'save', 'flash' => 'message']) ?>
    <?=
    GridView::widget([
        'modelName' => Event::class,
        'filterScenario' => Event::SCENARIO_DEFAULT,
        'tableName' => 'События',
        'columns' => [
            'id',
            [
                'class' => GridFormatColumn::class,
                'attribute' => 'name',
                'label' => 'Название',
                'format' => 'link',
                'options' => [
                    'link' => ['events/edit', 'id'],
                ],
            ],
            [
                'class' => GridFormatColumn::class,
                'attribute' => 'link',
                'label' => 'Ссылка',
                'format' => 'input',
            ],
            [
                'class' => GridFormatColumn::class,
                'attribute' => 'image',
                'format' => 'img',
                'label' => 'Изображение',
                'options' => [
                    'resize' => ['type' => 3],
                ],
            ],
            [
                'class' => GridFormatColumn::class,
                'attribute' => 'created_at',
                'label' => 'Дата создания',
                'visible' => Yii::$app->user->can('developer'),
                'format' => 'date',
            ],
            [
                'class' => GridFormatColumn::class,
                'attribute' => 'updated_at',
                'label' => 'Дата изменения',
                'visible' => Yii::$app->user->can('developer'),
                'format' => 'date',
            ],
            [
                'class' => GridActionColumn::class,
                'save' => true,
                'delete' => true,
            ],
        ],
    ]);
    ?>

</div>
