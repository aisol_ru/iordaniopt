<?php
/* @var $this \yii\web\View */
/* @var $model Menu */

use app\models\Menu;
use app\modules\icms\widgets\ActiveFormIcms;
use app\modules\icms\widgets\grid\GridActionColumn;
use app\modules\icms\widgets\grid\GridFormatColumn;
use app\modules\icms\widgets\grid\GridView;
use app\modules\icms\widgets\NotificationGritter;
use yii\helpers\Html;

?>
<div class="data">

    <?php
    $form = ActiveFormIcms::begin();
    ?>
    <fieldset class="line-box">
        <legend>Добить пункт меню</legend>
        <div class='col-85'>
            <div class="col-45">
                <?= $form->field($model, 'name')->textInput(['class' => 'width-100'])->label('Название') ?>
            </div>
            <div class="col-45 float_r">
                <?= $form->field($model, 'url')->textInput(['class' => 'width-100', ])->label('URL') ?>
            </div>
        </div>
        <div class='col-10 float_r'>
            <label>&nbsp;</label>
            <button class="button" type="submit">Добавить</button>
        </div>
    </fieldset>

    <?php $form->end() ?>

    <?= NotificationGritter::widget(['preset' => 'save', 'flash' => 'message']) ?>
    <?=
    GridView::widget([
        'modelName' => Menu::class,
        'filterScenario' => Menu::SCENARIO_DEFAULT,
        'defaultSort' => ['sort' => SORT_ASC],
        'filter' => [],
        'columns' => [
            'id',
            [
                'class' => GridFormatColumn::class,
                'attribute' => 'name',
                'format' => 'input',
                'label' => 'Название',
                'contentOptions' => ['class' => 'width-200'],
            ],
            [
                'class' => GridFormatColumn::class,
                'attribute' => 'url',
                'format' => 'input',
                'label' => 'URL',
                'contentOptions' => ['class' => 'width-400'],
            ],
            [
                'class' => GridFormatColumn::class,
                'attribute' => 'is_premium',
                'label' => 'Премиум?',
                'format' => 'checkbox'
            ],
            [
                'class' => GridFormatColumn::class,
                'attribute' => 'show_guest',
                'label' => 'Видят НЕ авторизованные',
                'format' => 'checkbox'
            ],
            [
                'class' => GridFormatColumn::class,
                'attribute' => 'show_user',
                'label' => 'Видят авторизованные',
                'format' => 'checkbox'
            ],
            [
                'class' => GridFormatColumn::class,
                'attribute' => 'sort',
                'format' => 'input',
                'label' => 'Сортировка',
                'contentOptions' => ['class' => 'width-120'],
            ],
            [
                'class' => GridFormatColumn::class,
                'attribute' => 'status',
                'format' => 'select',
                'label' => 'Статус',
                'options' => [
                    'items' => Menu::getStatuses(),
                ],
            ],
            [
                'class' => GridActionColumn::class,
                'delete' => true,
                'save' => true,
            ],
        ],
    ]);
    ?>

</div>
