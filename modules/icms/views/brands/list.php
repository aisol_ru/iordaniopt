<?php
/* @var $this \yii\web\View */

use app\models\Brand;
use app\modules\icms\widgets\grid\GridActionColumn;
use app\modules\icms\widgets\grid\GridFormatColumn;
use app\modules\icms\widgets\grid\GridView;
use app\modules\icms\widgets\NotificationGritter;

?>
<div class="data">

    <?= NotificationGritter::widget(['preset' => 'save', 'flash' => 'message']) ?>
    <?=
    GridView::widget([
        'modelName' => Brand::class,
        'filterScenario' => Brand::SCENARIO_DEFAULT,
        'columns' => [
            'id',
            [
                'class' => GridFormatColumn::class,
                'attribute' => 'image',
                'format' => ['img', 'link'],
                'label' => 'Изображение',
                'options' => [
                    'link' => ['brands/edit', 'id'],
                    'resize' => ['type' => 3],
                ],
            ],
            [
                'class' => GridFormatColumn::class,
                'attribute' => 'created_at',
                'label' => 'Дата создания',
                'visible' => Yii::$app->user->can('developer'),
                'format' => 'date',
            ],
            [
                'class' => GridFormatColumn::class,
                'attribute' => 'updated_at',
                'label' => 'Дата изменения',
                'visible' => Yii::$app->user->can('developer'),
                'format' => 'date',
            ],
            [
                'class' => GridActionColumn::class,
                'view' => ['brands/edit', 'id'],
                'save' => true,
                'delete' => true,
            ],
        ],
    ]);
    ?>

</div>
