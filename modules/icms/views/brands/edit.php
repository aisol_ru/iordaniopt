<?php
/* @var $this \yii\web\View */
/* @var $model \app\models\Brand */

use app\models\catalog\Brand;
use app\modules\icms\widgets\CheckBox;
use app\modules\icms\widgets\ckeditor\CKEditor;
use app\modules\icms\widgets\drop_down_list\DropDownList;
use yii\helpers\Html;
use app\modules\icms\widgets\ActiveFormIcms;
use app\modules\icms\widgets\Tabs;
use app\modules\icms\widgets\FileImageInput;
?>
<div class="data">
    <?php
    $form = ActiveFormIcms::begin();
    ?>
    <?php
        $tabs = Tabs::begin([
            'tabNames' => ['Общая информация']
        ]);
        ?>

        <?php $tabs->beginTab() ?>
            <div class='col-70'>
                <?= $form->field($model, 'content')->widget(CKEditor::class)->label('Описание') ?>
            </div>
            <div class="col-25 float_r">

                <?= $form->field($model, 'sort')->textInput([])->label('Сортировка') ?>
                <?= $form->field($model, 'status')->widget(DropDownList::class, ['items' => $model::getStatuses()])->label('Статус') ?>

                <?= $form->field($model, 'image')->widget(FileImageInput::class)->label('Изображение') ?>

            </div>
        <?php $tabs->endTab() ?>
        <?php $tabs::end() ?>
    <div class="clear"></div>
    <div class='col-100'>
        <div class="action_buttons">
            <a class="back">Назад</a>
            <?= Html::submitButton('Сохранить', ['class' => 'save', 'name' => 'save-button']) ?>
        </div>
    </div>
    <?php $form->end() ?>
</div>
