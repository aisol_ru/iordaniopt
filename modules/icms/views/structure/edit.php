<?php
/* @var $this \yii\web\View */
/* @var $model \app\models\structure\Tree */

/* @var $moduleId integer */

use app\modules\icms\widgets\FileImageInput;
use app\modules\icms\widgets\grid\GridView;
use yii\helpers\Html;
use app\modules\icms\widgets\ActiveFormIcms;
use app\modules\icms\widgets\drop_down_list\DropDownList;
use app\modules\icms\widgets\CheckBoxSlide;
use app\modules\icms\widgets\Tabs;
use app\modules\icms\widgets\AliasInput;
use app\modules\icms\widgets\ckeditor\CKEditor;

?>
<div class="data">
    <?php
    $form = ActiveFormIcms::begin();
    ?>
    <?php
    $arrTabs = [
        'tabNames' => ['Общая информация', 'SEO']
    ];
    if ($model->id === 3) {
        $arrTabs['tabNames'][] = "Меню каталога";
    }
    $tabs = Tabs::begin($arrTabs);
    ?>

    <?php $tabs->beginTab() ?>
    <div class='col-70'>
        <?= $form->field($model, 'name_menu')->textInput()->label('Название страницы') ?>
        <?php if ($model->url != '/') {
            echo $form->field($model, 'pid')->widget(DropDownList::class, [
                'items' => \app\models\structure\Tree::getNamedTreeAsArray(),
                'options' => ['options' => $model::getDisabledBranch($model->id)]
            ])->label('Родительская страница');
            echo $form->field($model, 'name')->widget(AliasInput::class, [
                'from' => Html::getInputId($model, 'name_menu'),
                'autoEnabledField' => 'auto_url',
                'autoEnabled' => false
            ])->label('URL');
        } else {
            echo $form->field($model, 'pid')->hiddenInput()->label('');
            echo $form->field($model, 'name')->hiddenInput()->label('');
        } ?>
        <?= $form->field($model, 'content')->widget(CKEditor::class)->label('Контент') ?>
    </div>
    <div class='col-25 float_r'>

        <?php if (Yii::$app->user->can('developer')) {
            echo $form->field($model, 'is_safe')->widget(CheckBoxSlide::class, ['choiceLabel' => 'Нет /Да'])->label('Защита от удаления');
        } ?>
        <?= $form->field($model, 'in_map')->widget(CheckBoxSlide::class, ['choiceLabel' => 'Нет /Да'])->label('Отображать на карте сайта') ?>
        <?= $form->field($model, 'sort')->textInput()->label('Сортировка') ?>
        <?= $form->field($model, 'status')->widget(DropDownList::class, ['items' => $model::getStatuses()])->label('Статус') ?>
        <?php
        if (Yii::$app->user->can('developer') && $model->url !== '/') {
            echo Html::label('Модуль');
            echo DropDownList::widget(['name' => 'moduleTreeId', 'selection' => $moduleId, 'items' => app\models\Module::getNamesAsArray()]);
        }
        ?>
        <?= $form->field($model, 'image')->widget(FileImageInput::class)->label('Изображение') ?>
    </div>
    <?php $tabs->endTab() ?>
    <?php $tabs->beginTab() ?>
    <div class='col-70'>
        <?= $form->field($model, 'seo_h1')->textInput()->label('H1') ?>
        <?= $form->field($model, 'seo_title')->textInput()->label('Title') ?>
        <?= $form->field($model, 'seo_description')->textarea()->label('Description') ?>
        <?= $form->field($model, 'seo_keywords')->textarea()->label('Keywords') ?>
    </div>
    <div class='col-25 float_r'>
        <?= $form->field($model, 'nofollow')->widget(CheckBoxSlide::class, ['choiceLabel' => 'Нет /Да'])->label('Делать ссылку в меню с nofollow') ?>
    </div>
    <?php $tabs->endTab() ?>

    <?php if ($model->id === 3) {
        $tabs->beginTab();

        echo GridView::widget([
            'modelName' => \app\models\catalog\Category::class,
            'filterScenario' => false,
            'tableName' => 'Категории',
            'relations' => ["parent_outercode" => ""],
            'columns' => [
                'id',
                [
                    'class' => 'app\modules\icms\widgets\grid\GridFormatColumn',
                    'attribute' => 'name',
                    'label' => 'Название',
                    'format' => 'link',
                    'options' => [
                        'link' => '/icms/structure/catalog_categorie_edit?catalog_categorie_id=',
                    ]
                ],
                [
                    'class' => 'app\modules\icms\widgets\grid\GridActionColumn',
                    'save' => true,
                    'view' => '/icms/structure/catalog_categorie_edit?catalog_categorie_id='
                ],
            ],
        ]);


        $tabs->endTab();
    } ?>

    <?php Tabs::end(); ?>
    <div class="clear"></div>
    <div class='col-70'>
        <div class="action_buttons">
            <a class="back">Назад</a>
            <?= Html::submitButton('Сохранить', ['class' => 'save', 'name' => 'save-button']) ?>
        </div>
    </div>
    <?php ActiveFormIcms::end(); ?>
</div>

