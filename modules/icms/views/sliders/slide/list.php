<?php

use app\models\slider\Slide;
use app\modules\icms\widgets\grid\GridActionColumn;
use app\modules\icms\widgets\grid\GridFormatColumn;
use app\modules\icms\widgets\grid\GridView;
use app\modules\icms\widgets\NotificationGritter;

?>
<div class="data">

    <?= NotificationGritter::widget(['preset' => 'save', 'flash' => 'message']) ?>
    <?=
    GridView::widget([
        'modelName' => Slide::class,
        'filterScenario' => 'filter',
        'tableName' => 'Слайды',
        'relations' => ['slider_id' => 'id'],
        'columns' => [
            'id',
            [
                'class' => GridFormatColumn::class,
                'attribute' => 'name',
                'format' => 'link',
                'label' => 'Название',
                'options' => [
                    'link' => ['sliders/slide-edit', 'id']
                ]
            ],
            [
                'class' => GridFormatColumn::class,
                'attribute' => 'image',
                'format' => 'img',
                'label' => 'Изображение',
                'options' => ['resize' => ['type' => 3]]
            ],
            [
                'class' => GridFormatColumn::class,
                'attribute' => 'status',
                'format' => 'select',
                'label' => 'Статус',
                'options' => [
                    'items' => Slide::getStatuses()
                ],
                'filter' => Slide::getStatuses()
            ],
            [
                'class' => GridFormatColumn::class,
                'attribute' => 'sort',
                'format' => 'input',
                'label' => 'Сортировка',
                'contentOptions' => ['class' => 'width-120'],
            ],
            [
                'class' => GridFormatColumn::class,
                'attribute' => 'created_at',
                'label' => 'Дата создания',
                'visible' => Yii::$app->user->can('developer'),
                'format' => 'date'
            ],
            [
                'class' => GridFormatColumn::class,
                'attribute' => 'updated_at',
                'label' => 'Дата изменения',
                'visible' => Yii::$app->user->can('developer'),
                'format' => 'date'
            ],
            [
                'class' => GridActionColumn::class,
                'delete' => true,
                'save' => true
            ],
        ],
    ]);
    ?>
</div>
