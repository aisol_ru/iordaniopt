<?php

use app\models\slider\Slider;
use app\modules\icms\widgets\grid\GridActionColumn;
use app\modules\icms\widgets\grid\GridFormatColumn;
use app\modules\icms\widgets\grid\GridView;
use app\modules\icms\widgets\NotificationGritter;

?>
<div class="data">

    <?= NotificationGritter::widget(['preset' => 'save', 'flash' => 'message']) ?>
    <?=
    GridView::widget([
        'modelName' => Slider::class,
        'filterScenario' => 'filter',
        'tableName' => 'Слайдеры',
        'columns' => [
            'id',
            [
                'class' => GridFormatColumn::class,
                'attribute' => 'name',
                'label' => 'Название',
                'format' => 'link',
                'options' => [
                    'link' => ['sliders/slider', 'id'],
                    'is-pjax' => true,
                ]
            ],
            [
                'class' => GridFormatColumn::class,
                'attribute' => 'slides',
                'label' => 'Количество баннеров',
                'format' => 'count'
            ],
            [
                'class' => GridFormatColumn::class,
                'attribute' => 'created_at',
                'label' => 'Дата создания',
                'format' => 'date'
            ],
            [
                'class' => GridFormatColumn::class,
                'attribute' => 'updated_at',
                'label' => 'Дата изменения',
                'format' => 'date'
            ],
            [
                'class' => GridActionColumn::class,
                'delete' => Yii::$app->getUser()->can('developer') === true,
                'view' => ['sliders/slider-edit', 'id']
            ],
        ],
    ]);
    ?>

</div>
