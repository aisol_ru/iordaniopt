<?php
/* @var $this \app\components\View */
/* @var $model \app\models\user\User */
/* @var $roles array */

use app\modules\icms\widgets\FileInput;
use yii\helpers\Html;
use app\modules\icms\widgets\ActiveFormIcms;
use app\modules\icms\widgets\drop_down_list\DropDownList;
use app\modules\icms\widgets\Tabs;
use yii\widgets\MaskedInput;

?>
<div class="data">
    <?php
    $form = ActiveFormIcms::begin();
    ?>
    <?php
        $tabs = Tabs::begin([
            'tabNames' => ['Общая информация']
        ]);
        ?>

        <?php $tabs->beginTab() ?>
            <div class='col-70'>
                <?= $form->field($model, 'login')->textInput()->label("Login") ?>
                <?= $form->field($model, 'last_name')->textInput()->label('Фамилия') ?>
                <?= $form->field($model, 'first_name')->textInput()->label('Имя') ?>
                <?= $form->field($model, 'middle_name')->textInput()->label('Отчество') ?>
                <?= $form->field($model, 'phone')->widget(MaskedInput::class, ['mask' => '+7 (999) 999-99-99'])->label('Телефон') ?>
                <?= $form->field($model, 'email')->textInput()->label('Е-mail') ?>
                <?= $form->field($model, 'organization')->textInput()->label('Организация') ?>
                <?= $form->field($model, 'inn')->textInput()->label('ИНН') ?>
                <?= $form->field($model, 'ogrn')->textInput()->label('ОГРН') ?>
                <?= $form->field($model, 'city')->textInput()->label('Город') ?>
                <?= $form->field($model, 'certificate')->textInput()->label('Сертификат') ?>
                <?= $form->field($model, 'password')->input('password')->label('Пароль') ?>
                <?= $form->field($model, 'password_repeat')->input('password')->label('Повторите пароль') ?>
            </div>
            <div class="col-25 float_r">
                <?= $form->field($model, 'document')->widget(FileInput::class)->label('Свидетельство о постановке на спецучет') ?>
                <?= $form->field($model, 'role')->widget(DropDownList::class, ['items' => $roles, 'placeholder' => 'Выберите группу'])->label('Группа') ?>
                <?= $form->field($model, 'status')->widget(DropDownList::class, ['items' => $model::getStatuses(), 'placeholder' => 'Выберите статус'])->label('Статус') ?>
            </div>
            <div class="clear"></div>
            <div class='col-100'>
                <div class="action_buttons">
                    <a class="back">Назад</a>
                    <?= Html::submitButton('Сохранить', ['class' => 'save', 'name' => 'save-button']) ?>
                </div>
            </div>
        <?php $tabs->endTab() ?>
        <?php $tabs::end() ?>
    <?php ActiveFormIcms::end(); ?>
</div>
