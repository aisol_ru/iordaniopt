<?php
/* @var $this \yii\web\View */
/* @var $model \app\models\user\User */
/* @var $roles array */
/* @var $domains array */
/* @var $managers array */

use app\models\Manager;
use app\modules\icms\widgets\FileInput;
use yii\helpers\Html;
use app\modules\icms\widgets\ActiveFormIcms;
use app\modules\icms\widgets\drop_down_list\DropDownList;
use app\modules\icms\widgets\Tabs;
use app\models\user\User;
use yii\widgets\MaskedInput;

?>
<div class="data">
    <?php
    $form = ActiveFormIcms::begin();
    ?>
    <?php
        $tabs = Tabs::begin([
            'tabNames' => ['Общая информация']
        ]);
        ?>

        <?php $tabs->beginTab() ?>
            <div class='col-70'>
              <label>CSV</label>
              <?= \app\modules\icms\widgets\FileInput::widget(['name' => 'csv']) ?>
            </div>
            <div class="clear"></div>
            <div class='col-100'>
                <div class="action_buttons">
                    <a class="back">Назад</a>
                    <?= Html::submitButton('Сохранить', ['class' => 'save', 'name' => 'save-button']) ?>
                </div>
            </div>
        <?php $tabs->endTab() ?>
        <?php $tabs::end() ?>
    <?php ActiveFormIcms::end(); ?>
</div>
