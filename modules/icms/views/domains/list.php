<?php
/* @var $this \yii\web\View */

use app\models\Domain;
use app\modules\icms\widgets\grid\GridActionColumn;
use app\modules\icms\widgets\grid\GridFormatColumn;
use app\modules\icms\widgets\grid\GridView;
use app\modules\icms\widgets\NotificationGritter;

?>
<div class="data">

    <?= NotificationGritter::widget(['preset' => 'save', 'flash' => 'message']) ?>
    <?=
    GridView::widget([
        'modelName' => Domain::class,
        'filterScenario' => Domain::SCENARIO_DEFAULT,
        'tableName' => 'Домены',
        'columns' => [
            'id',
            [
                'class' => GridFormatColumn::class,
                'attribute' => 'name',
                'label' => 'Название',
                'format' => 'link',
                'options' => [
                    'link' => ['domains/edit', 'id'],
                ],
            ],
            [
                'class' => GridFormatColumn::class,
                'attribute' => 'domain',
                'label' => 'Домен',
            ],
            [
                'class' => GridFormatColumn::class,
                'attribute' => 'logo_header',
                'format' => 'img',
                'label' => 'Изображение',
                'options' => [
                    'resize' => ['type' => 3],
                ],
            ],
            [
                'class' => GridFormatColumn::class,
                'attribute' => 'created_at',
                'label' => 'Дата создания',
                'visible' => Yii::$app->user->can('developer'),
                'format' => 'date',
            ],
            [
                'class' => GridFormatColumn::class,
                'attribute' => 'updated_at',
                'label' => 'Дата изменения',
                'visible' => Yii::$app->user->can('developer'),
                'format' => 'date',
            ],
            [
                'class' => GridActionColumn::class,
                'delete' => Yii::$app->getUser()->can('developer') === true,
            ],
        ],
    ]);
    ?>

</div>
