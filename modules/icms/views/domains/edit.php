<?php
/* @var $this \yii\web\View */
/* @var $model \app\models\Domain */
/* @var $contact \app\models\contact\Contact */
/* @var $social Social */

use app\components\Pjax;
use app\models\catalog\Brand;
use app\models\contact\Social;
use app\models\map\Map;
use app\modules\icms\widgets\CheckBox;
use app\modules\icms\widgets\drop_down_list\DropDownList;
use app\modules\icms\widgets\FileInput;
use app\modules\icms\widgets\grid\GridActionColumn;
use app\modules\icms\widgets\grid\GridFormatColumn;
use app\modules\icms\widgets\grid\GridView;
use app\widgets\AjaxSubmitButton;
use yii\helpers\Html;
use app\modules\icms\widgets\ActiveFormIcms;
use app\modules\icms\widgets\Tabs;
use app\modules\icms\widgets\FileImageInput;
use yii\web\JsExpression;

?>
<div class="data">
    <?php
        $tabs = Tabs::begin([
            'tabNames' => [
                    'Общая информация',
                    $model->isNewRecord === true ? false : 'Контакты',
            ],
        ]);
        ?>

        <?php $tabs->beginTab() ?>

            <?php
            $form = ActiveFormIcms::begin();
            ?>
            <div class='col-70'>
                <?= $form->field($model, 'name')->textInput(['class' => 'width-100'])->label('Название') ?>
                <?= $form->field($model, 'domain')->textInput(['class' => 'width-100', 'readonly' => Yii::$app->getUser()->can('developer') === false])->label('Домен') ?>
            </div>
            <div class="col-25 float_r">
                <?= $form->field($model, 'brand_id')->widget(DropDownList::class, [
                    'items' => Brand::getNamesAsArray('name', ['name' => SORT_ASC]),
                    'options' => [
                        'prompt' => 'Не выбран',
                    ],
                ])->label('Бренд') ?>

                <?= $form->field($model, 'is_premium')->widget(CheckBox::class, ['choiceLabel' => 'Премиум'])->label(false) ?>

                <?= $form->field($model, 'jsonData[class]')->textInput(['class' => 'width-100'])->label('Класс контейнера') ?>

                <fieldset class="line-box">
                    <legend>Логотипы</legend>
                    <?= $form->field($model, 'logo_preloader')->widget(FileImageInput::class)->label('Прелоадер') ?>
                    <?= $form->field($model, 'logo_header')->widget(FileImageInput::class)->label('Хедер') ?>
                    <?= $form->field($model, 'logo_footer')->widget(FileImageInput::class)->label('Футер') ?>
                </fieldset>
                <fieldset class="line-box">
                    <legend>SEO</legend>
                    <?= $form->field($model, 'robots')->widget(FileInput::class)->label('Robots.txt') ?>
                    <?= $form->field($model, 'sitemap')->widget(FileInput::class)->label('Sitemap.xml') ?>
                    <?= $form->field($model, 'favicon')->widget(FileImageInput::class)->label('Favicon') ?>
                </fieldset>

            </div>

            <div class="clear"></div>
            <div class='col-100'>
                <div class="action_buttons">
                    <a class="back">Назад</a>
                    <?= Html::submitButton('Сохранить', ['class' => 'save', 'name' => 'save-button']) ?>
                </div>
            </div>
            <?php $form->end() ?>

        <?php $tabs->endTab() ?>
        <?php $tabs->beginTab() ?>

            <?php $form = ActiveFormIcms::begin() ?>
            <fieldset class="three-column line-box">
                <legend>Контактные данные</legend>
                <div class="flex-column">

                    <?= $form->field($contact, 'address')->textInput()->label('Адрес') ?>
                    <?= $form->field($contact, 'working_hours')->textInput()->label('Рабочее время') ?>

                </div>
                <div class="flex-column">

                    <?= $form->field($contact, 'phone')->textInput()->label('Телефон') ?>
                    <?= $form->field($contact, 'email')->textInput()->label('Email') ?>

                </div>
                <div class="flex-column">

                    <?= $form->field($contact, 'map_id')->widget(DropDownList::class, ['items' => Map::getNamesAsArray(), 'options' => ['prompt' => 'Выберите карту']])->label('Карта') ?>

                    <div class="clear"></div>
                    <div class='col-100'>
                        <div class="action_buttons">
                            <?=
                            AjaxSubmitButton::widget([
                                'useWithActiveForm' => $form->getId(),
                                'label' => 'Сохранить',
                                'options' => ['class' => 'save'],
                                'ajaxOptions' => [
                                    'type' => 'POST',
                                    'url' => '/icms/ajax/contact-save',
                                    'dataType' => 'json',
                                    'success' => new JsExpression("function(data){
                                        if (data.success) {
                                            showGreenLine('Контактные данные сохранены');
                                        } else {
                                        
                                        }
                                    }"),
                                ],
                            ])
                            ?>
                        </div>
                    </div>

                </div>
            </fieldset>
            <?php $form->end() ?>

            <?php $form = ActiveFormIcms::begin([
                    'action' => '/icms/ajax/social-add',
                    'options' => [
                        'class' => 'js-xhr-form',
                        'data-pjax' => '#social-list',
                    ]
            ]) ?>
                <fieldset class="three-column line-box">
                    <legend>Добавление социальной сети</legend>
                    <div class="flex-column">

                        <?= $form->field($social, 'name')->textInput()->label('Название') ?>
                        <?= $form->field($social, 'link')->textInput()->label('Ссылка') ?>

                    </div>
                    <div class="flex-column">

                        <?= $form->field($social, 'sort')->textInput()->label('Сортировка') ?>
                        <?= $form->field($social, 'status')->widget(DropDownList::class, ['items' => Social::getStatuses()])->label('Статус') ?>

                    </div>
                    <div class="flex-column">

                        <?= $form->field($social, 'image')->widget(FileImageInput::class)->label('Изображение') ?>

                        <div class="clear"></div>
                        <div class='col-100'>
                            <div class="action_buttons">
                                <button type="submit" class="save">Добавить</button>
                            </div>
                        </div>

                    </div>
                </fieldset>
            <?php $form->end() ?>

            <?php Pjax::begin(['id' => 'social-list', 'options' => ['class' => 'pjax-wraper']]) ?>
            <?=
            GridView::widget([
                'modelName' => Social::class,
                'filterScenario' => Social::SCENARIO_DEFAULT,
                'tableName' => 'Социальные сети',
                'options' => ['class' => 'table-tab'],
                'filter' => [],
                'columns' => [
                    'id',
                    [
                        'class' => GridFormatColumn::class,
                        'attribute' => 'name',
                        'format' => 'input',
                        'label' => 'Название',
                    ],
                    [
                        'class' => GridFormatColumn::class,
                        'attribute' => 'link',
                        'format' => 'input',
                        'label' => 'Ссылка',
                    ],
                    [
                        'class' => GridFormatColumn::class,
                        'attribute' => 'status',
                        'format' => 'select',
                        'label' => 'Статус',
                        'options' => [
                            'items' => Social::getStatuses()
                        ],
                    ],
                    [
                        'class' => GridFormatColumn::class,
                        'attribute' => 'sort',
                        'format' => 'input',
                        'label' => 'Сортировка',
                    ],
                    [
                        'class' => GridFormatColumn::class,
                        'attribute' => 'image',
                        'format' => 'img',
                        'label' => 'Изображение',
                        'options' => [
                            'resize' => ['type' => 3],
                        ],
                    ],
                    [
                        'class' => GridFormatColumn::class,
                        'attribute' => 'created_at',
                        'label' => 'Дата создания',
                        'visible' => Yii::$app->user->can('developer'),
                        'format' => 'date',
                    ],
                    [
                        'class' => GridFormatColumn::class,
                        'attribute' => 'updated_at',
                        'label' => 'Дата изменения',
                        'visible' => Yii::$app->user->can('developer'),
                        'format' => 'date',
                    ],
                    [
                        'class' => GridActionColumn::class,
                        'delete' => true,
                        'save' => true,
                    ],
                ],
            ]);
            ?>
            <?php Pjax::end() ?>

        <?php $tabs->endTab() ?>
        <?php $tabs::end() ?>
</div>
