<?php
/* @var $this yii\web\View */
/* @var $order Order */
/* @var $model \app\modules\icms\forms\Order */
/* @var $newStatus Status */

use app\models\order\Good;
use app\models\order\Order;
use app\models\order\Status;
use yii\helpers\Html;
use app\modules\icms\widgets\ActiveFormIcms;
use app\modules\icms\widgets\drop_down_list\DropDownList;
use app\modules\icms\widgets\Tabs;
?>
<div class="data">
    <?php $form = ActiveFormIcms::begin() ?>
        <?php
        $tabs = Tabs::begin([
            'tabNames' => ['Общая информация', 'Статусы'],
        ]);
        ?>

        <?php $tabs->beginTab() ?>
            <h2 class="padd" style="margin-top: -10px;">Заказ №<?= $order->number ?></h2>
            <table class="width-100 table-striped table-vertical" style="margin: 0px -30px 0px -30px;width: calc(100% + 60px);">
                <tbody>
                    <tr>
                        <td>Дата оформления заказа</td>
                        <td><?= date('d.m.Y H:i:s', $newStatus->created_at) ?></td>
                    </tr>
                    <tr>
                        <td>Клиент</td>
                        <td><?= $order->client->getFullName() ?></td>
                    </tr>
                    <tr>
                        <td>Телефон</td>
                        <td><?= $order->client->phone ?></td>
                    </tr>
                    <tr>
                        <td>Email</td>
                        <td><?= $order->client->email ?></td>
                    </tr>
                    <tr>
                        <td>Организация</td>
                        <td><?= $order->client->organization ?></td>
                    </tr>
                    <tr>
                        <td>ИНН</td>
                        <td><?= $order->client->inn ?></td>
                    </tr>
                    <tr>
                        <td>ОГРН</td>
                        <td><?= $order->client->ogrn ?></td>
                    </tr>
                    <tr>
                        <td>Город</td>
                        <td><?= $order->client->city ?></td>
                    </tr>
                    <?php if (empty($order->comment) === false) { ?>
                        <tr>
                            <td>Комментарий</td>
                            <td><?= nl2br($order->comment) ?></td>
                        </tr>
                    <?php } ?>

                    <tr>
                        <td>Количество товаров</td>
                        <td><?= $order->good_count ?></td>
                    </tr>

                    <tr>
                        <td>Сумма заказа</td>
                        <td><?= number_format($order->sum, 2, '.', ' ') ?></td>
                    </tr>
                    <tr>
                        <td>Статус</td>
                        <td>
                            <?= $form->field($model, 'status')->widget(DropDownList::class, [
                                'width' => '300px',
                                'items' => $order->getStatuses(),
                                'options' => [
                                    'options' => [
                                        Order::STATUS_NEW => ['disabled' => true],
                                    ],
                                ],
                            ])->label(false) ?>

                        </td>
                    </tr>
                </tbody>
            </table>
            <h2 class="padd">Состав заказа</h2>
            <table class="table table-striped table-bordered" style="margin: 0px -30px 0px -30px;width: calc(100% + 60px);">
                <thead>
                    <tr>
                        <th>Изображение</th>
                        <th>Наименование</th>
                        <th>Проба</th>
                        <th>Количество (шт.)</th>
                        <th>Комментарий</th>
                        <th>Размеры</th>
                        <th>Вес (гр.)</th>
                        <th>Итоговый вес (гр.)</th>
                        <th>Цена (руб.)</th>
                        <th>Сумма (руб.)</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    /* @var $good Good */
                    foreach ($order->getGoods()->each() as $good) { ?>
                    <tr>
                        <td>
                            <?php if (is_null($good->good) === false && empty($good->good->image) === false) { ?>
                                <img
                                        src="http://<?= $order->domain->domain ?><?= $good->good->getResizeCache('image', 100, 100, 5) ?>"
                                        alt="<?= Html::encode($good->jsonData['name']) ?>"
                                >
                            <?php } ?>
                        </td>
                        <td>
                            <?= $good->jsonData['name'] ?>
                            <?php if (empty($good->jsonData['attributes']['vendor_code']) === false) { ?>
                                <br>
                                <small><?= $good->jsonData['attributes']['vendor_code'] ?></small>
                            <?php } ?>
                        </td>
                        <td>
                            <?= $good->count ?>
                        </td>
                        <td>
                            <?php if (empty($good->jsonData['sizes']) === false) { ?>
                                <?php foreach ($good->jsonData['sizes'] as $key => $size) {
                                    if ($size['count'] == 0) {
                                        continue;
                                    }
                                    ?>
                                    <?= $size['size'] ?>: <?= $size['count'] ?> шт.;
                                <?php } ?>
                            <?php } else { ?>
                                ---
                            <?php } ?>
                        </td>
                        <td>
                            <?= $good->comment ?>
                        </td>
                        <td>
                            <?= $good->jsonData['weight'] ?>
                        </td>
                        <td>
                            <?= $good->weight ?>
                        </td>
                        <td>
                            <?= number_format($good->jsonData['price_unit'], 2, '.', '&nbsp;') ?>
                        </td>
                        <td>
                            <?= number_format($good->sum, 2, '.', '&nbsp;') ?>
                        </td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
            <div class="clear"></div>
            <div class='col-100'>
                <div class="action_buttons">
                    <a class="back">Назад</a>
                    <?= Html::submitButton('Сохранить', ['class' => 'save', 'name' => 'save-button']) ?>
                </div>
            </div>
        <?php $tabs->endTab() ?>
        <?php $tabs->beginTab() ?>
            <table class="table table-striped table-bordered" style="margin: -30px -30px 0px -30px;width: calc(100% + 60px);">
                <thead>
                    <tr>
                        <th>Статус</th>
                        <th>Дата установки</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    /* @var $status Status */
                    foreach ($order->getHistoryStatuses()->andWhere(['!=' , 'status', app\models\order\Order::STATUS_NOT_COMPLETED])->orderBy(['created_at' => SORT_DESC])->each() as $status) { ?>
                    <tr>
                        <td><?= $order->getStatuses()[$status->status] ?></td>
                        <td><?= date('d.m.Y H:i:s', $status->created_at) ?></td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
        <?php $tabs->endTab() ?>
        <?php $tabs::end() ?>
    <?php $form->end() ?>
</div>
