<?php
/* @var $this yii\web\View */

use app\modules\icms\widgets\grid\GridActionColumn;
use app\modules\icms\widgets\grid\GridFormatColumn;
use app\modules\icms\widgets\grid\GridView;
use app\models\order\Order;
use app\modules\icms\widgets\NotificationGritter;

?>
<div class="data">
    <?= NotificationGritter::widget(['preset' => 'save', 'flash' => 'message']) ?>
    <?=
    GridView::widget([
        'modelName' => Order::class,
        'tableName' => 'Заказы',
        'conditions' => ['!=', 'status', Order::STATUS_NOT_COMPLETED],
        'defaultSort' => ['number' => SORT_DESC],
        'columns' => [
            [
                'class' => GridFormatColumn::class,
                'attribute' => 'number',
                'format' => 'link',
                'label' => 'Номер',
                'options' => [
                    'link' => ['orders/view', 'number' => 'number'],
                ]
            ],
            [
                'attribute' => 'client.fullName',
                'label' => 'Клиент',
            ],
            [
                'attribute' => 'client.organization',
                'label' => 'Организация',
            ],
            [
                'attribute' => 'client.phone',
                'label' => 'Телефон',
            ],
            [
                'attribute' => 'client.email',
                'label' => 'Email',
            ],
            [
                'class' => GridFormatColumn::class,
                'attribute' => 'status',
                'label' => 'Статус',
                'format' => 'select',
                'contentOptions' => ['class' => 'width-200'],
                'options' => [
                    'items' => Order::getStatuses(),
                    'options' => [
                        Order::STATUS_NEW => ['disabled' => true],
                    ],
                ],
            ],
            [
                'class' => GridFormatColumn::class,
                'attribute' => 'sum',
                'format' => 'number',
                'label' => 'Итого',
            ],
            [
                'class' => GridFormatColumn::class,
                'label' => 'Дата создания',
                'format' => 'raw',
                'function' => function($model) {
                    $newStatus = $model->getHistoryStatuses()->andWhere(['status' => Order::STATUS_NOT_COMPLETED])->one();
                    if (is_null($newStatus) === true) {
                        return null;
                    }
                    return app\components\IcmsHelper::dateTimeFormat('d.m.Y H:i:s', $newStatus->created_at);
                },
            ],
            [
                'class' => GridActionColumn::class,
                'view' => ['orders/view', 'number' => 'number'],
                'delete' => true,
                'save' => true,
            ],
        ],
    ])
    ?>
</div>
