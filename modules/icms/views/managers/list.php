<?php

use app\models\Manager;
use app\modules\icms\widgets\grid\GridActionColumn;
use app\modules\icms\widgets\grid\GridFormatColumn;
use app\modules\icms\widgets\grid\GridView;
use app\modules\icms\widgets\NotificationGritter;

?>
<div class="data">

    <?= NotificationGritter::widget(['preset' => 'save', 'flash' => 'message']) ?>
    <?=
    GridView::widget([
        'modelName' => Manager::class,
        'filterScenario' => Manager::SCENARIO_DEFAULT,
        'tableName' => 'Менеджеры',
        'columns' => [
            'id',
            [
                'class' => GridFormatColumn::class,
                'attribute' => 'name',
                'label' => 'ФИО',
                'format' => 'link',
                'options' => [
                    'link' => ['managers/edit', 'id'],
                    'is-pjax' => true,
                ]
            ],
            [
                'class' => GridFormatColumn::class,
                'attribute' => 'created_at',
                'label' => 'Дата создания',
                'format' => 'date'
            ],
            [
                'class' => GridFormatColumn::class,
                'attribute' => 'updated_at',
                'label' => 'Дата изменения',
                'format' => 'date'
            ],
            [
                'class' => GridActionColumn::class,
                'delete' => true,
            ],
        ],
    ]);
    ?>

</div>
