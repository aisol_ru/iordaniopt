<?php
/* @var $this \yii\web\View */
/* @var $model \app\models\Manager */

use app\modules\icms\widgets\MultipleTextInput;
use yii\helpers\Html;
use app\modules\icms\widgets\ActiveFormIcms;
use app\modules\icms\widgets\Tabs;
?>
<div class="data">
    <?php
    $form = ActiveFormIcms::begin();
    ?>
    <?php
        $tabs = Tabs::begin([
            'tabNames' => ['Общая информация']
        ]);
        ?>

        <?php $tabs->beginTab() ?>
            <div class='col-70'>
                <?= $form->field($model, 'name')->textInput()->label('ФИО') ?>
            </div>
            <div class="float_r col-25">
                <?= $form->field($model, 'jsonData[phones]')->widget(MultipleTextInput::class)->label('Телефоны') ?>
                <?= $form->field($model, 'jsonData[emails]')->widget(MultipleTextInput::class)->label('Email') ?>
            </div>
            <div class="clear"></div>
            <div class='col-100'>
                <div class="action_buttons">
                    <a class="back">Назад</a>
                    <?= Html::submitButton('Сохранить', ['class' => 'save', 'name' => 'save-button']) ?>
                </div>
            </div>
        <?php $tabs->endTab() ?>
        <?php $tabs::end() ?>
    <?php ActiveFormIcms::end(); ?>
</div>
