<?php
/* @var $this \yii\web\View */
/* @var $model \app\models\banner\Banner */

use app\modules\icms\widgets\CheckBox;
use yii\helpers\Html;
use app\modules\icms\widgets\ActiveFormIcms;
use app\modules\icms\widgets\FileImageInput;
use app\modules\icms\widgets\drop_down_list\DropDownList;
use app\models\banner\Category;
use app\modules\icms\widgets\Tabs;
?>
<div class="data">
    <?php
    $form = ActiveFormIcms::begin();
    ?>
        <?php
        $tabs = Tabs::begin([
            'tabNames' => ['Общая информация']
        ]);
        ?>

        <?php $tabs->beginTab() ?>
            <div class='col-70'>
                <?= $form->field($model, 'name')->textInput()->label('Название') ?>
                <?= $form->field($model, 'link')->textInput()->label('Ссылка') ?>
            </div>
            <div class="col-25 float_r">
                <?= $form->field($model, 'is_premium')->widget(CheckBox::class, ['choiceLabel' => 'Тэг премиум'])->label(false) ?>
                <?= $form->field($model, 'sort')->textInput([])->label('Сортировка') ?>
                <?= $form->field($model, 'image')->widget(FileImageInput::class)->label('Изображение') ?>
                <?= $form->field($model, 'category_id')->widget(DropDownList::class, ['items' => Category::getNamesAsArray()])->label('Группа') ?>
                <?= $form->field($model, 'status')->widget(DropDownList::class, ['items' => $model::getStatuses()])->label('Статус') ?>
            </div>
            <div class="clear"></div>
            <div class='col-100'>
                <div class="action_buttons">
                    <a class="back">Назад</a>
                    <?= Html::submitButton('Сохранить', ['class' => 'save', 'name' => 'save-button']) ?>
                </div>
            </div>
        <?php $tabs->endTab() ?>
        <?php $tabs::end() ?>
    <?php ActiveFormIcms::end(); ?>
</div>
