<?php

use app\models\banner\Category;
use app\modules\icms\widgets\grid\GridActionColumn;
use app\modules\icms\widgets\grid\GridFormatColumn;
use app\modules\icms\widgets\grid\GridView;
use app\modules\icms\widgets\NotificationGritter;

?>
<div class="data">

    <?= NotificationGritter::widget(['preset' => 'save', 'flash' => 'message']) ?>
    <?=
    GridView::widget([
        'modelName' => Category::class,
        'filterScenario' => 'filter',
        'tableName' => 'Категории',
        'columns' => [
            'id',
            [
                'class' => GridFormatColumn::class,
                'attribute' => 'name',
                'label' => 'Название',
                'format' => 'link',
                'options' => [
                    'link' => ['banners/category', 'id'],
                    'is-pjax' => true,
                ]
            ],
            [
                'class' => GridFormatColumn::class,
                'attribute' => 'banners',
                'label' => 'Количество баннеров',
                'format' => 'count'
            ],
            [
                'class' => GridFormatColumn::class,
                'attribute' => 'created_at',
                'label' => 'Дата создания',
                'format' => 'date'
            ],
            [
                'class' => GridFormatColumn::class,
                'attribute' => 'updated_at',
                'label' => 'Дата изменения',
                'format' => 'date'
            ],
            [
                'class' => GridActionColumn::class,
                'delete' => Yii::$app->getUser()->can('developer') === true,
                'view' => ['banners/category-edit', 'id']
            ],
        ],
    ]);
    ?>

</div>
