<?php

namespace app\modules\icms\components;

use app\components\interfaces\UrlManagerMultiDomain;
use app\models\Domain;

class UrlManager extends \yii\web\UrlManager implements UrlManagerMultiDomain
{
    /**
     * @var null|Domain
     */
    public $domain = null;

    public function init()
    {
        parent::init();

        $baseUrl = $this->getBaseUrl();
        if ($baseUrl === '/icms') {
            $this->setBaseUrl('/');
        }
    }

    public function getDomain()
    {
        return $this->domain;
    }

}
