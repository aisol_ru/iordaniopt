$(document).ready(function () {

    $('.user').click(function () {
        if ($(".user i").hasClass('actv')) {
            $(".user i").removeClass("actv");
            $('.subnav_footer').hide();
        } else {
            $(".user i").addClass("actv");
            $(".subnav_footer").addClass("animated bounceInUp");
            $('.subnav_footer').toggle();
        }
        return false;
    });

    $('.action_buttons .back').click(function () {
        window.history.back();
    });

    $('.js-xhr-form').on('beforeSubmit', function() {
        var form = $(this);
        if ($(this).find('.has-error').length < 1) {

            var formData = new FormData(form[0]);

            var xhr = new XMLHttpRequest();

            xhr.open('POST', form.attr('action'), false);

            xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');

            form.find(':input:not(:button)').prop('readonly', true);
            form.find(':input:button').prop('disabled', true);

            xhr.send(formData);

            var result = JSON.parse(xhr.response);
            if (result.success === true) {
                if (form.data('pjax')) {
                    var pjax = $(form.data('pjax'));
                    if (pjax.length > 0) {
                        $.pjax.reload(form.data('pjax'), {timeout: 5000});
                    }
                }

                form.find('input[type=\"text\"], textarea').val('');
                form.yiiActiveForm('resetForm');
                form.trigger('cancel');
            }



            form.find(':input:not(:button)').prop('readonly', false);
            form.find(':input:button').prop('disabled', false);

        }
        return false;
    });

});



var Helper = function () {
    return {
        /**
         * Разбирает GET строку в объект
         * @param string url GET параметры (строка .serialize()) для разбора
         * @returns Object разобранная строка
         */
        parse_url: function (url) {
            url = url.replace(/^\?/g, '').replace(/\+/g, ' ').replace(/%2B/g, '+');
            parameters = {};

            pieces = url.split('&');

            for (i = 0; i < pieces.length; i++) {
                name = decodeURI(pieces[i].replace(/=.*?$/g, ''));
                value = decodeURI(pieces[i].replace(/^.*?=/g, '').replace(/%26/g, '&').replace(/%2F/g, '/').replace(/%3A/g, ':'));
                parameters[name] = value;
            }
            return parameters;
        }
    };
}();