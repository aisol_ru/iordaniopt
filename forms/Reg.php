<?php


namespace app\forms;


use app\components\Mailer;
use yii\base\Model;
use app\models\parameter\Parameter;

class Reg extends Model
{

    public $login;
    public $password;
    public $name;
    public $middle_name;
    public $lastname;
    public $inn;
    public $ogrn;
    public $city;
    public $certificate;
    public $nameOrg;
    public $email;
    public $phone;

    public function rules()
    {
        return [
            ['email', 'email', 'message' => 'Укажите валидный Email'],
            [
                [
                    'login',
                    'password',
                    'name',
                    'lastname',
                    'inn',
                    'ogrn',
                    'city',
                    'certificate',
                    'nameOrg',
                    'email',
                    'middle_name',
                    'phone',
                ],
                'required', 'message' => 'Обязательное поле'
            ],
            [
                [
                    'login',
                    'password',
                    'name',
                    'lastname',
                    'inn',
                    'ogrn',
                    'city',
                    'certificate',
                    'nameOrg',
                    'email',
                    'middle_name',
                    'phone',
                ],
                'filter', 'filter' => function ($value) {return trim(strip_tags($value));}
            ],
        ];
    }

    /**
     * @return bool
     */
    public function send()
    {
        $message = <<<HTML
<p><strong>Название ораганизации:</strong> {$this->nameOrg}</p>
<p><strong>ИНН:</strong> {$this->inn}</p>
<p><strong>ОГРН:</strong> {$this->ogrn}</p>
<p><strong>Город:</strong> {$this->city}</p>
<p><strong>Адрес доставки:</strong> {$this->certificate}</p>
<p></p>
<p><strong>Фамилия:</strong> {$this->lastname}</p>
<p><strong>Имя:</strong> {$this->name}</p>
<p><strong>Отчество:</strong> {$this->middle_name}</p>
<p><strong>Телефон:</strong> {$this->phone}</p>
<p><strong>Логин:</strong> {$this->login}</p>
HTML;


        return Mailer::send(Parameter::getValue(9, false, false), "Одобрение регистрации", $message);
    }

}
