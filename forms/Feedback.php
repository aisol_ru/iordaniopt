<?php


namespace app\forms;


use app\components\Mailer;
use yii\base\Model;

class Feedback extends Model
{

    public $name;
    public $phone;
    public $comment;

    public $agreement = 1;

    public function rules()
    {
        return [
            ['name', 'required', 'message' => 'Обязательное поле'],
            ['phone', 'required', 'message' => 'Обязательное поле'],
            ['comment', 'required', 'message' => 'Обязательное поле'],
            [['name', 'phone', 'comment'], 'filter', 'filter' => function($value) {
                return trim(strip_tags($value));
            }],
            ['agreement', 'in', 'range' => [1], 'message' => 'Необходимо дать согласие на обработку персованальных данных'],
        ];
    }

    /**
     * @return bool
     */
    public function send()
    {
        $message = <<<HTML
<p><strong>Имя:</strong> {$this->name}</p>
<p><strong>Телефон:</strong> {$this->phone}</p>
<p><strong>Комментарий:</strong> {$this->comment}</p>
HTML;


        return Mailer::send(null, "Обратная связь", $message);
    }

}
