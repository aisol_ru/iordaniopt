<?php


namespace app\forms;


use app\components\Mailer;
use yii\base\Model;

class Auth extends Model
{

    public $login;
    public $password;

    public function rules()
    {
        return [
            ['login', 'required', 'message' => 'Обязательное поле'],
            ['password', 'required', 'message' => 'Обязательное поле'],
            [['login', 'password'], 'filter', 'filter' => function($value) {
                return trim(strip_tags($value));
            }],
        ];
    }

//     /**
//      * @return bool
//      */
//     public function send()
//     {
//         $message = <<<HTML
// <p><strong>Имя:</strong> {$this->name}</p>
// <p><strong>Телефон:</strong> {$this->phone}</p>
// <p><strong>Комментарий:</strong> {$this->comment}</p>
// HTML;
//
//
//         return Mailer::send(null, "Обратная связь", $message);
//     }

}
