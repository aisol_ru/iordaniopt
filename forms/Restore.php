<?php


namespace app\forms;


use app\components\Mailer;
use yii\base\Model;

class Restore extends Model
{

    public $login;

    public function rules()
    {
        return [
            ['login', 'required', 'message' => 'Обязательное поле'],
            [['login'], 'filter', 'filter' => function($value) {
                return trim(strip_tags($value));
            }],
        ];
    }

     /**
      * @return bool
      */
    public function send($password, $email)
    {
        $message = <<<HTML
<p>Вами был отправлен запрос на восстановление пароля, ваш новый пароль:</p>
<p><strong>Логин:</strong> {$this->login}</p>
<p><strong>Пароль:</strong> {$password}</p>
HTML;


        return Mailer::send($email, "Восстановление пароля", $message);
    }

}
