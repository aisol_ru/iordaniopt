<?php

namespace app\forms;

use app\components\db\ActiveQuery;
use app\models\catalog\Category;
use yii\base\Model;
use yii\db\Expression;

class CatalogFilter extends Model
{

    public $word;
    public $isNew;
    public $categories = [];
    public $metals = [];
    public $inserts = [];
    public $collections = [];
    public $sort;

    public $priceMin;
    public $priceMax;

    public $defaultPriceMin;
    public $defaultPriceMax;

    public function rules()
    {
        return [
            ['word', 'default', 'value' => ''],
            ['word', 'filter', 'filter' => function ($value) {
                return trim(strip_tags($value));
            }],
            [['isNew', "sort"], 'safe'],
            ['categories', 'default', 'value' => []],
            ['metals', 'default', 'value' => []],
            ['inserts', 'default', 'value' => []],
            ['collections', 'default', 'value' => []],
            [['priceMin', 'priceMax'], 'filter', 'filter' => function ($value) {
                return preg_replace(['/,/', '/[^0-9.]/'], ['.', ''], trim($value));
            }],
        ];
    }

    /**
     * @param ActiveQuery $query
     */
    public function getDefaultPrices(&$query)
    {
        //$this->defaultPriceMin = (int) $query->min(new Expression('IF(`t`.`price` < `t`.`price`, `t`.`price`, `t`.`price`)')) ?: 0;
        //$this->defaultPriceMax = (int) $query->max(new Expression('IF(`t`.`price` > `t`.`price`, `t`.`price`, `t`.`price`)')) ?: 0;
        $this->defaultPriceMin = (int)$query->min('t.price') ?: 0;
        $this->defaultPriceMax = (int)$query->max('t.price') ?: 0;
    }

    /**
     * @param ActiveQuery $query
     */
    public function apply(&$query)
    {
        if (empty($this->word) === false) {
            $query
                ->andFilterWhere(['LIKE', 't.vendor_code', $this->word . '%', false])
                ->orFilterWhere(['LIKE', 't.name', $this->word]);
        }

        $trueCategoriesIds = [];
        foreach ($this->categories as $categoryId) {
            $trueCategoriesIds[] = $categoryId;

            $category = Category::find()
                ->andWhere(["outer_code" => $categoryId])
                ->one();

            if (!empty($category->children) && empty($category->parent_outercode)) {
                foreach ($category->children as $child) {
                    $trueCategoriesIds[] = $child->outer_code;
                }
            }
        }

        $query->andFilterWhere(['t.category_id' => array_unique($trueCategoriesIds, SORT_STRING)]);

//        $query->andFilterWhere(['t.metal_id' => $this->metals]);
//        $query->andFilterWhere(['t.insert_id' => $this->inserts]);
//        if (empty($this->collections) === false && is_array($this->collections) === true) {
//            $query->joinWith(['collections collection']);
//            $query->andFilterWhere(['collection.id' => $this->collections]);
//        }
        if (!empty($this->priceMin) && (!empty($this->priceMax))) {
            $query->andWhere('(t.price>=:price_start and t.price<=:price_max)',
                [
                    'price_start' => intval($this->priceMin),
                    'price_max' => intval($this->priceMax)
                ]);
        }
        if ($this->isNew) {
            $query->andWhere(['>', 't.created_at', time() - 3600 * 24 * 60]);
        }

    }

}
