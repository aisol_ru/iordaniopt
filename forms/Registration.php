<?php


namespace app\forms;


use app\models\user\User;
use yii\base\Model;

class Registration extends Model
{

    public $lastName;
    public $firstName;
    public $middleName;
    public $phone;
    public $email;
    public $organization;
    public $inn;
    public $ogrn;
    public $city;
    public $certificate;
    public $document;

    public $agreement = 1;

    public function rules()
    {
        return [
            ['lastName', 'required', 'message' => 'Обязательное поле'],
            ['firstName', 'required', 'message' => 'Обязательное поле'],
            ['middleName', 'required', 'message' => 'Обязательное поле'],
            ['email', 'required', 'message' => 'Обязательное поле'],
            ['email', 'email', 'message' => 'Введите правильный email'],
            ['organization', 'required', 'message' => 'Обязательное поле'],
            ['inn', 'required', 'message' => 'Обязательное поле'],
            ['ogrn', 'required', 'message' => 'Обязательное поле'],
            ['city', 'required', 'message' => 'Обязательное поле'],
            ['certificate', 'required', 'message' => 'Обязательное поле'],
            [
                '!document', 'file',
                'extensions' => ['doc', 'docx', 'pdf', 'jpg', 'png'], 'wrongExtension' => 'Доступные форматы: {extensions}',
                'maxSize' => 5242880, 'tooBig' => 'Не больше 5Мб',
                'skipOnEmpty' => false, 'message' => 'Выберите файл (файл не загружен)',
            ],

            ['agreement', 'in', 'range' => [1], 'message' => 'Необходимо дать согласие на обработку персованальных данных'],
        ];
    }

    /**
     * @return User
     * @throws \yii\web\HttpException
     */
    public function save()
    {
        $user = new User();
        $user->status = User::STATUS_DELETED;
        $user->login = $user->phone = $this->phone;
        $user->role = 'user';
        $user->first_name = $this->firstName;
        $user->last_name = $this->lastName;
        $user->middle_name = $this->middleName;
        $user->email = $this->email;
        $user->organization = $this->organization;
        $user->inn = $this->inn;
        $user->ogrn = $this->ogrn;
        $user->city = $this->city;
        $user->certificate = $this->certificate;
        $isSave = $user->save(false);
        if ($isSave === true) {
            $user->saveFile('document', null, $this->document);
        }
        return $user;
    }

}
