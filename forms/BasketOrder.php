<?php


namespace app\forms;


use yii\base\Model;

class BasketOrder extends Model
{

    public $comment;


    public function rules()
    {
        return [
            ['comment', 'default', 'value' => ''],
            ['comment', 'filter', 'filter' => function($value) {
                return trim(strip_tags($value));
            }],
        ];
    }

}
